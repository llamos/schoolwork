/**
 * @author Paul Norton
 * An implementation of a stack based on arrays
 */
public class MyStack<T> implements MyStackADT<T> {

    private T[] arrayStack;
    private int numItems;
    private int maximumCapacity;

    /**
     * Default constructor with initial capacity of 10, and maximum capacity of 1000
     */
    public MyStack() {
        this(10, 1000);
    }

    /**
     * Creates a stack with the specified initial and maximum capacity
     * @param initialCapacity The initial capacity of the stack
     * @param maxCapacity The maximum capacity to support stack growing until
     */
    public MyStack(int initialCapacity, int maxCapacity) {
        //noinspection unchecked
        arrayStack = (T[]) new Object[initialCapacity]; //Workaround for generic array creation
        this.numItems = 0;
        this.maximumCapacity = maxCapacity;
    }

    /**
     * Adds an element to the stack, expanding the internal array if needed
     * @param dataItem data item to be pushed onto stack
     * @throws OverflowException if the stack exceeds its maximum capacity
     */
    public void push(T dataItem) throws OverflowException {
        ensureCapacity(numItems + 1);
        for (int i = numItems; i-->0;) { //Decrement to zero
            arrayStack[i + 1] = arrayStack[i]; //Shift right
        }
        arrayStack[0] = dataItem;
        numItems++;
    }

    /**
     * Ensures that the underlying array has enough space for size elements and expands if needed
     * @param size The size to check for
     * @throws OverflowException if the array exceeds its maximum capacity
     */
    private void ensureCapacity(int size) throws OverflowException {
        if (arrayStack.length < size) {
            int newCapacity = arrayStack.length < 60 ? arrayStack.length * 3 : arrayStack.length + 100;
            if (newCapacity > maximumCapacity) {
                throw new OverflowException("Stack has surpassed maximum capacity");
            }
            //noinspection unchecked
            T[] newArray = (T[]) new Object[newCapacity]; //Workaround for generic array creation
            System.arraycopy(arrayStack, 0, newArray, 0, numItems);
            arrayStack = newArray;
        }
    }

    /**
     * Pops the top element off the stacks and returns it
     * @return the top element of the stack, after removing it
     * @throws EmptyStackException if the stack is empty
     */
    public T pop() throws EmptyStackException {
        if (isEmpty())
            throw new EmptyStackException("pop called on empty stack");
        T topItem = arrayStack[0]; //Keep return object aside during shifting
        for (int i = 0; i < numItems - 1; i++)
            arrayStack[i] = arrayStack[i + 1]; //Shift left
        numItems--;
        return topItem;
    }

    /**
     * Returns the top element of the stack, without popping it off
     * @return The top element of the stack
     * @throws EmptyStackException if the stack is empty
     */
    public T peek() throws EmptyStackException {
        if (isEmpty())
            throw new EmptyStackException("peek called on empty stack");
        return arrayStack[0];
    }

    /**
     * Determines whether the stack has any elements in it
     * @return true if and only if the stack has no elements
     */
    public boolean isEmpty() {
        return numItems == 0;
    }

    /**
     * Determines the number of elements in the stack
     * @return the count of elements currently in the stack
     */
    public int size() {
        return numItems;
    }

    /**
     * Creates and returns a representation of the stack
     * @return String representation of the stack
     */
    public String toString() {
        String rval = "Stack (";
        for (int i = 0; i < numItems; i++)
            rval += arrayStack[i] + ", ";
        return (numItems > 0 ? rval.substring(0, rval.length() - 2) : rval) + ")";
    }

}
