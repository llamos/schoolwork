import java.io.IOException;

/**
 * @author Paul Norton pnorton4
 * Program to find whether there is an available path for a drone to get from the UWO to the customer's house
 */
public class ComputePath {

    private Map cityMap;

    /**
     * Constructor for ComputePath which also computes the path for the droid
     * @param fileName The map file
     */
    @SuppressWarnings("WeakerAccess")
    public ComputePath(String fileName) {
        try {
            cityMap = new Map(fileName);

        } catch (IOException e) {
            fatal(e.getMessage());
        }
    }

    /**
     * Push a MapCell to the given stack and mark it as in stack
     * @param stack The stack to push to
     * @param toAdd The MapCell to push
     */
    private static void push(MyStack<MapCell> stack, MapCell toAdd) {
        stack.push(toAdd);
        toAdd.markInStack();
    }

    /**
     * Pop a MapCell off the given stack, and mark it out of stack
     * @param stack The stack to pop off of
     */
    private static void pop(MyStack<MapCell> stack) {
        MapCell cell = stack.pop();
        cell.markOutStack();
    }

    /**
     * Main method, entry point for program
     * @param args Command line arguments
     */
    public static void main(String[] args) {
        ComputePath path = new ComputePath(args[0]);
        //Pathfinding

        MyStack<MapCell> pathStack = new MyStack<>();
        push(pathStack, path.cityMap.getStart());

        MapCell current;
        try {
            do { //Loop until no path available (exception) or destination reached
                current = pathStack.peek();
                if (current == null) {
                    pop(pathStack);
                    continue;
                }
                if (current.isCustomer()) {
                    System.out.println("Found path in " + (pathStack.size()) + " cells.");
                    break;
                }
                if (path.interference(current)) {
                    pop(pathStack);
                    continue;
                }
                MapCell maybeNext = path.nextCell(current);
                if (maybeNext != null)
                    push(pathStack, maybeNext);
                else
                    pop(pathStack);
            } while (true);
        } catch (EmptyStackException ignored) { //No cells left to traverse
            fatal("No path exists");
        }}

    /**
     * Determines if there is any interference on a map cell, aka if there are any tower cells adjacent to the cell
     * @param cell The MapCell to check
     * @return true if there are no adjacent tower cells
     */
    private boolean interference(MapCell cell) {
        for (int i = 0; i < 6; i++) {
            MapCell neighbour = cell.getNeighbour(i);
            if (neighbour != null && neighbour.isTower())
                return true;
        }
        return false;
    }

    /**
     * Find the next available neighbour cell to the given cell, prioritizes free over high-altitude over thief cells
     * @param cell The cell to find neighbours of
     * @return The next available MapCell, or null if none is found
     */
    private MapCell nextCell(MapCell cell) {
        int freeIndex = -1; //First free cell index
        int highAltitudeIndex = -1;
        int thiefIndex = -1;
        for (int i = 0; i < 6; i++) {
            MapCell neighbour = cell.getNeighbour(i);
            if (neighbour != null && !neighbour.isMarked()) {
                if (neighbour.isCustomer())
                    return neighbour;
                if (neighbour.isFree() && freeIndex == -1) //Sort cell by type
                    freeIndex = i;
                else if (neighbour.isHighAltitude() && highAltitudeIndex == -1)
                    highAltitudeIndex = i;
                else if (neighbour.isThief() && thiefIndex == -1)
                    thiefIndex = i;
            }
        }
        if (freeIndex != -1) //Return free, then high-alt then thief
            return cell.getNeighbour(freeIndex);
        else if (highAltitudeIndex != -1)
            return cell.getNeighbour(highAltitudeIndex);
        else if (thiefIndex != -1)
            return cell.getNeighbour(thiefIndex);
        return null;
    }

    /**
     * Utility method to print a fatal error message, and exit the program
     * @param message The message to print
     */
    private static void fatal(String message) {
        System.out.println("FATAL: " + message);
        System.exit(1);
    }

}
