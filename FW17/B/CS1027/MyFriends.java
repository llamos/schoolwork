
public class MyFriends {
  public static void main(String args[]) {

    SocialNetwork contacts = new SocialNetwork();

    contacts.add("Snoopy","Dog","snoopy@uwo.ca");
    contacts.add("Felix","Cat","felix@uwo.ca");
    contacts.add("Mickey","Mouse","mickey@uwo.ca");

    System.out.println(contacts.toString());
    System.out.println("I have " + contacts.getNumFriends() + " friends in my list.");
    
    if (contacts.remove("Mickey", "Mouse")) {
    	System.out.println("Mickey Mouse was removed successfully.");
    } else {
    	System.out.println("Mickey Mouse couldn't be removed. BAD");
    }
    
    if (contacts.remove("Fake", "Friend")) {
    	System.out.println("Fake Friend was removed. BAD");
    } else {
    	System.out.println("Fake Friend couldn't be removed.");
    }
    
  }
}
