            AREA q2_c, CODE, READONLY
            ENTRY

            LDR r0,=STRING      ; Starts r0 (forward read location register) at the start of the palindrome to check
            LDR r1,=EoS         ; Starts r1 (backward read location register) at the end of the palindrome
            SUB r1,#1           ; Subtract 1 from backward read location to skip EoS

read_loop1  LDRB r3,[r0],#1     ; read_loop1 reads characters starting from the left side of the string
            CMP r3,#0           ; If it has reached the null-terminator
            BEQ success         ; all letters have been checked so it is a success
            BL chk_letter       ; Otherwise, it ensures it has an actual letter with the chk_letter subroutine
            BNE read_loop1      ; If it is not a real letter, it loops to read the next character instead
            MOV r4,r3           ; Move r4 over to let read_loop2 to read into r3 again

read_loop2  LDRB r3,[r1],#-1    ; read_loop2 reads characters starting from the right side of the string
            BL chk_letter       ; It also confirms that it has read a valid letter
            BNE read_loop2      ; Looping to read another if reads a non-letter

            CMP r3,r4           ; Once both characters are read, compare them
            BNE fail            ; If they are not the same character, this is not a palindrome
            B read_loop1        ; If they are the same character, go to check the next one

fail        MOV r0,#0           ; Sets r0 (output bit) as 0 (not a palindrome)
            B inf_loop          ; Skips over the success setter
    
success     MOV r0,#1           ; Sets r0 (output bit) as 1 (is a palindrome)

inf_loop    B inf_loop          ; Infinite loop to prevent access exception

; Expects letter in r3
; Z = 1 if pass
; Z = 0 if fail
chk_letter  CMP r3,#65          ; Checks if character is below 'A' numerically
            BLT chk_fail        ; Marks fail if it is
            CMP r3,#91          ; Checks if character is below '[' ('Z'+1) numerically
            BLT chk_pass        ; If it is (between 'A' and 'Z' inclusive), this letter is valid
            CMP r3,#97          ; Otherwise, check if it's less than 'a'
            BLT chk_fail        ; Fail if so
            CMP r3,#122         ; Check if it is greater than 'z'
            BGT chk_fail        ; Fail if so
            SUB r3,#32          ; If we've reached this point, the letter is in 'a'-'z', so subtract 20 to make it capital
chk_pass    MOV r12,#0          ; If a success, mark r12=0 to force
            CMP r12,#0          ; this compare to set Z=1
            BX LR               ; return to call-site
chk_fail    MOV r12,#1          ; If a fail, set r12=1 to force
            CMP r12,#0          ; this compare to set Z=0
            BX LR               ; return to call-site

            AREA q2_d, DATA, READONLY
STRING      DCB "He lived as a devil, eh?"  ; String to check
EoS         DCB 0x00                        ; End of string
            END