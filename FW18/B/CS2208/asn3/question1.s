            AREA q1_c, CODE, READONLY
            ;ENTRY

            LDR r0,=UPC             ; Load r0 as the address for the UPC, will be used to track read location
            ADD r3,r0,#12           ; Mark r3 as the last read location for S1 (the first sum)

S1_read     LDRB r1,[r0],#2         ; Load the first byte of the UPC (also shifts r0 over by 2 to read every other byte)
            SUB r1,#48              ; Subtract ASCII '0' to get numeric value
            ADD r2,r1,r2            ; Add numerical value to r2 (the S1 accumulator)
            CMP r3,r0               ; Check if we have reached the end of the UPC by comparing to r3
            BNE S1_read             ; If we haven't, loop back to S1_read

            LDR r0,=UPC             ; Repeat most of the steps from S1_read, but into a new accumulator of r4 (S2)
            ADD r0,r0,#1            ; Offset read index by 1
            ADD r3,r3,#1            ; Mark end of read 1 byte later as well (includes check byte)
S2_read     LDRB r1,[r0],#2         ; Perform S1_read into r4 this time
            SUB r1,#48
            ADD r4,r1,r4
            CMP r3,r0
            BNE S2_read

            MOV r5,#3               ; Coefficient for MLA
            MLA r6,r5,r2,r4         ; Now r6 contains 3 x S1 + S2

mod_loop    SUB r6,r6,#10           ; Decrement r6 by 10
            CMP r6,#10              ; until it is less than 10
            BLT fail                ; If r6 < 10, then it is not 0 mod 10, so mark it invalid
            BEQ success             ; If r6 == 10, then it is 0 mod 10, so it must be a valid UPC
            B mod_loop              ; If r6 > 10, we have not finished determining if it is 0 mod 10, divide again

fail        MOV r0,#0               ; Set r0 as 0 (not a valid UPC)
            B inf_loop              ; skip success line
success     MOV r0,#1               ; Set r0 as 1 (valid UPC)

inf_loop    B inf_loop              ; Infinite loop to prevent access exception

            AREA q1_d, DATA, READONLY
UPC         DCB "060383755577"  ; UPC to check: 0 60383 75557 7 in this case
            END