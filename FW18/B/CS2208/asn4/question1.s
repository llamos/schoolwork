            AREA question1, CODE, READONLY
            ENTRY
            
            LDR r0,=STRING1             ; Input string pointer
            LDR r1,=STRING2             ; Output string pointer
			
main        BL read						; Read one byte into r2
            BEQ inf_loop				; If it is EoS, (CMP in read), go to end of program
            CMP r2,#'t'					; Check if r2 is 't'
            BNE main					; If not, return to main and read again
			
            BL read						; If it was, read again here
            BEQ inf_loop				; Check for EoS
            CMP r2,#'h'					; Check if r2 is 'h' ('th')
            BNE main					; If not, return to main and read again
			
            BL read						; If it was, read again here
            BEQ inf_loop				; Check for EoS
            CMP r2,#'e'					; Check if r2 is 'e' ('the')
            BNE main					; If not, return to main and read again
			
            BL read						; If it was, read again here
            BEQ end_write				; Now we need to check whether we have EoS, a space, or something else. If EoS, jump to end_write for a special overwrite
            CMP r2,#' '					; Check for space
            BNE main					; If it was not a space (word starts with 'the' but isn't 'the'), go back to main and ignore it
			
            SUB r1,r1,#4				; If it was a space (word is 'the'), move the output string pointer back 4 places to overwrite 'the'
            STRB r2,[r1],#1				; Rewrite the space character into the new location (over where 't' was)
            B main						; Go back to start to continue overwriting and checking for 'the'
			
read        LDRB r2,[r0],#1             ; Read character from input string, shifting input pointer over
            CMP r2,#0                   ; Check if EoS (branch instructions are in call site) in case of 'the' at EoS
            STRB r2,[r1],#1				; Write character from input string into next location of output string to complete the copy
            BX LR						; Return to the call site
			
; This is called if we read 'the' at the end of the string.
; In that case, we have to write zeros over the copied 'the'
; in order to remove what was already written
end_write   SUB r1,r1,#4				; Move output pointer back 4 places
            STRB r3,[r1],#1				; Write 3 zeros (1)
            STRB r3,[r1],#1				; Write 3 zeros (2)
            STRB r3,[r1],#1				; Write 3 zeros (3)
			
inf_loop    B inf_loop					; Loop forever to prevent access exception

            AREA question1, DATA, READWRITE
STRING1     DCB "and the man said they must go"	; Input string
EoS         DCB 0x00							; End of input string
STRING2     SPACE 0xFF							; Output string
            END