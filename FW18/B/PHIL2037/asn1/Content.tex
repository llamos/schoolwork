%! TEX root = asn1.tex

In \textit{Artificial Intelligence: What Everyone Needs to Know}, 
Jerry Kaplan outlines many strong arguments on both sides of artificial intelligence debates.
More specifically, this essay will analyze the two sides presented 
regarding the ability for artificial intelligence to achieve consciousness.
Although many machines can display conscious-like actions,
while also being reducible to simple functions,
it is my belief that these same machines are capable of conscious artificial intelligence.

Bostrom begins by presenting an argument disproving that artificial intelligence has the capability to be conscious.
As he states, 
``it's relatively simple to build a robot that flinches, cries out, and/or simply says, `Ouch, that hurts' when you pinch it.
[...]Because we are able to look beyond its reactions to its internal structure,'' it cannot be conscious.
The second argument that Bostrom provides runs along the story of a brain that is slowly replaced, neuron by neuron, with perfect, neuron-simulating electronic implants.
Once the entire brain is replaced, is the brain still conscious? Is it only simulating consciousness?
Although I believe both of these arguments are strong,
neither one is completely compelling.
Both of them assume that a small portion of the brain's function can be replicated.
The first argument assumes that the final behaviours can be replicated,
which can already be seen in our world today through artificial intelligences
that specialize in classification or text generation, 
particularly in the recent \textit{Language Models are Unsupervised Multitask Learners} by OpenAI\autocite[1]{WEBSITE:1}.
The second argument implies that the basic function of a human neuron
can be replicated perfectly.
To my knowledge this has not been done yet,
but I assume it is possible given its seemingly simple function as a biological transistor,
directing electrical signals around the brain to other neurons.
Although they argue for different methods of replicating consciousness,
I don't believe that either one argues well for its own point.
It is my opinion that consciousness instead lies in the coalition
of smaller pieces, but is comprised entirely on these pieces.

I see both arguments pushing towards the same functionalist idea 
that components in the brain can be replicated,
and the replications can simulate consciousness,
but simulated consciousness is not true consciousness.
In some sense, I believe this to be true.
I believe that we can pick any single component of the human brain
and replicate it perfectly given enough time.
However, I do not believe that this fact also disallows for consciousness to exist in a higher plane 
where the simulated function is only a small piece.
In \textit{Introducing Philosophy: Questions and Readings},
John Searle argues that Watson, IBM's \textit{Jeopardy!}-winning artificial intelligence is not concious
on the basis that it does not know that it was even participating on \textit{Jeopardy!}, let alone winning the show.
At the time, Watson was specifically trained to do well at that game, by humans supervising its learning.
Through the Chinese Room argument, he provides a great analogue to why Watson could never be truly conscious in this manner.
However, it is possible to see the same problem in humans.
A single blood cell is not capable of consciousness,
and it cannot know that it is carrying vital oxygen to a quadricep during a record-setting 100-metre dash.
Likewise, a neuron in the brain is not capable of being conscious in its transmission of electrical signals.
It only receives signals, processes them, and optionally continues the chain of transmission to the next neuron.
Conscious instead lies in a layer above these two processes.

For humans, conscious thought is not aware of neurons processing information,
nor aware of blood cells moving important biological fuels across the body.
Similarly, I believe that Watson is not conscious of its victory on \textit{Jeopardy!}
only because it was not given a next-level coordinating piece.
It was crafted to win on the game show, but not to decide if it wanted to,
nor to decide how or to decide why.
Higher-order consciousness allows humans to work as more general biological machines.
Rather than being designed to only mine, or only blow glass,
consciousness enables humans to learn in a much more direct way than other machines.
Although it has not been done yet,
I believe that an artificial intelligence with the goal of self-preservation
would evolve to have similar traits as humans.
Human adaptability has been what kept us going as a species
and took over 100,000 years to reach its current state.
Although electrical machines are still behind us,
I am certain that they are capable of consciousness.
Even if we can classify all the individual components of the simulated brain,
the collective function will forever be too complex for analysis
and can certainly achieve consciousness.

I believe that many current techniques used for current artificial intelligence 
contain all the necessary components for consciousness: regression, adaptability and generality.
Both arguments that Jerry Kaplan outlines in \textit{Artificial Intelligence: What Everyone Needs to Know} are strong functionalist thought experiments,
but are intentionally not concrete in the opinion.
