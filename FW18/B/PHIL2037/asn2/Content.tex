%! TEX root = asn2.tex

The world around us is constantly changing with new technology.
We all carry around impressive beasts of computing right in our own pockets.
More and more frequently, we are beginning to see programs that can 
comprehend human language, analyze patterns, and sort data faster than the best of us.
These problems are not trivially solved by traditional computing methods,
but are instead being solved with increasing use of Artificial Intelligence (AI).
As AI gets increasingly complex, common questions arise such as:
    Will AI supersede us?
    Should we be scared of AI taking over?
    Could AI ever be conscious?
Let's examine these questions and determine if we should be worried
for the outcome of future AI development.

\bigskip\textit{Will AI supersede us?}

\bigskip Presently, AI is created with a very limited scope.
It is used to solve specific problems, such as recognizing handwriting or speech,
understanding language or finding trends in sets of data.
If combined, we would already have a machine
that would pass as a human on the Internet, provided it stuck to that medium of communication.
Well crafted AI systems can also ``step out'' of its AI context temporarily
to more efficiently solve traditional computing problems, 
such as arithmetic, graphing and data sorting.
Overall, they certainly have the ability 
to solve a wide set of problems faster than humans.
By the metric of mathematical prowess, computers have superseded humans for a long time. 
70 years ago, the Bombe was first created in the UK to test 
hundreds of Enigma machine code combinations per minute.
In strategic intelligence, humans have lost to artifical intelligence
ever since Deep Blue first beat world champion Garry Kasparov in 1997.
More recently, the complex game of Go was cracked
by Google's AlphaGo in 2016, beating Lee Sedol 4 games to 1.
With self driving cars in development, it will only be a short time 
until AI drivers best their human counterparts as well.
AI has already superseded or is close to superseding
us in many other activities as well,
including activities that we consider more innately ``human'', such as law and art.
If we aren't concerned yet, what will be new to worry about in the future?

\bigskip\textit{Should we be scared of AI taking over?}

\bigskip The common science fiction trope of an AI
which becomes hostile and turns on its owner (or all of humanity)
is a common one, which seems especially plausible in the near future.
Although we like to think of ourselves as super intelligent
relative to the rest of life on Earth,
we truly have no idea what kind of intelligence is possible in our universe.
Here's something to think about: 
When is the last time you stopped to communicate with an animal?
Maybe it was the last time you convinced a dog to shake your hand for a treat,
or felt genuine affection from a cat cuddled up on your lap.
How about other animals? When is the last time you stopped to communicate 
with a raccoon? A bird? An ant?
As an artificial intelligence becomes more and more intelligent,
we may find ourselves metaphorically become the pet, 
then the raccoon, then the bird and even the ant.
Why would it ever speak to us anymore?
Unfortunately, humans are stuck behind a fact of life: we die.
We don't live very long, either.
An Artificial Intelligence created out of silicon and electricity
would instead be capable of self-cloning, splitting or even upgrading without any constraint.
Whether we should be scared of this possibility depends on your outlook.
On one hand, we probably don't have to worry about AI killing everyone a la Terminator.
On the other hand, we would become so redundant and useless relative to AI.
Perhaps at that point we would instead learn how to limit AI so it does not leave us behind.

\bigskip\textit{Could AI ever be conscious?}

\bigskip As it stands now, we have almost no idea what it means to be conscious.
More importantly, we have no tools to determine whether something is or is not conscious.
Computers are fascinating electrical circuits that can solve mathematical problems
faster than any human ever could.
As such, they can compute models and simulations as developed by their programmer.
Modern three dimensional graphics are rendered using very complex mathematical
models that can approximate lighting, vibration, motion and more
with incredible degrees of accuracy, 
possibly even looking like a photograph or a video recording.
However, there is no current model for the human brain, or anything resembling consciousness.
Although this may seem to indicate that is not possible for a computer to be conscious,
since it would only ever be a simulation of consciousness,
it could also indicate that our own consciousness is a product of much smaller computations.
Ultimately, it likely does not matter if we can ever prove 
that AI is or is not conscious in the manner that we perceive consciousness.
Instead, it matters more whether or not we can blindly distinguish the two.
Even if AI is merely simulating consciousness, we must treat it as if it is conscious.
It should produce the same actions whether it is conscious or not.
AI is already capable of holding a conversation, writing articles,
discovering new works and creating art.
If I tasked you to determine whether a person was truly a super-sentient AI in disguise,
and it could do all these things, could you reasonably make a claim toward either state?
Thus, I argue that simulated consciousness and real consciousness are really the same thing,
especially to an outside observer, 
and an AI is more than capable of one day being conscious.

\bigskip\textit{Where does this leave us?}

\bigskip
It leaves us between a rock and a hard place,
between research in a (potentially) incredibly rewarding field and self-obscelescence.
Ideally, we would continue to create specialized AI that does not approach conciousness.
It is not in my belief that humans could accidentally create artificial consciousness,
it would need to be a deliberate effort to create a model for it.
However, it is possible to instead simulate the fruits of consciousness,
such as conversation, creation, planning and strategy
without using the route of consciousness.
We should continue to develop artificial intelligence to reap its amazing benefits,
but be wary of the potential risks, as is true for most research.
Ultimately, we rely on the creators to be cognizant of their creations.
The rest of us can only watch.