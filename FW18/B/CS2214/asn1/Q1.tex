%! TEX root = asn1.tex

\begin{problem}

	\begin{problemTitle}
		Let $p,q$ be two Boolean variables.
		By definition,
		the implication $p \rightarrow q$
		is true if and only if
		$p$ is false or $q$ is true.
		Based on that,
		we have established the following practical tautologies:
		\begin{enumerate}[label=\arabic*.]
			\item $(p \rightarrow q) \leftrightarrow (\neg q \rightarrow \neg p)$
			\item $(p \leftrightarrow q) \leftrightarrow ((p \rightarrow q) \wedge (q \rightarrow p))$
		\end{enumerate}
		Would these two tautologies still be true
		if we were changing the truth value
		of the implication $p \rightarrow q$ to that of
		\begin{enumerate}[label=\alph*.]
			\item $p \wedge q$
			\item $p \vee q$
		\end{enumerate}
		Justify your answer.
	\end{problemTitle}

	None of these statements will remain as tautologies. The implication operator ($\alpha \rightarrow \beta$) is equivalent to $\neg \alpha \vee \beta$. This has a clearly different truth value compared to both substitution a and b.

	\vspace{2mm}

	However, for some more solid evidence:

	\begin{enumerate}[label=\arabic*.]
		\item
		      \begin{enumerate}[label=\alph*.]
			      \item $(p \wedge q) \leftrightarrow (\neg q \wedge \neg p)$

			            By a truth table:

			            \begin{tabular}{|c c|c|c|c|}
				            \hline
				                   &        & \circlednum{1} & \circlednum{2}         &                                                 \\
				            $p$    & $q$    & $p \wedge q$   & $\neg q \wedge \neg p$ & $\circlednum{1} \leftrightarrow \circlednum{2}$ \\
				            \hline
				            $\bot$ & $\bot$ & $\bot$         & $\top$                 & $\bot$                                          \\
				            $\bot$ & $\top$ & $\bot$         & $\bot$                 & $\top$                                          \\
				            $\top$ & $\bot$ & $\bot$         & $\bot$                 & $\top$                                          \\
				            $\top$ & $\top$ & $\top$         & $\bot$                 & $\bot$                                          \\
				            \hline
			            \end{tabular}

			            This statement is satisfiable, but is no longer a tautology.

			      \item $(p \vee q) \leftrightarrow (\neg q \vee \neg p)$

			            By a truth table:

			            \begin{tabular}{|c c|c|c|c|}
				            \hline
				                   &        & \circlednum{1} & \circlednum{2}       &                                                 \\
				            $p$    & $q$    & $p \vee q$     & $\neg q \vee \neg p$ & $\circlednum{1} \leftrightarrow \circlednum{2}$ \\
				            \hline
				            $\bot$ & $\bot$ & $\bot$         & $\top$               & $\bot$                                          \\
				            $\bot$ & $\top$ & $\top$         & $\top$               & $\top$                                          \\
				            $\top$ & $\bot$ & $\top$         & $\top$               & $\top$                                          \\
				            $\top$ & $\top$ & $\top$         & $\bot$               & $\bot$                                          \\
				            \hline
			            \end{tabular}

			            This statement is also satisfiable, but not a tautology.
		      \end{enumerate}

		      \pagebreak

		\item

		      \begin{enumerate}[label=\alph*.]
			      \item $(p \leftrightarrow q) \leftrightarrow ((p \wedge q) \wedge (q \wedge p))$

			            Since the $\wedge$ operator is commutative, I will first rewrite $q \wedge p$ as $p \wedge q$
			            \[
				            (p \leftrightarrow q) \leftrightarrow ((p \wedge q) \wedge (p \wedge q))
			            \]

			            Now the $(p \wedge q) \wedge (p \wedge q)$ can be simplified as a single $p \wedge q$:
			            \[
				            (p \leftrightarrow q) \leftrightarrow (p \wedge q)
			            \]

			            Finally, for a truth table:

			            \begin{tabular}{|c c|c|c|c|}
				            \hline
				                   &        & \circlednum{1}        & \circlednum{2} &                                                 \\
				            $p$    & $q$    & $p \leftrightarrow q$ & $p \wedge q$   & $\circlednum{1} \leftrightarrow \circlednum{2}$ \\
				            \hline
				            $\bot$ & $\bot$ & $\top$                & $\bot$         & $\bot$                                          \\
				            $\bot$ & $\top$ & $\bot$                & $\bot$         & $\top$                                          \\
				            $\top$ & $\bot$ & $\bot$                & $\bot$         & $\top$                                          \\
				            $\top$ & $\top$ & $\top$                & $\top$         & $\top$                                          \\
				            \hline
			            \end{tabular}

			            Once again, this statement is satisfiable, but not a tautology.

			      \item $(p \leftrightarrow q) \leftrightarrow ((p \vee q) \wedge (q \vee p))$

			            Since the $\vee$ operator is also commutative, it can be simplified in the same way as 2a:
			            \[
				            (p \leftrightarrow q) \leftrightarrow (p \vee q)
			            \]

			            With another truth table:

			            \begin{tabular}{|c c|c|c|c|}
				            \hline
				                   &        & \circlednum{1}        & \circlednum{2} &                                                 \\
				            $p$    & $q$    & $p \leftrightarrow q$ & $p \vee q$     & $\circlednum{1} \leftrightarrow \circlednum{2}$ \\
				            \hline
				            $\bot$ & $\bot$ & $\top$                & $\bot$         & $\bot$                                          \\
				            $\bot$ & $\top$ & $\bot$                & $\top$         & $\bot$                                          \\
				            $\top$ & $\bot$ & $\bot$                & $\top$         & $\bot$                                          \\
				            $\top$ & $\top$ & $\top$                & $\top$         & $\top$                                          \\
				            \hline
			            \end{tabular}

			            Another satisfiable statement, but also not a tautology.

		      \end{enumerate}

	\end{enumerate}

\end{problem}