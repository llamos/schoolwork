#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

// Copied from evaluate.c
char get_continue();
void print_number_string(const char * str);

int main() {

    // Loop until quit
    bool running = true;
    while (running) {
        printf("Please enter a number: ");

        // Get input
        char str[60];
        fgets(str, 60, stdin);

        print_number_string(str);

        // Assume we aren't looping again
        running = false;
        printf("Do you want to enter another number? [y/N] ");
        char response = get_continue();
        if (response == 'y' || response == 'Y') {

            // Get ready to loop again
            running = true;
        }
    }



}

void print_number_string(const char* str) {
    // 3 characters for a row accessed by numbers[numberToPrint][row]
    //region Numbers
    const char numbers[10][3][3] = {
            {
                    {' ', '_', ' '},
                    {'|', ' ', '|'},
                    {'|', '_', '|'}
            },
            {
                    {' ', ' ', ' '},
                    {' ', ' ', '|'},
                    {' ', ' ', '|'}
            },
            {
                    {' ', '_', ' '},
                    {' ', '_', '|'},
                    {'|', '_', ' '}
            },
            {
                    {' ', '_', ' '},
                    {' ', '_', '|'},
                    {' ', '_', '|'}
            },
            {
                    {' ', ' ', ' '},
                    {'|', '_', '|'},
                    {' ', ' ', '|'}
            },
            {
                    {' ', '_', ' '},
                    {'|', '_', ' '},
                    {' ', '_', '|'}
            },
            {
                    {' ', '_', ' '},
                    {'|', '_', ' '},
                    {'|', '_', '|'}
            },
            {
                    {' ', '_', ' '},
                    {' ', ' ', '|'},
                    {' ', ' ', '|'}
            },
            {
                    {' ', '_', ' '},
                    {'|', '_', '|'},
                    {'|', '_', '|'}
            },
            {
                    {' ', '_', ' '},
                    {'|', '_', '|'},
                    {' ', '_', '|'}
            },
    };
    //endregion Numbers

    // Loop three times for the three output lines
    int i;
    for (i = 0; i < 3; ++i) {
        int index = 0;
        char currentCharacter = str[index++];

        // Loop until null character (end of input line)
        while (currentCharacter != '\0') {
            // Print i-th line, current character's info
            switch (currentCharacter) {
                // Special case for the sign
                case '-':
                    if (i == 0 || i == 2)
                        printf("   ");
                    else
                        printf(" __");
                    break;
                    // Print characters based on current row and character
                case '0':
                    printf("%c%c%c", numbers[0][i][0], numbers[0][i][1], numbers[0][i][2]);
                    break;
                case '1':
                    printf("%c%c%c", numbers[1][i][0], numbers[1][i][1], numbers[1][i][2]);
                    break;
                case '2':
                    printf("%c%c%c", numbers[2][i][0], numbers[2][i][1], numbers[2][i][2]);
                    break;
                case '3':
                    printf("%c%c%c", numbers[3][i][0], numbers[3][i][1], numbers[3][i][2]);
                    break;
                case '4':
                    printf("%c%c%c", numbers[4][i][0], numbers[4][i][1], numbers[4][i][2]);
                    break;
                case '5':
                    printf("%c%c%c", numbers[5][i][0], numbers[5][i][1], numbers[5][i][2]);
                    break;
                case '6':
                    printf("%c%c%c", numbers[6][i][0], numbers[6][i][1], numbers[6][i][2]);
                    break;
                case '7':
                    printf("%c%c%c", numbers[7][i][0], numbers[7][i][1], numbers[7][i][2]);
                    break;
                case '8':
                    printf("%c%c%c", numbers[8][i][0], numbers[8][i][1], numbers[8][i][2]);
                    break;
                case '9':
                    printf("%c%c%c", numbers[9][i][0], numbers[9][i][1], numbers[9][i][2]);
                    break;
                default:break; // Skip unknown characters
            }

            // Print an extra space after each character for padding
            printf(" ");

            // Get next character (if it is \0 the loop will end)
            currentCharacter = str[index++];
        }

        // End line after each iteration
        printf("\n");
    }
}

// Copied from evaluate.c
char get_continue() {
    // Default value
    char answer = 'n';

    bool searching = true;
    while (searching) {
        char currentChar = (char) getc(stdin);
        if (currentChar == 'y' || currentChar == 'Y' || currentChar == 'n' || currentChar == 'N') {
            // Store it until a newline is found
            answer = currentChar;
        } else if (currentChar == '\n') {
            searching = false;
        }
    }

    return answer;
}