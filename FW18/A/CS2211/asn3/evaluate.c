#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

float s_exp(float sub_exp, char op);

float m_exp(float sub_exp, char op);

char get_op();

float get_num();

char get_continue();

int main() {
    // Loop until quit
    bool running = true;
    while (running) {
        // Calculate
        printf("Please enter your expression: ");
        float result = s_exp(0, '+');
        printf("The result is %f.\n", result);


        // Assume we aren't looping again
        running = false;
        printf("Do you want to evaluate an expression? [y/N] ");
        char response = get_continue();
        if (response == 'y' || response == 'Y') {

            // Get ready to loop again
            running = true;
        }
    }

    // Exit
    printf("Goodbye!\n");
    return 0;
}

float s_exp(float sub_exp, char op) {
    // Return result
    if (op == '\n') {
        return sub_exp;
    }

    // Pull next number from m_exp and next operator
    float next_num = m_exp(1, '*');
    char next_op = get_op();

    // Perform op
    if (op == '+') {
        next_num = sub_exp + next_num;
    } else if (op == '-') {
        next_num = sub_exp - next_num;
    }

    // Recurse
    return s_exp(next_num, next_op);
}

float m_exp(float sub_exp, char op) {
    // Push back to s_exp
    if (op == '+' || op == '-' || op == '\n') {
        ungetc(op, stdin);
        return sub_exp;
    }

    // Pull next number for current and next calculation
    float next_num = get_num();
    char next_op = get_op();

    // Perform op
    if (op == '*') {
        next_num = sub_exp * next_num;
    } else if (op == '/') {
        next_num = sub_exp / next_num;
    }

    // Recurse
    return m_exp(next_num, next_op);
}

char get_op() {
    char op = ' ';
    while (op == ' ') {
        // Read one char
        op = (char) getc(stdin);
        // Then check validity of the character
        if (op == '+' || op == '-' || op == '*' || op == '/' || op == '\n') {
            return op;
        }
        // If it didn't pass, we'll loop again
    }

    // If the loop exists, we got an invalid operator
    printf("Unrecognized operator %c.\n", op);
    exit(EXIT_FAILURE);
}

float get_num() {
    float ret;
    scanf(" %f", &ret);
    return ret;
}

// Basically a clone of get_op with different target characters
// And it goes until a y or n, regardless of other characters
// Also consumes all following characters until a newline
char get_continue() {
    // Default value
    char answer = 'n';

    bool searching = true;
    while (searching) {
        char currentChar = (char) getc(stdin);
        if (currentChar == 'y' || currentChar == 'Y' || currentChar == 'n' || currentChar == 'N') {
            // Store it until a newline is found
            answer = currentChar;
        } else if (currentChar == '\n') {
            searching = false;
        }
    }

    return answer;
}