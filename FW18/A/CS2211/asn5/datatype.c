#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "datatype.h"


char *string_dup(char *orig) {
    // Allocate memory of correct size for new_str
    size_t len = strlen(orig);

    char* new_str = malloc(len + 1); // +1 for null-terminator

    // Copy chars over
    for (int i = 0; i < len; ++i) {
         new_str[i] = orig[i];
    }

    // Null-terminate
    new_str[len] = '\0';

    // Return pointer to cloned string
    return new_str;
}

Key key_gen(char *skey1, char *skey2) {
    // Allocate memory and duplicate strings
    Key key = malloc(sizeof(Key_struct));
    char* key1 = string_dup(skey1);
    char* key2 = string_dup(skey2);


    // Assign values
    key->skey1 = key1;
    key->skey2 = key2;

    // Return new key
    return key;
}

int key_comp(Key key1, Key key2) {
    // Check key1 first, but if it is 0, use key2
    int key1Result = strcmp(key1->skey1, key2->skey1);
    if (key1Result == 0) {
        return strcmp(key1->skey2, key2->skey2);
    }

    return key1Result;
}

void key_print(Key key) {
    // Get size of each string for padding
    char* k1 = key->skey1;
    char* k2 = key->skey2;
    int k1_len = (int) strlen(k1);
    int k2_len = (int) strlen(k2);

    // Print and pad k1
    printf("%s", k1);
    int i;
    for (i = 0; i < 16 - k1_len; ++i) {
        printf(" ");
    }

    // Print and pad k2
    printf("%s", k2);
    for (i = 0; i < 16 - k2_len; ++i) {
        printf(" ");
    }
}

void key_free(Key key) {
    // Destroy key data before struct
    free(key->skey1);
    free(key->skey2);
    free(key);
}

Data data_gen(int idata) {
    Data ret = malloc(sizeof(int));
    *ret = idata;
    return ret;
}

void data_set(Data data, int idata) {
    *data = idata;
}

void data_print(Data data) {
    printf("%d", *data);
}

void data_free(Data data) {
    free(data);
}
