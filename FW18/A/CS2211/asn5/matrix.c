#include <stdlib.h>
#include <stdio.h>
#include "matrix.h"

Matrix matrix_construction(void) {
    return bstree_ini();
}

unsigned char matrix_isin(Matrix m, Index index1, Index index2) {
    // bstree_search, but check for null instead of returning the data
    Key k = key_gen(index1, index2);
    Data result = bstree_search(m, k);
    key_free(k);
    return (unsigned char) (result != NULL);
}

Value* matrix_get(Matrix m, Index index1, Index index2) {
    // Same as isin, but return data instead
    Key k = key_gen(index1, index2);
    Data result = bstree_search(m, k);
    key_free(k);
    return result;
}

void matrix_set(Matrix m, Index index1, Index index2, Value value) {
    Key k = key_gen(index1, index2);
    Data current_data = bstree_search(m, k);

    // Check if the data exists
    if (current_data == NULL) {
        // Add the data
        bstree_insert(m, k, data_gen(value));
    } else {
        // If it was there, we overwrite the data
        data_set(current_data, value);
        key_free(k);
    }
}

void matrix_inc(Matrix m, Index index1, Index index2, Value value) {
    Key k = key_gen(index1, index2);
    Data current_data = bstree_search(m, k);

    // Check if the data exists
    if (current_data == NULL) {
        printf("Attempted to increment key (%s, %s) that was not found in the matrix.", index1, index2);
    } else {
        // If it was there, we increment the data
        data_set(current_data, *current_data + value);
    }
    key_free(k);
}

void matrix_list(Matrix m) {
    printf("Key 1           Key 2           Occurrences\n");
    printf("===========================================\n");
    bstree_traversal(m);
}

void matrix_destruction(Matrix m) {
    bstree_free(m);
}
