#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "matrix.h"

int main() {

    //region KEYS

    printf("=== KEYS ===\n");

    printf("\tTest 1: String duplication.... ");
    char* rightSize = "Right Size";
    char overSize[64] = "Under size";
    char* rightSizeClone = string_dup(rightSize);
    char* overSizeClone = string_dup(overSize);
    if (rightSize == rightSizeClone || overSize == overSizeClone) {
        printf("FAIL\n\t\tDuplicated strings point to same address.\n");
        return 1;
    } else if (strcmp(rightSize, rightSizeClone) != 0) {
        printf("FAIL\n\t\tDuplicated strings are not equal. actual: (%s) expected: (%s)\n", rightSizeClone, rightSize);
        return 1;
    } else if (strcmp(overSize, overSizeClone) != 0) {
        printf("FAIL\n\t\tDuplicated strings are not equal. actual: (%s) expected: (%s)\n", overSizeClone, overSize);
        return 1;
    } else {
        printf("OK\n");
    }
    free(rightSizeClone);
    free(overSizeClone);

    printf("\tTest 2: Key creation.... ");
    Key key = key_gen(rightSize, overSize);
    if (key == NULL) {
        printf("FAIL\n\t\tKey is null.\n");
        return 1;
    } else if (key->skey1 == rightSize || key->skey2 == overSize) {
        printf("FAUL\n\t\tKey values point to original memory addresses.\n");
        return 1;
    } else if (strcmp(key->skey1, rightSize) != 0) {
        printf("FAIL\n\t\tKey->skey1 does not match input. actual: (%s) expected: (%s)\n", key->skey1, rightSize);
        return 1;
    } else if (strcmp(key->skey2, overSize) != 0) {
        printf("FAIL\n\t\tKey->skey2 does not match input. actual: (%s) expected: (%s)\n", key->skey2, overSize);
        return 1;
    } else {
        printf("OK\n");
    }

    printf("\tTest 3: Key comparison.... ");
    Key keyOuterLess = key_gen("Less than other key", overSize);
    Key keyOuterMore = key_gen("ZMore than other key", overSize);
    Key keyInnerLess = key_gen(rightSize, "Less than other key");
    Key keyInnerMore = key_gen(rightSize, "ZMore than other key");
    if (key_comp(keyOuterLess, key) >= 0) {
        printf("FAIL\n\t\tExpected (%s, %s) to be less than (%s, %s)", keyOuterLess->skey1, keyOuterLess->skey2, key->skey1, key->skey2);
        return 1;
    } else if (key_comp(keyOuterMore, key) <= 0) {
        printf("FAIL\n\t\tExpected (%s, %s) to be more than (%s, %s)", keyOuterMore->skey1, keyOuterMore->skey2, key->skey1, key->skey2);
        return 1;
    } else if (key_comp(keyInnerLess, key) >= 0) {
        printf("FAIL\n\t\tExpected (%s, %s) to be less than (%s, %s)", keyInnerLess->skey1, keyInnerLess->skey2, key->skey1, key->skey2);
        return 1;
    } else if (key_comp(keyInnerMore, key) <= 0) {
        printf("FAIL\n\t\tExpected (%s, %s) to be more than (%s, %s)", keyInnerMore->skey1, keyInnerMore->skey2, key->skey1, key->skey2);
        return 1;
    } else if (key_comp(key, key) != 0) {
        printf("FAIL\n\t\tExpected (%s, %s) to equal (%s, %s)", key->skey1, key->skey2, key->skey1, key->skey2);
        return 1;
    } else {
        printf("OK\n");
    }

    printf("\tNot testing key_print.\n");

    printf("\tNot testing key_free.\n");

    //endregion Keys

    //region TREES

    printf("\n\n=== TREES ===\n");

    printf("\tTest 1: Tree creation.... ");
    BStree tree = bstree_ini();
    if (tree == NULL) {
        printf("FAIL\n\t\tTop level pointer points to NULL.\n");
        return 1;
    } else if (*tree != NULL) {
        printf("FAIL\n\t\tInitial tree does not point to NULL.\n");
        return 1;
    } else {
        printf("OK\n");
    }

    printf("\tTest 2: Tree root insertion.... ");
    int data = 1;
    bstree_insert(tree, key, &data);
    if ((*tree)->key != key) {
        printf("FAIL\n\t\tInserted key does not match. expected: (%s, %s) actual: (%s, %s)\n", key->skey1, key->skey2, (*tree)->key->skey1, (*tree)->key->skey2);
        return 1;
    } else if (*(*tree)->data != data) {
        printf("FAIL\n\t\tInserted data does not match. expected: (%d) actual: (%d)\n", *(*tree)->data, data);
        return 1;
    } else {
        printf("OK\n");
    }

    printf("\tTest 3: Tree left side node insertion.... ");
    int data2 = 2;
    bstree_insert(tree, keyOuterLess, &data2);
    if ((*tree)->key != key) {
        printf("FAIL\n\t\tRoot node key was moved. expected: (%s, %s) actual: (%s, %s)\n", key->skey1, key->skey2, (*tree)->key->skey1, (*tree)->key->skey2);
        return 1;
    } else if (*(*tree)->data != data) {
        printf("FAIL\n\t\tRoot node data was moved. expected: (%d) actual: (%d)\n", *(*tree)->data, data);
        return 1;
    } else if ((*tree)->left == NULL) {
        printf("FAIL\n\t\tNode was not found on left side.");
        return 1;
    } else if ((*tree)->left->key != keyOuterLess) {
        printf("FAIL\n\t\tLeft node key did not match. expected: (%s, %s) actual: (%s, %s)\n", keyInnerLess->skey1, keyInnerLess->skey2, (*tree)->left->key->skey1, (*tree)->left->key->skey2);
        return 1;
    } else if (*(*tree)->left->data != data2) {
        printf("FAIL\n\t\tLeft node data did not match. expected: (%d) actual: (%d)\n", data2, *(*tree)->left->data);
        return 1;
    } else {
        printf("OK\n");
    }

    printf("\tTest 4: Tree right side node insertion.... ");
    int data3 = 3;
    bstree_insert(tree, keyInnerMore, &data3);
    if ((*tree)->key != key) {
        printf("FAIL\n\t\tRoot node key was moved. expected: (%s, %s) actual: (%s, %s)\n", key->skey1, key->skey2, (*tree)->key->skey1, (*tree)->key->skey2);
        return 1;
    } else if (*(*tree)->data != data) {
        printf("FAIL\n\t\tRoot node data was moved. expected: (%d) actual: (%d)\n", *(*tree)->data, data);
        return 1;
    } else if ((*tree)->right == NULL) {
        printf("FAIL\n\t\tNode was not found on right side.");
        return 1;
    } else if ((*tree)->right->key != keyInnerMore) {
        printf("FAIL\n\t\tRight node key did not match. expected: (%s, %s) actual: (%s, %s)\n", keyInnerMore->skey1, keyInnerMore->skey2, (*tree)->right->key->skey1, (*tree)->right->key->skey2);
        return 1;
    } else if (*(*tree)->right->data != data3) {
        printf("FAIL\n\t\tRight node data did not match. expected: (%d) actual: (%d)\n", data3, *(*tree)->right->data);
        return 1;
    } else {
        printf("OK\n");
    }

    printf("\tTest 5: Tree left side leaf node insertion.... ");
    int data4 = 4;
    bstree_insert(tree, keyInnerLess, &data4);
    if ((*tree)->key != key) {
        printf("FAIL\n\t\tRoot node key was moved. expected: (%s, %s) actual: (%s, %s)\n", key->skey1, key->skey2, (*tree)->key->skey1, (*tree)->key->skey2);
        return 1;
    } else if (*(*tree)->data != data) {
        printf("FAIL\n\t\tRoot node data was moved. expected: (%d) actual: (%d)\n", *(*tree)->data, data);
        return 1;
    } else if ((*tree)->left->right == NULL) {
        printf("FAIL\n\t\tNo node was found at position ROOT-LEFT-RIGHT\n");
        return 1;
    } else if ((*tree)->left->right->key != keyInnerLess) {
        printf("FAIL\n\t\tNode key did not match. expected: (%s, %s) actual: (%s, %s)\n", keyInnerLess->skey1, keyInnerLess->skey2, (*tree)->left->right->key->skey1, (*tree)->left->right->key->skey2);
        return 1;
    } else if (*(*tree)->left->right->data != data4) {
        printf("FAIL\n\t\tNode data did not match. expected: (%d) actual: (%d)\n", data4, *(*tree)->left->right->data);
        return 1;
    } else {
        printf("OK\n");
    }

    printf("\tTest 6: Tree right side leaf node insertion....");
    int data5 = 5;
    bstree_insert(tree, keyOuterMore, &data5);
    if ((*tree)->key != key) {
        printf("FAIL\n\t\tRoot node key was moved. expected: (%s, %s) actual: (%s, %s)\n", key->skey1, key->skey2, (*tree)->key->skey1, (*tree)->key->skey2);
        return 1;
    } else if (*(*tree)->data != data) {
        printf("FAIL\n\t\tRoot node data was moved. expected: (%d) actual: (%d)\n", *(*tree)->data, data);
        return 1;
    } else if ((*tree)->right->right == NULL) {
        printf("FAIL\n\t\tNo node was found at position ROOT-RIGHT-RIGHT\n");
        return 1;
    } else if ((*tree)->right->right->key != keyOuterMore) {
        printf("FAIL\n\t\tNode key did not match. expected: (%s, %s) actual: (%s, %s)\n", keyOuterMore->skey1, keyOuterMore->skey2, (*tree)->left->right->key->skey1, (*tree)->left->right->key->skey2);
        return 1;
    } else if (*(*tree)->right->right->data != data5) {
        printf("FAIL\n\t\tNode data did not match. expected: (%d) actual: (%d)\n", data5, *(*tree)->left->right->data);
        return 1;
    } else {
        printf("OK\n");
    }

    printf("\tTest 7: Search for root data.... ");
    Data result = bstree_search(tree, key);
    if (*result != data) {
        printf("FAIL\n\t\tWrong data returned. expected: (%d) actual: (%d)\n", data, *result);
    } else {
        printf("OK\n");
    }

    printf("\tTest 8: Search for leaf data.... ");
    result = bstree_search(tree, keyOuterMore);
    if (*result != data5) {
        printf("FAIL\n\t\tWrong data returned. expected: (%d) actual: (%d)\n", data5, *result);
    } else {
        printf("OK\n");
    }

    printf("\tTest 9: Search for non-existent data.... ");
    Key non_existent = key_gen("I don't", "exist");
    result = bstree_search(tree, non_existent);
    if (result != NULL) {
        printf("FAIL\n\t\tWrong data returned. expected: (NULL) actual: (%d)\n", *result);
    } else {
        printf("OK\n");
    }

    printf("\tNot testing bstree_traversal.\n");
    printf("\tNot testing bstree_free.\n");

    //endregion TREES

    //region MATRICES

    printf("\n\n=== MATRICES ===\n");
    Matrix m = matrix_construction();

    printf("\tTest 1: Adding initial test values.... ");
    matrix_set(m, key->skey1, key->skey2, 1);
    matrix_set(m, keyInnerMore->skey1, keyInnerMore->skey2, 2);
    matrix_set(m, keyOuterLess->skey1, keyOuterLess->skey2, 3);
    printf("OK\n");

    printf("\tTest 2: matrix_isin.... ");
    unsigned char res = matrix_isin(m, key->skey1, key->skey2);
    if (res != 1) {
        printf("FAIL\n\t\tKey that was added with matrix_set was not found in matrix. expected: (1) actual: (%d)\n", res);
        return 1;
    }
    res = matrix_isin(m, keyInnerMore->skey1, keyInnerMore->skey2);
    if (res != 1) {
        printf("FAIL\n\t\tKey that was added with matrix_set was not found in matrix. expected: (1) actual: (%d)\n", res);
        return 1;
    }
    res = matrix_isin(m, keyOuterMore->skey1, keyOuterMore->skey2);
    if (res != 0) {
        printf("FAIL\n\t\tKey that was not added with matrix_set was found in matrix. expected: (0) actual: (%d)\n", res);
        return 1;
    }
    printf("OK\n");

    printf("\tTest 3: matrix_get.... ");
    Data c_val = matrix_get(m, key->skey1, key->skey2);
    if (*c_val != 1) {
        printf("FAIL\n\t\tGot wrong value. expected: (1) actual: (%d)\n", *c_val);
        return 1;
    }
    c_val = matrix_get(m, keyInnerMore->skey1, keyInnerMore->skey2);
    if (*c_val != 2) {
        printf("FAIL\n\t\tGot wrong value. expected: (2) actual: (%d)\n", *c_val);
        return 1;
    }
    c_val = matrix_get(m, non_existent->skey1, non_existent->skey2);
    if (c_val != NULL) {
        printf("FAIL\n\t\tNon-existent key was found in matrix. expected: (NULL) actual: (%d)\n", *c_val);
        return 1;
    }
    printf("OK\n");

    printf("\tTest 4: matrix_inc.... ");
    matrix_inc(m, key->skey1, key->skey2, 3);
    matrix_inc(m, keyInnerMore->skey1, keyInnerMore->skey2, 5);
    c_val = matrix_get(m, key->skey1, key->skey2);
    if (*c_val != 4) {
        printf("FAIL\n\t\tGot wrong value after increment. expected: (4) actual: (%d)\n", *c_val);
        return 1;
    }
    c_val = matrix_get(m, keyInnerMore->skey1, keyInnerMore->skey2);
    if (*c_val != 7) {
        printf("FAIL\n\t\tGot wrong value after increment. expected: (7) actual: (%d)\n", *c_val);
        return 1;
    }
    printf("OK\n");

    printf("\tTest 5: matrix_set.... ");
    matrix_set(m, key->skey1, key->skey2, 20);
    matrix_set(m, keyInnerMore->skey1, keyInnerMore->skey2, 40);
    c_val = matrix_get(m, key->skey1, key->skey2);
    if (*c_val != 20) {
        printf("FAIL\n\t\tGot wrong value after set. expected: (20) actual: (%d)\n", *c_val);
        return 1;
    }
    c_val = matrix_get(m, keyInnerMore->skey1, keyInnerMore->skey2);
    if (*c_val != 40) {
        printf("FAIL\n\t\tGot wrong value after set. expected: (40) actual: (%d)\n", *c_val);
        return 1;
    }
    printf("OK\n");

    printf("\tNot testing matrix_list.\n");
    printf("\tNot testing matrix_free.\n");

    //endregion

    return 0;

}