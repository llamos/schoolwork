#ifndef _DATATYPE_H
#define _DATATYPE_H

typedef int* Data;
typedef struct {char* skey1; char* skey2;} Key_struct;
typedef Key_struct* Key;

/// Duplicates the given string and returns a pointer to the new copy
/// \param orig the string to copy
/// \return the cloned string
char* string_dup(char* orig);

/// Creates a new key with the given pair of strings
/// \param skey1 The primary key string
/// \param skey2 The secondary key string
/// \return The newly allocated and initialized Key
Key key_gen(char* skey1, char* skey2);

/// Compares two Keys determine their order. Prioritizes key1 of each Key
/// \param key1 The first key to compare
/// \param key2 The second key to compare to
/// \return < 0 if key1 < key2, 0 if they are the same, > 0 if key2 > key1
int key_comp(Key key1, Key key2);

/// Prints the contents of key to stdout
/// \param key The Key to print
void key_print(Key key);

/// Deallocates the memory used by key and its contents
/// \param key The Key to destroy
void key_free(Key key);

/// Creates a new Data struct with the given value
/// \param idata The value to place in the Data struct
/// \return The newly allocated and created Data
Data data_gen(int idata);

/// Updates the data struct with the new provided value
/// \param data The data struct to update
/// \param idata The new value
void data_set(Data data, int idata);

/// Prints the contents of data to stdout
/// \param data The Data to print
void data_print(Data data);

/// Deallocates the memory used by data and its contents
/// \param data The Data to destroy
void data_free(Data data);

#endif