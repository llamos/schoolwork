#include <stdio.h>
#include "datatype.h"
#include "bstree.h"
#include "matrix.h"

//
int main( )
{
    // Create matrix
    Matrix m = matrix_construction();

    printf("Please enter matrix entries below:\n");
    char key1[64], key2[64];
    while (scanf("%s %s", key1, key2) > 0) {
        // If the pair exists in the matrix, increment the value by 1,
        // otherwise set it to an initial value of 1
        if (matrix_isin(m, key1, key2)) {
            matrix_inc(m, key1, key2, 1);
        } else {
            matrix_set(m, key1, key2, 1);
        }
    }

    // Now that reading has stopped, print the matrix and free memory
    matrix_list(m);
    matrix_destruction(m);

    return 0;
}
