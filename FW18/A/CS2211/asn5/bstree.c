#include "bstree.h"
#include "datatype.h"
#include <stdlib.h>
#include <stdio.h>

// Helper for insert
BStree_node* create_node(Key, Data);

BStree bstree_ini(void) {
    // Create space for pointer (but not data) and assign to null
    BStree tree = malloc(sizeof tree);
    *tree = NULL;
    return tree;
}

// This function is recursive, at each call bst is the "root" of the subtree (or entire tree)
void bstree_insert(BStree bst, Key key, Data data) {
    if (*bst == NULL) {
        // Tree is empty, allocate memory and assign
        *bst = create_node(key, data);
        return;
    }

    // Node wasn't empty, branch left or right
    int cmp_result = key_comp(key, (*bst)->key);
    if (cmp_result == 0) {
        // Node already in tree, do nothing
        return;
    } else if (cmp_result < 0) {
        // Key is less than current node, move left
        bstree_insert(&(*bst)->left, key, data);
    } else {
        // Key is more than current node, move right
        bstree_insert(&(*bst)->right, key, data);
    }
}

Data bstree_search(BStree bst, Key key) {
    // Check for root to not be null
    if (*bst == NULL) {
        return NULL;
    }

    // Compare keys
    int cmp_result = key_comp(key, (*bst)->key);

    // Check for root to contain search key
    if (cmp_result == 0) {
        return (*bst)->data;
    }

    // Move left or right down the tree
    if (cmp_result < 0) {
        return bstree_search(&(*bst)->left, key);
    } else {
        return bstree_search(&(*bst)->right, key);
    }
}

void bstree_traversal(BStree bst) {
    // Base case, do nothing when null
    if (*bst == NULL) {
        return;
    }

    // Travel left first
    bstree_traversal(&(*bst)->left);

    // Print current
    key_print((*bst)->key);
    data_print((*bst)->data);
    printf("\n");

    // Travel right
    bstree_traversal(&(*bst)->right);
}

void bstree_free(BStree bst) {
    // Need to free ALL data, not just root
    // So travel depth-first recursively

    // Check current node for nullity
    if (*bst == NULL) {
        return;
    }

    // Free children
    bstree_free(&(*bst)->left);
    bstree_free(&(*bst)->right);

    // Free current
    key_free((*bst)->key);
    data_free((*bst)->data);
    free(*bst);
}

BStree_node* create_node(Key key, Data data) {
    BStree_node* bst = malloc(sizeof(BStree_node));
    bst->key = key;
    bst->data = data;
    bst->left = NULL;
    bst->right = NULL;
}
