#include "datatype.h"

#ifndef _BSTREE_H
#define _BSTREE_H

typedef struct BStree_node {
    Key key;
    Data data;
    struct BStree_node *left, *right;
} BStree_node;
typedef BStree_node** BStree;

/// Allocates space for a new binary search tree, without initializing values
/// \return The BStree pointer initialized to NULL
BStree bstree_ini(void);

/// Inserts the given data into bst
/// \param bst The tree to insert data to
/// \param key The key to insert the data as
/// \param data The data that the key will point to
void bstree_insert(BStree bst, Key key, Data data);

/// Finds the data represented by the given key
/// \param bst The tree to search in
/// \param key The key to search for in bst
/// \return The data represented by key, or NULL if it does not exist
Data bstree_search(BStree bst, Key key);

/// Performs an inorder traversal of bst and prints each node's key and data
/// \param bst The tree to traverse
void bstree_traversal(BStree bst);

/// Deallocates bst and all its nodes, keys and data
/// \param bst The bst to destroy
void bstree_free(BStree bst);

#endif