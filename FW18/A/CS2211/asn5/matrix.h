#include "bstree.h"

#ifndef _MATRIX_H
#define _MATRIX_H

typedef BStree Matrix;
typedef char* Index;
typedef int Value;

/// Creates a new empty matrix
/// \return The allocated, but empty Matrix
Matrix matrix_construction(void);

/// Checks whether a given index pair is defined in m
/// \param m The matrix to check
/// \param index1 The first index
/// \param index2 The second index
/// \return 1 if the index pair is defined, 0 if it is not
unsigned char matrix_isin(Matrix m, Index index1, Index index2);

/// Retrieves the value from m at the given index pair
/// \param m The Matrix to search
/// \param index1 The first index
/// \param index2 The second index
/// \return The value at (index1, index2) or NULL if it does not exist
Value* matrix_get(Matrix m, Index index1, Index index2);

/// Sets or overwrites the value at the given index pair
/// \param m The matrix to set the value in
/// \param index1 The first index
/// \param index2 The second index
/// \param value The value to set at (index1, index2)
void matrix_set(Matrix m, Index index1, Index index2, Value value);

/// Increments the value at the given index pair by value, or reports an error if the value does not exist
/// \param m The matrix to increment the value in
/// \param index1 The first index
/// \param index2 The second index
/// \param value The value to increment by
void matrix_inc(Matrix m, Index index1, Index index2, Value value);

/// Prints all the values in m
/// \param m The matrix to print
void matrix_list(Matrix m);

/// Deallocates all the memory held by m, and all its indices and values
/// \param m The Matrix to destroy
void matrix_destruction(Matrix m);

#endif