#include <stdio.h>

float calculateExponent(float base, int exponent);

int main()
{

    float base;
    int exponent;

    // Get user input
    printf("Please enter a base:\n");
    scanf(" %f", &base);
    printf("Please enter an exponent:\n");
    scanf(" %d", &exponent);

    // Ensure positive base
    if (base < 0)
    {
        printf("Can't work with a negative base.\n");
        return 1;
    }

    // Calculate and print
    float result = calculateExponent(base, exponent);
    printf("%f ^ %d = %f\n", base, exponent, result);

    return 0;
}

float calculateExponent(float base, int exponent)
{
    if (exponent < 0){ // Use positive power, then reciprocate
        return 1 / calculateExponent(base, -exponent);
    }
    if (exponent == 1)
    { // Base case
        return base;
    }

    if (exponent % 2 == 0)
    { // If even,
        float sqrtOfAnswer = calculateExponent(base, exponent / 2);
        return sqrtOfAnswer * sqrtOfAnswer;
    }
    else
    { // Odd
        float sqrtOfOneLess = calculateExponent(base, (exponent - 1) / 2);
        return sqrtOfOneLess * sqrtOfOneLess * base;
    }
}