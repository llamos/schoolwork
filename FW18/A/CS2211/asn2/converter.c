#include <stdio.h>
#include <stdbool.h>

// Pre-definitions to keep main on top
void getInput(char *, char *);
void kilometreMileConverter();
void metreFeetConverter();
void centimetreInchConverter();
void temperatureConverter();

// This must be outside main or it will be cleared when moving outside main's scope (for subcalls)
bool running = true;

int main()
{
    char conversionType;

    // Loop until quit
    while (running)
    {
        do // Loop conversion prompt until we get a valid character
        {
            printf("Please enter a choice:\n");
            printf("[1] Kilometre <-> Mile\n");
            printf("[2] Metre <-> Feet\n");
            printf("[3] Centimetre <-> Inch\n");
            printf("[4] Celsius <-> Farenheit\n");
            printf("[5] Quit\n");

            getInput(&conversionType, " %[1-5]"); // Force input to be 1, 2, 3, 4 or 5 or scanf fails
        } while (conversionType <= 0); // While it is not a valid input

        switch (conversionType)
        { // Jump out based on chosen converter
        case '1':
            kilometreMileConverter();
            break;
        case '2':
            metreFeetConverter();
            break;
        case '3':
            centimetreInchConverter();
            break;
        case '4':
            temperatureConverter();
            break;
        case '5': // Prepare to quit
            running = false;
            break;
        default: // Should never hit default, loop again
            break;
        }
    } // Master loop

    printf("Goodbye!\n");

    return 0;
}

void getInput(char *buf, char *format)
{
    int charsRead;
    // Loop while scanf fails
    while (!(charsRead = scanf(format, buf))) {
        if (charsRead == 0) {
            while ((fgetc(stdin)) != '\n'); // Discard input until end of line
        }
    }
}

void kilometreMileConverter()
{
    printf("Sure! Converting kilometres and miles.\n");

    char conversionMode; // Holds the character indicating direction of conversion
    do
    {
        printf("Do you want to go from\n[K] km to mi or \n[M] mi to km?\n");
        getInput(&conversionMode, " %[KkMm]");
    } while (conversionMode <= 0); // Loop again until valid

    float km, mi;
    switch (conversionMode)
    {
    case 'K': // km to mi
    case 'k':
        printf("How many kilometres?\n");
        scanf(" %f", &km);
        getchar();
        mi = km * 0.621371;                             // Magic constant from Google
        printf("%g kilometres is %g miles.\n", km, mi); // Print result
        break;
    case 'M': // mi to km
    case 'm':
        printf("How many miles?\n");
        scanf(" %f", &mi);
        getchar();
        km = mi * 1.60934;                              // Magic constant from Google
        printf("%g miles is %g kilometres.\n", km, mi); // Print result
        break;
    }

    //Return to main
    return;
}

void metreFeetConverter()
{
    printf("Sure! Converting metres and feet.\n");

    char conversionMode; // Holds the character indicating direction of conversion
    do
    {
        printf("Do you want to go from\n[M] m to ft or \n[F] ft to m?\n");
        getInput(&conversionMode, " %[MmFf]");
    } while (conversionMode <= 0); // Loop again until valid

    float m, ft;
    switch (conversionMode)
    {
    case 'M': // m to ft
    case 'm':
        printf("How many metres?\n");
        scanf(" %f", &m);
        getchar();
        ft = m * 3.28084;                         // Magic constant from Google
        printf("%g metres is %g feet.\n", m, ft); // Print result
        break;
    case 'F': // ft to m
    case 'f':
        printf("How many feet?\n");
        scanf(" %f", &ft);
        getchar();
        m = ft * 0.3048;                          // Magic constant from Google
        printf("%g feet is %g metres.\n", ft, m); // Print result
        break;
    }
    //Return to main
}

void centimetreInchConverter()
{
    printf("Sure! Converting centimetres and inches.\n");

    char conversionMode; // Holds the character indicating direction of conversion
    do
    {
        printf("Do you want to go from\n[C] cm to in or \n[I] in to cm?\n");
        getInput(&conversionMode, " %[CcIi]");
    } while (conversionMode <= 0); // Loop again until valid

    float cm, in;
    switch (conversionMode)
    {
    case 'C': // cm to in
    case 'c':
        printf("How many centimetres?\n");
        scanf(" %f", &cm);
        getchar();
        in = cm * 0.393701;                               // Magic constant from Google
        printf("%g centimetres is %g inches.\n", cm, in); // Print result
        break;
    case 'I': // ft to m
    case 'i':
        printf("How many inches?\n");
        scanf(" %f", &in);
        getchar();
        cm = in * 2.54;                                   // Magic constant from Google
        printf("%g inches is %g centimetres.\n", in, cm); // Print result
        break;
    }
    //Return to main
}

void temperatureConverter()
{
    printf("Sure! Converting Celsius and Farenheit.\n");

    char conversionMode; // Holds the character indicating direction of conversion
    do
    {
        printf("Do you want to go from\n[C] Celsius to Farenheit or \n[F] Farenheit to Celsius?\n");
        getInput(&conversionMode, " %[CcFf]");
    } while (conversionMode <= 0); // Loop again until valid

    float c, f;
    switch (conversionMode)
    {
    case 'C': // Celsius to Farenheit
    case 'c':
        printf("How many degrees Celsius?\n");
        scanf(" %f", &c);
        getchar();
        f = c * 1.8 + 32;                                              // Conversion
        printf("%g degrees Celsius is %g degrees Farenheit.\n", c, f); // Print result
        break;
    case 'F': // Farenheit to Celsius
    case 'f':
        printf("How many degrees Farenheit?\n");
        scanf(" %f", &f);
        getchar();
        c = (f - 32) / 1.8;                                            // Magic constant from Google
        printf("%g degrees Farenheit is %g degrees Celsius.\n", f, c); // Print result
        break;
    }
    //Return to main
}
