cmake_minimum_required(VERSION 3.11)
project(asn4 C)

set(CMAKE_C_STANDARD 99)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

add_executable(bst bst.c bst.h data.c data.h main.c)
add_executable(pi pi.c)