#include "bst.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

// Hidden from header
void bstree_insert_recursive(BStree bst, Key *key, int data, int currentIndex);
void bstree_traversal_recursive(BStree bst, int index);

// Input: ’size’: size of an array
// Output: a pointer of type BStree,
// i.e. a pointer to an allocated memory of BStree_struct type
// Effect: dynamically allocate memory of type BStree_struct
// allocate memory for a Node array of size+1 for member tree_nodes
// allocate memory for an unsigned char array of size+1 for member is_free
// set all entries of is_free to 1
// set member size to ’size’;
BStree bstree_ini(int size) {
    // Allocate the memory
    BStree tree = malloc(sizeof(BStree_struct));
    Node *nodes = malloc(sizeof(Node) * (size + 1));
    unsigned char *isFree = malloc(sizeof(unsigned char) * (size + 1));

    // Initialize is_free
    int i;
    for (i = 1; i <= size; i++)
        isFree[i] = 1;

    // Assign values
    tree->tree_nodes = nodes;
    tree->is_free = isFree;
    tree->size = size;

    return tree;
}

// Input: ’bst’: a binary search tree
// ’key’: a pointer to Key
// ’data’: an integer
// Effect: ’data’ with ’key’ is inserted into ’bst’
// if ’key’ is already in ’bst’, do nothing
void bstree_insert(BStree bst, Key *key, int data) {
    bstree_insert_recursive(bst, key, data, 1);
}

void bstree_insert_recursive(BStree bst, Key *key, int data, int currentIndex) {
    // If the current index is empty, insert here
    if (bst->is_free[currentIndex]) {
        // Create node
        Node newNode;
        newNode.key = key;
        newNode.data = data;

        // Add to BST
        bst->tree_nodes[currentIndex] = newNode;
        bst->is_free[currentIndex] = 0;
        return;
    }

    // Otherwise, move down the tree

    int cmpResult = key_comp(*bst->tree_nodes[currentIndex].key, *key);

    // Ignore if the key already exists
    if (cmpResult == 0)
        return;

    // Check index bounds before recursing
    int newIndex = 2 * currentIndex + (cmpResult > 0 ? 0 : 1);
    if (newIndex <= bst->size)
        bstree_insert_recursive(bst, key, data, newIndex);
}

// Input: ’bst’: a binary search tree
// Effect: print all the nodes in bst using in order traversal
void bstree_traversal(BStree bst) {
    bstree_traversal_recursive(bst, 1);
}

// Inorder recursive travsersal
void bstree_traversal_recursive(BStree bst, int index) {
    // Traverse left if it exists
    if (2 * index <= bst->size && !bst->is_free[2 * index]) {
        bstree_traversal_recursive(bst, 2 * index);
    }

    // Print current
    print_node(bst->tree_nodes[index]);

    // Traverse right if it exists
    if (2 * index + 1 <= bst->size && bst->is_free[2 * index + 1] == 0) {
        bstree_traversal_recursive(bst, 2 * index + 1);
    }
}

// Input: ’bst’: a binary search tree
// Effect: all memory used by bst are freed
void bstree_free(BStree bst) {
    free(bst->tree_nodes);
    free(bst->is_free);
    free(bst);
}