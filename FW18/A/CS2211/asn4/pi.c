#include <stdlib.h>
#include <stdio.h>
#include <math.h>

double calculate_pi(double epsilon);

int main() {
    printf("Please enter an epsilon to calculate pi within epsilon: ");

    double accuracy;
    scanf(" %lf", &accuracy);

    printf("%.16f\n", calculate_pi(accuracy));
}

double calculate_pi(double epsilon) {
    // Running Pi value
    double pi = 0;
    // error holds the current loop's calculated value
    double error = epsilon + 1;

    long long n = 1;
    while (fabs(error) > epsilon) {
        error = n % 2 == 0 ? -1 : 1; // (-1)^(n+1)
        error *= 4e1L / (2e1L * (n++) - 1e1L); // e1L ensures that the literal is interpreted as a long double
        pi += error; // Add current value to pi
    }

    // Return accumulated value
    return pi;
}