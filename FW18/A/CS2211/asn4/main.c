#include <stdio.h>
#include "bst.h"

int main(void) {
    // Get size of tree
    printf("Please enter a size for the tree: ");
    int tree_size;
    scanf(" %d", &tree_size);

    // Create tree
    BStree bst = bstree_ini(tree_size);

    // Read lines of text until scanf fails (probably due to EOF)
    printf("Please enter tree entries as lines of (key_name key_id data):\n");
    int read_items;
    char key_str[32];
    int key_id, entry_data;
    while ((read_items = scanf("%s %d %d", key_str, &key_id, &entry_data)) > 0) {
        printf("Inserting %s %d %d\n", key_str, key_id, entry_data);
        bstree_insert(bst, key_construct(key_str, key_id), entry_data);
    }

    printf("Printing tree\n");
    // Print tree and exit
    bstree_traversal(bst);
    bstree_free(bst);
    return 0;
}