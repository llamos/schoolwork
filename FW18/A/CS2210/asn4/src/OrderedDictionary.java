public class OrderedDictionary implements OrderedDictionaryADT {

    // Helper data class to store binary tree Nodes
    private class Node {
        private Record data;
        private Node left;
        private Node right;
        private Node parent;

        // At creation time, left and right will be empty Nodes always (since the current Node is replacing a leaf)
        Node(Record data, Node parent) {
            this.data = data;
            this.parent = parent;
        }

        public String toString() {
            return "Node(" + this.data + "," + (this.parent == null ? "null": this.parent.data) + "," + (this.left == null ? "null": this.left.data) + "," + (this.right == null ? "null" : this.right.data) + ")";
        }
    }

    // Initially empty node
    private Node root = new Node(null, null);

    /**
     * Returns the Record object with key k, or it returns null if such
     * a record is not in the dictionary.
     *
     * @param k the pair to find the record of
     */
    public Record get(Pair k) {
        Node current = root;
        while (current.data != null) {
            // Compare the two keys
            int compareResult = current.data.getKey().compareTo(k);
            // Return if the key was found
            if (compareResult == 0)
                return current.data;
            // Otherwise, go left or right based on compare result
            current = (compareResult < 0) ? current.left : current.right;
        }
        // If the key wasn't found yet, it's not in the tree
        return null;
    }

    /**
     * Inserts r into the ordered dictionary. It throws a DictionaryException
     * if a record with the same key as r is already in the dictionary.
     *
     * @param r the record to insert
     */
    public void put(Record r) throws DictionaryException {
        Node current = root;

        // Traverse the tree until an empty Node is found
        while (current.data != null) {
            int compareResult = current.data.getKey().compareTo(r.getKey());
            if (compareResult == 0) // Make sure it doesn't already exist
                throw new DictionaryException("Tried to add already-existing key to dictionary.");
            current = (compareResult < 0) ? current.left : current.right;
        }

        // Insert the current record on the newly found empty Node
        current.data = r;
        // And add empty children
        current.left = new Node(null, current);
        current.right = new Node(null, current);
    }

    /**
     * Removes the record with key k from the dictionary. It throws a
     * DictionaryException if the record is not in the dictionary.
     *
     * @param k the pair to remove
     */
    public void remove(Pair k) throws DictionaryException {
        // If this is the last node, reset root
        if (root.left.data == null && root.right.data == null && root.data.getKey().compareTo(k) == 0) {
            root = new Node(null, null);
            return;
        }

        Node nodeToRemove = root;
        boolean found = false;
        // Traverse the tree very similarly to get() to find the node to remove
        while (nodeToRemove.data != null) {
            int compareResult = nodeToRemove.data.getKey().compareTo(k);
            if (compareResult == 0) {
                found = true;
                break;
            }
            nodeToRemove = (compareResult < 0) ? nodeToRemove.left : nodeToRemove.right;
        }

        if (!found)
            throw new DictionaryException("Tried to remove non-existent key");

        // Now replace current node's data
        // If both children are empty, we can just remove the current node's data and children to convert it to an empty node
        if (nodeToRemove.left.data == null && nodeToRemove.right.data == null) {
            nodeToRemove.left = null;
            nodeToRemove.right = null;
            nodeToRemove.data = null;
            return;
        }

        // If only one child is null, we can remove the current node by relinking the child to the parent
        if (nodeToRemove.left.data == null) {
            // Shift left child up
            if (nodeToRemove == nodeToRemove.parent.left)
                nodeToRemove.parent.left = nodeToRemove.right;
            else
                nodeToRemove.parent.right = nodeToRemove.right;
            return;
        }

        if (nodeToRemove.right.data == null) {
            if (nodeToRemove == nodeToRemove.parent.left)
                nodeToRemove.parent.left = nodeToRemove.left;
            else
                nodeToRemove.parent.right = nodeToRemove.left;
            return;
        }

        // If both children are present, then we must rotate
        // We'll take the left subtree's right-most child
        // And shift it up as needed
        Node leftMax = nodeToRemove.left;
        while (leftMax.right.data != null)
            leftMax = leftMax.right;

        // Now we'll replace the current node's key and data with leftMax
        // so we don't disturb the left/right/parent relationships
        nodeToRemove.data = leftMax.data;

        // And remove the leftover duplicate data from leftMax by removing that node instead
        if (leftMax.left != null)
            leftMax.parent.left = nodeToRemove.left;
    }

    /**
     * Returns the successor of k (the record from the ordered dictionary
     * with smallest key larger than k); it returns null if the given key has
     * no successor. The given key DOES NOT need to be in the dictionary.
     *
     * @param k the pair to find the successor of
     */
    public Record successor(Pair k) {
        if (root.data == null)
            return null;

        return successor(k, root);
    }

    // Recurisve traversal
    private Record successor(Pair k, Node root) {
        // Only accept the root as a suitable successor if it is greater than k
        Record currentMin;
        if (root.data.getKey().compareTo(k) < 0)
            currentMin = root.data;
        else
            currentMin = null;

        // Check left subtree
        if (root.left.data != null) {
            Record leftSuccessor = successor(k, root.left);
            if (leftSuccessor != null)
                if (currentMin == null || leftSuccessor.getKey().compareTo(currentMin.getKey()) > 0)
                    currentMin = leftSuccessor;
        }

        // Check right subtree
        if (root.right.data != null) {
            Record rightSuccessor = successor(k, root.right);
            if (rightSuccessor != null)
                if (currentMin == null || rightSuccessor.getKey().compareTo(currentMin.getKey()) > 0)
                    currentMin = rightSuccessor;
        }

        // Return the accumulated successor of left subtree, right subtree and current node
        return currentMin;
    }

    /**
     * Returns the predecessor of k (the record from the ordered dictionary
     * with largest key smaller than k; it returns null if the given key has
     * no predecessor. The given key DOES NOT need to be in the dictionary.
     *
     * @param k the pair to find the predecessor of
     */
    public Record predecessor(Pair k) {
        if (root == null)
            return null;

        return predecessor(k, root);
    }

    // Recursive traversal
    private Record predecessor(Pair k, Node root) {
        // Only accept the root as a suitable predecessor if it is less than k
        Record currentMax;
        if (root.data.getKey().compareTo(k) > 0)
            currentMax = root.data;
        else
            currentMax = null;

        // Check left subtree
        if (root.left.data != null) {
            Record leftPredecessor = predecessor(k, root.left);
            if (leftPredecessor != null)
                if (currentMax == null || leftPredecessor.getKey().compareTo(currentMax.getKey()) < 0)
                    currentMax = leftPredecessor;
        }

        // Check right subtree
        if (root.right.data != null) {
            Record rightPredecessor = predecessor(k, root.right);
            if (rightPredecessor != null)
                if (currentMax == null || rightPredecessor.getKey().compareTo(currentMax.getKey()) < 0)
                    currentMax = rightPredecessor;
        }

        // Return the accumulated predecessor of left subtree, right subtree and current node
        return currentMax;
    }

    /**
     * Returns the record with smallest key in the ordered dictionary.
     * Returns null if the dictionary is empty.
     */
    public Record smallest() {
        // Check for empty
        if (root.data == null)
            return null;

        // Traverse all the way left
        Node current = root;
        while (current.left.data != null)
            current = current.left;

        // Return whatever node was landed on
        return current.data;
    }

    /**
     * Returns the record with largest key in the ordered dictionary.
     * Returns null if the dictionary is empty.
     */
    public Record largest() {
        // Do inverse of smallest()
        // Check for empty
        if (root.data == null)
            return null;

        // Traverse all the way right
        Node current = root;
        while (current.right.data != null)
            current = current.right;

        // Return whatever node was landed on
        return current.data;
    }
}
