public class Pair implements Comparable<Pair> {

    private String word;
    private String type;

    public Pair(String word, String type) {
        this.word = word.toLowerCase();
        this.type = type.toLowerCase();
    }

    public String getWord() {
        return word;
    }

    public String getType() {
        return type;
    }

    public int compareTo(Pair o) {
        int wordCompare = -word.compareTo(o.word);
        if (wordCompare == 0)
            return -type.compareTo(o.type);
        return wordCompare;
    }

}
