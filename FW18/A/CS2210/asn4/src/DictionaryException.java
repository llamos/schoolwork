public class DictionaryException extends Exception {

    public DictionaryException() {
    }

    public DictionaryException(String message) {
        super(message);
    }
}
