import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class UserInterface {

    private static OrderedDictionary dict;
    private static StringReader input;

    public static void main(String[] args) throws FileNotFoundException {
        // Get the input file if possible
        if (args.length < 1) {
            System.out.println("No input file was specified.");
            return;
        }

        File inputFile = new File(args[0]);
        if (!inputFile.exists()) {
            System.out.println("The input file does not exist.");
            return;
        }

        // Create an empty dictionary and add values from the file
        dict = new OrderedDictionary();
        Scanner dictFileReader = new Scanner(inputFile);

        while (dictFileReader.hasNextLine()) {
            // Pull the first line in a pair as the key, and second as the value
            String key = dictFileReader.nextLine();
            String value = dictFileReader.nextLine();

            // Detect value type
            String type = "text";
            if (value.endsWith(".jpg") || value.endsWith(".gif"))
                type = "image";
            else if (value.endsWith(".wav") || value.endsWith(".mid"))
                type = "audio";

            // Add it to the dict
            try {
                dict.put(new Record(new Pair(key, type), value));
            } catch (DictionaryException e) {
                System.out.println(e.getMessage());
                return;
            }
        }

        // Close the file
        dictFileReader.close();

        // Now let the user play around with the values in the dictionary
        input = new StringReader();
        boolean running = true;
        while (running) {
            // Take each command and parse it
            String[] commandParts = input.read("Please enter a command: ").split(" ");
            // The command is the first part of the line
            String command = commandParts[0].toLowerCase();
            // The arguments are everything else
            String[] arguments = commandParts.length > 1 ? Arrays.copyOfRange(commandParts, 1, commandParts.length) : new String[0];

            switch (command) {
                case "get":
                    doGet(arguments);
                    break;
                case "delete":
                    doDelete(arguments);
                    break;
                case "put":
                    doPut(arguments);
                    break;
                case "list":
                    doList(arguments);
                    break;
                case "smallest":
                    doSmallest();
                    break;
                case "largest":
                    doLargest();
                    break;
                case "finish":
                    running = false;
                    break;
                default:
                    System.out.println("Unrecognized command " + command);
                    break;
            }
        }
    }

    private static void doGet(String[] args) {
        // Check that arg exists
        if (args.length < 1) {
            System.out.println("Command get requires 1 argument: a key to lookup.");
            return;
        }

        String word = args[0];

        // Lookup the audio-type key and play it if it exists
        Pair audioKey = new Pair(word, "audio");
        Record audioRecord = dict.get(audioKey);
        if (audioRecord != null) {
            // Play the sound or print the error it produces
            try {
                new SoundPlayer().play(audioRecord.getData());
            } catch (MultimediaException e) {
                System.out.println(e.getMessage());
            }
        }

        // Lookup the image-type key and show it if it exists
        Pair imageKey = new Pair(word, "image");
        Record imageRecord = dict.get(imageKey);
        if (imageRecord != null) {
            // Show the image or print the error it produces
            try {
                new PictureViewer().show(imageRecord.getData());
            } catch (MultimediaException e) {
                System.out.println(e.getMessage());
            }
        }

        // Lookup the text-type key and print it if it exists
        Pair textKey = new Pair(word, "text");
        Record textRecord = dict.get(textKey);
        if (textRecord != null) {
            System.out.println(textRecord.getData());
        }

        // If none of the keys existed, we need to print some info about nearby keys
        if (audioRecord == null && imageRecord == null && textRecord == null) {
            System.out.println("The word " + word + " is not in the dictionary.");
            Record predecessor = dict.predecessor(textKey);
            Record successor = dict.successor(textKey);
            System.out.println("Preceding word: " + (predecessor != null ? predecessor.getKey().getWord() : ""));
            System.out.println("Following word: " + (successor != null ? successor.getKey().getWord() : ""));
        }
    }

    private static void doDelete(String[] args) {
        if (args.length < 2) {
            System.out.println("Command delete requires 2 arguments: a word and a type to delete.");
            return;
        }

        // Create and remove the key
        Pair key = new Pair(args[0], args[1]);
        try {
            dict.remove(key);
        } catch (DictionaryException e) {
            // If it failed the key didn't exist
            System.out.println("No record in the dictionary has key (" + key.getWord() + ", " + key.getType() + ")");
        }
    }

    private static void doPut(String[] args) {
        if (args.length < 3) {
            System.out.println("Command put requires 3 arguments: a word, a type and data to put in the dictionary.");
            return;
        }

        // Create and insert the record
        // all of the remaining args from args[2] until args[max] become the data
        Record toInsert = new Record(new Pair(args[0], args[1]), String.join(" ", Arrays.copyOfRange(args, 2, args.length)));
        try {
            dict.put(toInsert);
        } catch (DictionaryException e) {
            // If it failed, the key already existed
            System.out.println("A record with the given key (" + toInsert.getKey().getWord() + ", " + toInsert.getKey().getType() + ") is already in the ordered dictionary.");
        }
    }

    private static void doList(String[] args) {
        if (args.length < 1) {
            System.out.println("Command put requires 1 argument: a prefix to lookup in the dictionary.");
            return;
        }

        // The smallest possible key in the dictionary that starts with the prefix
        // is the key (word, audio)
        String prefix = args[0].toLowerCase();
        Pair lookupKey = new Pair(prefix, "audio");

        // First check if that key exists in the dictionary
        Record lookupRecord = dict.get(lookupKey);
        if (lookupRecord != null) {
            System.out.println(prefix);
        }

        // Now we loop through all the further successors that still begin with the prefix
        // And print their word
        lookupRecord = dict.successor(lookupKey);
        while (lookupRecord != null && lookupRecord.getKey().getWord().startsWith(prefix)) {
            System.out.println(lookupRecord.getKey().getWord());
            lookupRecord = dict.successor(lookupRecord.getKey());
        }
    }

    private static void doSmallest() {
        // Find the smallest and print it if it exists
        Record smallest = dict.smallest();
        if (smallest != null) {
            System.out.println(smallest.getKey().getWord() + ", " + smallest.getKey().getType() + ", " + smallest.getData());
        } else {
            System.out.println("The dictionary is empty.");
        }
    }

    private static void doLargest() {
        // Find the largest and print it if it exists
        Record largest = dict.largest();
        if (largest != null) {
            System.out.println(largest.getKey().getWord() + ", " + largest.getKey().getType() + ", " + largest.getData());
        } else {
            System.out.println("The dictionary is empty.");
        }
    }

}
