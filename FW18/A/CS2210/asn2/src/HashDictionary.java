public class HashDictionary implements DictionaryADT {

    private class HashDictionaryNode {
        private Configuration value;
        private HashDictionaryNode next;

        HashDictionaryNode(Configuration value, HashDictionaryNode next) {
            this.value = value;
            this.next = next;
        }
    }

    private int size;
    private HashDictionaryNode[] scores;

    public HashDictionary(int size) {
        this.size = size;
        scores = new HashDictionaryNode[size];
    }

    public int put(Configuration pair) throws DictionaryException {
        int index = hash(pair.getStringConfiguration(), size);
        // The node to insert will always be the last node in the list at insertion time, so null next is suitable
        HashDictionaryNode nodeToInsert = new HashDictionaryNode(pair, null);

        if (scores[index] == null) { // Nothing exists there, insert
            scores[index] = nodeToInsert;
            return 0;
        } else { // Something already exists there, add to Linked List
            HashDictionaryNode current = scores[index];
            // Check for duplicate value
            if (current.value.getStringConfiguration().equals(pair.getStringConfiguration())) {
                throw new DictionaryException("Key already exists in HashDictionary");
            }

            while (current.next != null) { // Cycle to end of list
                current = current.next;
                // While cycling, make sure it doesn't already exist in the list
                // Have to check outside the loop as well since the first one is skipped
                if (current.value.getStringConfiguration().equals(pair.getStringConfiguration())) {
                    throw new DictionaryException("Key already exists in HashDictionary");
                }
            }

            current.next = nodeToInsert; // Insert and return
            return 1;
        }
    }

    public void remove(String config) throws DictionaryException {
        // Get index
        int index = hash(config, size);

        // Get start of linked list
        HashDictionaryNode current = scores[index];
        if (current == null)
            throw new DictionaryException("Key does not exist in HashDictionary");

        // If it's the first element in the list, it's different than modifying the middle of the list
        if (current.value.getStringConfiguration().equals(config)) {
            scores[index] = current.next;
            return;
        }

        // If it wasn't, go check the rest of the list
        HashDictionaryNode previous = current;
        current = current.next;
        while (current != null) {
            // If it's found, set previous pointer forward to skip current node
            if (current.value.getStringConfiguration().equals(config)) {
                previous.next = current.next;
                // Found it, return early
                return;
            }

            // If it isn't found, keep looping
            previous = current;
            current = current.next;
        }

        // If it hasn't been found yet, it never existed
        throw new DictionaryException("Key does not exist in HashDictionary");
    }

    public int getScore(String config) {
        // Get index
        int index = hash(config, size);

        // Get start of linked list
        HashDictionaryNode current = scores[index];
        // start cycling the linked list (or skip the loop if nothing was there)
        while (current != null) {
            // Check for equality and return if it's found
            if (current.value.getStringConfiguration().equals(config)) {
                return current.value.getScore();
            }
            // Otherwise go to next item in list
            current = current.next;
        }

        // If nothing has been found yet, it's not in the dictionary
        return -1;
    }

    private static int hash(String toHash, int modulo) {
        // Initial value of a large prime
        int rollingCounter = 433024253;
        // Pull bytes of string
        byte[] bytes = toHash.getBytes();

        // For each byte, XOR with current counter and multiply by another large (but constant) prime
        for (int i = 0; i < bytes.length; i++) {
            int magic1 = 613651369;
            rollingCounter = (rollingCounter ^ bytes[i]) * magic1;
        }

        // Make sure number is within 0 - modulo
        rollingCounter %= modulo;
        if (rollingCounter < 0)
            rollingCounter *= -1;

        // Return hash result
        return rollingCounter;
    }

}
