import java.util.Arrays;

public class BoardGame {

    private int boardSize;
    private int emptyPositions;
    private int maxLevels;

    private char[][] gameBoard;

    // Initializes required fields
    public BoardGame(int boardSize, int emptyPositions, int maxLevels) {
        this.boardSize = boardSize;
        this.emptyPositions = emptyPositions;
        this.maxLevels = maxLevels;

        gameBoard = new char[boardSize][boardSize];
        for (char[] row : gameBoard)
            Arrays.fill(row, 'g');
    }

    public HashDictionary makeDictionary() {
        return new HashDictionary(9887);
    }

    public int isRepeatedConfig(HashDictionary dict) {
        String state = generateStateString();
        return dict.getScore(state); // Returns -1 if it doesn't exist, no need to check for existence first
    }

    public void putConfig(HashDictionary dict, int score) {
        String state = generateStateString();
        try {
            dict.put(new Configuration(state, score)); // Insert score to dict
        } catch (DictionaryException e) {
            System.err.println("Score already exists in list.");
        }
    }

    public void savePlay(int row, int col, char symbol) {
        gameBoard[row][col] = symbol;
    }

    public boolean positionIsEmpty(int row, int col) {
        return gameBoard[row][col] == 'g';
    }

    public boolean tileOfComputer(int row, int col) {
        return gameBoard[row][col] == 'o';
    }

    public boolean tileOfHuman(int row, int col) {
        return gameBoard[row][col] == 'b';
    }

    public boolean wins(char symbol) {
        boolean diag = true; // Top left to bottom right
        boolean backDiag = true; // Top right to bottom left
        // Check diagonal
        for (int i = 0; i < boardSize; i++) {
            // diag will flip to false if any chars on the diagonal are not 'symbol'
            diag &= gameBoard[i][i] == symbol;
            backDiag &= gameBoard[i][(boardSize - 1) - i] == symbol;
        }
        if (diag || backDiag)
            return true;

        // Check rows and columns
        for (int i = 0; i < boardSize; i++) {
            boolean row = true;
            boolean col = true;
            for (int j = 0; j < boardSize; j++) {
                // Inverse lookup of row gives column
                row &= gameBoard[i][j] == symbol;
                col &= gameBoard[j][i] == symbol;
            }
            if (row || col)
                return true;
        }

        // If no win condition has been found yet, the 'symbol' player is not currently winning
        return false;
    }

    public boolean isDraw(char symbol, int emptyPositionsRequired) {
        // Make sure no one has already won
        if (wins('o') || wins('b'))
            return false;

        int emptyPositionCount = 0;

        // Check all tiles (to count for empty spaces, and to check for available moves)
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                // Count if empty
                if (gameBoard[i][j] == 'g')
                    emptyPositionCount++;

                // Check available moves
                if (gameBoard[i][j] == symbol) {
                    if (hasEmptyNeighbour(i, j))
                        return false;
                }
            }
        }

        // If there were no available sliding moves,
        // the remaining condition to check for draw is to check if there are any free spaces left
        // If there are still more than the required positions, it's not a draw
        // This condition works with 0 spaces required, or multiple spaces required
        return emptyPositionCount == emptyPositionsRequired;
    }

    // This method determines whether a piece has an adjacent empty space
    private boolean hasEmptyNeighbour(int row, int col) {
        for (int i = row - 1; i <= row + 1; i++) {
            for (int j = col - 1; j <= col + 1; j++) {
                // Check dimensions won't throw out of bounds exception before checking array
                if (i > 0 && i < boardSize) {
                    if (j > 0 && j < boardSize) {
                        if (gameBoard[i][j] == 'g') {
                            return true;
                        }
                    }
                }
            }
        }

        // If no neighbours found yet
        return false;
    }

    public int evalBoard(char symbol, int emptyPositions) {
        if (wins('o')) // Computer wins
            return 3;
        else if (wins('b')) // Human wins
            return 0;
        else if (isDraw(symbol, emptyPositions)) // Draw
            return 2;
        else // None of the above
            return 1;
    }

    private String generateStateString() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) { // Concatenate result as gameBoard[0][0] + gameBoard[1][0] + gameBoard[2][0] + ...
                result.append(gameBoard[j][i]);
            }
        }

        return result.toString();
    }

}
