public class DictionaryException extends Exception {

    private static final long serialVersionUID = 1L;

    public DictionaryException() {
        super();
    }

    public DictionaryException(String message) {
        super(message);
    }

}