/**
 * A holder class for necessary data in the MapTreeNode,
 * to determine whether the next node on an edge should be allowed.
 */
public class MapPathData {
    private GraphNode data;
    private int changes;
    private char enterRoute;

    public MapPathData(GraphNode data, int changes, char enterRoute) {
        this.data = data;
        this.changes = changes;
        this.enterRoute = enterRoute;
    }

    public GraphNode getData() {
        return data;
    }

    public int getChanges() {
        return changes;
    }

    public char getEnterRoute() {
        return enterRoute;
    }
}
