import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Iterator;

public class BusLines {

    private int maxChanges, startIndex, destIndex;
    private Graph graph;
    private boolean initialized;

    /**
     * Constructor for building a city map with its bus lines from the input file.
     *
     * @param inputFile The input file to read bus lines from
     * @throws MapException if the input file does not exist
     */
    public BusLines(String inputFile) throws MapException {
        try {
            Iterator<String> lines = Files.readAllLines(new File(inputFile).toPath()).iterator();

            // Read and parse metadata
            String metaLine = lines.next();
            String[] metaParts = metaLine.split(" ");
            int width = Integer.parseInt(metaParts[1]);
            int height = Integer.parseInt(metaParts[2]);
            this.maxChanges = Integer.parseInt(metaParts[3]);

            // Create empty graph
            graph = new Graph(width * height);

            // Counts rows passed
            int c = 0;
            while (lines.hasNext()) {
                String line1 = lines.next();
                String line2 = lines.hasNext() ? lines.next() : null; // This may be null if it is the bottom row
                for (int i = 0; i < line1.length(); i++) {
                    if (i % 2 == 0) {
                        // Check for startIndex or destination
                        char possibleStartOrDest = line1.charAt(i);
                        if (possibleStartOrDest == 'S')
                            startIndex = c * width + i / 2;
                        else if (possibleStartOrDest == 'D')
                            destIndex = c * width + i / 2;

                        if (line2 != null) {
                            // Check for bus route
                            char possibleRoute = line2.charAt(i);
                            if (possibleRoute != ' ') {
                                // Get nodes and create edge
                                GraphNode u = graph.getNode(c * width + i / 2); // Current node
                                GraphNode v = graph.getNode((c * width + i / 2) + width); // Row below current
                                graph.insertEdge(u, v, possibleRoute);
                            }
                        }
                    } else {
                        // Check for horizontal bus routes
                        char possibleRoute = line1.charAt(i);
                        if (possibleRoute != ' ') {
                            GraphNode u = graph.getNode(c * width + i / 2); // Current node (rounds down)
                            GraphNode v = graph.getNode((c * width + i / 2) + 1); // To right of current
                            graph.insertEdge(u, v, possibleRoute);
                        }
                    }
                }
                // Increment row counter
                c++;
            }
            // Mark that the graph was successfully created
            initialized = true;
        } catch (java.io.IOException e) {
            throw new MapException("Cannot create map from non-existent input file " + inputFile);
        } catch (GraphException e) {
            initialized = false;
        }
    }

    public Graph getGraph() throws MapException {
        if (!initialized)
            throw new MapException("Map could not be created from the file.");

        return graph;
    }

    /////////////////////////////////////////////////
    /*
        For this problem, I decided to implement an unbalanced tree for searching.
        The tree is implemented in files MapPathData.java and MapTreeNode.java
        The tree data structure allows for very easy backtracking
        while still keeping track of all the current possible paths.

        The algorithm starts by creating an empty tree,
        then adding the start graph node to it.
        Once there, it follows these steps:
            1. trip(N): - N is the "root" node of that subtree
                2. If N is the destination,
                    3. return a path with only it.
                4. Otherwise,
                    5. For each child C of N,
                        5. Add each unmarked child of C to the tree
                        6. Return the path given by tree(C), if one exists
                    7. Return null if no path was found
    */
    /////////////////////////////////////////////////

    public Iterator<GraphNode> trip() {
        try {
            // Add the starting node to the tree, and its children
            GraphNode start = graph.getNode(startIndex);
            start.setMarked(true);
            MapTreeNode startNode = new MapTreeNode(start.getName(), new MapPathData(start, 0, '*'));
            addChildrenOfNode(startNode);

            // Get the path found by children nodes (or null) and return that
            ArrayList<GraphNode> path = trip(startNode);
            if (path == null)
                return null;

            return path.iterator();
        } catch (GraphException e) {
            e.printStackTrace();
            return null;
        }
    }

    private ArrayList<GraphNode> trip(MapTreeNode root) throws GraphException {
        // Check if we are at the destination, and return a path stem if we are
        if (root.getName() == destIndex) {
            ArrayList<GraphNode> path = new ArrayList<>();
            path.add(root.getData().getData());
            return path;
        }

        // Otherwise continue checking children
        ArrayList<MapTreeNode> children = root.getChildren();
        for (MapTreeNode child : children) {
            addChildrenOfNode(child);
            ArrayList<GraphNode> path = trip(child);

            // Unmark the visited nodes if it wasn't successful
            if (path == null) {
                child.unmarkAllChildren();
            } else {
                // If it was successful, add the current node to the
                // start of the path, and continue returning up the call stack
                path.add(0, root.getData().getData());
                return path;
            }
        }
        // If we get here, no path was found :(
        return null;
    }

    private void addChildrenOfNode(MapTreeNode node) throws GraphException {
        Iterator<GraphEdge> edges = graph.incidentEdges(node.getData().getData());

        // If the given node has edges,
        if (edges != null) {
            edges.forEachRemaining(edge -> {
                // Get both nodes on the edge
                GraphNode endOne = edge.firstEndpoint();
                GraphNode endTwo = edge.secondEndpoint();

                // Determine whether this edge requires a bus route change
                char origRoute = node.getData().getEnterRoute();
                boolean routeChange = origRoute != '*' && origRoute != edge.getBusLine();
                int totalBusChanges = node.getData().getChanges();
                if (routeChange)
                    totalBusChanges += 1;

                // Then add the unmarked ones to the tree, if they are under the allowed bus changes.
                if (totalBusChanges <= maxChanges) {
                    if (!endOne.isMarked()) {
                        endOne.setMarked(true);
                        node.addChild(new MapPathData(endOne, totalBusChanges, edge.getBusLine()));
                    }
                    if (!endTwo.isMarked()) {
                        endTwo.setMarked(true);
                        node.addChild(new MapPathData(endTwo, totalBusChanges, edge.getBusLine()));
                    }
                }
            });
        }
    }

}
