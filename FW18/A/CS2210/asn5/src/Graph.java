import java.util.ArrayList;
import java.util.Iterator;

public class Graph implements GraphADT {

    private GraphEdge[][] matrix;
    private GraphNode[] nodes;
    private int size;

    public Graph(int n) {
        this.size = n;
        this.matrix = new GraphEdge[n][n];

        // Initialize nodes
        this.nodes = new GraphNode[n];
        for (int i = 0; i < n; i++) {
            nodes[i] = new GraphNode(i);
        }
    }

    /**
     * Adds to the graph an edge connecting the given vertices.
     * The type of the edge is as indicated. Throws a GraphException
     * if either node does not exist or if the edge is already
     * in the graph.
     */
    public void insertEdge(GraphNode nodeu, GraphNode nodev, char busLine) throws GraphException {
        // Check nodes are in graph
        checkRange(nodeu.getName(), nodev.getName());

        // Check bus route doesn't already exist
        if (matrix[nodeu.getName()][nodev.getName()] != null)
            throw new GraphException("Nodes " + nodeu.getName() + " and " + nodev.getName() + " already have an edge.");

        // Create edge and add it to both indices of the matrix
        GraphEdge edge = new GraphEdge(nodeu, nodev, busLine);
        matrix[nodeu.getName()][nodev.getName()] = edge;
        matrix[nodev.getName()][nodeu.getName()] = edge;
    }

    /**
     * Returns the node with the specified name. Throws a
     * GraphException if the node does not exist.
     */
    public GraphNode getNode(int name) throws GraphException {
        // Check node exists
        checkRange(name);

        // Return node
        return nodes[name];
    }

    /**
     * Returns a Java Iterator storing all the edges incident
     * on the specified node. It returns null if the node does
     * not have any edges incident on it. Throws a GraphException
     * if the node does not exist.
     */
    public Iterator<GraphEdge> incidentEdges(GraphNode u) throws GraphException {
        // Check node exists
        checkRange(u.getName());

        // Accumulate edges
        ArrayList<GraphEdge> edges = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            if (matrix[u.getName()][i] != null)
                edges.add(matrix[u.getName()][i]);
        }

        // If there are edges, return the iterator of edges, otherwise null
        if (edges.size() > 0)
            return edges.iterator();
        else
            return null;
    }

    /**
     * Returns the edge connecting the given vertices. Throws
     * a GraphException if there is no edge conencting the given
     * vertices or if u or v do not exist.
     */
    public GraphEdge getEdge(GraphNode u, GraphNode v) throws GraphException {
        // Check nodes are in graph
        checkRange(u.getName(), v.getName());

        // Check edge exists
        if (matrix[u.getName()][v.getName()] == null)
            throw new GraphException("No edge exists between " + u.getName() + " and " + v.getName());

        // Return edge
        return matrix[u.getName()][v.getName()];
    }

    /**
     * Returns true is u and v are adjacent, and false otherwise.
     * It throws a GraphException if either vertex does not
     * exist.
     */
    public boolean areAdjacent(GraphNode u, GraphNode v) throws GraphException {
        // Check nodes exist
        checkRange(u.getName(), v.getName());

        // Return whether the edge exists
        return matrix[u.getName()][v.getName()] != null;
    }

    /**
     * Throws a GraphException if any of the provided nodes are not in the graph,
     * otherwise does nothing
     * @param nodeNames The nodes to check for validity
     * @throws GraphException if any node is not in the graph
     */
    private void checkRange(int... nodeNames) throws GraphException {
        for (int u : nodeNames)
            if (u >= size)
                throw new GraphException("Node " + u + " does not exist in the graph.");
    }

}
