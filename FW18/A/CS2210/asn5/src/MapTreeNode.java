import java.util.ArrayList;

/**
 * This tree is used to keep track of backtracking and search patterns
 * during the path finding algorithm.
 * Once a path is found,
 * stepping back up the tree shows the path taken.
 */
public class MapTreeNode {
    private int name;
    private MapPathData data;
    private ArrayList<MapTreeNode> children;

    public MapTreeNode(int name, MapPathData data) {
        this.name = name;
        this.data = data;
        this.children = new ArrayList<>();
    }

    public int getName() {
        return name;
    }

    public MapPathData getData() {
        return data;
    }

    public ArrayList<MapTreeNode> getChildren() {
        return children;
    }

    public void addChild(MapPathData childData) {
        MapTreeNode child = new MapTreeNode(childData.getData().getName(), childData);
        children.add(child);
    }

    /**
     * Unmarks all children of the current node,
     * used when the search tree is backtracking.
     */
    public void unmarkAllChildren() {
        unmarkAllChildren(this);
    }

    /**
     * Recursively unmarks all the children visited,
     * used when the search tree is backtracking.
     *
     * @param root The "root" of the current subtree
     */
    private static void unmarkAllChildren(MapTreeNode root) {
        for (MapTreeNode child : root.children) {
            unmarkAllChildren(child);
        }
        root.getData().getData().setMarked(false);
    }
}
