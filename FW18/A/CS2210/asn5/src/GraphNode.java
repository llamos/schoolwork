public class GraphNode {

    private int name;
    private boolean marked;

    public GraphNode(int name) {
        this.name = name;
    }

    public int getName() {
        return name;
    }

    public boolean isMarked() {
        return marked;
    }

    public void setMarked(boolean marked) {
        this.marked = marked;
    }


}
