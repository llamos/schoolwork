% A match is found if the list contains only one item, 
% and that item is the item we expect to find at the end of the list
% (or is a variable to be unified)
last([LastItem], LastItem).

% Otherwise, the function trims the top element off the list,
% and tries again recursively
last([_|Tail], LastItem) :- last(Tail, LastItem).