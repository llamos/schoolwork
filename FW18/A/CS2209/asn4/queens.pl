% N-Form of Queens problem, that attempts to find a solution to the N-Queens 
% problem at N
nQueens(Board, N) :- 
    numlist(1, N, X), % Generate [1...N]
    % Ensure the board is a permutation of [1...N] (has no duplicates)
    permutation(X, Board),
    checkDiagonals(Board).

% An empty array means all diagonals have been checked (base case)
checkDiagonals([]).
% Takes the board and runs checkDiagonals on the head element, 
% for each further index,
% as well as checking the next index's diagonals
checkDiagonals([Head|Tail]) :-
    checkDiagonals(Head, Tail, 1),
    checkDiagonals(Tail).

% Similarly, an empty array means all diagonals of the 
% current index have been checked
checkDiagonals(_, [], _).
% Declarative implementation of 
% for each i from 1 to N
%     for each j from i to N
%         Board[i+j] can't equal Board[i] +- j
checkDiagonals(LookFor, [NextValue|RemainingBoard], Index) :-
    CantMatchDown is LookFor + Index, % Looks up and to the right
    CantMatchUp is LookFor - Index, % Looks down and to the right
    NextValue \== CantMatchDown,
    NextValue \== CantMatchUp,
    NewIndex is Index + 1, % Iterates index, and loops again at next "j"
    checkDiagonals(LookFor, RemainingBoard, NewIndex).