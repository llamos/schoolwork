#! /bin/bash

# Identify key dirs
ROOTDIR="$(pwd)"
EXPORTDIR="$ROOTDIR/public"

# Generate export dir
mkdir -p "$EXPORTDIR"

# Find all applicable tex files
for f in $(find . -name "asn*.tex" -o -name "notes.tex" -o -name "Textbook.pdf" -o -name "midterm.tex") 
do
    # Determine file pathing info
    PARENT=$(dirname "$f")
    FILENAME=$(basename -- "$f")
    
    # Open containing folder
    cd "$PARENT"

    # If it's a tex file, build it
    if [[ ${FILENAME##*.} == "tex" ]]
    then
        echo -n "Building and copying $PARENT/$FILENAME.... "
        latexmk -pdf $FILENAME 2&> /dev/null
    else
        echo -n "Copying $PARENT/$FILENAME.... "
    fi

    # Copy to output
    mkdir -p "$EXPORTDIR/$PARENT/"
    cp "${FILENAME%.*}.pdf" "$EXPORTDIR/$PARENT/${FILENAME%.*}.pdf"
    echo "Done!"

    # return to workspace main
    cd "$ROOTDIR"

done

# Enter the public folder
cd "$EXPORTDIR"

# Stub index
echo -n "Generating HTML.... "
echo "<!DOCTYPE HTML>" > index.html
echo "<html><head><title>Paul's Schoolwork</title></head><body><a href=\"https://gitlab.com/llamos/schoolwork\">View the Repository Here</a>" >> index.html

for term in "FW20" "FW19" "FW18" "SU18"
do
    # Open that term
    cd $term
    echo "<h1>$term</h1><ul>" >> $EXPORTDIR/index.html

    # Find all applicable PDFs
    COURSE=""
    for pdf in $(find . -name "asn*.pdf" -o -name "notes.pdf" -o -name "Textbook.pdf" -o -name "midterm.pdf" | sort -r)
    do
        # Determine the course
        CURRCOURSE="$(dirname $pdf)"
        if [[ "$(dirname $CURRCOURSE)" != "./A" && "$(dirname $CURRCOURSE)" != "./B" && "$(dirname $CURRCOURSE)" != "." ]]
        then
            CURRCOURSE="$(dirname $CURRCOURSE)"
        fi

        # Determine the term
        TERM="$(basename -- $(dirname $CURRCOURSE))"
        if [[ $TERM != "." ]]
        then
            CURRCOURSE="$(basename -- $CURRCOURSE)$TERM"
        else
            CURRCOURSE="$(basename -- $CURRCOURSE)"
        fi

        # Start a new course header if needed
        if [[ $CURRCOURSE != $COURSE ]]
        then
            echo "</ul><h3>$CURRCOURSE</h3><ul>" >> $EXPORTDIR/index.html
            COURSE="$CURRCOURSE"
        fi

        # Add the file entry to the list
        FILENAME="$(basename -- "$pdf")"
        STRIPPED=${FILENAME%.*}
        NICET=${STRIPPED//asn/A}
        NICEU=${NICET//notes/Notes}
        NICE=${NICEU//midterm/Midterm}
        echo "<li><a href=\"$term/$pdf\">$CURRCOURSE-$NICE</a></li>" >> $EXPORTDIR/index.html
    done

    # Close the term
    echo "</ul>" >> $EXPORTDIR/index.html
    cd "$EXPORTDIR"
done

# Close off index
echo "</body></html>" >> index.html
echo "Done!"
