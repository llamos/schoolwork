%!TEX root = Assignment1.tex

\begin{problem}
	\begin{problemTitle}
		Let $C$ be the polar curve defined by $r=1+2\sin(2\theta)$.
		\begin{enumerate}
			\item Sketch $C$.
			\item Find the area of a big loop of $C$.
			\item Find the arc length of a small loop of $C$.
			\item Let $P$ be the farthest point of $C$ in the first quadrant from the origin. Write the equation of the tangent to $C$ at $P$.
		\end{enumerate}
	\end{problemTitle}

	\begin{enumerate}
		\item
		      To graph this polar equation,
		      I first graphed $r=1+2\sin(2\theta)$ on a cartesian grid:

		      \begin{center}
			      \begin{tikzpicture}
				      \draw [->,thick] (0,0) -- (12,0) node [right] {$\theta$};
				      \draw [->,thick] (0,-1) -- (0,2) node [above] {$r$};
				      \foreach \p in {0,...,24} {
						      \draw [lightgray] (\p/2, -1) -- (\p/2, 2);
					      }
				      \foreach \p in {-2,...,4} {
						      \draw [lightgray] (0, \p/2) -- (12, \p/2);
						      \node at (-0.5,\p/2) {\scriptsize \p};
					      }

				      \foreach \p/\l in {
						      0/0,
						      1.5/$\frac{\pi}{4}$,
						      3/$\frac{\pi}{2}$,
						      4.5/$\frac{3\pi}{4}$,
						      6/$\pi$,
						      7.5/$\frac{5\pi}{4}$,
						      9/$\frac{3\pi}{2}$,
						      10.5/$\frac{7\pi}{4}$,
						      12/$2\pi$,
					      } {
						      \node at (\p, -1.5) {\scriptsize \l};
						      \draw [gray] (\p, -1) -- (\p, 2);
					      }

				      \draw [thick,samples=200,smooth,domain=0:24] plot ({\x/2}, {(1+2*sin(2*pi*\x/12 r))/2});

				      \foreach \x/\l in {
						      0/A,
						      3/P,
						      7/B,
						      9/C,
						      11/D,
						      15/E,
						      19/F,
						      21/G,
						      23/H
					      } {
						      \node [circle,draw,fill=black,scale=0.5] at ({\x/2}, {(1+2*sin(2*pi*\x/12 r))/2}) {};
						      \node [draw,fill=white] at ({\x/2}, {(2+2*sin(2*pi*\x/12 r))/2}) {\l};
					      }
			      \end{tikzpicture}
		      \end{center}

		      I used the plot above to graph the equation in polar coordinates:

		      \begin{center}
			      \begin{tikzpicture}
				      \draw [style=double] (0,0) circle (4);
				      \foreach \p in {0,...,24} {
						      \draw [lightgray] (0:0) -- (\p*pi/12 r:4);
					      }
				      \foreach \r/\l/\pos in {
				      0 r/0/{right},
				      {pi/4 r}/$\frac{\pi}{4}$/{above right},
				      {pi/2 r}/$\frac{\pi}{2}$/{above},
				      {3*pi/4 r}/$\frac{3\pi}{4}$/{above left},
				      {pi r}/$\pi$/{left},
				      {5*pi/4 r}/$\frac{5\pi}{4}$/{below left},
				      {3*pi/2 r}/$\frac{3\pi}{2}$/{below},
				      {7*pi/4 r}/$\frac{7\pi}{4}$/{below right}}
				      {
				      \draw (0,0) -- (\r:4);
				      \node at (\r:4.1) [\pos] {\scriptsize \l};
				      }
				      \foreach \r in {0,...,3} {
						      \draw (0,0) circle (\r);
						      \draw [lightgray] (0,0) circle (\r+0.5);
						      \node [fill=white] at (\r,-0.3) {\scriptsize \r};
					      }

				      \draw [thick,smooth,samples=200,domain=0:2*pi] plot (xy polar cs:angle=\x r, radius={1+2*sin(2*\x r)});

				      \foreach \x/\l in {
				      0/A,
				      3/P,
				      7/B,
				      9/C,
				      11/D,
				      15/E,
				      19/F,
				      21/G,
				      23/H
				      } {
				      \node [circle,draw,fill=black,scale=0.5] at (\x*pi/12 r:{1+2*sin(2*pi*\x/12 r)}) {};
				      \node [draw,fill=white,above] at (\x*pi/12 r:{(1+2*sin(2*pi*\x/12 r)}) {\l};
				      }
			      \end{tikzpicture}
		      \end{center}

		      For the rest of the solution, I will use the following graph:

		      \begin{center}
			      \begin{tikzpicture}
				      % Draw the lines at multiples of pi/12
				      \foreach \ang in {0,...,23} {
						      \draw [lightgray] (0,0) -- (\ang * 180 / 12:4);
					      }

				      % Concentric circles and radius labels
				      \foreach \s in {0, 1, 2, 3} {
						      \draw [lightgray] (0,0) circle (\s + 0.5);
						      \draw (0,0) circle (\s);
						      \node [fill=white] at (\s, 0) [below] {\scriptsize $\s$};
					      }

				      \node [fill=white] at (pi/12 r:4.1) [right] {\scriptsize $\pi/12$};

				      % Add the labels at multiples of pi/4
				      \foreach \ang/\lab/\dir in {
				      0/0/right,
				      1/{\pi/4}/{above right},
				      2/{\pi/2}/above,
				      3/{3\pi/4}/{above left},
				      4/{\pi}/left,
				      5/{5\pi/4}/{below left},
				      7/{7\pi/4}/{below right},
				      6/{3\pi/2}/below} {
				      \draw (0,0) -- (\ang * 180 / 4:4.1);
				      \node [fill=white] at (\ang * 180 / 4:4.2) [\dir] {\scriptsize $\lab$};
				      }
				      \draw [style=double] (0,0) circle (4);

				      \fill [fill=red!50!black, opacity=0.5, smooth, samples=200] plot [domain=-pi/12:7*pi/12] (xy polar cs:angle=\x r, radius={1+2*sin(2*\x r)});
				      \draw [thick, color=red, domain=0:2*pi, samples=200, smooth] plot (xy polar cs:angle=\x r, radius={1+2*sin(2*\x r)});
				      \draw [thick, color=blue, domain=7*pi/12:11*pi/12, samples=200, smooth] plot (xy polar cs:angle=\x r, radius={1+2*sin(2*\x r)});

				      \foreach \letter/\theta/\r/\pos in {
				      A/{0 r}/1/{above},
				      P/{pi/4 r}/3/{above},
				      /{7*pi/12 r}/0/{above right},
				      C/{3*pi/4 r}/-1/{right},
				      % D/{11*pi/12 r}/0,
				      E/{5*pi/4 r}/3/{below},
				      % F/{19*pi/12 r}/0,
				      G/{7*pi/4 r}/-1/{left}
				      % H/{23*pi/12 r}/0
				      } {
				      \node [label={\pos:\letter},circle,draw,fill=black,scale=0.5](\letter) at (\theta:\r) {};
				      }

				      % Alpha label
				      \node [draw,fill=white](ALabel) at (5*pi/12 r:3) {$\alpha$};
				      \draw [color=black] (ALabel) -- (7*pi/24 r:1.75);

				      % Gamma label
				      \node [draw,fill=white](GLabel) at (-pi/12 r:1.75) {$\gamma$};
				      \draw [color=black] (GLabel) -- (11*pi/6 r:0.75);

				      %Tangent to P
				      \draw[<->,dotted,thick] (0.75,3.5) -- (3.5,0.75) node[right] {t};

				      % Metatext
				      \node [fill=white,draw] at (-7.25,3.1) {$r=1+2\sin(2\theta)$};
				      \node [fill=white] at (-7.25,2.5)      {\small{A is the "start" of the graph at $\theta=0$.}};
				      \node [fill=white] at (-7.25,2)      {\small{The origin contains points B, D, F and H.}};
				      \node [fill=white] at (-7.25,1.5)        {\small{$t$ is the tangent line to P.}};
				      \node [fill=white] at (-7.25,1)      {\small{$\alpha$ is the shaded area enclosed by H and B.}};
				      \node [fill=white] at (-7.25,0.5)        {\small{The blue segment is the curve from B to D,}};
				      \node [fill=white] at (-7.25,0)      {\small{labeled as $\gamma$.}};
			      \end{tikzpicture}
		      \end{center}
		\item
		      From the graph above, the area of a big loop of $C$ is $\alpha$,
		      from H to B.

		      \begin{align*}
			      \shortintertext{By the area formula of polar curves,}
			      \alpha & =\frac{1}{2}\int_H^B \frac{1}{2}r^2d\theta                                                                                                                                                                                                                                       \\
			             & =\frac{1}{2}\int_{-\pi/12}^{7\pi/12}(1+2\sin(2\theta))^2d\theta                                                                                                                                                                                                                  \\
			      \shortintertext{Starting with an indefinite integral to keep notation shorter, let $u=2\theta$, $du=\frac{d\theta}{2}$}
			             & =\frac{1}{4}\int(1+2\sin(u))^2du                                                                                                                                                                                                                                                 \\
			             & =\frac{1}{4}\int(4\sin^2(u)+4\sin(u)+1)du                                                                                                                                                                                                                                        \\
			             & =\frac{1}{4}(4\int\sin^2(u)du+4\int\sin(u)du+\int du)                                                                                                                                                                                                                            \\
			             & =\frac{1}{4}(4\int\frac{1-\cos(2u)}{2}du-4\cos(u)+u + C)                                                                                                                                                                                                                         \\
			             & =\frac{1}{4}(2\int(1-\cos(2u))du-4\cos(u)+u + C)                                                                                                                                                                                                                                 \\
			             & =\frac{1}{4}(2\int du-2\int\cos(2u)du-4\cos(u)+u + C)                                                                                                                                                                                                                            \\
			             & =\frac{1}{4}(2u-\sin(2u)-4\cos(u)+u + C)                                                                                                                                                                                                                                         \\
			             & =\frac{-\sin(2u)}{4}-\cos(u)+\frac{3u}{4} + C                                                                                                                                                                                                                                    \\
			      \shortintertext{Substituting back to $\theta$,}
			             & =\frac{-\sin(4\theta)}{4}-\cos(2\theta)+\frac{3\theta}{2}+C                                                                                                                                                                                                                      \\
			      \shortintertext{And evaluating over the bounded area's domain,}
			             & =\left.\left(\frac{-\sin(4\theta)}{4}-\cos(2\theta)+\frac{3\theta}{2}\right)\right|_{-\pi/12}^{7\pi/12}                                                                                                                                                                          \\
			             & =\left(\frac{-\sin\left(4(\frac{-\pi}{12})\right)}{4}-\cos\left(2\left(\frac{-\pi}{12}\right)\right)+\frac{3(\frac{-\pi}{12})}{2}\right)-\left(\frac{-\sin\left(4(\frac{7\pi}{12})\right)}{4}-\cos\left(2\left(\frac{7\pi}{12}\right)\right)+\frac{3(\frac{7\pi}{12})}{2}\right) \\
			             & =\frac{-\sin\left(\frac{-\pi}{3}\right)}{4}-\cos\left(\frac{-\pi}{6}\right)+\frac{-\pi}{8}+\frac{\sin(\frac{7\pi}{3})}{4}+\cos\left(\frac{7\pi}{6}\right)+\frac{7\pi}{8}                                                                                                         \\
			             & =\frac{-\sin\left(\frac{5\pi}{3}\right)}{4}-\cos\left(\frac{11\pi}{6}\right)+\frac{\sin(\frac{\pi}{3})}{4}+\cos\left(\frac{7\pi}{6}\right)+\frac{3\pi}{4}                                                                                                                        \\
			             & =\frac{-(-\frac{\sqrt 3}{2})}{4}-\frac{\sqrt 3}{2}+\frac{\frac{\sqrt 3}{2}}{4}-\frac{\sqrt 3}{2}+\frac{3\pi}{4}                                                                                                                                                                  \\
			             & =\frac{\sqrt 3}{4}-\frac{4\sqrt 3}{4}+\frac{3\pi}{4}                                                                                                                                                                                                                             \\
			             & =\frac{-3(\sqrt 3+\pi)}{4}
		      \end{align*}
		      Thus, the area $\alpha$ from $H$ to $B$ is exactly $\frac{-3(\sqrt 3+\pi)}{4}$.
		\item From the graph above, the arc of a small loop of $C$ is $\gamma$ from $B$ to $D$.
		      \begin{align*}
			      \shortintertext{By the arc length of a polar curve,}
			      \gamma & = \int_B^D \sqrt{ r^2 + \left(\frac{dr}{d\theta}\right)^2} \quad d\theta                                           \\
			             & = \int_{7\pi/12}^{11\pi/12} \sqrt{ (1 + 2\sin(2\theta))^2 + (\frac{d}{d\theta}(1+2\sin(2\theta))^2 } \quad d\theta \\
			             & = \int_{7\pi/12}^{11\pi/12} \sqrt{ (4\sin^2(2\theta) + 4\sin(2\theta) + 1) + (4\cos(2\theta))^2 } \quad d\theta    \\
			             & = \int_{7\pi/12}^{11\pi/12} \sqrt{ 4\sin^2(2\theta) + 4\sin(2\theta) + 1 + 16\cos^2(2\theta) } \quad d\theta       \\
			             & = \int_{7\pi/12}^{11\pi/12} \sqrt{ 4\sin^2(2\theta) + 4\sin(2\theta) + 1 + 16(1 - \sin^2(2\theta)) } \quad d\theta \\
			             & = \int_{7\pi/12}^{11\pi/12} \sqrt{ -12\sin^2(2\theta) + 4\sin(2\theta) + 17 } \quad d\theta                        \\
		      \end{align*}
		      Thus, the arc length of a small loop of $C$ is $\int_{7\pi/12}^{11\pi/12} \sqrt{ -12\sin^2(2\theta) + 4\sin(2\theta) + 17 } \quad d\theta$.
		\item From the graph above, $P$ is labeled at $(\pi/4, 3)$.

		      The tangent line to P is labeled $t$ and can be defined as $y-y_P=\frac{dy}{dx}(x-x_P)$,
		      \begin{align*}
			      x                  & = r\cos(\theta)                                                             & y                  & = r\sin(\theta)                                                             \\
			                         & = (1+2\sin(2\theta))\cos(\theta)                                            &                    & = (1+2\sin(2\theta))\sin(\theta)                                            \\
			      \frac{dx}{d\theta} & = \frac{d}{d\theta}(\cos(\theta)+2\sin(2\theta)\cos(\theta))                & \frac{dy}{d\theta} & = \frac{d}{d\theta}(\sin(\theta)+2\sin(2\theta)\sin(\theta))                \\
			                         & = \frac{d}{d\theta}\cos(\theta)+\frac{d}{d\theta}2\sin(2\theta)\cos(\theta) &                    & = \frac{d}{d\theta}\sin(\theta)+\frac{d}{d\theta}2\sin(2\theta)\sin(\theta) \\
			                         & = -\sin(\theta) + 4\cos(2\theta)\cos(\theta) - 2\sin(2\theta)\sin(\theta)   &                    & = \cos(\theta) + 4\cos(2\theta)\sin(\theta) + 2\sin(2\theta)\cos(\theta)    \\
		      \end{align*}
		      \begin{align*}
			      \frac{dy}{dx} & = \frac{\frac{dy}{d\theta}}{\frac{dx}{d\theta}}                                                                                                          \\
			                    & = \frac{\cos(\theta) + 4\cos(2\theta)\sin(\theta) + 2\sin(2\theta)\cos(\theta)}{-\sin(\theta) + 4\cos(2\theta)\cos(\theta) - 2\sin(2\theta)\sin(\theta)} \\
			                    & = \frac{\cos(\theta)(1+2\sin(2\theta))+4\cos(2\theta)\sin(\theta)}{-\sin(\theta)(1+2\sin(2\theta))+4\cos(2\theta)\cos(\theta)}                           \\
			                    & = \frac{x+4\cos(2\theta)\sin(\theta)}{-y+4\cos(2\theta)\cos(\theta)}                                                                                     \\
		      \end{align*}
		      \begin{align*}
			      \shortintertext{At $P$, $\theta=\pi/4$, so}
			      x_P & =(1 + 2\sin(2(\pi/4)))\cos(\pi/4)    & y_P & =(1 +2\sin(2(\pi/4)))\sin(\pi/4)      \\
			          & =(1 + 2\sin(\pi/2))\frac{1}{\sqrt 2} &     & =(1 + 2\sin(2\pi/2))\frac{1}{\sqrt 2} \\
			          & =(1 + 2(1))\frac{1}{\sqrt 2}         &     & =(1 + 2(1))\frac{1}{\sqrt 2}          \\
			          & =\frac{3}{\sqrt 2}                   &     & =\frac{3}{\sqrt 2}                    \\
		      \end{align*}
		      \begin{align*}
			      \frac{dy}{dx}         & = \frac{\frac{3}{\sqrt 2} + 4\cos(2\pi/4)\sin(\pi/4)} {-\frac{3}{\sqrt 2} + 4\cos(2\pi/4)\cos(\pi/4)} \\
			                            & = \frac{\frac{3}{\sqrt 2} + 4(0)(\frac{1}{\sqrt 2})} {-\frac{3}{\sqrt 2} + 4(0)(\frac{1}{\sqrt 2})}   \\
			                            & = \frac{\frac{3}{\sqrt 2}}{-\frac{3}{\sqrt 2}}                                                        \\
			                            & = -1                                                                                                  \\
			      \shortintertext{Plugging all these values into $y-y_P=\frac{dy}{dx}(x-x_P)$, the equation for $t$ is}
			      y - \frac{3}{\sqrt 2} & = -1(x - \frac{3}{\sqrt 2})                                                                           \\
			      y                     & = -x + \frac{6}{\sqrt 2}
		      \end{align*}
		      Thus, the equation of the tangent to P is $y = -x + \frac{6}{\sqrt 2}$.

	\end{enumerate}
\end{problem}