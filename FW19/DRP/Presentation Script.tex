\documentclass{article}

\usepackage{tikz}

\usepackage{enumitem}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{mathtools}

\usepackage{parskip}
\setlength{\parskip}{2mm}
\setlength{\parindent}{0pt}

\newcommand{\mangle}[1]{\langle #1 \rangle}

\begin{document}

\textit{Skipping the axioms....}

\section{If necessary, duality}

Within categories is the notion of duality.
That is, for any category \textbf{$C$},
there is an opposite category \textbf{$C^{op}$},
given by reversing all the arrows within \textbf{$C$}.

\textit{Use terminology ``have this diagram'' not ``category represented by''.}

\begin{center}
	\begin{tikzpicture}
		\node at (1,0.5) {$C$};
		\node (A) at (0,0) {$A$};
		\node (B) at (2,0) {$B$};
		\node (C) at (1,-1) {$C$};
		\path[draw,->] (A) edge (B)
		(C) edge (A)
		(C) edge (B);
		
		\node at (5,0.5) {$C^{op}$};
		\node (AO) at (4,0) {$A$};
		\node (BO) at (6,0) {$B$};
		\node (CO) at (5,-1) {$C$};
		\path[draw,->] (BO) edge (AO)
		(AO) edge (CO)
		(BO) edge (CO);
	\end{tikzpicture}
\end{center}

This allows us to work within category theory,
and somewhat double our findings.
If we can demonstrate a theorem true in \textbf{$C$},
we are given a dual theorem in \textbf{$C^{op}$}.

\section{Products}

Products within categories are defined by the following properties:

Suppose we have a family of objects $\{A_i\} \subseteq \text{Obj}(\textbf{C})$.
An object $P$,
with projections $\pi_i : P \rightarrow A_i$ 
is a product if
for any other object $X \in \textbf{C}$,
with mappings $x_i : X \rightarrow A_i$,
it induces a unique mapping $u : X \rightarrow P$
that commutes in the following diagram:

\begin{center}
	\begin{tikzpicture}
		\node (Prod) at (0,0) {$P/\prod_I A_i$};
		\node (Ai) at (-2,0) {$A_i$};
		\node (X) at (0,-2) {$X$};
		
		\path[draw,->] (Prod) edge node[above] {$\pi_i$} (Ai)
		(X) edge node[left] {$x_i$} (Ai)
		(X) edge[dashed] node[right] {$\exists!u$} (Prod);
	\end{tikzpicture}
\end{center}

Since products can be shown unique up to isomorphism,
we can also denote $P := \prod_I A_i$
This induction of a unique $u$
is called the \underline{universal property of products}.
It is very helpful in proving things within category theory.

\pagebreak

\subsection{Example in \textbf{Sets}}

In \textbf{Sets}, 
the concept of products matches that of cartesian products.
Given two sets $A,B$,
we want to show that $A \times B$
holds this universal property of products.
To begin, we'll find projections.
Here, we use the left and right projections:
\begin{align*}
	\pi_A : A \times B & \rightarrow A & \pi_B : A \times B & \rightarrow B \\
	(a,b)              & \mapsto a     & (a,b)              & \mapsto b     
\end{align*}

Then, given any other set $X \in \textbf{C}$,
with mappings $x_A : X \rightarrow A$ and $x_B : X \rightarrow B$,
we want to find a unique mapping $u : X \rightarrow A \times B$.
This can be given by $p \mapsto (x_A(p), x_B(p))$.

\begin{center}
	\begin{tikzpicture}
		\node (Prod) at (0,0) {$A \times B$};
		\node (A) at (-2,0) {$A$};
		\node (B) at (2,0) {$B$};
		\node (X) at (0,-2) {$X$};
		
		\path[draw,->] (Prod) edge node[above] {$\pi_A$} (A)
		(Prod) edge node[above] {$\pi_B$} (B)
		(X) edge node[left] {$x_A$} (A)
		(X) edge node[right] {$x_B$} (B)
		(X) edge[dashed] node[right] {$\exists!u$} (Prod);
	\end{tikzpicture}
\end{center}

It's easy to see that this diagram commutes,
since $\pi_A \circ u = x_A$ and $\pi_B \circ u = x_B$.

If you have studied groups,
you can also see that this applies to products of groups,
by a similar composition with groups and homomorphisms,
instead of sets and functions.

\subsection{Example in Posets}

For another example, I will use the poset $(\mathbb{Z}, \leq)$ as a category.
Within this category, the objects are the elements of $\mathbb{Z}$,
and an arrow $a \rightarrow b$ exists iff $a \leq b$.

In posets,
the concept of products matches that of the greatest lower bound.
With values $10$ and $15$ we get the following diagram:

\begin{center}
	\begin{tikzpicture}
		\node (Prod) at (0,0) {$10 \wedge 15 = 10$};
		\node (10) at (-4,0) {$10$};
		\node (15) at (4,0) {$15$};
		\node (5) at (0,-4) {$5/x$};
		
		\path[draw,->] (Prod) edge node[above] {$10 \leq 10$} (10)
		(Prod) edge node[above] {$10 \leq 15$} (15)
		(5) edge node[left] {$5/x \leq 10$} (10)
		(5) edge node[right] {$5/x \leq 15$} (15)
		(5) edge[dashed] node[right] {$5/x \leq 10$} (Prod);
	\end{tikzpicture}
\end{center}

Note that the two projections 
combined with the mappings from 5 to 10 and 5 to 15,
imply that 5 is less than their greatest lower bound.
More aptly, realize that you can replace 5 with $x$,
and for any $x \leq 10$ and $x \leq 15$, $x \leq 10 \wedge 15$.
A poset will only have products if it has a greatest lower bound.

This may seem very obvious, 
but it shows that these two concepts 
(set products and greatest lower bound in posets)
are actually very similar.
If we can demonstrate that a concept applies to all category-products,
then it applies to both sets and posets,
in their respective product forms.

\section{Coproducts}

If you'll recall the concept of duality,
you may consider what happens when we reverse the arrows of a product.
Such a concept is called a coproduct.

In the general form,
with again a family of objects $\{A_i\} \subseteq \text{Obj}(\textbf{C})$,
a coproduct is an object $C$,
and inclusions $j_i : A_i \rightarrow C$ such that
for any other object $X \in \textbf{C}$,
and mappings $x_i : A_i \rightarrow X$,
it induces a unique mapping $u : C \rightarrow X$
and the following diagram commutes:

\begin{center}
	\begin{tikzpicture}
		\node (Prod) at (0,0) {$C/\amalg_I A_i$};
		\node (Ai) at (-2,0) {$A_i$};
		\node (X) at (0,-2) {$X$};
		
		\path[draw,->] (Ai) edge node[above] {$j_i$} (Prod)
		(Ai) edge node[left] {$x_i$} (X)
		(Prod) edge[dashed] node[right] {$\exists!u$} (X);
	\end{tikzpicture}
\end{center}

Again, since coproducts can be shown unique up to isomorphism,
we can now denote $C := \amalg_I A_i$.
Note that this diagram is the opposite of a product:

\begin{center}
	\begin{tikzpicture}
		\node (Prod) at (0,0) {$C/\amalg_I A_i$};
		\node (Ai) at (-2,0) {$A_i$};
		\node (X) at (0,-2) {$X$};
		
		\path[draw,->] (Ai) edge node[above] {$j_i$} (Prod)
		(Ai) edge node[left] {$x_i$} (X)
		(Prod) edge[dashed] node[right] {$\exists!u$} (X);

		\node at (1.5,-1) {$\longleftrightarrow$};
		\node at (1.5,-0.75) {``op''};

		\node (ProdOp) at (4,0) {$\prod_I A_i$};
		\node (AiOp) at (2,0) {$A_i$};
		\node (XOp) at (4,-2) {$X$};

		\path[draw,->] (ProdOp) edge node[above] {$\pi_i$} (AiOp)
					   (XOp) edge node[left] {$x_i$} (AiOp)
					   (XOp) edge[dashed] node[right] {$\exists!u$} (ProdOp);
	\end{tikzpicture}
\end{center}

\pagebreak

\subsection{Coproduct in Sets}

Going back to \textbf{Sets},
the coproduct of two sets $A$ and $B$
is their disjoint union, $A \sqcup B$.
In this case, we have these inclusions:
\begin{align*}
    \pi_A : A &\rightarrow A \sqcup B & \pi_B : B &\rightarrow A \sqcup B \\
    a &\mapsto (a,0) & b &\mapsto (b,1)
\end{align*}

Now, given any other set $X$,
and two mappings $x_A : A \rightarrow X$ and $x_B : B \rightarrow X$,
we can construct a unique mapping
\begin{align*}
    u : A \sqcup B &\rightarrow X \\
    (p,\delta) &\mapsto \begin{cases}
        x_A(p) & \delta = 0 \\
        x_B(p) & \delta = 1
    \end{cases}
\end{align*}

\begin{center}
	\begin{tikzpicture}
		\node (Prod) at (0,0) {$A \sqcup B$};
		\node (A) at (-2,0) {$A$};
		\node (B) at (2,0) {$B$};
		\node (X) at (0,-2) {$X$};
		        
		\path[draw,->] (A) edge node[above] {$\pi_A$} (Prod)
		(B) edge node[above] {$\pi_B$} (Prod)
		(A) edge node[left] {$x_A$} (X)
		(B) edge node[right] {$x_B$} (X)
		(Prod) edge[dashed] node[right] {$\exists!u$} (X);
	\end{tikzpicture}
\end{center}

Great! 
Now we've shown that the cartesian product of sets
and the disjoint union of sets,
are opposite concepts within category theory.
Just as one concept in products would apply to both sets and posets,
a concept regarding products of sets would also apply to disjoint unions.

\end{document}