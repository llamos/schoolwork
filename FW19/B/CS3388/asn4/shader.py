class shader:

    def __shadowed(self, object, I, S, objectList):
        M = object.getT()
        I = M * (I + S.scalarMultiply(0.001))
        S = M * S
        for k in objectList:
            Minv = k.getT().inverse()
            It = Minv * I
            St = (Minv * S).normalize()
            if k.intersection(It, St) != -1.0:
                return True
        return False

    def __init__(self, intersection, direction, camera, objectList, light):
        object = [x for x in objectList if x == intersection[0]][0]
        t0 = intersection[1]
        Minv = object.getT().inverse()
        Ts = Minv * light.getPosition()

        Te = Minv * camera.getE()
        Td = Minv * direction

        I = Te + Td.scalarMultiply(t0)
        S = (Ts - I).normalize()
        N = object.normalVector(I)

        R = -S + N.scalarMultiply(2 * S.dotProduct(N))
        V = (Te - I).normalize()

        Id = max(N.dotProduct(S), 0)
        Is = max(R.dotProduct(V), 0)

        r = object.getReflectance()
        c = object.getColor()
        Li = light.getIntensity()

        if not self.__shadowed(object, I, S, objectList):
            f = r[0] + r[1] * Id + r[2] * (Is ** r[3])
        else:
            f = r[0]

        r = int(f * (c[0]*Li[0]))
        g = int(f * (c[1]*Li[1]))
        b = int(f * (c[2]*Li[2]))

        self.__color = (
            r,
            g,
            b
        )

    def getShade(self):
        return self.__color
