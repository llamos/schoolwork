import operator
import numpy as np
from matrix import matrix
import time
import cameraMatrix
import lightSource

class tessel:

    def __init__(self, objectTuple, camera: cameraMatrix, light: lightSource):
        self.__faceList = []  # List of faces with attributes
        EPSILON = 0.001

        #Transform light position into viewing coordinates
        Lv = camera.worldToViewingCoordinates(light.getPosition()).removeRow(3)
        Li = light.getIntensity()

        startTime = time.time()

        objCount = len(objectTuple)
        objI = 0
        for object in objectTuple:
            polyCount = ((object.getURange()[1] - object.getURange()[0]) / object.getUVDelta()[0]) * ((object.getVRange()[1] - object.getVRange()[0]) / object.getUVDelta()[1])
            polyI = 0
            u = object.getURange()[0]
            while u + object.getUVDelta()[0] < object.getURange()[1] + EPSILON:
                v = object.getVRange()[0]
                while v + object.getUVDelta()[1] < object.getVRange()[1] + EPSILON:

                    print("\r%d/%d \t %0.3f/%0.3f \t %0.3f/%0.3f (%5d/%5d) %.2fs" % (objI, objCount, u, object.getURange()[1], v, object.getVRange()[1], polyI, polyCount, (time.time() - startTime)), end="")

                    # Collect surface points and transform them into viewing coordinates
                    facePoints = [
                        camera.worldToViewingCoordinates(object.getT() * object.getPoint(u, v)),
                        camera.worldToViewingCoordinates(object.getT() * object.getPoint(u + object.getUVDelta()[0], v)),
                        camera.worldToViewingCoordinates(object.getT() * object.getPoint(u + object.getUVDelta()[0], v + object.getUVDelta()[1])),
                        camera.worldToViewingCoordinates(object.getT() * object.getPoint(u, v + object.getUVDelta()[1]))
                    ]

                    # Compute vector elements necessary for face shading
                    C = self.__centroid(facePoints)  # Find centroid point of face
                    N = self.__vectorNormal(facePoints)  # Find normal vector to face
                    S = self.__vectorToLightSource(Lv,C)  # Find vector to light source
                    R = self.__vectorSpecular(S,N)  # Find specular reflection vector
                    V = self.__vectorToCentroid(C)  # Find vector from surface centroid to origin of viewing coordinates

                    # If surface is not a back face
                    snDot = (S.transpose() * N).get(0,0) / (S.norm() * N.norm())
                    rvDot = (R.transpose() * V).get(0,0) / (R.norm() * V.norm())
                    ambi = object.getReflectance()[0]
                    if snDot != 0 or rvDot != 0:

                        # Compute face shading
                        diff = object.getReflectance()[1] * max(0, snDot)
                        spec = object.getReflectance()[2] * (max(0, rvDot) ** object.getReflectance()[3])

                        shading = (
                            int(Li[0] * (ambi + diff + spec) * object.getColor()[0]),
                            int(Li[1] * (ambi + diff + spec) * object.getColor()[1]),
                            int(Li[2] * (ambi + diff + spec) * object.getColor()[2])
                        )

                    else:
                        shading = (
                            int(Li[0] * ambi * object.getColor()[0]),
                            int(Li[1] * ambi * object.getColor()[1]),
                            int(Li[2] * ambi * object.getColor()[2])
                        )

                    # Transform 3D points expressed in viewing coordinates into 2D pixel coordinates
                    pixelPoints = [camera.viewingToPixelCoordinates(x) for x in facePoints]

                    # Add the surface to the face list. Each list element is composed of the following items:
                    # [depth of the face centroid point (its Z coordinate), list of face points in pixel coordinates, face shading]
                    self.__faceList.append([C.get(2,0), pixelPoints, shading, type(object).__name__, u, v, ambi, diff, spec])

                    v += object.getUVDelta()[1]
                    polyI += 1
                u += object.getUVDelta()[0]
            objI += 1

        print("\rFinished execution in %.2fs" % (time.time() - startTime))


    # Returns the column matrix containing the face centroid point
    def __centroid(self,facePoints):
        xS = 0.0
        yS = 0.0
        zS = 0.0

        for point in facePoints:
            xS += point.get(0,0)
            yS += point.get(1,0)
            zS += point.get(2,0)

        centroid = matrix(np.zeros((3,1)))
        centroid.set(0,0, xS / len(facePoints))
        centroid.set(1,0, yS / len(facePoints))
        centroid.set(2,0, zS / len(facePoints))
        return centroid

    # Returns the column matrix containing the normal vector to the face.
    def __vectorNormal(self,facePoints):
        p1 = facePoints[0].copyMatrix().removeRow(3)
        p2 = facePoints[1].copyMatrix().removeRow(3)
        p3 = facePoints[2].copyMatrix().removeRow(3)
        if p2 == p3:
            p3 = facePoints[3].copyMatrix().removeRow(3)

        v1 = p2 - p1
        v2 = p3 - p1
        normal = v1.transpose().crossProduct(v2.transpose()).transpose()

        return normal

    # Returns the column matrix containing the vector from the centroid to the light source
    def __vectorToLightSource(self,L,C):
        return L - C

    # Returns the column matrix containing the vector of specular reflection
    def __vectorSpecular(self,S,N):
        return -S + (N.scalarMultiply(2 * ((S.transpose() * N).get(0,0) / (N.norm() ** 2))))

    # Returns the column matrix containing the vector from the face centroid point to the origin of the viewing coordinates
    def __vectorToCentroid(self,C):
        return -C

    # Returns the face list ready for drawing
    def getFaceList(self):
        return self.__faceList