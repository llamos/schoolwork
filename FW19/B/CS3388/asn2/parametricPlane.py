from math import *
import numpy as np
from matrix import matrix
from parametricObject import parametricObject


class parametricPlane(parametricObject):

    def __init__(self, T=matrix(np.identity(4)), width=20.0, length=20.0, color=(0, 0, 0), reflectance=(0.0, 0.0, 0.0), uRange=(0.0, 0.0), vRange=(0.0, 0.0), uvDelta=(0.0, 0.0)):
        super().__init__(T, color, reflectance, uRange, vRange, uvDelta)
        self.__width = width
        self.__length = length

    def getPoint(self, u, v):
        __P = matrix(np.ones((4,1)))
        __P.set(0, 0, self.__width*u)  # width times u
        __P.set(1, 0, self.__length*v)  # length (height) times u
        __P.set(2, 0, 0)
        return __P
