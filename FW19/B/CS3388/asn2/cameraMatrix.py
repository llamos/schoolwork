from math import *
import numpy as np
from matrix import matrix

class cameraMatrix:

    def __init__(self,UP,E,G,nearPlane=10.0,farPlane=50.0,width=640,height=480,theta=90.0):
        __Mp = self.__setMp(nearPlane,farPlane)
        __T1 = self.__setT1(nearPlane,theta,width/height)
        __S1 = self.__setS1(nearPlane,theta,width/height)
        __T2 = self.__setT2()
        __S2 = self.__setS2(width,height)
        __W2 = self.__setW2(height)

        self.__UP = UP.normalize()
        self.__N = (E - G).removeRow(3).normalize()
        self.__U = self.__UP.removeRow(3).transpose().crossProduct(self.__N.transpose()).normalize().transpose()
        self.__V = self.__N.transpose().crossProduct(self.__U.transpose()).transpose()
        self.__Mv = self.__setMv(self.__U,self.__V,self.__N,E)
        self.__C = __W2*__S2*__T2*__S1*__T1*__Mp
        self.__M = self.__C*self.__Mv

    def __setMv(self,U,V,N,E):

        # just need a 4x4, nearly everything is overwritten
        __Mv = matrix(np.zeros((4, 4)))

        # first row is X value of each UVNE
        __Mv.set(0, 0, U.get(0, 0))
        __Mv.set(0, 1, V.get(0, 0))
        __Mv.set(0, 2, N.get(0, 0))
        __Mv.set(0, 3, E.get(0, 0))

        # then Y value of UVNE
        __Mv.set(1, 0, U.get(1, 0))
        __Mv.set(1, 1, V.get(1, 0))
        __Mv.set(1, 2, N.get(1, 0))
        __Mv.set(1, 3, E.get(1, 0))

        # Z value of UVNE
        __Mv.set(2, 0, U.get(2, 0))
        __Mv.set(2, 1, V.get(2, 0))
        __Mv.set(2, 2, N.get(2, 0))
        __Mv.set(2, 3, E.get(2, 0))

        # finally, place a 1 in the bottom right entry
        __Mv.set(3, 3, 1)

        # invert it, and return
        __Mv = __Mv.inverse()
        return __Mv

    def __setMp(self,nearPlane,farPlane):

        # calculate a and b constants for scaling images down to pinhole
        a = (farPlane + nearPlane)/(farPlane - nearPlane)
        b = (-2 * farPlane * nearPlane)/(farPlane - nearPlane)

        # construct the matrix N 0 0 0 // 0 N 0 0 // 0 0 a b // 0 0 -1 0
        __Mp = matrix(np.zeros((4, 4)))
        __Mp.set(0, 0, nearPlane)
        __Mp.set(1, 1, nearPlane)
        __Mp.set(2, 2, a)
        __Mp.set(2, 3, b)
        __Mp.set(3, 2, -1)

        return __Mp

    def __setT1(self,nearPlane,theta,aspect):

        # construct the viewpoint limits based on view angle and aspect ratio
        tT = nearPlane * tan((pi / 180) * (theta / 2))
        tB = -tT
        tR = aspect * tT
        tL = -tR

        # construct T1 with these four constants
        __T1 = matrix(np.identity(4))
        __T1.set(0, 3, -(tR + tL)/2)
        __T1.set(1, 3, -(tT + tB)/2)

        return __T1

    def __setS1(self,nearPlane,theta,aspect):

        # same as T1
        tT = nearPlane * tan((pi / 180) * (theta / 2))
        tB = -tT
        tR = aspect * tT
        tL = -tR

        # construct similar scaling matrix to T1
        __S1 = matrix(np.identity(4))
        __S1.set(0, 0, 2/(tR - tL))
        __S1.set(1, 1, 2/(tT - tB))

        return __S1

    def __setT2(self):

        # T2 is very simple, identity w/ two extra 1s on the right side
        __T2 = matrix(np.identity(4))
        __T2.set(0, 3, 1)
        __T2.set(1, 3, 1)

        return __T2

    def __setS2(self,width,height):

        # S2 is a similarly simple matrix to T2
        __S2 = matrix(np.identity(4))
        __S2.set(0, 0, width/2)
        __S2.set(1, 1, height/2)

        return __S2

    def __setW2(self,height):

        # W2 scales based on height of image, just an h scale in the last column
        __W2 = matrix(np.identity(4))
        __W2.set(1, 1, -1)
        __W2.set(1, 3, height)

        return __W2

    def worldToViewingCoordinates(self,P):
        return self.__Mv*P

    def viewingToImageCoordinates(self,P):
        return self.__C*P

    def imageToPixelCoordinates(self,P):
        return P.scalarMultiply(1.0/P.get(3,0))

    def worldToImageCoordinates(self,P):
        return self.__M*P

    def worldToPixelCoordinates(self,P):
        return self.__M*P.scalarMultiply(1.0/(self.__M*P).get(3,0))

    def getUP(self):
        return self.__UP

    def getU(self):
        return self.__U

    def getV(self):
        return self.__V

    def getN(self):
        return self.__N

    def getMv(self):
        return self.__Mv

    def getC(self):
        return self.__C

    def getM(self):
        return self.__M
