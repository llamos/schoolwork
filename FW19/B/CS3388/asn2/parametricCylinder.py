from math import *
import numpy as np
from matrix import matrix
from parametricObject import parametricObject

class parametricCylinder(parametricObject):

    def __init__(self, T=matrix(np.identity(4)), height=20.0, radius=10.0, color=(0, 0, 0), reflectance=(0.0, 0.0, 0.0), uRange=(0.0, 0.0), vRange=(0.0, 0.0), uvDelta=(0.0, 0.0)):
        super().__init__(T, color, reflectance, uRange, vRange, uvDelta)
        self.__height = height
        self.__radius = radius

    def getPoint(self, u, v):
        __P = matrix(np.ones((4,1)))
        __P.set(0, 0, self.__radius*sin(v))  # radius as it ranges over sin(v)
        __P.set(1, 0, self.__radius*cos(v))  # radius as it ranges over cos(v)
        __P.set(2, 0, self.__height*u)  # height as it ranges over u
        return __P
