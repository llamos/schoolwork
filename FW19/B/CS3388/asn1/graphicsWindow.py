from PIL import Image


class graphicsWindow:

    def __init__(self, width=640, height=480):
        self.__mode = 'RGB'
        self.__width = width
        self.__height = height
        self.__canvas = Image.new(self.__mode, (self.__width, self.__height))
        self.__image = self.__canvas.load()

    def getWidth(self):
        return self.__width

    def getHeight(self):
        return self.__height

    def drawPixel(self, pixel, color):
        self.__image[pixel[0], pixel[1]] = color

    def saveImage(self, fileName):
        self.__canvas.save(fileName)

    # To account for drawing lines with slopes directed at all 8 quadrants,
    # instead of using the decimal form of dy, and comparing y values at the "current" x value,
    # this algorithm instead "slides" on both the x and y variables, depending on slope,
    # by comparing the current location to the destination coordinates
    def drawLine(self, p1, p2, color):
        # determine the absolute change in x,
        # dx is only used for distance to target,
        # so direction sign is instead stored in sx
        dx = abs(p2[0] - p1[0])
        if p1[0] < p2[0]:
            sx = 1
        else:
            sx = -1

        # same as above for y,
        # but take the negative distance
        # so that error calculation is gives x/y by positive/negative
        dy = -abs(p2[1] - p1[1])
        if p1[1] < p2[1]:
            sy = 1
        else:
            sy = -1

        # now calculate the difference in distances required
        # note that this err variable will accumulate both x and y error,
        # with xerr toward positive, and yerr toward negative
        err = dx + dy

        # mark off starting coordinates,
        # these are the "slider" variables that move along the line as it's drawn,
        # we draw this point early, since our first iteration in the loop will move before drawing
        # otherwise only one end of the line will be drawn
        x = p1[0]
        y = p1[1]
        self.drawPixel((x, y), color)

        # loop until our sliders reach the end of the line
        while x != p2[0] or y != p2[1]:

            # calculate double the error
            # this is used to ensure that both sliders don't move on every iteration
            double_error = 2 * err

            # if this doubled error value is larger than dy (which is negative),
            # then the current x-pixel is farther from the line than the next one,
            # so we increment x in the direction of sx
            # and update error to reflect our change in position.
            # using dy to check for x sounds backwards,
            # but it ensures that we only move the x variable once dy has gotten
            # one "x-value" closer to the target destination
            if double_error >= dy:
                x += sx
                err += dy

            # same here for dx, but if double_error is _smaller_ than dx (which is positive).
            # note that both if statements could succeed on one iteration,
            # in which case the movement would be diagonal.
            if double_error <= dx:
                err += dx
                y += sy

            # now that the new coordinate has been calculated,
            # light up the respective pixel
            self.drawPixel((x, y), color)
