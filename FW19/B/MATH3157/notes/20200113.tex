%! TEX root = notes.tex

\subsection{Backward Induction}

\textbf{Definition}:
In a game with perfect information,
each node other than a leaf determines a \underline{subgame}.

\textbf{Example}: Kayle's has 6 subgames. \\
\textbf{Note}: $G = G_6$ is a subgame of itself.

\textbf{Definition}: Let $H$ be a subgame of $G$.
Then the \underline{value} of $H$ is
\begin{equation*}
    v(H) = \left\{\begin{aligned}
        W \text{ if P1 has a winning strategy}\\
        L \text{ if P2 has a winning strategy}\\
        \text{undefined otherwise}
    \end{aligned}\right.
\end{equation*}

\begin{center}
\begin{tikzpicture}[every node/.style={circle,fill=black,inner sep=0,minimum size=4}]
    \node[label=left:1] (1A) at (0,0) {};
    \node[label=below:$a$] at (1A) {};
    \node[label=left:2] (2A) at (-20/7,1) {};
    \node[label=left:2] (2B) at (0,1) {};
    \node[label=left:2] (2C) at (20/7,1) {};
    \node[label=left:2] (2D) at (34/7,1) {};
    \node[label=above:$L_1$] at (2D) {};
    \node[label=left:1] (3A) at (-4,2) {};
    \node[label=below:$b$] at (3A) {};
    \node[label=left:1] (3B) at (-20/7,2) {};
    \node[label=above:$L_6$] at (3B) {};
    \node[label=left:1] (3C) at (-12/7,2) {};
    \node[label=above:$W_5$] at (3C) {};
    \node[label=left:1] (3D) at (-4/7,2) {};
    \node[label=above:$L_4$] at (3D) {};
    \node[label=left:1] (3E) at (4/7,2) {};
    \node[label=above:$W_3$] at (3E) {};
    \node[label=left:1] (3F) at (12/7,2) {};
    \node[label=below:$c$] at (3F) {};
    \node[label=left:1] (3G) at (20/7,2) {};
    \node[label=above:$W_1$] at (3G) {};
    \node[label=left:1] (3H) at (4,2) {};
    \node[label=above:$L_2$] at (3H) {};
    \node[label=left:2] (4A) at (-32/7,3) {};
    \node[label=above:$W_4$] at (4A) {};
    \node[label=left:2] (4B) at (-24/7,3) {};
    \node[label=above:$L_5$] at (4B) {};
    \node[label=left:2] (4C) at (8/7,3) {};
    \node[label=above:$W_2$] at (4C) {};
    \node[label=left:2] (4D) at (16/7,3) {};
    \node[label=above:$L_3$] at (4D) {};
    \path[draw] (1A) edge (2A)
    (1A) edge (2B)
    (1A) edge (2C)
    (1A) edge (2D)
    (2A) edge (3A)
    (2A) edge (3B)
    (2A) edge (3C)
    (2B) edge[double] (3D)
    (2B) edge (3E)
    (2C) edge (3F)
    (2C) edge (3G)
    (2C) edge (3H)
    (3A) edge[double] (4A)
    (3A) edge (4B)
    (3F) edge[double] (4C)
    (3F) edge (4D);

    \node[fill=white] (G1) at (-16/7,3) {$G_1$};
    \node[fill=white] (G2) at (0,1.5) {$G_2$};
    \node[fill=white] (G3) at (22/7,3) {$G_3$};
    \node[fill=white] (G4) at (-4,2.5) {$G_4$};
    \node[fill=white] (G5) at (12/7,2.5) {$G_5$};

    \draw[dashed] (3B)+(-0.25,0.5) ellipse (2 and 2); % G1
    \draw[dashed] (G2) ellipse (1 and 1.25);
    \draw[dashed] (3G)+(-0.25,0.5) ellipse (2 and 2); % G3
    \draw[dashed] (G4) ellipse (1 and 1.25);
    \draw[dashed] (G5) ellipse (1 and 1.25);
\end{tikzpicture}
\end{center}

\textbf{Example}: If $G=$ Kayles,
then $v(G) = L$ via $MLR$.

Suppose we didn't know $v(G)$.
Then we can use Zermelo's algorithm 
for backward strategy induction 
to find $v(G)$.

First notice $G_4, G_2, G_5$ 
are one-player subgames of $G$
with no proper subgames.
These are the \underline{minimal} subgames of $G$.

$v(G_2) = L$\\
$v(G_4) = W$\\
$v(G_5) = W$

This induces a new game $G'$ 
by collapsing $G_2, G_4, G_5$.
Note that $G'$ is not a subgame of $G$.

\textbf{Key Observation}:
If $v(G)$ is defined then $v(G) = v(G')$.
Now repeat everything for $G'$:

$v(G'_1)=L$\\
$v(G'_2)=L$

Collapsing $G'_1$ and $G'_2$ gives $G''$.
Since $G''$ has no proper subgames, we stop.
Record the information on $G$.
We have $v(G) = v(G') = v(G'') = L$.
Thus, P2 has a winning stragey.
To find it, look for the double-lined path from the root to a leaf.

\pagebreak

\subsection{Solving Nim}

The rules of Nim are:
\begin{itemize}
    \item The game starts with several piles of sticks
    \item The players take turns removing any number of sticks from one pile
    \item The player who takes the last stick wins
\end{itemize}

This game is too big to be solved by a strategic form or backward induction.

\textbf{Example}:
Three piles of sticks: 
$\left[\begin{matrix} 3 & 11 & 6 \end{matrix}\right]$.

First we represent the piles in binary, 
with the sums of each column:
\[
    \left[\begin{matrix}
        3 \\
        11 \\
        6
    \end{matrix}\right] = \left[\begin{array}{c c c c}
        0 & 0 & 1 & 1 \\
        1 & 0 & 1 & 1 \\
        0 & 1 & 1 & 0 \\
        \hline
        1 & 1 & 3 & 2
    \end{array}\right]
\]

We say a game is \underline{balanced}
if the number of 1's in each column is even.
Otherwise, it is \underline{unbalanced}.
Notice:
\begin{enumerate}
    \item If a game is balanced, then it becomes unbalanced after one move.
    \item If a game is unbalanced, a player can't win in one move.
    So, always forcing your opponent into a balanced game 
    means your opponent can't win.
    \item If the game is unbalanced, the it can be balanced in one move.
    For example,
    \[
        \left[\begin{array}{c c c c c c}
            1 & 1 & 0 & 0 & 0 & 1 \\
            0 & 1 & 0 & 1 & 1 & 1 \\
            1 & 0 & 1 & 1 & 0 & 1 \\
            \hline
            2 & 2 & 1 & 2 & 1 & 3
        \end{array}\right]
    \]
    We want $100110$ in the third row.
    Subtract $101101 - 100110 = 000111$,
    so remove 7 sticks from the third pile.
\end{enumerate}

Let $G = \text{Nim}$.
Then $v(G) = W$ if the game is unbalanced.
$v(G) = L$ if the game is balanced.
So in general, P1 is better off 
since unbalanced is more common than balanced.

\subsection{Chess}

\textbf{Definition}:
A game is \underline{finite} if the game tree
has a finite number of nodes and branches.

\input{20200122.tex}
