%! TEX root = notes.tex

\section{Games with Probability}

\subsection{Guessing in Exams}

Say a single multiple choice question has $m$ answers.
Suppose that a student is either:
\begin{itemize}
	\item $ig$, guessing randomly
	\item $om$, omniscient (knows the answer)
\end{itemize}

If the proportion of omniscient students is $p$,
what is the probability 
that a student who got the right answer was just guessing?
\begin{align*}
	\text{prob}(ig \mid right) & = \frac{\text{prob}(right \mid ig) \cdot \text{prob}(ig)}{\text{prob}(right)} \\
	                           & = \frac{\frac{1}{m} \cdot (1 - p)}{\text{prob}(right)}                        
\end{align*}

What is $\text{prob}(right)$?
Let $c = \frac{1}{\text{prob}(right)}$.
Similarly, $\text{prob}(om \mid right) = \frac{\text{prob}(right | om) \cdot \text{prob}(om)}{\text{prob}(right)} = \frac{1 \cdot p}{\frac{1}{c}} = cp$.
Also, $\text{prob}(ig | right) + \text{prob}(om | right) = 1$.
Then, $\frac{c \cdot (1 - p)}{m} + cp = 1$, so $c = \frac{m}{1 - p + pm}$.
Therefore, $\text{prob}(ig | right) = \frac{1 - p}{1 - p + pm}$.

If $m = 3$, $|class| = 100$, and $p = \frac{1}{100}$,
then $\text{prob}(ig | right) \simeq 0.971$.

\subsection{Lotteries}

Suppose you are given odds $3:4$ 
against an even number being rolled on a die.
So if you bet $\$4$ and win, you get $\$3$,
or you lose your $\$4$.
This is a lottery with two ``prizes'':
$L = $ 
\begin{tabular}{|c|c|l}
	\cline{1-2}
	$\$3$         & $-\$4$        & $\longleftarrow$ outcomes      \\
	\cline{1-2}
	$\frac{1}{2}$ & $\frac{1}{2}$ & $\longleftarrow$ probabilities \\
	\cline{1-2}
\end{tabular}

Another lottery with 3 prizes: $M = $
\begin{tabular}{|c|c|c|}
	\hline
	$-\$4$        & $\$24$         & $\$3$         \\
	\hline
	$\frac{1}{4}$ & $\frac{5}{12}$ & $\frac{1}{3}$ \\
	\hline
\end{tabular}

A \underline{random variable} is a function
$X: \Omega \rightarrow \mathbb{R}$.
\[
	L \equiv X(\omega) = \left\{\begin{aligned}
	3 & w \in \{2,4,6\} \\
	-4 & w \in \{1,3,5\}
	\end{aligned}\right.
\]

So $\text{prob}(X = 3) = \text{prob}(\{2,4,6\}) = \frac{1}{2}$
and $\text{prob}(X = -4) = \text{prob}(\{1,3,5\}) = \frac{1}{2}$.
How bad is $L$?

The \underline{expected value} of $X$ is
$\Sigma(X) = \Sigma (k \cdot \text{prob}(X=k))$.
In this case, $\Sigma(L) = 3 \cdot \frac{1}{2} + (-4) \cdot \frac{1}{2} = -\frac{1}{2}$.
So if you play $L$ a lot,
you expect to lose $\$0.50$ on average.
Conversely, $\Sigma(M) = (-4) \cdot \frac{1}{4} + (24) \cdot \frac{5}{12} = (3) \cdot \frac{1}{3} = 10$.
So if you play $M$ a lot,
you expect to win $\$10$.

\subsection{Games with Chance (Monty Hall)}

\textbf{Recall}:
Every strictly competitive game 
with perfect information and no chance moves
has a value $v$.
For games with chance moves,
a pure strategy pair only determines a lottery. 
If the game is win-or-lose,
then a pure strategy pair determines a lottery of the following form:

\begin{multicols}{2}

	\[
		\bar{p} = \begin{array}{|c|c|}
		\hline
		W & L \\
		\hline
		p & 1 - p \\
		\hline
		\end{array}
    \]
    
    \columnbreak
    
	\[
		\bar{q} = \begin{array}{|c|c|}
		\hline
		W & L \\
		\hline
		q & 1 - q \\
		\hline
		\end{array}
    \]
    
\end{multicols}
A rational player seeks to maximize their probability of winning.
Then, $\bar{p} \succeq_1 \bar{q}$ iff $p \geq q$.

Regarding these lotteries as \underline{outcomes},
we obtain a strictly competitive game
with perfect information
and no chance moves.
This new game has a value.
How do we calculate it?

In the Monty Hall problem,
\begin{center}
    \resizebox{\textwidth}{!}{
    \begin{tikzpicture}
        \node[fill=black,label=below:Chance] (C) at (0,0) {};
        \node[circle,minimum size=4,inner sep=0,fill=black,label=left:$M$] (M1) at (-4,1) {};
        \node[circle,minimum size=4,inner sep=0,fill=black,label=right:$M$] (M2) at (0,1) {};
        \node[circle,minimum size=4,inner sep=0,fill=black,label=right:$M$] (M3) at (4,1) {};
        \node[circle,minimum size=4,inner sep=0,fill=black,label=right:$A$] (A1) at (-6,2) {};
        \node[circle,minimum size=4,inner sep=0,fill=black,label=right:$A$] (A2) at (-2,2) {};
        \node[circle,minimum size=4,inner sep=0,fill=black,label=right:$A$] (A3) at (2,2) {};
        \node[circle,minimum size=4,inner sep=0,fill=black,label=right:$A$] (A4) at (6,2) {};
        \node[circle,minimum size=4,inner sep=0,fill=black,label=above:$W$] (W1) at (-7,3) {};
        \node[circle,minimum size=4,inner sep=0,fill=black,label=above:$L$] (L1) at (-5,3) {};
        \node[circle,minimum size=4,inner sep=0,fill=black,label=above:$L$] (L2) at (-3,3) {};
        \node[circle,minimum size=4,inner sep=0,fill=black,label=above:$W$] (W2) at (-1,3) {};
        \node[circle,minimum size=4,inner sep=0,fill=black,label=above:$L$] (L3) at (1,3) {};
        \node[circle,minimum size=4,inner sep=0,fill=black,label=above:$W$] (W3) at (3,3) {};
        \node[circle,minimum size=4,inner sep=0,fill=black,label=above:$W$] (W4) at (5,3) {};
        \node[circle,minimum size=4,inner sep=0,fill=black,label=above:$L$] (L4) at (7,3) {};
        \draw[dashed, rounded corners=5] (-6.25,2.25) rectangle (-1.75,1.75);
        \draw[dashed, rounded corners=5] (6.25,2.25) rectangle (1.75,1.75);

        \path[draw] (C) edge node[below] {$1$} (M1)
                    (C) edge node[right] {$2$} (M2)
                    (C) edge node[below] {$3$} (M3)
                    (M1) edge node[below] {$3$} (A1)
                    (M2) edge node[below] {$3$} (A2)
                    (M2) edge node[below] {$1$} (A3)
                    (M3) edge node[below] {$1$} (A4)
                    (A1) edge node[left] {$s$} (W1)
                    (A1) edge node[right] {$t$} (L1)
                    (A2) edge node[left] {$s$} (L2)
                    (A2) edge node[right] {$t$} (W2)
                    (A3) edge node[left] {$s$} (L3)
                    (A3) edge node[right] {$t$} (W3)
                    (A4) edge node[left] {$s$} (W4)
                    (A4) edge node[right] {$t$} (L4)
                    ;
    \end{tikzpicture}
    }
\end{center}

Alice has 4 pure strategies: $ss, st, ts, tt$. \\
Monty has 2 pure strategies: $1, 3$ 
and only applies if door 2 was chosen by chance.

The pure strategy pair $(st,3)$ has what outcome?\\
If chance chooses 1: $W$.\\
If chance chooses 2: $L$.\\
If chance chooses 3: $L$.\\
So Alice's probability of winning for $(st,3)$ is $\frac{1}{3}$.

In this game, $(ss,1)$ and $(ss,3)$ are saddle points.
So, the value of the game is $\frac{2}{3}$,
the value at those points.

\input{20200124.tex}
