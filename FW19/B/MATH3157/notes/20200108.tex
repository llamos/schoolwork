%! TEX root = notes.tex

Suppose $p = \frac{12}{a + b}$.
The payoff table:
\begin{center}    
\mpayoff{5 \newline 5}{4 \newline 3}{3 \newline 4}{2 \newline 2}
\end{center}

Thus, Dove strongly dominates Hawk.
This is the Prisoner's Delight.

Suppose $p = \frac{72}{(a+b)^2}$. 
The payoff table:
\begin{center}
    \mpayoff{18 \newline 18}{16 \newline 8}{8 \newline 16}{9 \newline 9}
\end{center}

Neither strategy strongly dominates the other
(this is more common in real life).
This is called the \underline{Stag Hunt Game}.

\subsection{Nash Equilibria}

\textbf{Definition}:
A pair of strategies $(s,t)$ in a game
is a \underline{Nash Equilibrium} if
each strategy is the best reply to the other.

\textbf{Examples}:

\begin{enumerate}[label=(\arabic*)]
    \item (hawk,hawk) in Prisoner's Dilemma
    \item (dove,dove) in Prisoner's Delight
    \item (dove,dove) and (hawk,hawk) in the Stag Hunt
\end{enumerate}

Nash Equilibria are considered to be the ``rational solutions'' to a game.

\textbf{Remark}: 
So far, our games are very simple:
no timing of moves, noncooperative, no chance, no information.

\subsection{Win-or-Lose Games}

We start with \underline{strictly competitive} games,
i.e. 2-player games with only 2 outcomes:
\begin{itemize}
    \item $W =$ Adam wins and Eve loses
    \item $L =$ Eve wins and Adam loses
\end{itemize}

We always assume players prefer to win. 
That is, $L <_A W$ and $W <_E L$.

\pagebreak

\section{Game Trees}

\subsection{Matching Pennies}

Adam covers a penny with his hand.
Eve guesses heads (H) or tails (T).
Eve wins if she guesses correctly.
Otherwise, Adam wins.
Eve doesn't know Adam's move, 
so her \underline{information set} includes both nodes.

\begin{center}
    \begin{tikzpicture}
        \node (A) at (0,0) {Adam};
        \node (E1) at (-2,2) {Eve};
        \node (E2) at (2,2) {Eve};
        \node (L1) at (-3,4) {L};
        \node (W1) at (-1,4) {W};
        \node (W2) at (1,4) {W};
        \node (L2) at (3,4) {L};

        \path[draw,->] (A) edge node[left] {T} (E1)
                       (A) edge node[right] {H} (E2)
                       (E1) edge node[left] {T} (L1)
                       (E1) edge node[right] {H} (W1)
                       (E2) edge node[left] {T} (W2)
                       (E2) edge node[right] {H} (L2);

        \draw[dashed] (0,2) ellipse (2.5 and 0.5);
    \end{tikzpicture}
\end{center}

\subsection{Peeking Pennies (The Tipoff Game)}

In this game, Eve peeks.
So, Eve has perfect information.
Her winning strategies are found by \underline{backwards induction}:

\begin{center}
    \begin{tikzpicture}
        \node (A) at (0,0) {Adam};
        \node (E1) at (-2,2) {Eve};
        \node (E2) at (2,2) {Eve};
        \node (L1) at (-3,4) {L};
        \node (W1) at (-1,4) {W};
        \node (W2) at (1,4) {W};
        \node (L2) at (3,4) {L};

        \path[draw,->] (A) edge node[left] {T} (E1)
                       (A) edge node[right] {H} (E2)
                       (E1) edge[dashed] node[left] {T} (L1)
                       (E1) edge node[right] {H} (W1)
                       (E2) edge node[left] {T} (W2)
                       (E2) edge[dashed] node[right] {H} (L2);

        \draw[dashed] (-2,2) ellipse (0.5 and 0.5);
        \draw[dashed] (2,2) ellipse (0.5 and 0.5);
    \end{tikzpicture}
\end{center}


\begin{multicols}{2}
    \begin{center}
        Matching Pennies:

        \begin{tabular}{ccccc}
            && \multicolumn{2}{c}{Eve} \\
            && T & H \\
            \cline{3-4}
            \multirow{2}{*}{Adam} & T & \multicolumn{1}{|c|}{L} & \multicolumn{1}{|c|}{W} \\
            \cline{3-4}
            & H & \multicolumn{1}{|c|}{W} & \multicolumn{1}{|c|}{L} \\
            \cline{3-4}
       \end{tabular}

        This game has no Nash equilibra.

    \end{center}
    \columnbreak
    \begin{center}
        Peeking Pennies:

        \begin{tabular}{cccccc}
            &&& \multicolumn{2}{c}{Eve} \\
            && TT & TH & HT & HH \\
            \cline{3-6}
            \multirow{2}{*}{Adam} & T & \multicolumn{1}{|c|}{L} & \multicolumn{1}{|c|}{L} & \multicolumn{1}{|c|}{W} & \multicolumn{1}{|c|}{W} \\
            \cline{3-6}
            & H & \multicolumn{1}{|c|}{W} & \multicolumn{1}{|c|}{L} & \multicolumn{1}{|c|}{W} & \multicolumn{1}{|c|}{L} \\
            \cline{3-6}
       \end{tabular}

       Eve's winning strategy is TH. 
       This is the \underline{solution} to Peeking Pennies.
       Note that (T,TH) and (H,TH) are both Nash equilibria.
    \end{center}
\end{multicols}

\subsection{Perfect Information}

\textbf{Definition}: 
When the players know everything that has happened sor before their move,
this is \underline{perfect information.}

\textbf{Examples}:
\begin{enumerate}
    \item Peeking Pennies has perfect information, but Matching Pennies does not.
    \item Chess has perfect information but has 3 outcomes: W, L and D.
    \item Tic-tac-toe has perfect information and 3 outcomes as well.
    \item Backgammon has perfect information, but chance moves.
    \item Kayles has perfect information.
\end{enumerate}

\subsection{Mixed Strategies}

In real life, if Adam and Eve played Matching Pennies a lot,
he should randomize his choice of T or H.
Using probability to decide your move is a \underline{mixed strategy}.
Otherwise, the strategy is \underline{pure}.


\input{20200110.tex}
