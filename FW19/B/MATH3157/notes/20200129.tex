%! TEX root = notes.tex

Consider the strategy pair $(AA,AD)$.
The outcomes for $(AA,AD)$ are the lotteries:
\begin{multicols}{2}
	\begin{center}
		Boris: \begin{tabular}{|c|c|c|}
		\hline
		$L$ & $D$ & $W$ \\
		\hline
		$1/2$ & $0$ & $1/2$ \\
		\hline
		\end{tabular}
								
		\columnbreak
								
		Vladimir: \begin{tabular}{|c|c|c|}
		\hline
		$L$ & $D$ & $W$ \\
		\hline
		$1/4$ & $1/4$ & $1/2$ \\
		\hline
		\end{tabular}
	\end{center}
\end{multicols}
	
For $(DD,AD)$,
Boris has the following lottery:
\begin{tabular}{|c|c|c|}
	\hline
	$L$ & $D$ & $W$ \\
	\hline
	$0$ & $1$ & $0$ \\
	\hline
\end{tabular}.
Which outcome is better for Boris?
The answer depends on more than just $L <_1 D <_1 W$.
How much risk are they willing to take?

What preferences should rational players have between lotteries?
One possible answer:
``rational'' players should place a monetary value on each prize
and then compare the expected values.
This is \underline{not} always rational.

\pagebreak

\subsection{St. Petersburg Paradox}

A fair coin is tossed until a head is shown for the first time.
If it is the $k$th toss,
you win $2^k$.
How much should you be willing to pay to play this lottery?

\begin{center}
	\begin{tabular}{r | c | c | c | c | c | c }
		\cline{2-7}
		Prize       & $\$2$ & $\$4$ & $\$8$ & $\dots$ & $\$2^n$    & $\dots$ \\
		\cline{2-7}
		Sequence    & $H$   & $TH$  & $TTH$ & $\dots$ & $T^{n-1}H$ & $\dots$ \\
		\cline{2-7}
		Probability & $1/2$ & $1/4$ & $1/8$ & $\dots$ & $1/2^n$    & $\dots$ \\
		\cline{2-7}
	\end{tabular}
\end{center}
So, the \underline{expected dollar value} of $L$ is:\\
$\Sigma(L) = \Sigma k \cdot \text{prob}(X = k) 
= 2 \cdot 1/2 + 4 \cdot 1/4 + \dots = 1 + 1 + \dots = \infty$.

So, should a rational player, Olga, be willing to pay everything to play?
The probability of winning $\leq \$8$ is $7/8$.
These are not attractive odds for such a high risk.
A rational player should instead try to \underline{maximize} something.
This is sometimes called the \underline{Von Neumann and Morgenstern (VN\&M) utility}.

\textbf{Postulate 1}: 
A rational player prefers whichever of 2 win-or-lose lotteries 
has the highest probability of winning.

If we define $u(\bar{p}) = \begin{array}{|c|c|}
\hline
b & a \\
\hline
p & 1-p \\
\hline
\end{array}$ then
\[
	\Sigma u(\bar{p}) = pb + (1 - p)a = p u(W) + (1-p)u(L) = a + p(b-a).
\]
Since $b - a > 0$,
$\Sigma u(\bar{p})$ is largest when $p$ is largest.

Now suppose $\Omega$ has any size $|\Omega| > 2$
and $\text{lott}(\Omega)$ is the set of all lotteries 
with prizes $\omega \in \Omega$.

\textbf{Postulate 2}:
Each prize $\omega$ with $L \leqslant \omega \leqslant W$
is equivalent to a win-or-lose lottery.
So, $\omega \sim \bar{q}$ for some $q$.
One can define a VN\&M utility $u : \Omega \rightarrow \mathbb{R}$
by $u(\omega) = q$.
Here, $W \sim \bar{1}$ and $L \sim \bar{0}$.

\textbf{Postulate 3}:
Players don't care if $\omega$ is replaced by $\overline{u(w)}$.

\begin{align*}
	L &= \begin{array}{|c|c|c|c|}
	\hline
	\omega_1 & \omega_2 & \dots & \omega_n \\
	\hline
	p_1      & p_2      & \dots & p_n      \\
	\hline
	\end{array} \\ 
	&\sim \begin{array}{|c|c|}
	\hline
	W & L \\
	\hline 
	p_1q_1 + \dots + p_nq_n & 1-(p_1q_1 + \dots + p_nq_n) \\
	\hline
	\end{array} \\
	&= \overline{p_1q_1 + \dots + p_nq_n}.
\end{align*}

\textbf{Postulate 4}:
Rational players don't care about the last kind of equivalence either.

Thus, by postulate 1,
Olga prefers whichever of the two lotteries like $L$
has the greatest expected 
\[
    \Sigma u(L) = p_1q_1 + \dots + \_nq_n = p_1u(\omega_n) + \dots + p_nu(\omega_n).
\]

\input{20200131.tex}
