%! TEX root = asn2.tex

\begin{problem}
\begin{problemTitle}
	Let $C_n = \mangle{x}$
	be a cyclic group of order $n$
	with generator $x$
	and let $H$ be a group
	written multiplicatively 
	with identity 1.
	\begin{enumerate}[label=(\alph*)]
		\item Show that if $\varphi : C_n \rightarrow H$ is a group homomorphism,
		      then $\varphi(x)^n = 1$.
		      Show conversely that if $h \in H$ satisfies $h^n = 1$,
		      then there exists a unique well defined group homomorphism
		      $\varphi : C_n \rightarrow H$
		      such that $\varphi(x) = h$.
		      		      		      
		\item If $A$ and $B$ are 2 abelian groups written multiplicatively,
		      show that $\text{Hom}(A,B)$,
		      the set of all group homomorphisms between $A$ and $B$,
		      is an abelian group under pointwise multiplication of functions.
		      		      		      
		\item Show that $\text{Hom}(C_m,C_n)$ is 
		      a cyclic group of order $\text{gcd}(m,n)$.
	\end{enumerate}
\end{problemTitle}

\begin{enumerate}[label=(\alph*)]
	\item $\varphi : C_n \rightarrow H$ is a group homomorphism $\Longrightarrow \varphi(x)^n = 1$: \\
	      Since $\varphi$ is a homomorphism,
	      it must preserve operations,
	      i.e. for all $a,b \in C_n, \varphi(a) * \varphi(b) = \varphi(a * b)$.
	      Since $\varphi(x)^n = \varphi(x) * \varphi(x) * \dots * \varphi(x)$,
	      with $n$ terms, 
	      it must equal $\varphi(x * x * \dots * x)$
	      with $n$ terms.
	      This is equal to $\varphi(x^n)$.
	      By definition, as the generator of a cyclic group, $x^n = 1$.
	      Therefore,
	      \begin{align*}
	      	h^n & = \varphi(x)^n \\
	      	    & = \varphi(x^n) \\
	      	    & = \varphi(1)   
	      \end{align*}
	      Since $\varphi$ is a homomorphism,
	      $\varphi(1) = 1$.
	      	      	      
	      \vspace{4mm}
	      	      	      
	      $\exists h \in H$ such that $h^n = 1 \Longrightarrow \exists!\varphi : C_n \rightarrow H$ such that $\varphi(x) = h$: \\
	      \textit{Existence}: 
	      Define $\varphi(x) = h$.
	      Then, for all $a \in C_n$,
	      $x = a^m$ for some $m \in \mathbb{N}$.
	      Thus, $\varphi$ is well defined on $C_n$,
	      as $\varphi(a) = \varphi(x^m) = \varphi(x)^m = h^m \in H$.
	      	      	      
	      \textit{Uniqueness}: 
	      Suppose there exists another $\psi : C_n \rightarrow H$
	      such that $\psi(x) = h$,
	      and $\psi \neq \varphi$.
	      Then, by definition, for some $a \in C_n, \psi(a) \neq \varphi(a)$.
	      However, $a = x^m$ for some $m \in \mathbb{N}$, as above.
	      So,
	      \begin{align*}
	      	\psi(a)   & \neq \varphi(a)   \\
	      	\psi(x^m) & \neq \varphi(x^m) \\
	      	\psi(x)^m & \neq \varphi(x)^m \\
	      	h^m       & \neq h^m          
	      \end{align*}
	      This is a contradiction, so $\psi = \varphi$.
	      	      	      
	      \pagebreak
	\item Let $a,b,c \in A$ and $\theta,\eta,\gamma \in \text{Hom}(A,B)$.
	      		  
	      Since $\theta(a),\eta(b) \in B$, 
	      $\theta(a) * \eta(b)$, the pointwise multiplication of the pair,
	      is also in $B$.
	      So $\text{Hom}(A,B)$ has closure.
	      
	      Since $\theta(a),\eta(b),\gamma(c) \in B$, 
	      $(\theta(a) * \eta(b)) * \gamma(c) = \theta(a) * (\eta(b) * \gamma(c))$,
	      by the associativity of $B$.
	      So $\text{Hom}(A,B)$ is associative.
	      
	      The homomorphism $1 \in \text{Hom}(A,B)$,
	      such that for any $x \in A$, $1(x) = 1$,
	      provides a unit homomorphism for $\text{Hom}(A,B)$
	      under pointwise multiplication.
	      Then $\theta(a) * 1(b) = \theta(a)$,
	      and $1(a) * \theta(b) = \theta(b)$.
		  So $\text{Hom}(A,B)$ has an identity element.
		  
		  Let $\theta^{-1} : A \rightarrow B$
		  be given by $a \mapsto (\theta(a))^{-1}$.
		  Since $B$ is a group,
		  and $\theta(a) \in B$,
		  then $(\theta(a))^{-1}$ must be defined.
		  Under pointwise multiplication,
		  $\theta(a) * \theta^{-1}(a) = \theta(a) * (\theta(a))^{-1} = 1$,
		  and similar for the opposite direction.
		  So $\text{Hom}(A,B)$ has inverses.
		  
		  These four properties show that $\text{Hom}(A,B)$ is a group.
		  
		  Finally, under commutativity of $B$,
	      $\theta(a) * \eta(b) = \eta(b) * \theta(a)$.
	      So $\text{Hom}(A,B)$ is abelian under pointwise multiplication.
	      
	\item Any cyclic group of order $n$
		  is isomorphic to $\mathbb{Z}/n$ under addition,
		  by an isomorphism given by indexing the elements.
		  For the rest of this question I will use $\mathbb{Z}_m$ and $\mathbb{Z}_n$,
		  to make the cyclic nature easier to understand.
		  However, by isomorphism, this applies to any cyclic groups.

		  In order to preserve unity,
		  each homomorphism $\alpha \in \text{Hom}(\mathbb{Z}_m, \mathbb{Z}_n)$
		  must contain the mapping $\alpha(0) = 0$.
		  If there exists a non-zero mapping,
		  then $\alpha(1) \neq 0$,
		  since any other value $a \in \mathbb{Z}_m = 1^k$
		  for some $k \in \mathbb{N}$,
		  and the homomorphism must then preserve operations.

		  Suppose $m$ and $n$ are coprime.
		  Let $1 \in \mathbb{Z}_m$
		  and $b \neq 0 \in \mathbb{Z}_n$,
		  such that $\alpha(1) = b$.
		  Then $1^m = 0$,
		  but since $m$ and $n$ are coprime,
		  $b^m \neq 0$.
		  This means that $\alpha(1^m) \neq \alpha(1)^m$,
		  so $\alpha$ cannot be a homomorphism.
		  Therefore, when $m$ and $n$ are coprime,
		  the only homomorphism that exists maps each value to 0.

		  Suppose $m$ and $n$ are not coprime.
		  Let $1 \in \mathbb{Z}_m$.
		  Let $b \in \mathbb{Z}_n$
		  such that $\mangle{b} = b\mathbb{Z}_n$
		  is a proper subgroup of $\mathbb{Z}_n$
		  and is also a subgroup of $\mathbb{Z}_m$.
		  This generates a homomorphism,
		  since $\alpha(1)$ will generate the entire subgroup,
		  without violating the homomorphism properties.
		  Since the generator of these subgroups 
		  must be present in both $\mathbb{Z}_m$ and $\mathbb{Z}_n$,
		  there are $\text{gcd}(m,n)$ shared subgroups,
		  and therefore $\text{gcd}(m,n)$ valid homomorphisms.
\end{enumerate}
\end{problem}