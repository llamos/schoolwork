%! TEX root = asn3.tex

\begin{problem}
\begin{problemTitle}
	Let $D_{2n}$ act on the set of vertices
	$X = \{P_0, \dots, P_{n-1}\}$
	of the regular $n$-gon $\Gamma_n$
	as defined in Assignment 1, Question 2.
	The group action determines a permutation representation
	$\varphi : D_{2n} \rightarrow S_X$.
	The set bijection $f : X \rightarrow Y$
	given by $f(P_k) = k + 1$ for $k = 0,\dots,{n-1}$
	determines an isomorphism $\psi_f : S_X \rightarrow S_n$
	as we saw in Assignment 2, Question 4.
	\begin{enumerate}[label=(\alph*)]
		\item Find the cycle decompositions of 
		      $\sigma = \psi_f \circ \varphi(R)$ and
		      $\tau = \psi_f \circ \varphi(S)$
		      where $\psi_f \circ \varphi : D_{2n} \rightarrow S_n$
		      is the composite of the homomorphisms described above.
		      		      		      		      		      		      
		\item Verify that $D_{2n} \cong \mangle{\sigma,\tau}$.
		      Deduce that $\psi_f \circ \varphi : D_{2n} \rightarrow S_n$ is injective
		      and the group action of $D_{2n}$ on $X$ is faithful.
	\end{enumerate}
\end{problemTitle}

\begin{enumerate}[label=(\alph*)]
	\item Observe the following diagrams for $R,S \in D_8$ acting on $X$.
	      	      	      	      	
	      \begin{center}
	      	\begin{tikzpicture}
	      			      			      			      			      	      
	      		\draw[thick] (-1.5,0) -- (1.5,0);
	      		\draw[thick] (0,-1.5) -- (0,1.5);
	      			      			      			      									  
	      		\foreach \p/\pos in {
	      			0/above right,
	      			1/above right,
	      			2/above right,
	      			3/above left,
	      			4/above left,
	      			5/below left,
	      			6/below right,
	      			7/below right
	      			} {
	      			\node[label=\pos:$P_\p$,fill=black,scale=0.5] (P\p) at (\p * pi/4 r:1) {};
	      		}
	      			      			      			      									  
	      		\path[draw,-Triangle] (P0) edge[bend right] (P1);
	      		\path[draw,-Triangle] (P1) edge[bend right] (P2);
	      		\path[draw,-Triangle] (P2) edge[bend right] (P3);
	      		\path[draw,-Triangle] (P3) edge[bend right] (P4);
	      		\path[draw,-Triangle] (P4) edge[bend right] (P5);
	      		\path[draw,-Triangle] (P5) edge[bend right] (P6);
	      		\path[draw,-Triangle] (P6) edge[bend right] (P7);
	      		\path[draw,-Triangle] (P7) edge[bend right] (P0);
	      			      			      			      					
	      		\node at (0,2) {$R$};
	      			      			      			      							  
	      		\begin{scope}[shift={(6,0)}]
	      				      				      				      									  
	      			\draw[thick] (-1.5,0) -- (1.5,0);
	      			\draw[thick] (0,-1.5) -- (0,1.5);
	      				      				      				      											  
	      			\foreach \p/\pos in {
	      				0/above right,
	      				1/above right,
	      				2/above right,
	      				3/above left,
	      				4/above left,
	      				5/below left,
	      				6/below right,
	      				7/below right
	      				} {
	      				\node[label=\pos:$P_\p$,fill=black,scale=0.5] (P\p) at (\p * pi/4 r:1) {};
	      			}
	      				      				      				      										
	      			\path[draw,-Triangle] (P0) edge[out=60,in=-60,looseness=12] (P0);
	      			\path[draw,-Triangle] (P1) edge[bend right] (P7);
	      			\path[draw,-Triangle] (P2) edge[bend right] (P6);
	      			\path[draw,-Triangle] (P3) edge[bend right] (P5);
	      			\path[draw,-Triangle] (P4) edge[out=-120,in=120,looseness=12] (P4);
	      			\path[draw,-Triangle] (P5) edge[bend right] (P3);
	      			\path[draw,-Triangle] (P6) edge[bend right] (P2);
	      			\path[draw,-Triangle] (P7) edge[bend right] (P1);
	      				      				      				      							
	      			\node at (0,2) {$S$};
	      		\end{scope}
	      	\end{tikzpicture}
	      \end{center}
	      	      	      		  
	      We can see that $R$ defines a standard 8-cycle on $X$, as expected.
	      Conversely, $S$ defines a pairwise swapping $\begin{pmatrix}
	      1
	\end{pmatrix}\begin{pmatrix}
	2 & 8
	\end{pmatrix}\begin{pmatrix}
	3 & 7
	\end{pmatrix}\begin{pmatrix}
	4 & 6
	\end{pmatrix}\begin{pmatrix}
	5
	\end{pmatrix}$.
			
	As $n$ grows, the general pattern is \begin{align*}
	\sigma &= \psi_f \circ \theta(R) = \begin{pmatrix}
	2 & 3 & \dots & n & 1
	\end{pmatrix} \\
	\tau &= \psi_f \circ \theta(S) = \left\{\begin{aligned}
	\begin{pmatrix}
		1 
	\end{pmatrix}\begin{pmatrix}
	2 & n
	\end{pmatrix}\begin{pmatrix}
	3 & n - 1
	\end{pmatrix}\begin{pmatrix}
	\dots
	\end{pmatrix}\begin{pmatrix}
	n/2 - 1 & n/2 + 1
	\end{pmatrix}\begin{pmatrix}
	n/2
	\end{pmatrix} &\quad \text{$n$ is even} \\
	\begin{pmatrix}
		1 
	\end{pmatrix}\begin{pmatrix}
	2 & n
	\end{pmatrix}\begin{pmatrix}
	3 & n - 1
	\end{pmatrix}\begin{pmatrix}
	\dots
	\end{pmatrix}\begin{pmatrix}
	\lfloor n/2 \rfloor & \lceil n/2 + 1 \rceil
	\end{pmatrix} &\quad \text{$n$ is odd}
	\end{aligned}\right.
	\end{align*}
	
	\item 
\end{enumerate}
\end{problem}
