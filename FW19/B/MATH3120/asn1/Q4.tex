%! TEX root = asn1.tex

\begin{problem}
\begin{problemTitle}
	\begin{enumerate}[label=(\alph*)]
		\item Let $P = [\textbf{p}_1,\dots,\textbf{p}_n] \in M_{nn}(\mathbb{R})$
		      be a matrix whose columns form an orthonormal set in $\mathbb{R}^n$.
		      Show that $P \in O_n(\mathbb{R})$.
		      Conversely, show that any $P \in O_n(\mathbb{R})$ has orthonormal columns.
		\item Let $A$ be a matrix with an orthonormal set of eigenvectors 
		      $\{\textbf{p}_1,\dots,\textbf{p}_n\}$.
		      Suppose $p_1$ is a -1 eigenvector for $A$
		      and the remaining eigenvectors for $A$ have eigenvalue 1.
		      Show that $A$ is orthogonally diagonalisable.
		      [Hint: compute $A[\textbf{p}_1,\dots,\textbf{p}_n]$.]
		\item Show $A \in O_n(\mathbb{R})$.
		\item Matrices as described in part (b) are called \textit{reflection matrices}.
		      Is the set $F_n = \{A \in O_n(\mathbb{R}) : A \text{ is a reflection matrix} \} \cup \{I_n\}$
		      a subgroup of $O_n(\mathbb{R})$?
		      Either prove that $F_n$ is a subgroup
		      or show it is not a subgroup using an explicit counterexample.
	\end{enumerate}
\end{problemTitle}

\begin{enumerate}[label=(\alph*)]
	\item We know that $p_1$ is orthogonal to all other columns of $P$.
	      Additionally, it is normal, with length 1.
	      To check if $P \in O_n(\mathbb{R})$,
	      we want to prove $PP^\mathsf{T} = I_n$.
	      	      
	      So, as the transpose of $P$, $P^\mathsf{T} = \left[\begin{smallmatrix}
	      	\textbf{p}_1 \\
	      	\vdots \\
	      	\textbf{p}_n
	      \end{smallmatrix}\right]$.
	      Then, for any indices $i,j < n$,
	      $(PP^\mathsf{T})_{ij} = \textbf{p}_i \cdot \textbf{p}_j$.
	      When $i = j$, $\textbf{p}_i = \textbf{p}_j$,
	      and we know that each entry $p_i$ is normal,
	      so the dot product will simply be the length squared,
	      or $1^2 = 1$.
	      When $i \neq j$, $\textbf{p}_i \neq \textbf{p}_j$,
	      and by hypothesis, they are orthogonal,
	      so their dot product will be 0.
	      This creates a diagonal matrix where the diagonals are 1,
	      and the remaining elements are 0.
	      This is $I_n$.
	      	      
	      Thus, since $P$'s inverse is its transpose,
	      $P \in O_n(\mathbb{R})$.
	      	      
	      % https://en.wikipedia.org/wiki/Eigenvalues_and_eigenvectors#Diagonalization_and_the_eigendecomposition
	\item Let $\lambda_i$ be the eigenvector of $\textbf{p}_i$.\\
	      Let $Q = \left[\begin{matrix}
	      	\textbf{p}_1 & \textbf{p}_2 & \dots & \textbf{p}_n
	      \end{matrix}\right]$.
	      Then, $AQ = \left[\begin{matrix}
	      	(-1)\textbf{p}_1 & (1)\textbf{p}_2 & \dots & (1)\textbf{p}_n
	      	\end{matrix}\right] = \left[\begin{matrix}
	      	-\textbf{p}_1 & \textbf{p}_2 & \dots & \textbf{p}_n
	      \end{matrix}\right]$.
	      Next, define $D$ as a diagonal matrix
	      where the diagonal at index $i$, $D_{ii} = \textbf{p}_i$.
	      As such, $AQ = QD$,
	      since each column is the scalar factor 
	      of that eigenvalue with its eigenvector.
	      Since each column of $Q$ is orthonormal,
	      it is linearly independent,
	      and so it is invertible.
	      So, right multiplying by $Q^{-1}$
	      gives $A = QDQ^{-1}$,
	      where $Q$ is an orthonogal matrix,
	      and $D$ is a diagonal matrix.
	      So $A$ is orthogonally diagonalisable.
	                
	\item Consider $Q^\mathsf{T} = \left[\begin{smallmatrix}
		      \textbf{p}_1 \\
		      \vdots \\
		      \textbf{p}_n
	\end{smallmatrix}\right]$.
	By (a), we know that $Q^{-1} = Q^\mathsf{T}$,
	since $Q$ is another orthonormal matrix. 
	Also, $DD = I_n$, since each diagonal is 
	either $(-1)(-1) = 1$, or $(1)(1) = 1$.
	So, $D = D^\mathsf{T} = D^{-1}$.
	Then, 
	$A^\mathsf{T} = (QDQ^{-1})^\mathsf{T} 
	= (QDQ^\mathsf{T})^\mathsf{T} 
	= Q^{\mathsf{T}\mathsf{T}}D^\mathsf{T}Q^\mathsf{T} 
	= QD^\mathsf{T}Q^\mathsf{T}$. Similarly, \\
	$A^{-1} = (QDQ^{-1})^{-1} 
	= Q^{(-1)(-1)}D^{-1}Q^{-1}
	= QD^{-1}Q^{-1}$.
	Since $D^\mathsf{T} = D^{-1}$ and $Q^\mathsf{T} = Q^{-1}$,
	then $QD^\mathsf{T}Q^\mathsf{T} = QD^{-1}Q^{-1}$
	and $A^\mathsf{T} = A^{-1}$.
	Thus, $A \in O_n(\mathbb{R})$.
	
	\item $F_n$ is not a subgroup of $O_n(\mathbb{R})$.
	      Let $A = \left[\begin{smallmatrix}
	      	-1 & 0 \\
	      	0 & 1
	      \end{smallmatrix}\right]$.
	      A has an orthonormal set of eigenvectors,
	      $\left\{\textbf{p}_1 = \left[\begin{smallmatrix}
	      	1 \\
	      	0
	      	\end{smallmatrix}\right], \textbf{p}_2 = \left[\begin{smallmatrix}
	      	0 \\
	      	1
	      \end{smallmatrix}\right]\right\}$,
	      where the eigenvalue of $\textbf{p}_1$ is -1
	      and the eigenvalue of $\textbf{p}_2$ is 1.
	      So, $A$ is a reflection matrix.
	      However, $AA = I_2$,
	      which does not have a eigenvalue of -1.
	      So, $F_n$ is not closed under matrix multiplication,
	      and cannot be a subgroup of $O_n$.
\end{enumerate}
\end{problem}
