%! TEX root = asn1.tex

\begin{problem}
\begin{problemTitle}
	Let
	\[
		R_\theta = \left[\begin{array}{lr}
			\cos(\theta) & -\sin(\theta) \\
			\sin(\theta) & \cos(\theta)
		\end{array}\right] \in \text{GL}_2(\mathbb{R})
	\]
	be the matrix of the rotation
	$T_\theta : \mathbb{R}^2 \rightarrow \mathbb{R}^2$
	in $\theta$ radians counterclockwise about the origin
	with respect to the standard basis $\{e_1, e_2\}$.
	\begin{enumerate}[label=(\alph*)]
		\item Show that $R_\theta R_\psi = R_{\theta + \psi}$
		      for any $\theta, \psi \in \mathbb{R}$.
		      Use this to show that $R_\theta^k = R_{k\theta}, k \in \mathbb{N}$ and $R_\theta^{-1}=R_{-\theta}$.
		\item Let $R = R_{\frac{2\pi}{n}}$
		      and $S = \left[\begin{array}{lr}
		      	1 & 0 \\
		      	0 & -1
		      \end{array}\right]$.\\
		      Show that $R^n = I_2, S^2 = I_2$ and $SRS^{-1} = R^{-1}$.
	\end{enumerate}
\end{problemTitle}

\begin{enumerate}
	\item We start by writing out the matrix multiplication,
	      \begin{align*}
	      	R_\theta R_\psi                                 & = \left[\begin{array}{lr}                          
	      	\cos(\theta)                                    & -\sin(\theta)                                      \\
	      	\sin(\theta)                                    & \cos(\theta)                                       
	      	\end{array}\right]\left[\begin{array}{lr}
	      	\cos(\psi)                                      & -\sin(\psi)                                        \\
	      	\sin(\psi)                                      & \cos(\psi)                                         
	      	\end{array}\right]
	      	\shortintertext{And compute the multiplication verbatim,}
	      	                                                & = \left[\begin{array}{lr}                          
	      	\cos(\theta)\cos(\psi) - \sin(\theta)\sin(\psi) & -\cos(\theta)\sin(\psi) - \sin(\theta)\cos(\psi)   \\
	      	\sin(\theta)\cos(\psi) + \cos(\theta)\sin(\psi) & -\sin(\theta)\sin(\psi) + \cos(\theta)\cos(\psi)   
	      	\end{array}\right]
	      	\shortintertext{Then rewrite the right hand terms,}
	      	                                                & = \left[\begin{array}{lr}                          
	      	\cos(\theta)\cos(\psi) - \sin(\theta)\sin(\psi) & -(\cos(\theta)\sin(\psi) + \sin(\theta)\cos(\psi)) \\
	      	\sin(\theta)\cos(\psi) + \cos(\theta)\sin(\psi) & \cos(\theta)\cos(\psi) - \sin(\theta)\sin(\psi)    
	      	\end{array}\right]
	      	\shortintertext{Now each of these terms can be condensed via Ptolemy's summation laws,}
	      	                                                & = \left[\begin{array}{lr}                          
	      	\cos(\theta + \psi)                             & -\sin(\theta + \psi)                               \\
	      	\sin(\theta + \psi)                             & \cos(\theta + \psi)                                
	      	\end{array}\right] \\
	      	                                                & = R_{\theta + \psi}                                
	      \end{align*}
	      I believe it is quite trivial that $R_\theta^1 = R_{1\theta}$, 
	      and the above proves the statement for $k = 2$,
	      so next we'll derive $R_\theta^k = R_{k\theta}$ by induction.
	      \begin{align*}
	      	\shortintertext{We begin by writing out what it means to have $R_\theta^{k+1}$,}
	      	R_\theta^{k+1}         & = R_\theta^k R_\theta     
	      	\shortintertext{Then we assume $R_\theta^k = R_{k\theta}$ by the inductive hypothesis,}
	      	                       & = R_{k\theta} R_\theta    \\
	      	                       & = \left[\begin{array}{lr} 
	      	\cos(k\theta)          & -\sin(k\theta)            \\
	      	\sin(k\theta)          & \cos(k\theta)             
	      	\end{array}\right]\left[\begin{array}{lr}
	      	\cos(\theta)           & -\sin(\theta)             \\
	      	\sin(\theta)           & \cos(\theta)              
	      	\end{array}\right]
	      	\shortintertext{Now we see that the problem reduces to the $k = 2$ instance shown above, so it simplifies to}
	      	                       & = \left[\begin{array}{lr} 
	      	\cos(k\theta + \theta) & -\sin(k\theta + \theta)   \\
	      	\sin(k\theta + \theta) & \cos(k\theta + \theta)    
	      	\end{array}\right] \\
	      	                       & = \left[\begin{array}{lr} 
	      	\cos((k + 1)\theta)    & -\sin((k + 1)\theta)      \\
	      	\sin((k + 1)\theta)    & \cos((k + 1)\theta)       
	      	\end{array}\right] \\
	      	                       & = R_\theta^{k + 1}        
	      \end{align*}
	      So we have an instance at $k = 2$,
	      as well as a proof that $R_\theta^k = R_{k\theta} \Longrightarrow R_\theta^{k+1} = R_{(k + 1)\theta}$,
	      so by the inductive hypothesis,
	      we know $R_\theta^k = R_{k\theta}$ to be true for all $k \in \mathbb{N}$.
	      	      	      	      	      	      	      	      	      	      
	      Finally, we want to find an inverse matrix to $R_\theta, R_\theta^{-1}$ 
	      such that $R_\theta R_\theta^{-1} = I_2$.
	      \begin{align*}
	      	\shortintertext{We start with $I_2$,}
	      	I_2                   & = \left[\begin{array}{lr} 
	      	1                     & 0                         \\
	      	0                     & 1                         
	      	\end{array}\right]
	      	\shortintertext{Next, we can substitute these elements with some helpful trigonometric equivalents,}
	      	                      & = \left[\begin{array}{lr} 
	      	\cos(0)               & -\sin(0)                  \\
	      	\sin(0)               & \cos(0)                   
	      	\end{array}\right] \\
	      	                      & = \left[\begin{array}{lr} 
	      	\cos(\theta - \theta) & -\sin(\theta - \theta)    \\
	      	\sin(\theta - \theta) & \cos(\theta - \theta)     
	      	\end{array}\right]
	      	\shortintertext{This is again the $k=2$ instance as above, so we can separate it to}
	      	                      & = \left[\begin{array}{lr} 
	      	\cos(\theta)          & -\sin(\theta)             \\
	      	\sin(\theta)          & \cos(\theta)              
	      	\end{array}\right]\left[\begin{array}{lr}
	      	\cos(-\theta)         & -\sin(-\theta)            \\
	      	\sin(-\theta)         & \cos(-\theta)             
	      	\end{array}\right] \\
	      	                      & = R_\theta R_{-\theta}    
	      \end{align*}
	      So, $R_\theta^{-1} = R_{-\theta}$.
	      	      	      	      	      	      	      	      	      	      
	\item For any $n \in \mathbb{N}$,
	      by the proof of $R_\theta^k$,
	      \begin{align*}
	      	R_\frac{2\pi}{n}^n & = R_{n\frac{2\pi}{n}}     \\
	      	                   & = R_{2\pi}                \\
	      	                   & = \left[\begin{array}{lr} 
	      	\cos(2\pi)         & -\sin(2\pi)               \\
	      	\sin(2\pi)         & \cos(2\pi)                \\
	      	\end{array}\right] \\
	      	                   & = \left[\begin{array}{lr} 
	      	1                  & 0                         \\
	      	0                  & 1                         
	      	\end{array}\right] \\
	      	                   & = I_2                     
	      \end{align*}
	      	      	      	      	      	      	      	      			  
	      Similarly, starting with $S$,
	      \begin{align*}
	      	S^2              & = \left[\begin{array}{lr} 
	      	1                & 0                   \\
	      	0                & -1                  
	      	\end{array}\right]\left[\begin{array}{lr}
	      	1                & 0                   \\
	      	0                & -1                  
	      	\end{array}\right] \\
	      	                 & = \left[\begin{array}{lr} 
	      	(1)(1) + (0)(0)  & (1)(0) + (0)(-1)    \\
	      	(0)(1) + (-1)(0) & (0)(0) + (-1)(-1)   
	      	\end{array}\right] \\
	      	                 & = \left[\begin{array}{lr} 
	      	1                & 0                   \\
	      	0                & 1                   
	      	\end{array}\right] \\
	      	                 & = I_2               
	      \end{align*}
	      	      	      	      	      	      	      		  
	      \pagebreak
	      	      	      	      	      	      	      
	      So, $R^n = I_2$ and $S^2 = I_2$.
	      This also gives us $S^{-1} = S$.
	      So,
	      \begin{align*}
	      	SRS^{-1}              & = SRS                  \\
	      	                      & = \left[\begin{matrix} 
	      	1                     & 0                      \\
	      	0                     & -1                     
	      	\end{matrix}\right]\left[\begin{matrix} 
	      	\cos(\frac{2\pi}{n})  & -\sin(\frac{2\pi}{n})  \\
	      	\sin(\frac{2\pi}{n})  & \cos(\frac{2\pi}{n})   \\
	      	\end{matrix}\right]\left[\begin{matrix}
	      	1                     & 0                      \\
	      	0                     & -1                     
	      	\end{matrix}\right] \\
	      	                      & = \left[\begin{matrix} 
	      	\cos(\frac{2\pi}{n})  & -\sin(\frac{2\pi}{n})  \\
	      	-\sin(\frac{2\pi}{n}) & -\cos(\frac{2\pi}{n})  
	      	\end{matrix}\right]\left[\begin{matrix}
	      	1                     & 0                      \\
	      	0                     & -1                     
	      	\end{matrix}\right] \\
	      	                      & = \left[\begin{matrix} 
	      	\cos(\frac{2\pi}{n})  & \sin(\frac{2\pi}{n})   \\
	      	-\sin(\frac{2\pi}{n}) & \cos(\frac{2\pi}{n})   
	      	\end{matrix}\right]
	      \end{align*}
	      We wish to check whether this is $R^{-1}$,
	      so we multiply again by $R$:
	      \begin{align*}
	      	R(SRS^{-1})                                                                               & =                                                                                      \\                                                                       
	      	                                                                                          & = \left[\begin{matrix}                                                                 
	      	\cos(\frac{2\pi}{n})                                                                      & \sin(\frac{2\pi}{n})                                                                   \\
	      	-\sin(\frac{2\pi}{n})                                                                     & \cos(\frac{2\pi}{n})                                                                   
	      	\end{matrix}\right]\left[\begin{matrix}           
	      	\cos(\frac{2\pi}{n})                                                                      & -\sin(\frac{2\pi}{n})                                                                  \\
	      	\sin(\frac{2\pi}{n})                                                                      & \cos(\frac{2\pi}{n})                                                                   
	      	\end{matrix}\right]
	      	\shortintertext{which has a very verbose immediate product,}
	      	                                                                                          & = \left[\begin{matrix}                                                                 
	      	\cos(\frac{2\pi}{n})\cos(\frac{2\pi}{n}) + (-\sin(\frac{2\pi}{n}))(-\sin(\frac{2\pi}{n})) & \cos(\frac{2\pi}{n})\sin(\frac{2\pi}{n}) + (-\sin(\frac{2\pi}{n})\cos(\frac{2\pi}{n})) \\
	      	\sin(\frac{2\pi}{n})\cos(\frac{2\pi}{n}) + \cos(\frac{2\pi}{n})(-\sin(\frac{2\pi}{n}))    & \sin(\frac{2\pi}{n})\sin(\frac{2\pi}{n}) + \cos(\frac{2\pi}{n})\cos(\frac{2\pi}{n})    
	      	\end{matrix}\right]
	      	\shortintertext{and reduces to}
	      	                                                                                          & = \left[\begin{matrix}                                                                 
	      	\cos^2(\frac{2\pi}{n}) + \sin^2(\frac{2\pi}{n})                                           & \cos(\frac{2\pi}{n})\sin(\frac{2\pi}{n}) - \cos(\frac{2\pi}{n})\sin(\frac{2\pi}{n})    \\
	      	\sin(\frac{2\pi}{n})\cos(\frac{2\pi}{n}) - \sin(\frac{2\pi}{n})\cos(\frac{2\pi}{n})       & \sin^2(\frac{2\pi}{n}) + \cos^2(\frac{2\pi}{n})                                        
	      	\end{matrix}\right]
	      	\shortintertext{Since, for any $\psi$, $\sin^2(\psi) + \cos^2(\psi) = 1$,}
	      	                                                                                          & = \left[\begin{matrix}                                                                 
	      	1                                                                                         & 0                                                                                      \\
	      	0                                                                                         & 1                                                                                      
	      	\end{matrix}\right]
		  \end{align*}
		  So, $SRS^{-1} = R^{-1}$.
\end{enumerate}
\end{problem}
