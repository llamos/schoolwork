%! TEX root = asn1.tex

\begin{problem}
\begin{problemTitle}
	Let $\Gamma_n$ be the regular $n$-gon in $\mathbb{R}^2$ with vertices
	\[
		\left\{P_k = \left(\cos\left(\frac{2 \pi k}{n}\right), \sin\left(\frac{2 \pi k}{n}\right)\right) : 0 \leq k \leq n - 1\right\}
	\]
	In particular,
	\begin{equation}
		\Gamma_n = \cup_{i=0}^{n-1} L_i
	\end{equation}
	where $L_i$ is the line segment 
	between $P_i$ and $P_{i+1}$,
	for $i = 0,\dots,n-2$
	and $L_{n-1}$ is the line segment 
	between $P_{n-1}$ and $P_0$.
																			
	In class, we proved that
	\[
		D_{2n} = \{A \in O_2(\mathbb{R}) : A(\Gamma_n) = \Gamma_n\}
	\]
	is a subgroup of $O_2(\mathbb{R})$.
																			
	Note that if $A \in \text{GL}_2(\mathbb{R})$
	and $P = (x,y) \in \mathbb{R}^2$ is a point in $\mathbb{R}^2$
	then $A(x,y) := A \left[\begin{array}{ll}
		x \\
		y
		\end{array}\right]$ where $\textbf{OP} = \left[\begin{array}{ll}
		x \\
		y
	\end{array}\right]$.
																			
	If $S \subseteq \mathbb{R}^2$, 
	then $A(S) = \{A(P) : P \in S\}$
	is the image of $S$ under $A$.
	\begin{enumerate}[label=(\alph*)]
		\item Show that if $A \in O_2(\mathbb{R})$,
		      then $A(\Gamma_n)$ is a regular (non-solid) $n$-gon
		      with vertices $\{A(P_0),\dots,A(P_{n-1})\}$.\\
		      {[Hint: Apply $A$ to (1) above 
		      and check all $n$ line segments have the same length.]}
		\item Let $D = \{\textbf{x} \in \mathbb{R}^2 : |\textbf{x}| \leq 1\}$
		      and $S^1 = \{ \textbf{x} \in \mathbb{R}^2 : |\textbf{x}| = 1 \}$.
		      Show that $\Gamma_n \subseteq D$
		      and $\Gamma_n \cap S^1 = \{P_0,\dots,P_{n-1}\}$.\\
		      If $A \in O_2(\mathbb{R})$,
		      show that $A(\Gamma_n) \cap S^1 = \{A(P_0),\dots,A(P_{n-1})\}$.
		\item Let $A \in O_2(\mathbb{R})$. Prove that $A \in D_{2n}$ if and only if
		      \[
		      	\{ A(P_0),\dots,A(P_{n-1}) \} = \{P_0,\dots,P_{n-1}\}
		      \]
		\item Let $\textbf{v}_k = \textbf{OP}_k$.
		      Let $R = R_{\frac{2\pi}{n}}$ and $S = \left[\begin{array}{lr}
		      	1 & 0 \\
		      	0 & -1
		      \end{array}\right]$ as in the previous question.
		      Show that $R(\textbf{v}_l) = \textbf{v}_{l+1 \text{ mod } n}, S\textbf{v}_l = \textbf{v}_{n-l \text{ mod } n}$ 
		      for all $0 \leq l \leq n -1$.
		      Deduce that $R,S \in D_{2n}$.
	\end{enumerate}
\end{problemTitle}

\begin{enumerate}[label=(\alph*)]
	\item Suppose we have two points in $\Gamma_n$, 
	      $Q_i = \left[\begin{smallmatrix}
	      	x_i \\
	      	y_i
	      	\end{smallmatrix}\right]$ and $Q_j = \left[\begin{smallmatrix}
	      	x_j \\
	      	y_j
	      \end{smallmatrix}\right]$.
	      These points can be vertices,
	      or anywhere along the edges.
	      Then, the straight-line distance
	      $|Q_i - Q_j| = \sqrt{(x_i - x_j)^2 + (y_i - y_j)^2}$.
	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      		  
	      Since $A \in O_2(\mathbb{R})$,
	      we know that it is of the form 
	      \[
	      	A = \left[\begin{matrix}
	      		\cos(\theta) & -\sin(\theta) \\
	      		\sin(\theta) & \cos(\theta)
	      		\end{matrix}\right]\qquad\text{or}\qquad A = \left[\begin{matrix}
	      		\cos(\theta) & \sin(\theta) \\
	      		\sin(\theta) & -\cos(\theta)
	      	\end{matrix}\right].
	      \]
	      \begin{align*}
	      	\shortintertext{Let's assume $A$ is of the first form. Then,}
	      	AQ_i &= \left[\begin{matrix}
	      	\cos(\theta) & -\sin(\theta) \\
	      	\sin(\theta) & \cos(\theta)
	      	\end{matrix}\right]\left[\begin{matrix}
	      	x_i \\
	      	y_i
	      	\end{matrix}\right] & AQ_j & = \left[\begin{matrix} 
	      	\cos(\theta) & -\sin(\theta) \\
	      	\sin(\theta) & \cos(\theta)
	      	\end{matrix}\right]\left[\begin{matrix}
	      	x_j \\
	      	y_j
	      	\end{matrix}\right] \\
	      	&= \left[\begin{matrix}
	      	\cos(\theta)x_i - \sin(\theta)y_i \\
	      	\sin(\theta)x_i + \cos(\theta)y_i
	      	\end{matrix}\right] &      & = \left[\begin{matrix} 
	      	\cos(\theta)x_j - \sin(\theta)y_j \\
	      	\sin(\theta)x_j + \cos(\theta)y_j
	      	\end{matrix}\right]
	      \end{align*}
	      \begin{align*}
	      	\shortintertext{So the distance between the points is}
	      	|AQ_i - AQ_j| & = \sqrt{\begin{aligned}                                                                                                    
	      	\begin{split}
	      	              & ((\cos(\theta)x_i - \sin(\theta)y_i) - (\cos(\theta)x_j - \sin(\theta)y_j))^2                                              \\
	      	+             & ((\sin(\theta)x_i + \cos(\theta)y_i) - (\sin(\theta)x_j + \cos(\theta)y_j))^2                                              
	      	\end{split}
	      	\end{aligned}
	      	} \\
	      	\shortintertext{Let $\Delta x = x_i - x_j, \Delta y = y_i - y_j$, for brevity,}
	      	              & = \sqrt{\begin{aligned}                                                                                                    
	      	\begin{split}
	      	              & (\cos(\theta)(\Delta x) - \sin(\theta)(\Delta y))^2                                                                        \\
	      	+             & (\sin(\theta)(\Delta x) + \cos(\theta)(\Delta y))^2                                                                        
	      	\end{split}
	      	\end{aligned}
	      	} \\
	      	              & = \sqrt{\begin{aligned}                                                                                                    
	      	\begin{split}
	      	              & (\cos^2(\theta)(\Delta x)^2 - 2\sin(\theta)\cos(\theta)(\Delta x)(\Delta y) + \sin^2(\theta)(\Delta y)^2)                  \\
	      	+             & (\sin^2(\theta)(\Delta x)^2 + 2\sin(\theta)\cos(\theta)(\Delta x)(\Delta y) + \cos^2(\theta)(\Delta y)^2)                  
	      	\end{split}
	      	\end{aligned}
	      	} \\
	      	              & = \sqrt{\cos^2(\theta)(\Delta x)^2 + \sin^2(\theta)(\Delta y)^2 + \sin^2(\theta)(\Delta x)^2 + \cos^2(\theta)(\Delta y)^2} \\
	      	              & = \sqrt{(\cos^2(\theta) + \sin^2(\theta))(\Delta x)^2 + (\sin^2(\theta) + \cos^2(\theta))(\Delta y)^2}                     \\
	      	              & = \sqrt{(x_i - x_j)^2 + (y_i - y_j)^2}                                                                                     \\
	      	              & = |Q_i - Q_j|                                                                                                              
	      \end{align*}
	      So, the distance between these two points 
	      before and after transformation are equal.
	      Since both points can be arbitrary,
	      it must be that the shape of $\Gamma_n = A(\Gamma_n)$,
	      and that the size of $\Gamma_n = A(\Gamma_n)$,
	      with each vertex $P_i \mapsto A(P_i)$.
	      	      	      	      	      	      	      	      	      	      
	      When $A$ is of the second form above,
	      it can be shown analogously to preserve the equality.
	      	      	      	      	      	      	      	      	      	      		  
	\item Points of the form $(\cos(\theta),\sin(\theta))$,
	      like the points in $\Gamma_n$,
	      lie on the unit circle.
	      So, all vertexes of $\Gamma_n$
	      are on the unit circle by definition.
	      	      	      	      	      	      	      	      	      	      		  
	      Any straight line distance between two points on a circle
	      will always cut through the inside of the circle,
	      and cannot extend outside the circle.
	      So, the distance of any point along those lines from the origin
	      will always be $\leq 1$,
	      any point on these lines that is not a vertex
	      will always be $< 1$.
	      	      	      	      	      	      	      	      	      	      
	      Therefore, $\Gamma_n \subseteq D$,
	      and $\Gamma_n \cap S^1$ will only be the vertices,
	      so it is equal to $\{P_0, \dots, P_{n-1}\}$.
	      	      	      	      	      	      	      	      	      	      
	      As shown in $(a)$,
	      applying the transformation $A$
	      maintains the shape and size of the polygon.
	      So, the set of vertices of $A(\Gamma_n)$
	      will be $\{A(P_0),\dots,A(P_{n-1})\}$,
	      and these will still be the only points within $\Gamma_n$
	      that have a distance of 1 from the origin.
	      	      	      	      	      	      	      	      	      	      
	\item \textbf{$A \in D_{2n} \Longrightarrow \{A(P_0),\dots,A(P_{n-1})\} = \{P_0,\dots,P_{n-1}\}$}:
	      Again, from (a) and (b),
	      after applying the transformation,
	      the size and shape of the polygon is preserved.
	      By hypothesis, $A \in D_{2n}$.
	      So, $A(\Gamma_n) = \Gamma_n$.
	      Since $A(\Gamma_n)$ is comprised of points
	      $\{P_0,\dots,P_{n-1}\}$ and edges between them,
	      then $A(\Gamma_n)$ can only equal $\Gamma_n$
	      if their set of vertices is the same.
	      So, $\{A(P_0),\dots,A(P_{n-1})\} = \{P_0,\dots,P_{n-1}\}$.
	      	      	      	      	      	      	      	      	      
	      \textbf{$\{A(P_0),\dots,A(P_{n-1})\} = \{P_0,\dots,P_{n-1}\} \Longrightarrow A \in D_{2n}$}:
	      If a transformation $A \in O_2(\mathbb{R})$
	      preserves the set of points from $\Gamma_n$ to $A(\Gamma_n)$,
	      then by the ``any two points'' proof above,
	      we must also have the same resultant shape,
	      otherwise there would be two points that no longer have the same distance.
	      So, $\Gamma_n = A(\Gamma_n)$.
	      	      	      	      	      	      	      	      	      		  
	      \pagebreak
	      	      	      	      	      	      	      	      	      		  
	\item From any vertex $P_k = (\cos(2 \pi k/n), \sin(2 \pi k/n))$, 
	      the vector $\textbf{v}_k = \left[\begin{smallmatrix}
	      	\cos(2 \pi k/n) \\
	      	\sin(2 \pi k/n)
	      \end{smallmatrix}\right]$.
	      Then,
	      \begin{align*}
	      	R\textbf{v}_l                    & = \left[\begin{array}{lr}         
	      	\cos\left(\frac{2 \pi}{n}\right) & -\sin\left(\frac{2 \pi}{n}\right) \\
	      	\sin\left(\frac{2 \pi}{n}\right) & \cos\left(\frac{2 \pi}{n}\right)  
	      	\end{array}\right]\left[\begin{array}{lr}
	      	\cos\left(\frac{2 \pi l}{n}\right) \\
	      	\sin\left(\frac{2 \pi l}{n}\right)
	      	\end{array}\right] \\
	      	                                 & = \left[\begin{matrix}            
	      	\cos\left(\frac{2 \pi}{n}\right)\cos\left(\frac{2 \pi l}{n}\right) - \sin\left(\frac{2 \pi}{n}\right)\sin\left(\frac{2 \pi l}{n}\right) \\
	      	\sin\left(\frac{2 \pi}{n}\right)\cos\left(\frac{2 \pi l}{n}\right) + \cos\left(\frac{2 \pi}{n}\right)\sin\left(\frac{2 \pi l}{n}\right)
	      	\end{matrix}\right]
	      	\shortintertext{Once again, by Ptolemy's summation laws,}
	      	                                 & = \left[\begin{matrix}            
	      	\cos\left(\frac{2 \pi}{n} + \frac{2 \pi l}{n}\right) \\
	      	\sin\left(\frac{2 \pi}{n} + \frac{2 \pi l}{n}\right)
	      	\end{matrix}\right] \\
	      	                                 & = \left[\begin{matrix}            
	      	\cos\left(\frac{2 \pi (l+1)}{n}\right) \\
	      	\sin\left(\frac{2 \pi (l+1)}{n}\right)
	      	\end{matrix}\right] \\
	      	                                 & = \textbf{v}_{l+1}                
	      \end{align*}
	      If $l+1$ is outside the range of values for $k$,
	      then the cyclic nature of $\sin$ and $\cos$ 
	      will wrap it around to the beginning of the range,
	      giving us $\textbf{v}_{l+1 \text{ mod } n}$.
	      	      	      	      
	      Similarly for $S$,
	      \begin{align*}
	      	S\textbf{v}_l & = \left[\begin{array}{lr}  
	      	1             & 0                          \\
	      	0             & -1                         
	      	\end{array}\right]\left[\begin{array}{lr}
	      	\cos\left(\frac{2 \pi l}{n}\right) \\
	      	\sin\left(\frac{2 \pi l}{n}\right)
	      	\end{array}\right] \\
	      	              & = \left[\begin{matrix}{lr} 
	      	\cos\left(\frac{2 \pi l}{n}\right) \\
	      	-\sin\left(\frac{2 \pi l}{n}\right)
	      	\end{matrix}\right] \\
	      	              & = \left[\begin{matrix}{lr} 
	      	\cos\left(\frac{2 \pi l}{n}\right) \\
	      	\sin\left(\frac{-2 \pi l}{n}\right)
	      	\end{matrix}\right] \\
	      	              & = \textbf{v}_{-l}          
		  \end{align*}
		  Again, the cyclic nature of sin and cos 
		  will wrap around the range of values for $k$,
		  so this is also equal to $\textbf{v}_{n - l \text{ mod } n}$.

		  Since transformations using $R$ and $S$
		  exclusively map vertices from their current location
		  to the location of another vertex,
		  and since they are both in $O_2(\mathbb{R})$,
		  they both preserve not only the shape and size of the vertices,
		  but also the position.
		  So, the set of vertices before and after the translation are the same,
		  which meets the criteria for $R,S \in D_{2n}$.
\end{enumerate}
\end{problem}