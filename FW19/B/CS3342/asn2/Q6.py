from math import ceil,sqrt
from sys import argv

# Returns an iterator generating primes up to N
def primes(N):
    # Loops through all integers in [2,N]
    for i in range(2,N + 1):
        prime = True
        # Check each potential factor (up to a max of sqrt(N))
        for factor in range(2,ceil(sqrt(i)+0.1)):
            # if it's a factor of i, i is not prime, quit early
            if i != factor and i % factor == 0:
                prime = False
                break
        # if a factor was not found, yield the prime number
        if prime:
            yield i

n = primes(11)
print(next(n))
print(next(n))
print(next(n))
print(next(n))
print(next(n))
print(next(n))
