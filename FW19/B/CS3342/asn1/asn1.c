#include <stdio.h>
#include <stdbool.h>

#define STATE_BASE 0
#define STATE_STRING 1
#define STATE_STRING_ESCAPE 2
#define STATE_SLASH 3
#define STATE_LINE_COMMENT 4
#define STATE_BLOCK_COMMENT 5
#define STATE_BLOCK_PENDING_CLOSE 6

// I chose to write this program in the style of an FSM
// with states and transitions between those states based on input

int main(int argc, char **argv) {

    // open input and output files
    FILE *in = fopen(argv[1], "r");
    FILE *out = fopen("input_C_source_rem.cpp", "w+");

    // buf_char holds each character as read, 
    // state tracks current execution state
    char buf_char;
    int state = STATE_BASE;

    // loop through each character of the file
    while ((buf_char = (char) fgetc(in)) != EOF) {

        // STATE_BASE is execution as normal
        if (state == STATE_BASE) {
            if (buf_char == '"') {
                // if a double quote is encountered, 
                // print and switch to STATE_STRING
                state = STATE_STRING;
                fputc(buf_char, out);
            } else if (buf_char == '/') {
                // slash *may* mark the start of a comment, 
                // so switch to that without printing
                state = STATE_SLASH;
            } else {
                // otherwise, print and continue
                state = STATE_BASE;
                fputc(buf_char, out);
            }
        }

        // when we are in a string, comments can't be started, 
        // print all characters encountered
        else if (state == STATE_STRING) {
            if (buf_char == '\\') {
                // if we encounter a backslash
                // switch to the string escape state 
                state = STATE_STRING_ESCAPE;
                fputc(buf_char, out);
            } else if (buf_char == '"') {
                // when a double quote is found,
                // string is closed if we are in this state 
                state = STATE_BASE;
                fputc(buf_char, out);
            } else {
                // continue in the string
                state = STATE_STRING;
                fputc(buf_char, out);
            }
        }

        // a backslash was encountered if we're here
        // prints anything and returns to STATE_STRING,
        // prevents double quote from closing STATE_STRING back to STATE_BASE
        else if (state == STATE_STRING_ESCAPE) {
            state = STATE_STRING;
            fputc(buf_char, out);
        }

        // when we encounter a slash, still need to ensure 
        // that the comment actually starts after
        else if (state == STATE_SLASH) {
            if (buf_char == '/') {
                // double slash implies start of line comment, 
                // don't print here
                state = STATE_LINE_COMMENT;
            } else if (buf_char == '*') {
                // slash star implies start of block comment, don't print here
                state = STATE_BLOCK_COMMENT;
            } else {
                // some other slash was found (maybe '/' or division?),
                // retroactively print the previous /, 
                // print the buffer character too,
                // and return to STATE_BASE
                state = STATE_BASE;
                fputc('/', out);
                fputc(buf_char, out);
            }
        }

        // during this state, we are in a line comment
        else if (state == STATE_LINE_COMMENT) {
            if (buf_char == '\n') {
                // switch away from this state once we reach a newline
                state = STATE_BASE;
            } else {
                // remain in the state by default
                state = STATE_LINE_COMMENT;
            }
        }

        // during this state, we are in a block comment
        else if (state == STATE_BLOCK_COMMENT) {
            if (buf_char == '*') {
                // if we read a star, need to check if it is 
                // followed by a slash
                state = STATE_BLOCK_PENDING_CLOSE;
            } else {
                // otherwise we stay here until that star
                state = STATE_BLOCK_COMMENT;
            }
        }

        // in this state, we are in a block comment, but the 
        else if (state == STATE_BLOCK_PENDING_CLOSE) {
            if (buf_char == '/') {
                // a closing slash denotes back to normal code
                state = STATE_BASE;
            } else if (buf_char == '*') {
                // a star input may still denote 
                // that the comment ends on the next input,
                // so we remain in this state
                state = STATE_BLOCK_PENDING_CLOSE;
            } else {
                // continue along in the comment
                state = STATE_BLOCK_COMMENT;
            }
        }

    }

    // close both files now that we are done
    fclose(in);
    fclose(out);

}
