#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// For my editor's sanity
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char* argv[]) {

    // Make sure we will have X and Y
    if (argc < 3) {
        printf("Please provide X and Y.\n");
        return 1;
    }
    int X, Y;

    // Make the pipe
    int mpipe[2];
    if (pipe(mpipe)) {
        printf("Pipe fail\n");
        return 1;
    }

    printf("A pipe is created for communication between parent (PID %d) and child\n", getpid());

    // Now fork, before reading
    pid_t child = fork(),
          current;
    current = getpid();

    if (child > 0) {
        // parent
        printf("parent (PID %d) created a child (PID %d)\n", current, child);

        // read X
        X = atoi(argv[1]);
        printf("parent (PID %d) reading X = %d from the user\n", current, X);

        // wait on child
        wait(NULL);

        // read Y from the pipe
        read(mpipe[0], &Y, sizeof(Y));
        printf("parent (PID %d) Reading Y from the pipe (Y = %d)\n", current, Y);

        // Add and exit
        int sum = X + Y;
        printf("parent (PID %d) adding X + Y = %d\n", current, sum);
    } else if (child == 0) {
        // child, read Y
        Y = atoi(argv[2]);
        printf("child (PID %d) reading Y = %d from the user\n", current, Y);

        // Write it to the pipe and exit
        printf("child (PID %d) Writing Y into the pipe\n", current);
        write(mpipe[1], &Y, sizeof(Y));
    }

}