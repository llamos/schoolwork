#include <stdio.h>
#include <unistd.h>

// For my editor's sanity
#include <sys/types.h>
#include <sys/wait.h>

int main() {
    // Store both the forked pid and a reference to the current one
    pid_t pid = fork(),
          current;
    current = getpid();

    // Now check for parent or child_1
    if (pid > 0) { 
        // parent, wait for child_1
        printf("parent process (PID %d) created child_1 (PID %d)\n", current, pid);
        printf("parent (PID %d) is waiting for child_1 (PID %d) to complete before creating child_2\n", current, pid);

        // Don't care about exit code
        wait(NULL);

        // now create child_2
        pid_t child_2 = fork();
        current = getpid();

        // Check parent or child_2
        if (child_2 > 0) {
            // parent
            printf("parent (PID %d) created child_2 (PID %d)\n", current, child_2);

            // This wait just prevents the program from exiting before the external one
            // Otherwise it writes over my bash prompt and it bothers me
            wait(NULL); 

        } else if (child_2 == 0) {
            // child, call external_program.out
            printf("child_2 (PID %d) is calling an external program external_program.out and leaving child_2...\n", current);
            execl("external_program.out", "external_program.out", NULL);
        }

    } else if (pid == 0) { 
        // child_1, create child_1.1
        pid_t grandchild = fork();

        if (grandchild != 0) {
            // print and return as child_1
            printf("child_1 (PID %d) created child_1.1 (PID %d)\n", current, grandchild);
            printf("child_1 (PID %d) is now completed\n", current);
        }

        // child_1.1 does nothing

    }
}
