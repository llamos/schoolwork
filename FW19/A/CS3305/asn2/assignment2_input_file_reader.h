/// \author Paul Norton 250957533
/// \date Oct 29, 2019

#ifndef ASN2_ASSIGNMENT2_INPUT_FILE_READER_H
#define ASN2_ASSIGNMENT2_INPUT_FILE_READER_H

#include "assignment2_queue.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/// Creates three queues based on the same input file data
/// \param input_line The line of input from the file
/// \return Three identical queues in a triple_queue struct
triple_queue* read_input(char* input_line);

#endif //ASN2_ASSIGNMENT2_INPUT_FILE_READER_H
