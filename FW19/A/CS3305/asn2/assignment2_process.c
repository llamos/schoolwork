#include "assignment2_process.h"
#include <stdlib.h>

/// Creates a new process object and initializes its values to their defaults
/// \param pid the PID of the process
/// \param req_exec_time the required execution time of the process
/// \return A pointer to a new process object
process *process_init(int pid, int req_exec_time) {
    // Allocate the process
    process *proc = malloc(sizeof(process));

    // Initialize all values
    proc->pid = pid;
    proc->required_execution_time = req_exec_time;
    proc->execution_time = 0;
    proc->queue_time = 0;
    proc->completed_at = 0;

    // Return the pointer
    return proc;
}

/// Destroys a process, freeing the memory associated with it
/// \param proc The process to free
void process_destroy(process *proc) {
    // Process has no sub-objects,
    // so just free its memory
    free(proc);
}
