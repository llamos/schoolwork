/// Part 2 of Assignment 2 for FW19-CS3305A
/// Simulates process execution queues
/// \author Paul Norton 250957533
/// \date Oct 29, 2019

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "assignment2_process.h"
#include "assignment2_queue.h"
#include "assignment2_input_file_reader.h"

// The "time" elapsed since start of queue execution
int global_time_step = 0;

/// Writes a line identically to both stdout and the log file
/// \param to_log The line to write
/// \param log_file The log file to write to
void write_to_log(char* to_log, FILE *log_file) {
    fprintf(stdout, "%s", to_log);
    fprintf(log_file, "%s", to_log);
}

/// Emulates execution of a FIFO queue.
/// This is the same method used by a SJF queue,
/// with the expectation that the processes are ordered by execution time, before being passed in.
/// \param log_buf The log file buffer, used for logging
/// \param log_file The log file descriptor, used for logging
/// \param queue The process queue to emulate execution of
/// \param queue_type The queue type (FIFO/SJF), used for printing
void run_execution_fifo_sjf(char* log_buf, FILE *log_file, process_queue *queue, char *queue_type) {
    // Print preliminary info about what we're doing to the log file
    sprintf(log_buf, "Ready Queue %d Applying %s Scheduling:\n\nOrder of selection by CPU:\n", queue->queue_number, queue_type);
    write_to_log(log_buf, log_file);

    // Loop through each process in the queue
    process *proc;
    while ((proc = process_queue_dequeue(queue)) != NULL) {
        // Print the pid, to show execution order
        sprintf(log_buf, "p%d ", proc->pid);
        write_to_log(log_buf, log_file);

        // "Execute" the process by incrementing the global time, and updating the process info accordingly
        proc->queue_time = global_time_step;
        global_time_step += proc->required_execution_time;
        proc->completed_at = global_time_step;
    }

    sprintf(log_buf, "\n\nIndividual waiting times for each process:\n");
    write_to_log(log_buf, log_file);

    // Rewind queue, to go through it again for logging
    process_queue_reset_read(queue);
    float total_waiting = 0;
    float queue_count = 0;

    // Loop again
    while ((proc = process_queue_dequeue(queue)) != NULL) {
        // Print the waiting time of each process
        sprintf(log_buf, "p%d = %d\n", proc->pid, proc->queue_time);
        write_to_log(log_buf, log_file);

        // Also accumulate the total waiting for an average
        total_waiting += proc->queue_time;
        queue_count++;
    }

    // Print the average wait time
    sprintf(log_buf, "\nAverage waiting time = %.1f\n\n", (total_waiting / queue_count));
    write_to_log(log_buf, log_file);
}

/// Emulates execution of a RR queue.
/// \param log_buf The log file buffer, used for logging
/// \param log_file The log file descriptor, used for logging
/// \param queue The process queue to emulate execution of
void run_execution_rr(char* log_buf, FILE *log_file, process_queue *queue) {
    // Print basic info about the queue, and execution method
    sprintf(log_buf, "Ready Queue %d Applying RR Scheduling:\n\nOrder of selection by CPU:\n", queue->queue_number);
    write_to_log(log_buf, log_file);

    // Loop through each process
    process *proc;
    while ((proc = process_queue_dequeue_rr(queue)) != NULL) {
        // Print the pid to show execution order
        sprintf(log_buf, "p%d ", proc->pid);
        write_to_log(log_buf, log_file);

        // Check if this is the process' first time being executed,
        // and mark it as the arrival time if so
        if (proc->execution_time == 0) {
            proc->queue_time = global_time_step;
        }

        // Calculate the execution time granted to the process (whether it's full time quotient, or less if process will end early)
        int remaining_exec_time = proc->required_execution_time - proc->execution_time;
        int exec_time = remaining_exec_time < queue->time_quotient ? remaining_exec_time : queue->time_quotient;

        // Simulate execution by advancing the global time, and update process accordingly
        global_time_step += exec_time;
        proc->execution_time += exec_time;

        // Check if the process has finished all required execution, since it may have been pre-empted
        if (proc->execution_time == proc->required_execution_time)
            proc->completed_at = global_time_step;
    }

    sprintf(log_buf, "\n\nTurnaround times for each process:\n");
    write_to_log(log_buf, log_file);

    // Sort queue based on completion time
    process_queue_sort_rr(queue);

    // Rewind queue to print each process' turnaround time, now that execution has finished
    // Loop by fifo, not rr, since we won't be executing
    process_queue_reset_read(queue);
    while ((proc = process_queue_dequeue(queue)) != NULL) {
        // Print turnaround time
        sprintf(log_buf, "p%d = %d\n", proc->pid, proc->completed_at - proc->queue_time);
        write_to_log(log_buf, log_file);
    }

    sprintf(log_buf, "\n");
    write_to_log(log_buf, log_file);
}

int main() {

    // Open the input file in read mode
    FILE *input_file = fopen("cpu_scheduling_input_file.txt", "r");

    // Open the log file, and create a flexible buffer for writing to it
    char log_buf[512];
    FILE *log_file = fopen("cpu_scheduling_output_file.txt", "w+");

    // Loop through each line of the input file
    char buf[512];
    while ((fgets(buf, 512, input_file)) != NULL) {

        // Each line should be a new queue specification, so create a triple queue to store it
        triple_queue *tq = read_input(buf);

        // Reset the global time value, and execute the fifo queue
        global_time_step = 0;
        run_execution_fifo_sjf(log_buf, log_file, tq->fifo_queue, "FCFS");

        // Reset the global time value, and execute the sjf queue after sorting
        global_time_step = 0;
        process_queue_sort_sjf(tq->sjf_queue);
        run_execution_fifo_sjf(log_buf, log_file, tq->sjf_queue, "SJF");

        // Reset the global time value, and execute the rr queue
        global_time_step = 0;
        run_execution_rr(log_buf, log_file, tq->rr_queue);

        // Destroy the queue now that we are done with it
        triple_queue_destroy(tq);

    }

    return 0;
}
