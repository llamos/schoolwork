#ifndef ASN2_ASSIGNMENT2_QUEUE_H
#define ASN2_ASSIGNMENT2_QUEUE_H

#include <sys/types.h>
#include "assignment2_process.h"

/// Represents a process queue in the execution simulation,
/// storing all the processes in a dynamic array
/// as well as all required information to execute the queue
typedef struct process_queue {
    process **internal_array;
    int queue_number, time_quotient;
    size_t size, used, queue_head;
} process_queue;

/// Creates an empty queue with the given id number and time quotient
/// with a default underlying array size of 16
/// \param queue_num The id of the queue
/// \param time_quotient The time quotient of the queue (used in RR execution)
process_queue *process_queue_init(int queue_num, int time_quotient);

/// Adds a process to the process queue, resizing the array if needed
/// \param queue The queue to insert the process to
/// \param proc The process to insert
void process_queue_insert(process_queue *queue, process *proc);

/// Resets the read head of the queue, allowing for it to be read again
/// \param queue The queue to reset
void process_queue_reset_read(process_queue *queue);

/// Sorts a queue for use in SJF execution,
/// by placing jobs with the shortest required execution time first.
/// Underlying sort is bubble sort.
/// \param queue The queue to sort.
void process_queue_sort_sjf(process_queue *queue);

/// Sorts a queue for printing after RR execution,
/// by placing jobs with the first completion time first.
/// Underlying sort is bubble sort.
/// \param queue The queue to sort.
void process_queue_sort_rr(process_queue *queue);

/// Returns the next element in the queue,
/// or null if the end has been reached.
/// Does not remove the element from the underlying array,
/// in case process_queue_reset_read() is called.
/// \param queue The queue to dequeue an element from.
/// \return The front element in the queue, or null if none.
process *process_queue_dequeue(process_queue *queue);

/// Returns the next element in the queue,
/// looping back to the start if the end has been reached,
/// while skipping processes that have already been completed.
/// Does not remove the element from the underlying array,
/// in case process_queue_reset_read() is called.
/// \param queue The queue to dequeue an element from.
/// \return The next element in the queue, or null if all have been completed.
process *process_queue_dequeue_rr(process_queue *queue);

/// Destroys a process queue, including all processes held within it.
/// \param queue The queue to destroy.
void process_queue_destroy(process_queue *queue);

/// Represents three queues,
/// used for executing them separately on the same data.
typedef struct triple_queue {
    process_queue *fifo_queue, *sjf_queue, *rr_queue;
} triple_queue;

/// Creates a triple queue, by allocating memory for all three internal queues.
/// \return A pointer to the new triple queue.
triple_queue *triple_queue_init();

/// Destroys a triple queue, the underlying queues, and all their processes.
/// \param tq The triple queue to destroy.
void triple_queue_destroy(triple_queue *tq);

#endif
