/// \author Paul Norton 250957533
/// \date Oct 29, 2019

#include "assignment2_input_file_reader.h"

/// Creates three queues based on the same input file data
/// \param input_line The line of input from the file
/// \return Three identical queues in a triple_queue struct
triple_queue *read_input(char *input_line) {

    // Start tokenizing input_line and create an empty triple_queue
    char* token = strtok(input_line, " ");
    triple_queue *queue = triple_queue_init();

    // Holders for later
    int queue_num;
    int time_quotient;

    // Loop until the end of the line
    while (token != NULL) {

        // If we get a q, then next token is the queue id
        if (strcmp(token, "q") == 0) {
            token = strtok(NULL, " ");
            queue_num = atoi(token);
        }

        // If we get a tq, the next token is the time quotient of the queue
        if (strcmp(token, "tq") == 0) {
            token = strtok(NULL, " ");
            time_quotient = atoi(token);

            // At this point we have a queue and a time quotient,
            // so initialize three queues with that information
            queue->fifo_queue = process_queue_init(queue_num, time_quotient);
            queue->sjf_queue = process_queue_init(queue_num, time_quotient);
            queue->rr_queue = process_queue_init(queue_num, time_quotient);
        }

        // If we get a p, we're creating a new
        if (token[0] == 'p') {
            // Get the process number as the number after the p
            int process_number = atoi(++token);

            // The required execution time of the process is the next token in the string
            token = strtok(NULL, " ");
            int exec_time = atoi(token);

            // Add this process to all three queues,
            // creating three instances so the data within can be altered during "execution"
            process_queue_insert(queue->fifo_queue, process_init(process_number, exec_time));
            process_queue_insert(queue->sjf_queue, process_init(process_number, exec_time));
            process_queue_insert(queue->rr_queue, process_init(process_number, exec_time));
        }

        // Go to the next token
        token = strtok(NULL, " ");
    }

    // Now that we've read the whole line,
    // the queues should be initialized
    return queue;

}
