#include <stdlib.h>
#include <stdbool.h>
#include "assignment2_queue.h"

/// Creates an empty queue with the given id number and time quotient
/// with a default underlying array size of 16
/// \param queue_num The id of the queue
/// \param time_quotient The time quotient of the queue (used in RR execution)
process_queue *process_queue_init(int queue_num, int time_quotient) {
    // Allocate the struct memory
    process_queue *queue = malloc(sizeof(process_queue));

    // Initialize values to their defaults
    queue->internal_array = malloc(sizeof(process *) * 16);
    queue->size = 16;
    queue->used = 0;
    queue->queue_head = 0;
    queue->queue_number = queue_num;
    queue->time_quotient = time_quotient;

    // Return the pointer
    return queue;
}

/// Adds a process to the process queue, resizing the array if needed
/// \param queue The queue to insert the process to
/// \param proc The process to insert
void process_queue_insert(process_queue *queue, process *proc) {
    // Check if resizing is required
    if (queue->used == queue->size) {
        // Double the size of the array, and reallocate
        queue->size *= 2;
        queue->internal_array = realloc(queue->internal_array, sizeof(process *) * queue->size);
    }

    // Place the new element in the queue, incrementing the used process count
    queue->internal_array[queue->used++] = proc;
}

/// Resets the read head of the queue, allowing for it to be read again
/// \param queue The queue to reset
void process_queue_reset_read(process_queue *queue) {
    // Simply place the queue head marker back to the beginning
    queue->queue_head = 0;
}

/// Sorts a queue for use in SJF execution,
/// by placing jobs with the shortest required execution time first.
/// Underlying sort is bubble sort.
/// \param queue The queue to sort.
void process_queue_sort_sjf(process_queue *queue) {
    // Mark for clean pass
    bool valid = false;

    // Loop until we've checked all elements without requiring a swap
    while (!valid) {
        valid = true;

        // Loop to each element in the array
        for (size_t i = 0; i < queue->used - 1; ++i) {
            // If the next element requires less execution time than the current,
            if (queue->internal_array[i + 1]->required_execution_time < queue->internal_array[i]->required_execution_time) {
                // swap them
                process *temp = queue->internal_array[i];
                queue->internal_array[i] = queue->internal_array[i + 1];
                queue->internal_array[i + 1] = temp;

                // and mark that this was not a clean pass
                valid = false;
            }
        }
    }
}

/// Sorts a queue for printing after RR execution,
/// by placing jobs with the first completion time first.
/// Underlying sort is bubble sort.
/// \param queue The queue to sort.
void process_queue_sort_rr(process_queue *queue) {
    // Mark for clean pass
    bool valid = false;

    // Loop until we've checked all elements without requiring a swap
    while (!valid) {
        valid = true;

        // Loop to each element in the array
        for (size_t i = 0; i < queue->used - 1; ++i) {
            // If the next element finished before the current,
            if (queue->internal_array[i + 1]->completed_at < queue->internal_array[i]->completed_at) {
                // swap them
                process *temp = queue->internal_array[i];
                queue->internal_array[i] = queue->internal_array[i+1];
                queue->internal_array[i + 1] = temp;

                // and mark that this was not a clean pass
                valid = false;
            }
        }
    }
}

/// Returns the next element in the queue,
/// or null if the end has been reached.
/// Does not remove the element from the underlying array,
/// in case process_queue_reset_read() is called.
/// \param queue The queue to dequeue an element from.
/// \return The front element in the queue, or null if none.
process *process_queue_dequeue(process_queue *queue) {
    // Check if end of queue has been reached
    if (queue->queue_head < queue->size) {
        // If not, return next element and increment queue head marker
        return queue->internal_array[queue->queue_head++];
    } else {
        // If end has been reached, return NULL
        return NULL;
    }
}

/// Returns the next element in the queue,
/// looping back to the start if the end has been reached,
/// while skipping processes that have already been completed.
/// Does not remove the element from the underlying array,
/// in case process_queue_reset_read() is called.
/// \param queue The queue to dequeue an element from.
/// \return The next element in the queue, or null if all have been completed.
process *process_queue_dequeue_rr(process_queue *queue) {
    // Preserve the current location in the queue, to check for loop
    size_t start_queue_head = queue->queue_head;

    do {
        // Grab the next element in the queue, and increment the queue head
        process *proc = queue->internal_array[queue->queue_head++];
        queue->queue_head %= queue->used;

        // Only return the process if it hasn't finished execution yet
        if (proc->execution_time < proc->required_execution_time)
            return proc;

        // Otherwise, loop to the next, breaking if we've checked all elements
    } while (queue->queue_head != start_queue_head);

    // If no element hasn't finished execution, return NULL
    return NULL;
}

/// Destroys a process queue, including all processes held within it.
/// \param queue The queue to destroy.
void process_queue_destroy(process_queue *queue) {
    // Destroy all the processes within the queue
    for (size_t i = 0; i < queue->used; i++) {
        process_destroy(queue->internal_array[i]);
    }

    // Free the memory of the queue struct
    free(queue);
}

/// Creates a triple queue, by allocating memory for all three internal queues.
/// \return A pointer to the new triple queue.
triple_queue *triple_queue_init() {
    // Allocate the triple queue memory
    return malloc(sizeof(triple_queue));
}

/// Destroys a triple queue, the underlying queues, and all their processes.
/// \param tq The triple queue to destroy.
void triple_queue_destroy(triple_queue *tq) {
    // Destroy all three queues
    process_queue_destroy(tq->fifo_queue);
    process_queue_destroy(tq->sjf_queue);
    process_queue_destroy(tq->rr_queue);

    // Free the memory of the struct
    free(tq);
}


