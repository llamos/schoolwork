#ifndef ASN2_ASSIGNMENT2_PROCESS_H
#define ASN2_ASSIGNMENT2_PROCESS_H

/// Represents a process in the simulation,
/// with execution time markers, and pid
typedef struct process {
    int pid;
    int required_execution_time, execution_time;
    int completed_at;
    int queue_time;
} process;

/// Creates a new process object and initializes its values to their defaults
/// \param pid the PID of the process
/// \param req_exec_time the required execution time of the process
/// \return A pointer to a new process object
process *process_init(int pid, int req_exec_time);

/// Destroys a process, freeing the memory associated with it
/// \param proc The process to free
void process_destroy(process *proc);

#endif
