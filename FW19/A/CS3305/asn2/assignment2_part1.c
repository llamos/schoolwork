/// Part 1 of Assignment 2 for FW19-CS3305A
/// Demonstrates differences in memory between threads and forked processes
/// \author Paul Norton 250957533
/// \date Oct 29, 2019

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/wait.h>

int x, y, z;

void *thread_main(void *params) {
    // Do the addition and return
    z = x + y;
    return NULL;
}

int main() {

    // x,y,z init
    x = 10;
    y = 20;
    z = 0;

    // Create the fork
    pid_t child_pid;
    child_pid = fork();

    // Check for failure
    if (child_pid < 0) {
        printf("Failed to fork.");
        return -1;
    }

    // Parent wait
    if (child_pid > 0) {
        wait(NULL);
    }

    // Child do the add and return
    if (child_pid == 0) {
        z = x + y;
        return 0;
    }

    // Now back in parent,
    // should print 0 since z here is not the same z in the child
    printf("Parent z after fork  : %d\n", z);

    // Create the thread process
    pthread_t thread_info;
    pthread_create(&thread_info, NULL, thread_main, NULL);

    // Wait for thread to finish, discarding retval
    pthread_join(thread_info, NULL);

    // Now print z again
    printf("Parent z after thread: %d\n", z);
}
