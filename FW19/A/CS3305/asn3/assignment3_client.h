#ifndef ASN3_ASSIGNMENT3_CLIENT_H
#define ASN3_ASSIGNMENT3_CLIENT_H

#include "assignment3_bank_account.h"
#include "assignment3_linked_list.h"

/**
 * Determines the type of client_action that is being performed.
 */
typedef enum client_action_type {
    DEPOSIT,
    WITHDRAW,
    TRANSFER
} client_action_type;

/**
 * Holds all the required information to queue a client action
 */
typedef struct client_action {
    client_action_type action_type;
    bank_account *from_account;
    bank_account *to_account;
    int amount;
} client_action;

/**
 * Identifies a client with their list of actions
 */
typedef struct client {
    int id;
    linked_list *actions;
} client;

#endif //ASN3_ASSIGNMENT3_CLIENT_H
