#ifndef ASN3_ASSIGNMENT3_LINKED_LIST_H
#define ASN3_ASSIGNMENT3_LINKED_LIST_H

/**
 * Points to its own internal data, and the next node in the list
 */
typedef struct linked_list_node {
    void *data;
    struct linked_list_node *next;
} linked_list_node;

/**
 * Points to the head and tail nodes of the list
 */
typedef struct linked_list {
    struct linked_list_node *head;
    struct linked_list_node *tail;
} linked_list;

/**
 * Create an empty linked list in memory
 * @return The newly created list
 */
linked_list *linked_list_create();

/**
 * Destroys a linked list, and all remaining internal nodes.
 * Does NOT destroy data within those nodes.
 * @param list The list to destroy.
 */
void linked_list_destroy(linked_list *list);

/**
 * Appends a new piece of data within a list by creating a new node for it.
 * @param list The list to append the data to
 * @param next_data The data to append to the list
 */
void linked_list_append(linked_list *list, void *next_data);

/**
 * Pops an element off the front of the list, destroying its node
 * @param list The list to pop an element from
 * @return A pointer to the data removed from the list
 */
void *linked_list_pop(linked_list *list);

#endif //ASN3_ASSIGNMENT3_LINKED_LIST_H
