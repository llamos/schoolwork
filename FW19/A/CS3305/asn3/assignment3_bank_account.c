#include "assignment3_bank_account.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

bank_account *bank_account_create(int id, char *type, int fee_deposit, int fee_withdraw, int fee_transfer, int transaction_allowance, int additional_transaction_fee, bool overdraft, int overdraft_fee) {
    // Allocate memory
    bank_account *account = malloc(sizeof(bank_account));

    // Assign provided values
    account->id = id;
    strcpy(account->type, type);
    account->fee_deposit = fee_deposit;
    account->fee_withdraw = fee_withdraw;
    account->fee_transfer = fee_transfer;
    account->transaction_allowance = transaction_allowance;
    account->additional_transaction_fee = additional_transaction_fee;
    account->overdraft = overdraft;
    account->overdraft_fee = overdraft_fee;

    // Initialize remaining fields to defaults
    account->balance = 0;
    account->transactions_used = 0;
    account->overdraft_tier = 0;
    account->mutex_lock = malloc(sizeof(pthread_mutex_t));
    pthread_mutex_init(account->mutex_lock, NULL);

    return account;
}

void bank_account_destroy(bank_account *account) {
    // No extra allocation to destroy, so simple free
    pthread_mutex_destroy(account->mutex_lock);
    free(account->mutex_lock);
    free(account);
}

bool bank_account_check_overdraft(bank_account *account, int new_balance, int current_overdraft_tier) {
    printf("Checking overdraft for account %s-%d (bal. %d)\n", account->type, account->id, account->balance);

    // calculate difference in tiers
    int new_overdraft_tier = ceil((float) new_balance / -500.0);
    if (new_overdraft_tier < 0)
        new_overdraft_tier = 0;
    int tier_changes = new_overdraft_tier - current_overdraft_tier;

    // If the new balance is positive, or is an increase from the old balance, we don't have to worry about overdraft
    if (new_balance >= 0 || new_balance > account->balance) {
        account->balance = new_balance;
        account->overdraft_tier = new_overdraft_tier;
        printf("Overdraft check skipped for account %s-%d with a new balance of %d\n", account->type, account->id,
               account->balance);
        return true;
    }

    // Check whether the account is allowed to overdraft to the new balance
    if ((new_balance < 0 && !account->overdraft) || new_balance < -5000) {
        printf("Overdraft check failed. Account %s-%d does not have sufficient overdraft protection for new balance %d.\n",
               account->type, account->id, new_balance);
        return false;
    }

    // If no change in tiers, no fee to apply
    if (tier_changes == 0) {
        account->balance = new_balance;
        account->overdraft_tier = new_overdraft_tier;
        printf("Overdraft check passed for account %s-%d with a new balance of %d\n", account->type, account->id,
               account->balance);
        return true;
    }

    // At this point, we must overdraft, so apply the charge and calculate again to ensure it is does not incur further charges
    int overdraft_fees = tier_changes * account->overdraft_fee;
    printf("Charging %d in overdraft fees to %s-%d\n", overdraft_fees, account->type, account->id);
    return bank_account_check_overdraft(account, new_balance - overdraft_fees, new_overdraft_tier);
}

bool bank_account_deposit(bank_account *account, int amount, bool transfer) {
    // Lock the account until after the deposit
    pthread_mutex_lock(account->mutex_lock);
    printf("Locked account %s-%d on thread %lx\n", account->type, account->id, pthread_self());

    printf("Depositing %d to account %s-%d, using transaction %d/%d\n", amount, account->type, account->id,
           account->transactions_used + 1, account->transaction_allowance);

    // Calculate the new target balance with fees deducted
    int target_balance = account->balance + amount - (transfer ? account->fee_transfer : account->fee_deposit);
    if (account->transactions_used + 1 > account->transaction_allowance) {
        target_balance -= account->additional_transaction_fee;
    }

    // And make sure the fees didn't outweigh the deposit
    if (bank_account_check_overdraft(account, target_balance, account->overdraft_tier)) {
        // If it succeeded, then the balance has been applied
        account->transactions_used++;
        printf("Deposit complete for account %s-%d with new balance %d\n", account->type, account->id, account->balance);

        pthread_mutex_unlock(account->mutex_lock);
        printf("Unlocked account %s-%d on thread %lx\n", account->type, account->id, pthread_self());
        return true;
    }

    // In this case, fees outweighed the deposit and were not covered by balance + overdraft
    pthread_mutex_unlock(account->mutex_lock);
    printf("Unlocked account %s-%d on thread %lx\n", account->type, account->id, pthread_self());
    return false;
}

bool bank_account_withdraw(bank_account *account, int amount, bool transfer) {
    // Lock the account until after the withdraw
    pthread_mutex_lock(account->mutex_lock);
    printf("Locked account %s-%d on thread %lx\n", account->type, account->id, pthread_self());

    printf("Withdrawing %d from account %s-%d (starting balance %d), using transaction %d/%d\n", amount, account->type,
           account->id, account->balance, account->transactions_used + 1, account->transaction_allowance);

    // Calculate the new target balance with fees
    int target_balance = account->balance - (amount + (transfer ? account->fee_transfer : account->fee_withdraw));
    if (account->transactions_used + 1 > account->transaction_allowance) {
        target_balance -= account->additional_transaction_fee;
    }

    // Make sure the transaction fits within balance + overdraft
    if (bank_account_check_overdraft(account, target_balance, account->overdraft_tier)) {
        // If it succeeded, then the balance has been applied
        account->transactions_used++;
        printf("Withdrawal complete for account %s-%d with new balance %d\n", account->type, account->id, account->balance);

        // Return a successful status
        pthread_mutex_unlock(account->mutex_lock);
        printf("Unlocked account %s-%d on thread %lx\n", account->type, account->id, pthread_self());
        return true;
    }

    // In this case, withdrawal were not covered by balance + overdraft
    pthread_mutex_unlock(account->mutex_lock);
    printf("Unlocked account %s-%d on thread %lx\n", account->type, account->id, pthread_self());
    return false;
}
