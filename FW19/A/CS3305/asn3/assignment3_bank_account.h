/**
 * @author Paul Norton 250957533
 * @date Nov 27 2019
 */

#ifndef ASN3_ASSIGNMENT3_BANK_ACCOUNT_H
#define ASN3_ASSIGNMENT3_BANK_ACCOUNT_H

#include <stdbool.h>
#include <pthread.h>

/**
 * Holds the information related to one bank account within the system.
 */
typedef struct bank_account {
    int id;
    char type[64];
    int balance;
    int fee_deposit;
    int fee_withdraw;
    int fee_transfer;
    int transaction_allowance;
    int transactions_used;
    int additional_transaction_fee;
    bool overdraft;
    int overdraft_fee;
    int overdraft_tier;
    pthread_mutex_t *mutex_lock;
} bank_account;

/**
 * Creates a new bank account by initializing all values to their provided/defaults
 * @param id The id number of the bank account
 * @param type A string representing the type of bank account
 * @param fee_deposit The fee applied to every deposit
 * @param fee_withdraw The fee applied to every withdrawal
 * @param fee_transfer The fee applied to every transfer (in and out)
 * @param transaction_allowance The number of transactions allowed before charging additional_transaction_fee
 * @param additional_transaction_fee The fee applied to each deposit, withdrawal and transfer after the transaction_allowance
 * @param overdraft Whether or not the account has overdraft protection
 * @param overdraft_fee The fee applied to each $500 of overdraft used
 * @returns A pointer to the newly created bank account
 */
bank_account *bank_account_create(int id, char *type, int fee_deposit, int fee_withdraw, int fee_transfer, int transaction_allowance, int additional_transaction_fee, bool overdraft, int overdraft_fee);

/**
 * Destroys a bank account, freeing the memory used
 * @param account The bank account to destroy
 */
void bank_account_destroy(bank_account *account);

/**
 * Deposits funds into the provided bank account.
 * @param account The account to deposit funds into.
 * @param amount The amount of funds to deposit.
 * @param transfer Whether or not these funds are coming from a transfer action
 * @return Whether or not the deposit was successful
 */
bool bank_account_deposit(bank_account *account, int amount, bool transfer);

/**
 * Withdraws funds from a provided bank account.
 * @param account The account to withdraw funds from.
 * @param amount The amount of funds to withdraw.
 * @param transfer Whether or not these funds are for a transfer action
 * @return Whether or not the withdrawal was succfessful
 */
bool bank_account_withdraw(bank_account *account, int amount, bool transfer);

#endif //ASN3_ASSIGNMENT3_BANK_ACCOUNT_H
