#include "assignment3_linked_list.h"

#include <stdlib.h>

linked_list_node *linked_list_node_create(void *data) {
    // Create a single node and point it to the data
    linked_list_node *node = malloc(sizeof(linked_list_node));
    node->data = data;
    return node;
}

void linked_list_node_destroy(linked_list_node *node) {
    // Simple memory free, data should be destroyed in its own way
    free(node);
}

linked_list *linked_list_create() {
    // simple empty list
    linked_list *list = malloc(sizeof(linked_list));
    list->head = NULL;
    list->tail = NULL;
    return list;
}

void linked_list_destroy(linked_list *list) {
    // Consume all the remaining node objects, hopefully the data was consumed before calling this, since it can't be dynamically freed
    while (linked_list_pop(list) != NULL);
    // Destroy the list
    free(list);
}

void linked_list_append(linked_list *list, void *next_data) {
    // Create the new node
    linked_list_node *next = malloc(sizeof(linked_list_node));
    next->data = next_data;
    next->next = NULL;

    if (list->head == NULL) {
        // If needed, initialize the list
        list->head = next;
        list->tail = next;
    } else {
        // Otherwise, append it to the tail and replace
        list->tail->next = next;
        list->tail = next;
    }
}

void *linked_list_pop(linked_list *list) {
    // Return null if empty
    if (list->head == NULL)
        return NULL;

    // Pop top element off, preserve data
    linked_list_node *node = list->head;
    void *data = node->data;
    list->head = node->next;

    // Clear tail if the list is now empty
    if (list->head == NULL)
        list->tail = NULL;

    // Destroy the node instance and return the data
    linked_list_node_destroy(node);
    return data;
}
