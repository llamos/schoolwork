#include <stdio.h>
#include "assignment3_bank_account.h"
#include "assignment3_linked_list.h"
#include "assignment3_client.h"
#include <string.h>
#include <stdlib.h>

bank_account *get_account_by_id(int id, linked_list *accounts) {
    // holder for return value
    bank_account *target_account = NULL;

    // start at head
    linked_list_node *node = accounts->head;
    do {
        // check each account for match
        bank_account *acc = node->data;
        if (acc->id == id)
            target_account = acc;
    } while ((node = node->next) != NULL && target_account == NULL);

    // return the one found
    return target_account;
}

bank_account *parse_account(int id) {
    // grab account type
    strtok(NULL, " ");
    char *type = strtok(NULL, " ");

    // and deposit fee
    strtok(NULL, " ");
    int deposit_fee = atoi(strtok(NULL, " "));

    // withdrawal fee
    strtok(NULL, " ");
    int withdrawal_fee = atoi(strtok(NULL, " "));

    // transfer fee
    strtok(NULL, " ");
    int transfer_fee = atoi(strtok(NULL, " "));

    // transaction limit and fee
    strtok(NULL, " ");
    int transaction_limit = atoi(strtok(NULL, " "));
    int additional_transaction_fee = atoi(strtok(NULL, " "));

    // and overdraft info
    strtok(NULL, " ");
    bool overdraft_allowed = *strtok(NULL, " ") == 'Y';
    int overdraft_fee = 0;
    if (overdraft_allowed)
        overdraft_fee = atoi(strtok(NULL, " "));

    // Now create the account and return it
    return bank_account_create(id, type, deposit_fee, withdrawal_fee, transfer_fee,
                               transaction_limit, additional_transaction_fee,
                               overdraft_allowed, overdraft_fee);
}

client *parse_depositor(int id, linked_list *accounts) {
    // Pull all the actions
    linked_list *actions = linked_list_create();
    char *token;
    while ((token = strtok(NULL, " ")) != NULL) {
        // get deposit info
        int account_number = atoi(strtok(NULL, " ") + 1);
        int amount = atoi(strtok(NULL, " "));

        // Create the deposit action
        client_action *deposit = malloc(sizeof(client_action));
        deposit->action_type = DEPOSIT;
        deposit->amount = amount;
        deposit->to_account = get_account_by_id(account_number, accounts);

        // add it to the list
        linked_list_append(actions, deposit);
    }

    // create the depositor from the above info and return it
    client *depositor = malloc(sizeof(client));
    depositor->actions = actions;
    depositor->id = id;
    return depositor;
}

client *parse_client(int id, linked_list *accounts) {
    // Pull all the actions
    linked_list *actions = linked_list_create();
    char *token;
    while ((token = strtok(NULL, " ")) != NULL) {
        // determine action type
        client_action *action = malloc(sizeof(client_action));
        if (token[0] == 'd') {
            // deposit
            int account_number = atoi(strtok(NULL, " ") + 1);
            int amount = atoi(strtok(NULL, " "));

            // create deposit action
            action->action_type = DEPOSIT;
            action->to_account = get_account_by_id(account_number, accounts);
            action->amount = amount;
        } else if (token[0] == 'w') {
            // withdrawal
            int account_number = atoi(strtok(NULL, " ") + 1);
            int amount = atoi(strtok(NULL, " "));

            // create withdrawal action
            action->action_type = WITHDRAW;
            action->from_account = get_account_by_id(account_number, accounts);
            action->amount = amount;
        } else if (token[0] == 't') {
            // transfer
            int from_account_number = atoi(strtok(NULL, " ") + 1);
            int to_account_number = atoi(strtok(NULL, " ") + 1);
            int amount = atoi(strtok(NULL, " "));

            // create transfer action
            action->action_type = TRANSFER;
            action->from_account = get_account_by_id(from_account_number, accounts);
            action->to_account = get_account_by_id(to_account_number, accounts);
            action->amount = amount;
        }

        // add it to the list
        linked_list_append(actions, action);
    }

    // create the client from the above info and return it
    client *new_client = malloc(sizeof(client));
    new_client->actions = actions;
    new_client->id = id;
    return new_client;
}

void *process_client(void *client_void) {
    // cast the parameter
    client *client = client_void;

    // loop through each action
    client_action *action;
    while ((action = linked_list_pop(client->actions)) != NULL) {
        // process it based on its type
        if (action->action_type == DEPOSIT) {
            bank_account_deposit(action->to_account, action->amount, false);
        } else if (action->action_type == WITHDRAW) {
            bank_account_withdraw(action->from_account, action->amount, false);
        } else if (action->action_type == TRANSFER) {
            // only process the deposit if the withdrawal succeeds
            if (bank_account_withdraw(action->from_account, action->amount, true)) {
                bank_account_deposit(action->to_account, action->amount, true);
            }
        }

        // delete the action now that it's complete so it isn't orphaned
        free(action);
    }

    // Now that the client is done processing, destroy it too
    free(client);

    return 0;
}

void process_clients(linked_list *clients) {
    // for storing the threads, for rejoin at the end
    linked_list *threads = linked_list_create();

    // loop through each client
    client *client;
    while ((client = linked_list_pop(clients)) != NULL) {
        // create a new thread for each one to process its transactions
        pthread_t thread_info;
        pthread_create(&thread_info, NULL, process_client, client);
        linked_list_append(threads, &thread_info);
    }

    // now wait for all clients to complete
    pthread_t *thread_info;
    while ((thread_info = linked_list_pop(threads)) != NULL) {
        pthread_join(*thread_info, NULL);
    }
    linked_list_destroy(threads);
}

int main() {
    // Open the input file for reading
    FILE *input_file = fopen("assignment_3_input_file.txt", "r");
    if (input_file == NULL) {
        printf("Unable to open input file assignment_3_input_file.txt\n");
        return 1;
    }

    // Create linked lists for each type of input
    linked_list *accounts = linked_list_create();
    linked_list *depositors = linked_list_create();
    linked_list *clients = linked_list_create();

    // Loop through each line of input
    char buf[512];
    while (fgets(buf, 512, input_file) != NULL) {
        // Parse each line based on the type identifier at the start of the line
        char *identifier = strtok(buf, " ");

        if (identifier[0] == 'a') {
            // New account
            int id = atoi(identifier + 1);
            linked_list_append(accounts, parse_account(id));
        } else if (identifier[0] == 'd') {
            // New depositor
            int id = atoi(identifier + 3);
            linked_list_append(depositors, parse_depositor(id, accounts));
        } else if (identifier[0] == 'c') {
            // New client
            int id = atoi(identifier + 1);
            linked_list_append(clients, parse_client(id, accounts));
        }
    }

    // close the input file, we're done reading
    fclose(input_file);

    // Now process all the depositors in their own threads
    process_clients(depositors);
    linked_list_destroy(depositors);

    // then the clients in the same way
    process_clients(clients);
    linked_list_destroy(clients);

    // now all transactions are done, so print the final balances to an output file
    // start by opening the output file
    FILE *output_file = fopen("assignment_3_output_file.txt", "w+");

    // loop through each account
    bank_account *account;
    while ((account = linked_list_pop(accounts)) != NULL) {
        // print the balance to the file
        fprintf(output_file, "a%d type %s %d\n", account->id, account->type, account->balance);
        // and destroy the account now that we're done with it
        bank_account_destroy(account);
    }

    // destroy the list of accounts, and close the file
    fclose(output_file);
    linked_list_destroy(accounts);
}
