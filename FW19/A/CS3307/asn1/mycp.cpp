/// @author Paul Norton
/// @date September 26, 2019
/// Copies a file to a new location.
/// Used by `mycp source new_location`.

#include <iostream>
#include <cstring>
#include "file_manager.h"

int main(int argc, char* argv[]) {

    // Make sure we have an input and output file name
    if (argc < 3) {
        std::cerr << argv[0] << " source destination" << std::endl;
        return EINVAL;
    }

    // Create a manager instance for the input file
    FileManager fm(argv[1]);
    if (fm.getError() != 0) {
        std::cerr << fm.getErrorString() << std::endl;
        return fm.getError();
    }

    // Open the destination file descriptor
    std::fstream out;
    out.open(argv[2], std::ios::binary | std::ios::out);

    // Make sure opening that succeeded
    if (!out.is_open()) {
        int error = errno;
        std::cerr << strerror(error) << std::endl;
        return error;
    }

    // Perform the copy
    fm.dump(&out);
    if (fm.getError() != 0) {
        std::cerr << fm.getErrorString() << std::endl;
        return fm.getError();
    }

    // Close the fd
    out.close();
    return 0;

}
