/// @author Paul Norton
/// @date September 26, 2019
/// Moves a file from one location on disk to another.
/// Used by `mymv source dest`.

#include <iostream>
#include "file_manager.h"

int main(int argc, char* argv[]) {

    // Make sure we have an input and output file name
    if (argc < 3) {
        std::cerr << argv[0] << " source destination" << std::endl;
        return EINVAL;
    }

    // Create a manager instance for the input file
    FileManager fm(argv[1]);
    if (fm.getError() != 0) {
        std::cerr << fm.getErrorString() << std::endl;
        return fm.getError();
    }

    // Perform the move
    fm.rename(argv[2]);
    if (fm.getError() != 0) {
        std::cerr << fm.getErrorString() << std::endl;
        return fm.getError();
    }

}
