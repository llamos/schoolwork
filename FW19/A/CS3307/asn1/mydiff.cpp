/// @author Paul Norton
/// @date September 26, 2019
/// Determines if two files are the same.
/// Does not determine differences.
/// Used by `mydiff source_1 source_2`.

#include <iostream>
#include "file_manager.h"

int main(int argc, char* argv[]) {

    // Make sure we have an input and output file name
    if (argc < 3) {
        std::cerr << argv[0] << " source comparator" << std::endl;
        return EINVAL;
    }

    // Create a manager instance for the input file
    FileManager fm(argv[1]);
    if (fm.getError() != 0) {
        std::cerr << fm.getErrorString() << std::endl;
        return fm.getError();
    }

    // Create a manager instance for the other file
    FileManager other(argv[2]);
    if (other.getError() != 0) {
        std::cerr << other.getErrorString() << std::endl;
        return other.getError();
    }

    // Compare the two
    bool result;
    fm.compare(&other, &result);
    if (fm.getError() != 0) {
        std::cerr << fm.getErrorString() << std::endl;
        return fm.getError();
    }
    if (other.getError() != 0) {
        std::cerr << other.getErrorString() << std::endl;
        return other.getError();
    }

    if (result) {
        std::cout << "Files are the same" << std::endl;
    } else {
        std::cout << "Files are not the same" << std::endl;
    }

}
