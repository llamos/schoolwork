/// @author Paul Norton
/// @date September 26, 2019
/// Removes a file from disk.
/// Used by `myrm source`

#include <iostream>
#include "file_manager.h"

int main(int argc, char* argv[]) {

    // Make sure we have an input and output file name
    if (argc < 2) {
        std::cerr << argv[0] << " source" << std::endl;
        return EINVAL;
    }

    // Create a manager instance for the input file
    FileManager fm(argv[1]);
    if (fm.getError() != 0) {
        std::cerr << fm.getErrorString() << std::endl;
        return fm.getError();
    }

    // Perform the deletion
    fm.remove();
    if (fm.getError() != 0) {
        std::cerr << fm.getErrorString() << std::endl;
        return fm.getError();
    }

}
