/// @author Paul Norton
/// @date September 26, 2019
/// This file manager provides utilities for reading, altering and deleting files.

#ifndef FILE_MANAGER_H
#define FILE_MANAGER_H

#include <sys/stat.h>
#include <sys/types.h>
#include <vector>
#include <fstream>
#include <ostream>

class FileManager {

public:

    /// Initializes the file manager with the information from the file given by file_name
    /// \param file_name The name of the underlying file to reference by this manager
    FileManager(char *file_name);

    /// Frees all resources used by this file manager instance
    ~FileManager();

    /// Gets the relative name of the file referenced by this file manager instance. Equivalent to the name passed in the constructor.
    /// \return A pointer to a null-terminated string with the name of the file.
    char *getName();

    /// If any error occurred in the constructor, or subsequent function, this will return the applicable error code.
    /// \return The error code of the last called function, or zero.
    int getError();

    /// Returns the standard string representation of the error code provided by getError()
    /// \return The strerror value of the last error code
    char *getErrorString();

    /// Clears the error flag, allowing further operations to take place.
    void clearError();

    /// Gets the type of the underlying file (file, dir, etc.)
    /// \return The mode_t of the file masked to the type bits
    mode_t getType();

    /// Gets the size of the entire file on disk
    /// \return The size of the file
    off_t getSize();

    /// Gets the bit representation of the file's permissions
    /// \return The mode_t of the file masked to the permission bits
    mode_t getPermissions();

    /// Gets the time the file was last accessed by another program
    /// \return A timespec of when the file was last accessed
    timespec getAccessTime();

    /// Gets the time the file was last written to by another program
    /// \return A timespec of when the file was last written to
    timespec getWriteTime();

    /// Gets the time the file metadata was last updated by another program
    /// \return A timespec of when the file metadata was last updated
    timespec getStatusChangeTime();

    /// Gets the block size of the file on disk
    /// \return The block size of the file on disk
    blksize_t getBlockSize();

    /// Returns the UID of the file's owner
    /// \return The UID of the file's owner
    uid_t getUID();

    /// Returns the GID of the file's group
    /// \return The GID of the file's group
    gid_t getGID();

    /// Returns the name of the user that owns the file
    /// \return The username of the owner
    char *getUserName();

    /// Returns the name of the group that owns the file
    /// \return The name of the owner group
    char *getGroupName();

    /// Gets a list of the children to this file, if this file is a directory.
    /// Must be expanded via expand(), otherwise this will return an empty vector.
    /// \return The list of children for this directory
    std::vector<FileManager *> *getChildren();

    /// Determines all children of this file, if this file is a directory.
    /// \return 0 if successful, -1 and getError() is set otherwise.
    int expand();

    /// Dumps the contents of this file into the provided stream.
    /// \param stream The stream to write to.
    /// \return 0 if successful, -1 and getError() is set otherwise.
    int dump(std::ostream *stream);

    /// Renames the file on disk.
    /// \param new_name The new name of the file
    /// \return 0 if successful, -1 and getError() is set otherwise.
    int rename(char *new_name);

    /// Removes the file from disk. After calling this, no further operations on the file will be successful and the data is not preserved.
    /// \return 0 if successful, -1 and getError() is set otherwise.
    int remove();

    /// Compares this file to another provided file, returning true iff they have the same contents.
    /// \return 0 if successful, -1 and getError() is set otherwise.
    int compare(FileManager *other, bool *result);

private:

    char name[256];
    int error;

    mode_t type;
    off_t size;
    mode_t perms;
    timespec atime;
    timespec wtime;
    timespec ctime;
    blksize_t blksize;

    uid_t owner;
    gid_t group;
    char *oname;
    char *gname;

    bool expanded;
    std::vector<FileManager *> *children;

};

#endif
