/// @author Paul Norton
/// @date September 26, 2019
/// This file manager implements file_manager.h, providing utilities for reading, altering and deleting files.

#include <cerrno>
#include <pwd.h>
#include <grp.h>
#include <unistd.h>
#include <cstring>
#include <iostream>
#include <dirent.h>
#include <sys/types.h>
#include "file_manager.h"

/// Initializes the file manager with the information from the file given by file_name
/// \param file_name The name of the underlying file to reference by this manager
FileManager::FileManager(char *file_name) {
    // Pull the file info
    struct stat st;
    int stat_result = stat(file_name, &st);
    if (stat_result == -1)
        this->error = errno;

    // Now map all the required fields
    if (stat_result == 0) {
        this->type = st.st_mode & S_IFMT;
        this->size = st.st_size;
        this->owner = st.st_uid;
        this->group = st.st_gid;
        this->perms = st.st_mode & (S_IRWXU | S_IRWXG | S_IRWXO);
        this->atime = st.st_atim;
        this->wtime = st.st_mtim;
        this->ctime = st.st_ctim;
        this->blksize = st.st_blksize;

        // Pull the user name and group name
        struct passwd *owner_info = getpwuid(this->owner);
        struct group *group_info = getgrgid(this->group);
        this->oname = owner_info->pw_name;
        this->gname = group_info->gr_name;

        // blank children until expand()-ed
        this->expanded = false;
        this->children = new std::vector<FileManager *>();

        // store the file name passed in
        strcpy(this->name, file_name);

        // No error occurred
        this->error = 0;
    }
}

/// Frees all resources used by this file manager instance
FileManager::~FileManager() {
    // If we've expanded to children, need to delete the children too
    if (expanded) {
        for (FileManager *child : *this->children) {
            delete child;
        }
    }

    // Mark the children vector for deletion
    delete this->children;
}

/// Gets the relative name of the file referenced by this file manager instance. Equivalent to the name passed in the constructor.
/// \return A pointer to a null-terminated string with the name of the file.
char *FileManager::getName() {
    return this->name;
}

/// If any error occurred in the constructor, or subsequent function, this will return the applicable error code.
/// \return The error code of the last called function, or zero.
int FileManager::getError() {
    return this->error;
}

/// Returns the standard string representation of the error code provided by getError()
/// \return The strerror value of the last error code
char *FileManager::getErrorString() {
    return strerror(this->error);
}

/// Clears the error flag, allowing further operations to take place.
void FileManager::clearError() {
    this->error = 0;
}

/// Gets the type of the underlying file (file, dir, etc.)
/// \return The mode_t of the file masked to the type bits
mode_t FileManager::getType() {
    return this->type;
}

/// Gets the size of the entire file on disk
/// \return The size of the file
off_t FileManager::getSize() {
    return this->size;
}

/// Gets the bit representation of the file's permissions
/// \return The mode_t of the file masked to the permission bits
mode_t FileManager::getPermissions() {
    return this->perms;
}

/// Gets the time the file was last accessed by another program
/// \return A timespec of when the file was last accessed
timespec FileManager::getAccessTime() {
    return this->atime;
}

/// Gets the time the file was last written to by another program
/// \return A timespec of when the file was last written to
timespec FileManager::getWriteTime() {
    return this->wtime;
}

/// Gets the time the file metadata was last updated by another program
/// \return A timespec of when the file metadata was last updated
timespec FileManager::getStatusChangeTime() {
    return this->ctime;
}

/// Gets the block size of the file on disk
/// \return The block size of the file on disk
blksize_t FileManager::getBlockSize() {
    return this->blksize;
}

/// Returns the UID of the file's owner
/// \return The UID of the file's owner
uid_t FileManager::getUID() {
    return this->owner;
}

/// Returns the GID of the file's group
/// \return The GID of the file's group
gid_t FileManager::getGID() {
    return this->group;
}

/// Returns the name of the user that owns the file
/// \return The username of the owner
char *FileManager::getUserName() {
    return this->oname;
}

/// Returns the name of the group that owns the file
/// \return The name of the owner group
char *FileManager::getGroupName() {
    return this->gname;
}

/// Gets a list of the children to this file, if this file is a directory.
/// Must be expanded via expand(), otherwise this will return an empty vector.
/// \return The list of children for this directory
std::vector<FileManager *> *FileManager::getChildren() {
    if (!expanded)
        this->error = ENODATA;

    return this->children;
}

/// Determines all children of this file, if this file is a directory.
/// \return 0 if successful, -1 and getError() is set otherwise.
int FileManager::expand() {
    // Make sure we aren't in an error state
    if (this->error != 0)
        return -1;

    // Make sure we have a directory
    if (this->type != S_IFDIR) {
        this->error = ENOTSUP;
        return -1;
    }

    // If it's already expanded, quit early
    if (expanded)
        return 0;

    // Open the directory
    DIR *dir = opendir(this->name);
    if (dir == nullptr) {
        this->error = errno;
        return -1;
    }

    // Read the directory children
    struct dirent *entry;
    while ((entry = readdir(dir)) != nullptr) {
        // Concatenate the parent folder name with the child's
        char buf[256];
        strcpy(buf, this->name);
        strcat(buf, "/");
        strcat(buf, entry->d_name);
        // Create the child instance
        children->push_back(new FileManager(buf));
    }

    // Check if we terminated the loop before getting all the children
    // Here, ENOENT is expected when the end of the dir is reached
    int possible_error = errno;
    if (possible_error != ENOENT) {
        this->error = possible_error;
        return -1;
    }

    // Close the dir
    closedir(dir);

    // Mark the expansion done, for future expand() and getChildren() calls
    expanded = true;
    return 0;
}

/// Dumps the contents of this file into the provided stream.
/// \param stream The stream to write to.
/// \return 0 if successful, -1 and getError() is set otherwise.
int FileManager::dump(std::ostream *out) {
    // Make sure we aren't in an error state
    if (this->error != 0)
        return -1;

    // Make sure it's a file, not a directory
    if (this->type != S_IFREG) {
        this->error = ENOTSUP;
        return -1;
    }

    // Open the file for read
    std::fstream in;
    in.open(this->name, std::ios::in | std::ios::binary);

    // Make sure opening the file worked
    if (!in.is_open()) {
        this->error = ENODATA;
        return -1;
    }

    // Make our buffer
    int remaining_bytes = this->size;
    char buf[this->blksize];

    // Copy the file contents
    while (in.read(buf, remaining_bytes > this->blksize ? this->blksize : remaining_bytes) && remaining_bytes > 0) {
        int read = in.gcount();
        out->write(buf, read);
        remaining_bytes -= read;
    }

    // Close the input file and return
    in.close();
    return 0;
}

/// Renames the file on disk.
/// \param new_name The new name of the file
/// \return 0 if successful, -1 and getError() is set otherwise.
int FileManager::rename(char *new_name) {
    // Make sure we aren't in an error state
    if (this->error != 0)
        return -1;

    // Move/rename the file, updating error if it fails
    if (std::rename(this->name, new_name) == -1) {
        this->error = errno;
        return -1;
    }

    // Move worked, so return
    return 0;
}

/// Removes the file from disk. After calling this, no further operations on the file will be successful and the data is not preserved.
/// \return 0 if successful, -1 and getError() is set otherwise.
int FileManager::remove() {
    // Make sure we aren't in an error state
    if (this->error != 0)
        return -1;

    // Delete the file, updating error if it fails
    if (unlink(this->name) == -1) {
        this->error = errno;
        return -1;
    }

    // Delete worked, so return
    return 0;
}

/// Compares this file to another provided file, returning true iff they have the same contents.
/// \return 0 if successful, -1 and getError() is set otherwise.
int FileManager::compare(FileManager *other, bool *result) {
    // Make sure we aren't in an error state
    if (this->error != 0 || other->error != 0)
        return -1;

    // Make sure it's two files, not directories
    if (this->type != S_IFREG || other->type != S_IFREG) {
        this->error = ENOTSUP;
        return -1;
    }

    // Check size first, wrong size is an easy cancel
    if (this->size != other->size) {
        *result = false;
        return -1;
    }

    // Open the files for read
    std::fstream this_in,
            other_in;
    this_in.open(this->name, std::ios::in | std::ios::binary);
    other_in.open(other->name, std::ios::in | std::ios::binary);

    // Make sure opening the files worked
    if (!this_in.is_open()) {
        this->error = ENODATA;
        if (other_in.is_open())
            other_in.close();
        return -1;
    }
    if (!other_in.is_open()) {
        this->error = ENODATA;
        if (this_in.is_open())
            this_in.close();
        return -1;
    }

    // Make our buffers
    int remaining_bytes = this->size;
    char this_buf[this->blksize],
            other_buf[this->blksize];

    // Compare the file contents
    bool same = true;
    while (this_in.read(this_buf, remaining_bytes > this->blksize ? this->blksize : remaining_bytes) &&
           remaining_bytes > 0 && same) {
        other_in.read(other_buf, remaining_bytes > this->blksize ? this->blksize : remaining_bytes);

        // check the bytes
        std::streamsize i;
        for (i = 0; i < this_in.gcount(); i++) {
            if (this_buf[i] != other_buf[i]) {
                same = false;
                break;
            }
        }
    }

    // Mark the result of comparison
    *result = same;

    // Close the input files and return
    this_in.close();
    other_in.close();
    return 0;
}
