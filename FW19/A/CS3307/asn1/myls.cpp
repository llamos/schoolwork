/// @author Paul Norton
/// @date September 26, 2019
/// Lists files in a directory, or the single provided file if regular.
/// Also can print extended information with the -l switch.
/// Used by `myls source`.

#include <iostream>
#include <cstring>
#include "file_manager.h"

int main(int argc, char *argv[]) {

    // Read the cmdline arguments
    char *input_file = nullptr;
    bool extended = false;
    for (int i = 1; i < argc; i++) {
        char *arg = argv[i];
        if (strcmp(arg, "-l") == 0)
            extended = true;
        else
            input_file = arg;
    }

    // Make sure we have a file, if none was provided
    if (input_file == nullptr)
        input_file = (char*) ".";

    // Create a manager instance for the input file
    FileManager fm(input_file);
    if (fm.getError() != 0) {
        std::cerr << fm.getErrorString() << std::endl;
        return fm.getError();
    }

    // Check if it's a single file
    if (fm.getType() == S_IFREG) {
        // Just print the one file name
        std::cout << fm.getName() << std::endl;
        return 0;
    }

    // Expand the directory to load children
    fm.expand();
    if (fm.getError() != 0) {
        std::cerr << fm.getErrorString() << std::endl;
        return fm.getError();
    }

    // Print all the children
    std::vector<FileManager *> *children = fm.getChildren();
    for (FileManager *child : *children) {
        if (extended) {
            std::cout << ((child->getPermissions() & S_IRUSR) == S_IRUSR ? "r" : "-"); // ur
            std::cout << ((child->getPermissions() & S_IWUSR) == S_IWUSR ? "w" : "-"); // uw
            std::cout << ((child->getPermissions() & S_IXUSR) == S_IXUSR ? "x" : "-"); // ux
            std::cout << ((child->getPermissions() & S_IRGRP) == S_IRGRP ? "r" : "-"); // gr
            std::cout << ((child->getPermissions() & S_IWGRP) == S_IWGRP ? "w" : "-"); // gw
            std::cout << ((child->getPermissions() & S_IXGRP) == S_IXGRP ? "x" : "-"); // gx
            std::cout << ((child->getPermissions() & S_IROTH) == S_IROTH ? "r" : "-"); // or
            std::cout << ((child->getPermissions() & S_IWOTH) == S_IWOTH ? "w" : "-"); // ow
            std::cout << ((child->getPermissions() & S_IXOTH) == S_IXOTH ? "x" : "-"); // ox
            std::cout << " " << child->getUserName(); // owner
            std::cout << " " << child->getGroupName(); // group
            std::cout << " " << child->getSize(); // file size
            std::cout << " " << child->getName(); // file name
            std::cout << std::endl;
        } else {
            // Just the name
            std::cout << child->getName() << std::endl;
        }
    }

}
