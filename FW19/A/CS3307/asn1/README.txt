Hello TA who is tasked to mark this project,

I hope you find the code easy to read.

If you use CMake,
    This project should be very easy to build via the CMakeLists.txt file provided.
    It was compiled with no extra options.

If you use the Makefile,
    This project should be very easy to build via `make all`.
    If that fails, you can build individual artifacts with
        * `make mycat`
        * `make mycp`
        * `make mydiff`
        * `make myls`
        * `make mymv`
        * `make myrm`
        * `make mystat`
    You can also quickly remove the generated files with `make clean`.

If you compile manually with g++,
    I feel bad for you.
    But, it's easy to compile each binary with no extra command line options.
    Each binary depends on file_manager.h and file_manager.cpp, with no other dependencies.
    For example, `g++ -o mycp mycp.cpp file_manager.h file_manager.cpp`.

Sincerely,
Paul Norton
250957533
pnorton4@uwo.ca
