/// @author Paul Norton
/// @date September 26, 2019
/// Displays extended file information for the provided file.
/// Used by `mystat source`.

#include <iostream>
#include "file_manager.h"

int main(int argc, char* argv[]) {

    // Make sure we have an input and output file name
    if (argc < 2) {
        std::cerr << argv[0] << " source" << std::endl;
        return EINVAL;
    }

    // Create a manager instance for the input file
    FileManager fm(argv[1]);
    if (fm.getError() != 0) {
        std::cerr << fm.getErrorString() << std::endl;
        return fm.getError();
    }

    // Start printing all the file info
    std::cout << "  File: " << fm.getName() << std::endl;
    std::cout << "  Size: " << fm.getSize() << std::endl;
    std::cout << "Blksiz: " << fm.getBlockSize() << std::endl;

    // Perms
    std::cout << "Access: ";
    std::cout << ((fm.getPermissions() & S_IRUSR) == S_IRUSR ? "r" : "-"); // ur
    std::cout << ((fm.getPermissions() & S_IWUSR) == S_IWUSR ? "w" : "-"); // uw
    std::cout << ((fm.getPermissions() & S_IXUSR) == S_IXUSR ? "x" : "-"); // ux
    std::cout << ((fm.getPermissions() & S_IRGRP) == S_IRGRP ? "r" : "-"); // gr
    std::cout << ((fm.getPermissions() & S_IWGRP) == S_IWGRP ? "w" : "-"); // gw
    std::cout << ((fm.getPermissions() & S_IXGRP) == S_IXGRP ? "x" : "-"); // gx
    std::cout << ((fm.getPermissions() & S_IROTH) == S_IROTH ? "r" : "-"); // or
    std::cout << ((fm.getPermissions() & S_IWOTH) == S_IWOTH ? "w" : "-"); // ow
    std::cout << ((fm.getPermissions() & S_IXOTH) == S_IXOTH ? "x" : "-"); // ox
    std::cout << " U(" << fm.getUserName() << "/" << fm.getUID() << ")"; // owner
    std::cout << " G(" << fm.getGroupName() << "/" << fm.getGID() << ")"; // group
    std::cout << std::endl;

    // Times
    char out[20];
    time_t time = fm.getWriteTime().tv_sec;
    strftime(out, sizeof(out), "%Y-%m-%d %H:%M:%S", localtime(&time));
    std::cout << " MTime: " << out << std::endl;
    time = fm.getAccessTime().tv_sec;
    strftime(out, sizeof(out), "%Y-%m-%d %H:%M:%S", localtime(&time));
    std::cout << " ATime: " << out << std::endl;
    time = fm.getStatusChangeTime().tv_sec;
    strftime(out, sizeof(out), "%Y-%m-%d %H:%M:%S", localtime(&time));
    std::cout << " CTime: " << out << std::endl;

}
