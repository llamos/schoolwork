/// @author Paul Norton
/// @date September 26, 2019
/// Dumps file contents to stdout.
/// Used by `mycat source [source2 [...]]`.

#include <iostream>
#include "file_manager.h"

int main(int argc, char* argv[]) {

    // Make sure we have an input and output file name
    if (argc < 2) {
        std::cerr << argv[0] << " source [source2 [...]]" << std::endl;
        return EINVAL;
    }

    // Loop through each file provided
    for (int i = 1; i < argc; i++) {
        // Create a manager instance for the input file
        FileManager fm(argv[i]);
        if (fm.getError() != 0) {
            std::cerr << fm.getErrorString() << std::endl;
            return fm.getError();
        }

        // Dump the contents to stdout
        fm.dump(&std::cout);
        if (fm.getError() != 0) {
            std::cerr << fm.getErrorString() << std::endl;
            return fm.getError();
        }
    }

}
