%! TEX root = ../notes.tex

\textbf{Proposition}: 
Let $(R, +, \cdot, 0)$ be a ring and $a,b \in R$.
Then,
\begin{enumerate}[label=(\arabic*)]
    \item $a \cdot 0 = 0 = 0 \cdot a$
    \item $(-a \cdot b) = - a \cdot b = (a \cdot (-b))$
    \item $(-a) \cdot (-b) = a \cdot b$
\end{enumerate}

\textbf{Proof}:
\begin{enumerate}[label=(\arabic*)]
    \item Let $a \in R$. We have $a \cdot 0 = a(0 + 0) = a \cdot 0 + a \cdot 0$.
    Since $(R, +, 0)$ is an abelian group, we can use cancellation laws:
    $a \cdot 0 = 0$.
    \item Let $a, b \in R$. $- a \cdot b$ is the unique element in $R$
    such that $a \cdot b + (- a \cdot b) = 0$.
    So it suffices to show that $a \cdot b + (-a) \cdot b = 0$.
    By distribution, 
    $a \cdot b + (-a) \cdot b = (a + (-a)) \cdot b = 0 \cdot b = 0$.
    \item Let $a,b \in R$. 
    $(-a) \cdot (-b) = -(a \cdot (-b)) = -(- a \cdot b) = a \cdot b$.
\end{enumerate}

\subsubsection{Ring Homomorphisms}

\textbf{Definition}:
Let $(R, +, \cdot, 0), (R', +', \cdot', 0')$ be rings.
A \underline{ring homomorphism} 
from $(R, +, \cdot, 0)$ to $(R', +', \cdot', 0')$
is a function $f : R \rightarrow R'$ such that
\begin{enumerate}[label=(\arabic*)]
    \item $f(a+b) = f(a) +' f(b)$ for all $a,b \in R$
    \item $f(a \cdot b) = f(a) \cdot' f(b)$ for all $a,b \in R$
\end{enumerate}

\textbf{Examples}:
\begin{itemize}
    \item $q_n : \mathbb{Z} \rightarrow \mathbb{Z}/n \mid q_n(k) = k \text{ mod } n$
    is a ring homomorphism.
    \item $i : \mathbb{Z} \rightarrow \mathbb{R} \mid i(k) = k$
    is a ring homomorphism.
    \item Note that $2\mathbb{Z}$ is a ring.
    $t : \mathbb{Z} \rightarrow 2\mathbb{Z} \mid t(k) = 2k$
    is not a homomorphism, as it does not satisfy (2).
\end{itemize}

\textbf{Definition}: A ring homomorphism $f : R \rightarrow R'$ is:
\begin{enumerate}
    \item a monomorphism if $f$ is injective
    \item an epimorphism if $f$ is surjective
    \item an isomorphism if $f$ is bijective
\end{enumerate}

\textbf{Examples}:
\begin{itemize}
    \item $q_n$ is an epimorphism
    \item $i$ is a monomorphism
\end{itemize}

\subsubsection{Commutativity and Unity}

\textbf{Definition}:
A ring $(R, +, \cdot, 0)$ is
\begin{enumerate}[label=(\arabic*)]
    \item \underline{commutative} if $a \cdot b = b \cdot a$ for all $a,b \in R$
    \item \underline{with unity} if there exists an element 
    $1 \in R$ such that $a \cdot 1 = a = 1 \cdot a$ for all $a \in R$.
\end{enumerate}

\textbf{Examples}:
\begin{tabular}{r | c | c}
    Ring & Comm. & Unity \\
    \hline 
    $(\mathbb{Z}, +, \cdot, 0)$ & Yes & 1 \\
    $(\mathbb{R}, +, \cdot,0 )$ & Yes & 1 \\
    $(M_{n \times n}(\mathbb{R}), +, \cdot, 0_{n \times n})$ & No unless $n = 1$ & $I_{n \times n}$ \\
    $(\{0\}, +, \cdot, 0)$ & Yes & 0 \\
    $(\mathbb{R}[x], +, \cdot, 0)$ & Yes & $x^0$ \\
    $(2\mathbb{Z}, +, \cdot, 0)$ & Yes & No
\end{tabular}

\textbf{Definition}:
Let $R$ be a ring with unity $1 \neq 0$.
A non-zero element $a \in R$ is a \underline{unit}
if $b \in R$ such that $a \cdot b = 1$.

\textbf{Definition}:
A \underline{field} is a commutative ring
with unity $1 \neq 0$ such that 
every non-zero element is a unit.

\textbf{Examples}:
\begin{itemize}
    \item $(\mathbb{Z}, +, \cdot, 0)$: units are $1, -1$; not a field
    \item $(\mathbb{Q}, +, \cdot, 0)$: units are $\mathbb{Q}^\times$; is a field
    \item $(\mathbb{R}, +, \cdot, 0)$: units are $\mathbb{R}^\times$; is a field
    \item $(\mathbb{Z}/2, +_2, \cdot_2, 0)$: units are $1$; is a field
    \item $(\mathbb{Z}/3, +_3, \cdot_3, 0)$: units are $1,2$; is a field
    \item $(\mathbb{Z}/4, +_4, \cdot_4, 0)$: units are $1,3$; not a field
    \item $(\mathbb{Z}/12, +_12, \cdot_12, 0)$: units are $1,5,7,11$; not a field
\end{itemize}

\textbf{Proposition}:
An element $a \in \mathbb{Z}/n$ is a unit iff $\text{gcd}(a,n) = 1$.

\textbf{Proof $\Rightarrow$}: \textit{By contrapositive}\\
Suppose $d = \text{gcd}(a,n) > 1$.
Then $d$ divides $ka$ for all $k$ in $\mathbb{Z}/n$.
Since $d$ also divides $n$, 
we have that $d$ divides $k \cdot_n a = (k \cdot a) \text{ mod } n$.
But $d > 1$, so $k \cdot_n a \neq 1$ for all $k$.

\textbf{Proof $\Leftarrow$}: \\
Suppose that $\text{gcd}(a,n) = 1$.
Then there are $u,v \in \mathbb{Z}$ such that
$u \cdot a + v \cdot n = \text{gcd}(a,n) = 1$.
Take both sides mod $n$:\\
$(u \cdot a + v \cdot n) \text{ mod } n = u \cdot_n a$.\\
$1 \text{ mod } n = 1$.

\textbf{Corollary}:
$\mathbb{Z}/n$ is a field iff $n$ is prime.

\subsubsection{Zero-Divisors}

\textbf{Definition}:
Let $(R, +, \cdot, 0)$ be a ring.
A \underline{zero-divisor} is a non-zero element $a$ 
such that there is $b \neq 0$ with $a \cdot b = 0$.

\textbf{Examples}:
\begin{itemize}
    \item $\mathbb{Z}$: None
    \item $\mathbb{Z}/4$: 2
    \item $\mathbb{Z}/12$: $2,3,4,6,8,9,10$
    \item $\mathbb{R}$: None
    \item $\mathbb{Q}$: None
    \item $M_{n \times n}(\mathbb{R})$: non-invertible matrices
    \begin{itemize}
        \item If $A$ is non-invertible, 
        then there is a vector $v \neq 0$ 
        such that $Av = 0$.
        Define $B = [v,v,v,\dots,v]$.
        Then $AB = 0_{n \times n}$.
    \end{itemize}
\end{itemize}

\textbf{Proposition}: An element $a \in \mathbb{Z}/n$ 
is a zero-divisor iff $\text{gcd}(a,n) > 1$.

\textbf{Proof}: Follows from previous proof.

\subsection{Integral Domains}

\textbf{Definition}:
An \underline{integral domain} is a commutative ring with unity
that has no zero-divisors.

\textbf{Examples}:
\begin{itemize}
    \item $\mathbb{R}$ is an integral domain
    \item $\mathbb{Z}$ is an integral domain
    \item $\mathbb{Q}$ is an integral domain
    \item $\mathbb{Z}/p \mid p$ is prime is an integral domain
    \item $\mathbb{Z}/n \mid n$ not prime is not an integral domain
    \item Let $D_1, D_2$ be integral domains.
    $D_1 \times D_2$ is not an integral domain 
    since $(1,0) \cdot (0,1) = (0,0)$
    \item Let $D$ be an integral domain.
    $M_{n \times n}(D)$ is not an integral domain.
\end{itemize}

\pagebreak
\subsubsection{Multiplicative Cancellation Laws}

\begin{enumerate}[label=(\arabic*)]
    \item If $a \cdot b = a \cdot c$ and $a \neq 0$, then $b = c$.
    \begin{itemize}
        \item Fails in $\mathbb{Z}/12$ by $6 \cdot_12 4 = 6 \cdot_12 8, 4 \neq 8$.
    \end{itemize}
    \item If $a \cdot c = b \cdot c$ and $c \neq 0$, then $a = b$.
    \begin{itemize}
        \item Fails by an analogous example.
    \end{itemize}
\end{enumerate}

\textbf{Theorem}:
The multiplicative cancellation laws $(1)$ and $(2)$
hold in a commutative ring with unity $1 \neq 0$
iff it is an integral domain.

\textbf{Proof $\Rightarrow$}: \textit{By contrapositive}\\
If $R$ is not an integral domain,
there is $a,b \neq 0$ 
such that $a \cdot b = 0$.
Since $a \cdot b = 0 = a \cdot 0, b \neq 0$, we are done.

\textbf{Proof $\Leftarrow$}: \\
Let $D$ be an integral domain.
Suppose $a \cdot b = a \cdot c, a \neq 0$.
Want to show $b = c$.
\begin{align*}
    a \cdot b &= a \cdot c \\
    a \cdot b - a \cdot c &= 0 \\
    a \cdot (b - c) &= 0 \\
    \Rightarrow b - c &= 0
\end{align*}
Therefore, $b = c$.

\textbf{Proposition}:
Every field is an integral domain.

\textbf{Proof}:
Let $(F, +, \cdot, 0, 1)$ is a field.
Suppose $a \cdot b = 0$.
Want to show $a = 0$ or $b = 0$.
Suppose $a \neq 0$, so there is $a^{-1}$ 
such that $a \cdot a^{-1} = 1 = a^{-1} \cdot a$.
So $b = 1 \cdot b = (a^{-1} \cdot a) \cdot b = a^{-1} \cdot (a \cdot b) = a^{-1} \cdot 0 = 0$.

\begin{center}
    \includegraphics[width=4in]{Foundations/20191017-venn-diagram.jpg}
\end{center}

\textbf{Proposition}: 
If $(D, +, \cdot, 0, 1)$ is a \underline{finite} integral domain,
then it is a field.

\textbf{Proof}:
Need to show that every non-zero element is a unit.
Let $a \in D$ such that $a \neq 0$.
List all non-zero elements of $D$:
\[
    a_1, a_2, a_3, \dots, a_n
\]
Consider the following list:
\[
    a \cdot a_1, a \cdot a_2, a \cdot a_3, \dots, a \cdot a_n
\]
Note that all elements in this list are distinct
(if $a \cdot a_i = a \cdot a_j$, 
then by cancellation laws, 
$a_i = a_j$, so $i = j$).
None of them are zero, since $D$ is an integral domain.
Thus, it is a list of all non-zero elements in $D$.
Therefore, for some $i$, $a \cdot a_i = 1$.

\input{Foundations/20191024.tex}
