%! TEX root = ../notes.tex

\subsubsection{Field of Fractions}

\textbf{Example}: $\mathbb{Z} \mapsto \mathbb{Q}$
\[
	\mathbb{Q} = \left\{ \frac{a}{b} \mid a,b \in \mathbb{Z}, b \neq 0 \right\}
\]
but $\frac{a}{b} = \frac{ka}{kb}$ for any $k \neq 0 \in \mathbb{Z}$.

Define $S = \{(a,b) \in \mathbb{Z} \times \mathbb{Z} \mid b \neq 0\}$
and define $\sim$ on $S$ by $(a,b) \sim (c,d)$ iff $ad = bc$.
Then, $\mathbb{Q} = S/\sim$.

\textbf{Goal}: Repeat that for $D$ instead of $\mathbb{Z}$.

Fix an integral domain $(D, +, \cdot, 0, 1)$.
Define $S = \{(a,b) \in D \times D \mid b \neq 0\}$.
Define $\sim$ on $S$ by $(a,b) \sim (c,d)$ iff $ad = bc$.

\textbf{Claim}: $\sim$ is an equivalence relation on $S$.

\textbf{Proof}: 
\begin{itemize}
	\item Reflexivity: 
	      $(a,b) \sim (a,b)$ since $a \cdot b = b \cdot a$
	      by commutativity of $\cdot$ in $D$.
	\item Symmetry: 
	      Suppose $(a,b) \sim (c,d)$,
	      i.e., $a \cdot d = b \cdot c$.
	      Then $c \cdot b = d \cdot a$
	      and $(c,d) \sim (a,b)$ by commutativity.
	\item Transitivity:
	      Suppose $(a,b) \sim (c,d)$ and $(c,d) \sim (r,s)$.
	      Then $a \cdot d = b \cdot c$ and $c \cdot s = d \cdot r$.
	      \begin{align*}
	      	a \cdot s \cdot d & = s \cdot a \cdot d \\
	      	                  & = s \cdot b \cdot c \\
	      	                  & = b \cdot s \cdot c \\
	      	                  & = b \cdot d \cdot r \\
	      	                  & = b \cdot r \cdot d 
	      \end{align*}
	      So, $a \cdot s \cdot d = b \cdot r \cdot d$.
	      Since $d \neq 0$, by cancellation, $a \cdot s = b \cdot r$.
	      \boxed{ }
\end{itemize}

\textbf{Claim}: 
Define $F = S/\sim$.
$F$ is a field.

Need:
\begin{enumerate}
	\item Addition
	\item Multiplication
	\item Zero
	\item One
	\item Check axioms
\end{enumerate}

\vspace{4mm}

\begin{enumerate}
	\item Addition 
	          
	      \textbf{Example}:
	      In $\mathbb{Q}$,
	      \[
	      	\frac{a}{b} + \frac{c}{d} = \frac{ad + bc}{bd}
	      \]
	          
	      In general, define
	      \[
	      	[(a,b)] + [(c,d)] = [(a \cdot d + b \cdot c, b \cdot d)]
	      \]
	      
	      \textbf{Claim}: $+$ in $F$ is well-defined.
	      
	      \textbf{Proof}: 
	      Suppose we have $(a', b') \in [(a,b)]$,
	      i.e., $a \cdot b' = b \cdot a'$
	      and $(c', d') \in [(c,d)]$,
	      i.e., $c \cdot d' = d \cdot c'$.
	      
	      Want to show:
	      $(a \cdot d + b \cdot c, b \cdot d) \sim (a' \cdot d' + b' \cdot c', b' \cdot d')$,
	      i.e., $(a \cdot d + b \cdot c) \cdot b' \cdot d' = b \cdot d  \cdot (a' \cdot d' + b' \cdot c')$.
	      
	      \begin{align*}
	      	(a \cdot d + b \cdot c) \cdot b' \cdot d' & = a \cdot d \cdot b' \cdot d' + b \cdot c \cdot b' \cdot d' \\
	      	                                          & = a \cdot b' \cdot d \cdot d' + c \cdot d' \cdot b \cdot b' \\
	      	                                          & = b \cdot a' \cdot d \cdot d' + d \cdot c' \cdot b \cdot b' \\
	      	                                          & = b \cdot d \cdot (a' \cdot d' + b' \cdot c')               
	      \end{align*}
	      \boxed{ }
	      
	\item Multiplication
	          
	      \textbf{Example}:
	      In $\mathbb{Q}$,
	      \[
	      	\frac{a}{b} \cdot \frac{c}{d} = \frac{a \cdot c}{b \cdot d}
	      \]
	      
	      In general, define
	      \[
	      	[(a,b)] \cdot [(c,d)] = [(a \cdot c, b \cdot d)]
	      \]
	      
	      \textbf{Claim}: $\cdot$ in $F$ is well-defined.
	      
	      \textbf{Proof}:
	      Suppose we have $(a', b') \in [(a,b)]$,
	      i.e., $a \cdot b' = b \cdot a'$
	      and $(c', d') \in [(c,d)]$,
	      i.e., $c \cdot d' = d \cdot c'$.
	      
	      Want to show: 
	      $(a \cdot c, b \cdot d) \sim (a' \cdot c', b' \cdot d')$,
	      i.e., $(a \cdot c \cdot b' \cdot d') = b \cdot d \cdot a' \cdot c'$.
	      \begin{align*}
	      	a \cdot c \cdot b' \cdot d' & = a \cdot b' \cdot c \cdot d' \\
	      	                            & = b \cdot a' \cdot d \cdot c' \\
	      	                            & = b \cdot d \cdot a' \cdot c' 
	      \end{align*}
	      
	      Also, $b \cdot d \neq 0$,
	      which follows from $D$ not having zero-divisors.
	      
	\item Zero
	          
	      $[(0, 1)]$
	      
	\item One
	          
	      $[(1, 1)]$
	      
	\item Check axioms
	      \begin{itemize}
	      	\item $\cdot$ is commutative
	      	      \[
	      	      	[(a,b)] \cdot [(c,d)] = [(a \cdot c, b \cdot d)] = [(c \cdot a, d \cdot b)] = [(c, d)] \cdot [(a, b)]
	      	      \]
	      	      
	      	\item $[(1,1)]$ is the unity
	      	      \[
	      	      	[(a,b)] \cdot [(1,1)] = [(a \cdot 1, b \cdot 1)] = [(a,b)]
	      	      \]
	      	      
	      	\item If $[(a,b)] \neq [(0,1)]$, then it is a unit
	      	      Since $[(a,b)] \neq [(0,1)]$,
	      	      we get $a \cdot 1 \neq b \cdot 0 = 0$.
	      	      Thus, $(b,a) \in S$.
	      	      So, 
	      	      \begin{align*}
	      	      	[(a,b)] \cdot [(b,a)]                    & = [(a \cdot b, b \cdot a)] \\
	      	      	\Longrightarrow [(a \cdot b, b \cdot a)] & \sim (1,1)                 \\
	      	      	a \cdot b \cdot 1                        & = b \cdot a \cdot 1        
	      	      \end{align*}
	      	      
	      	\item etc.
	      \end{itemize}
\end{enumerate}

\textbf{Definition}:
The field $F$ as constructed above
is called the \underline{field of fractions}
for/of an integral domain $D$.

\textbf{Question}: \textit{In what way is this process canonical?}

\textbf{Example}: $D = \mathbb{Z}, F = \mathbb{Q}$ \\
There is $i : Z \rightarrow Q$
given by $i(k) = \frac{k}{1}$.

In general, we have a function
\[
	i : D \rightarrow F \mid i(a) = [(a, 1)]
\]

\textbf{Lemma}:
$i$ is a ring homomorphism that preserves unity.

\textbf{Proof}:
Fix $a,b \in D$.
\[
	i(a + b) = [(a + b, 1)] = [(a \cdot 1 + 1 \cdot b, 1 \cdot 1)] = [(a, 1)] + [(b, 1)] = i(a) + i(b)
\]
\[
	i(a \cdot b) = [(a \cdot b, 1)] = [(a \cdot b, 1 \cdot 1)] = [(a, 1)] \cdot [(b, 1)] = i(a) \cdot i(b)
\]
\[
	i(1) = [(1, 1)]
\]

\textbf{Theorem}: $i$ is injective.

\textbf{Proof}:

If $i(a) = i(b)$, then $[(a,1)] = [(b, 1)]$,
i.e., $(a,1) \sim (b,1)$,
so $a \cdot 1 = b \cdot 1$.

\textbf{Theorem}:
Let $(D, +, \cdot, 0, 1)$ be an integral domain
and $(F, +, \cdot, [(0,1)], [(1,1)])$ be its field of fractions.
Given a ring homomorphism $f : D \rightarrow F'$
preserving unity, where $F'$ is a field,
there is a unique $\bar{f} : F \rightarrow F'$ ring homomorphism
preserving unity such that $\bar{f} \circ i = f$.

\begin{center}
	\begin{tikzpicture}
		\node (D) at (0,0) {$D$};
		\node (F') at (2,0) {$F'$};
		\node (F) at (1,-1) {$F$};
		
		\path (D) edge[draw,->] node[above] {$f$} (F')
		(D) edge[draw,->] node[below] {$i$} (F)
		(F) edge[dashed,->] node[below] {$\exists!\bar{f}$} (F');
	\end{tikzpicture}
\end{center}

\textbf{Example}: $D = \mathbb{Z}, F = \mathbb{Q}$

Let $F'$ be a field and 
$f : \mathbb{Z} \rightarrow F'$ be a ring homomorphism preserving unity.
\[
    \bar{f}\left(\frac{a}{b}\right) = f(a) \cdot f(b)^{-1}
\]

\textbf{Proof of Existence}: 
 
Define $\bar{f}([(a,b)]) = f(a) \cdot f(b)^{-1}$.
Check: 
\begin{itemize}
    \item $\bar{f}$ is well-defined
    \item $\bar{f}$ is a ring homomorphism preserving unity
    \item $\bar{f} \circ i = f$
\end{itemize}

\vspace{5mm}

\begin{itemize}
    \item Well-defined:
    
    Given $(a', b') \in [(a,b)]$,
    need to show $f(a) \cdot f(b)^{-1} = f(b') \cdot f(b')^{-1}$.
    By assumption, we have 
    \[
        f(a) \cdot f(b') = f(a \cdot b') = f(a') \cdot f(b)
    \]
    From this, get conclusion by cancellation.
\end{itemize}

