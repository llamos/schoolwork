%! TEX root = ../notes.tex

\subsection{Subrings}

\textbf{Definition}:
A \underline{subring} of a ring $(R, +, \cdot, 0)$
is a subset $R' \subseteq R$ such that
$R'$ is a ring with the same $+, \cdot, 0$.

\textbf{Proposition}:
Let $(R, +, \cdot, 0)$ be a ring.
A subset $R' \subseteq R$ is a subring iff
\begin{enumerate}[label=(\arabic*)]
    \item $(R', +, 0)$ is a subgroup of $(R, +, 0)$
    \item for any $a,b \in R'$, we have $a \cdot b \in R'$.
\end{enumerate}

\textbf{Proof}: Left as exercise.

\textbf{Examples}:
\begin{itemize}
    \item $2\mathbb{Z} \subseteq \mathbb{Z}$
    \item $\mathbb{Z} \subseteq \mathbb{R}$
\end{itemize}

\subsubsection{Ideals}

\begin{center}
    \begin{tabular}{c|c}
        Group theory & Ring theory \\
        \hline
        Groups & Rings \\
        Subgroups & Subrings \\
        Normal Subgroups & Ideals
    \end{tabular}
\end{center}

In group theory, we study both subgroups and normal subgroups.
In ring theory, we focus on ideals.

\textbf{Definition}: A subset $I \subseteq R$ 
of a ring $(R, +, \cdot, 0)$
is an \underline{ideal} if
\begin{enumerate}[label=(\arabic*)]
    \item $0 \in I$
    \item for any $a, b \in I$, we have $a + b \in I$
    \item for any $a \in I$, we have $-a \in I$
    \item for any $a \in I$ and $r \in R$, we have $a \cdot r \in I$
    \item for any $a \in I$ and $r \in R$, we have $r \cdot a \in I$
\end{enumerate}

\textbf{Examples}:
\begin{itemize}
    \item $2\mathbb{Z} \subseteq \mathbb{Z}$ is an ideal
    \begin{itemize}
        \item More generally, $n\mathbb{Z} \subseteq \mathbb{Z}$ is an ideal
    \end{itemize}
    \item $Z \subseteq R$ is not an ideal; $2 \in \mathbb{Z}, \frac{1}{3} \in \mathbb{R}, 2 \cdot \frac{1}{3} = \frac{2}{3} \notin \mathbb{Z}$
\end{itemize}

\textbf{Remark}:

(1) - (3) $\Leftrightarrow I$ is an additive subgroup \\
(4) - (5) is analogous to normality

\subsubsection{Kernels and Images}

\textbf{Definition}:
Let $f : R \rightarrow R'$ be a ring homomorphism
from $(R, +, \cdot, 0)$ to $(R', +', \cdot', 0')$.
\begin{enumerate}[label=(\arabic*)]
    \item Define the kernel of $f$ as 
    $\text{Ker}(f)=\{a \in R \mid f(a) = 0\}$.
    \item Given a subring $S \subseteq R$,
    the image of $S$ under $f$ is
    $f[S] = \{f(a) \mid a \in S\}$.
\end{enumerate}

\textbf{Proposition}:
For any ring homomorphism $f : R \rightarrow R'$,
$\text{Ker}(f)$ is an ideal in $R$.

\textbf{Proof}:

Conditions (1) - (3) follow from an analogous result for groups.

Take $a \in \text{Ker}(f)$ and $r \in R$.
We have that $f(a \cdot r) = f(a) \cdot' f(r) = 0 \cdot' f(r) = 0$,
which shows that $a \cdot r \in \text{Ker}(f)$.
To show $r \cdot a \in \text{Ker}(f)$,
proceed similarly with $a$ and $r$ swapped.

\textbf{Proposition}: 
For any ring homomorphism $f : R \rightarrow R'$,
and a subring $S \subseteq R$,
the image of $S$ under $f$ is a subring of $R'$.

\textbf{Proof}:

\begin{enumerate}[label=(\arabic*)]
    \item Follows by an analogous result for groups
    \item Take $a,b \in f[S]$.
    Then, there are $\bar{a},\bar{b} \in $
    such that $a = f(\bar{a})$ and $b \in f(\bar{b})$.
    Then $a \cdot b = f(\bar{a}) \cdot f(\bar{b}) = f(\bar{a} \cdot \bar{b})$.
\end{enumerate}

\textbf{Example}:
Image of an ideal need not be an ideal.
\begin{itemize}
    \item $2\mathbb{Z} \subseteq Z$ is an ideal,
    \item $f: \mathbb{Z} \rightarrow \mathbb{R}$,
    \item $f[2\mathbb{Z}] \subseteq \mathbb{R}$ is not an ideal.
\end{itemize}

\textbf{Proposition}:
Let $f : R \rightarrow R'$ be a ring epimorphism
and $I \subseteq R$ be an ideal.
Then $f[I]$ is an ideal in $R'$.

\textbf{Proof}:

Conditions (1) - (3) follow by an analogous result for groups.

Let $a \in f[I]$ and $r' \in R'$. Want to show: $a \cdot r' \in f[I]$.

Since $f$ is surjective, there is $r \in R$ such that $f(r) = r'$
and $\bar{a} \in I$ such that $f(\bar{a}) = a$.

So $a \cdot r' = f(\bar{a}) \cdot' f(r) = f(\bar{a} \cdot r) \in f[I]$
since $\bar{a} \cdot r \in I$ by $I$ being ideal.

\textbf{Proposition}: Let $f : R \rightarrow R'$ be a ring homomorphism
and $I' \subseteq R'$ be an ideal.
Then $f^{-1}[I'] = \{a \in R \mid f(a) \in I'\}$ is an ideal.

\textbf{Proof}:

Conditions (1) - (3) follow by an analogous result for groups.

$f(a \cdot r) = f(a) \cdot f(r) \in I'$ since $f(a) \in I'$ and $f(r) \in R'$.

\pagebreak
\subsubsection{Cosets and Slice}

Let $I \subseteq R$ be an ideal in $(R, +, \cdot, 0)$.

\textbf{Define}: $a + I = \{a + b \mid b \in I\}$.

\textbf{Remark}: Since $(R, +, 0)$ is abelian,
$a + I = I + a$.

\textbf{Define}: $R/I = \{a + I \mid a \in R\}$.

\textbf{Example}:
$R = \mathbb{Z}, I = 3\mathbb{Z}, R/I = \{0 + 3\mathbb{Z}, 1 + 3\mathbb{Z}, 2 + 3\mathbb{Z}\}$

\textbf{Define}:
\[
    + : R/I \times R/I \rightarrow R/I \mid (a + I) + (b + I) = (a + b) + I
\]
\[
    \cdot : R/I \times R/I \rightarrow R/I \mid (a + I) \cdot (b + I) = (a \cdot b) + I
\]

\textbf{Claim}: These are well-defined.

\textbf{Proof}:

For +, this follows by an analogous result for groups.

For $\cdot$, given $a + i \in a + I$ and $b + i' \in b + I$,
need to show $(a+i) \cdot (b + i') \in (a \cdot b) + I$.
\[
    (a + i) \cdot (b + i') = a \cdot b + a \cdot i' + i \cdot b + i \cdot i'
\]

Since $a \cdot i' \in I, i \cdot b \in I, i \cdot i' \in I$,
this reduces to $\in (a \cdot b) + I$.

\textbf{Theorem}: $(R/I, +, \cdot, I)$
is a ring for any ring $R$ and an ideal $I \subseteq R$.

Axioms (1) - (4) follow by an analogous result for groups.

\textit{Associativity of multiplication}:
\begin{align*}
    ((a + I) \cdot (b + I))(c + I) &= (a \cdot b + I)(c + I) \\
    &= ((a \cdot b) \cdot c) + I \\
    &= (a \cdot (b \cdot c)) + I \\
    &= (a + I) \cdot ((b \cdot c) + I) \\
    &= (a + I) \cdot ((b + I) \cdot (c + I))
\end{align*}

\textbf{Remark}: If $(R, +, \cdot, 0)$ is commutative,
then $(R/I, +, \cdot, I)$ is commutative.
If $(R, +, \cdot, 0, 1)$ is a ring with unity,
then $(R/I, +, \cdot, I, 1 + I)$ is a ring with unity.

\textbf{Claim}: 
Let $(R, +, \cdot, 0)$ be a ring and $I \subseteq R$ an ideal.
Define $q : R \rightarrow R/I \mid q(a) = a + I$.
$q : R \rightarrow R/I$ is a ring epimorphism.

\textbf{Proof}: 

\textit{$q$ preserves addition}: follows by an analogous result for groups.

\textit{$q$ preserves multiplication}: $q(a \cdot b) = (a \cdot b) + I) = (a + I) \cdot (b + I) = q(a) \cdot q(b)$

\textit{$q$ preserves unity}: If $R$ has unity 1, then $q(1) = 1 + I$.

\textit{$q$ is surjective}: follows by an analogous result for groups.

\subsubsection{First Isomorphism Theorem for Rings}

Let $(R, +, \cdot, 0)$ and $(S, +', \cdot', 0')$ be rings
and $f : R \rightarrow S$ be a ring epimorphism.
Let $I = \text{Ker}(f)$.
Then there exists a unique ring isomorphism 
$\bar{f} : R/I \rightarrow S$
such that $\bar{f} \circ q = f$.

\begin{center}
    \begin{tikzpicture}
        \node (R) at (0,0) {$R$};
        \node (S) at (4,0) {$S$};
        \node (RI) at (2,-2) {$R/I$};

        \path (R) edge[draw,->] node[above] {$f$} (S)
              (R) edge[draw,->] node[left] {$q$} (RI)
              (RI) edge[dashed,->] node[right] {$\bar{f}$} (S);
    \end{tikzpicture}
\end{center}

\textbf{Proof}:

\textit{Existence}: Define $\bar{f}(a + I) = f(a)$. Need to show:
\begin{itemize}
    \item $\bar{f}$ is well-defined follows by an analogous result for groups
    \item $\bar{f}$ is bijective follows by an analogous result for groups
    \item $\bar{f} \circ q = f$ follows by an analogous result for groups
    \item $\bar{f}$ is a ring homomorphism 
    \begin{itemize}
        \item \textit{$\bar{f}$ preserves addition}: follows by an analogous result for groups
        \item \textit{$\bar{f}$ preserves multiplication}:
        \begin{align*}
            \bar{f}((a + I) \cdot (b + I)) &= \bar{f}(a \cdot b + I) \\
            &= f(a \cdot b) \\
            &= f(a) \cdot f(b) \\
            &= \bar{f}(a + I) \cdot \bar{f}(b + I)
        \end{align*}
    \end{itemize}
\end{itemize}

\textit{Uniqueness}: follows by an analogous result for groups

\textbf{Remark}:
Let $G$ be a group and $H \leq G$ be a normal subgroup.
$G/H$ is a group.
If $G$ is abelian, $G/H$ is abelian.
If $G$ is cyclic, $G/H$ is cyclic.
For rings, if $R$ is commutative, $R/I$ is commutative.
If $R$ has unity, $R/I$ has unity.

\textbf{Question}: Let $(D, +, \cdot, 0, 1)$ be an integral domain
and $I \subseteq D$ is an ideal.
Does it follow that $(D/I, +, \cdot, I, 1 + I)$ is an integral domain?

Sometimes. 

Take an arbitrary $D, I = \{0\}$.
$D/I \cong D$, so it is clearly an integral domain.

$D = \mathbb{Z}, I = 4\mathbb{Z}$.
$D/I \cong \mathbb{Z}/4$, 
but this is not an integral domain because $2 \cdot_4 2 = 0$.

\input{Foundations/20191112.tex}
