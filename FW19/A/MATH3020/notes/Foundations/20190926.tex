%! TEX root = ../notes.tex

\fancyhead[R]{\date{Sept. 24 \& 26, 2019}}
\fancyhead[C]{Abstract Algebra}

\textbf{Lemma}: 
Let $f$ be a homomorphism $f : (G, \ast, e) \rightarrow (G', \ast', e')$.
Then $f$ is monomorphic iff $\text{Ker}(f) = \{e\}$.

\textbf{Proof $\Longrightarrow$}: \\
Suppose $f$ is monomorphic.
Let $a \in \text{Ker}(f)$.
Then $f(a) = e' = f(e)$.

\textbf{Proof $\Longleftarrow$}: \\
Suppose $\text{Ker}(f) = \{e\}$ and let $a,b \in G$
with $f(a) = f(b)$.
We have $f(a) \ast' f(b)^{-1} = e'$. So,
\begin{align*}
    e' &= f(a) \ast' f(b)^{-1} \\
    &= f(a) \ast' f(b^{-1}) \\
    &= f(a \ast b^{-1})
\end{align*}
So $a \ast b^{-1} \in \text{Ker}(f)$ hence $a \ast b^{-1} = e$.
Therefore $a = e$.

\textbf{Definition}: To show that two groups are \underline{isomorphic},
we need to construct $f : G \rightarrow G'$ and check
\begin{enumerate}
    \item $f(a \ast b) = f(a) \ast' f(b)$
    \item $f[G] = G'$
    \item $\text{Ker}(f) = \{e\}$
\end{enumerate}

\textbf{Notation}: Write $(G, \ast, e) \cong (G', \ast', e')$.

\subsubsection{Cyclic Groups}

Let $(G, \ast, e)$ be a group and $a \in G$.
For $n \in \mathbb{N}$, define $a^n$ inductively:
\begin{align*}
    a^0 &= e \\
    a^{n+1} &= a^n \ast a
\end{align*}

\textbf{Remark}:
\begin{align*}
    a^0 &= e \\
    a^1 &= a \\
    a^2 &= a \ast a \\
    a^3 &= a \ast a \ast a \\
    \dots &= \dots \\
    \text{and} \qquad a^{-n} &= (a^n)^{-1}
\end{align*}

\textbf{Definition}:
A group $(G, \ast, e)$ is \underline{cyclic}
if there is $a \in G$ such that $G = \{a^n \vert n \in \mathbb{Z}\}$.
$a$ is called the \underline{generator} of $G$.

\textbf{Examples}:
\begin{itemize}
    \item $(\mathbb{Z}/4, +_4, 0)$ is cyclic with $a = 1$ and $a = 3$.
    \begin{itemize}
        \item $(\mathbb{Z}/n, +_n, 0)$ is cyclic with $a = 1$.
    \end{itemize}
    \item $(\mathbb{Z}, +, 0)$ is cyclic with $a = 1$ and $a = -1$.
\end{itemize}

\textbf{Lemma}: Every cyclic group is abelian.

\textbf{Proof}: Let $(G, \ast, e)$ be a cyclic group with generator $a$.
Given $b, c \in G$, we need to show $b \ast c = c \ast b$.
Since $a$ is a generator, $b = a^m$ and $c = a^n$ for $m,n \in \mathbb{Z}$.
Thus, $b \ast c = a^m \ast a^n = a^m+n = a^n+m = a^n \ast a^m = c \ast b$.

\fancyhead[R]{\date{Sept. 26, 2019}}
\fancyhead[C]{Abstract Algebra}

\subsubsection{Cyclic Subgroups}

\textbf{Division Algorithm}: Let $a, b \in \mathbb{Z}$ with $b > 0$.
Then there are unique $q,r \in \mathbb{Z}$
such that $a = bq + r$ with $0 \leq r < b$.

\textbf{Lemma}: Any subgroup of a cyclic group is cyclic.

\textbf{Proof}: Let $(G, \ast, e)$ be a cyclic group with generator $a$.
Let $H$ be a subgroup. For some $m \in \mathbb{Z}$, $a^m \in H$.
Let $n$ be the smallest positive such $m$.

Claim: $c = a^n$ generates $H$. \\
We need to show that any $b$ in $H$ 
can be written as $b = c^k$ for some $k \in \mathbb{Z}$.
Since $(G, \ast, e)$ is cyclic, $b = a^l$ for some $l \in \mathbb{Z}$.
Divide $l$ by $n$ with remainder.
$l = nq + r, 0 \leq r < n$.
We need to show that $r = 0$.

we have that 
\begin{align*}
    a^l &= a^{nq + r} \\
    &= (a^n)^q \ast a^r \\
    \text{So, \qquad}a^r &= (a^n)^{-q} \ast a^l \\
    &= c^{-q} \ast b
\shortintertext{Since $c, b \in H$, $c^{-q} \ast b \in H$.}
\end{align*}
Since $0 \leq r \leq n$ and $n$ is the smallest positive integer such that $a^n \in H$, $r = 0$.

\textbf{Corollary}: The subgroups of $\mathbb{Z}$ are
of the form $n\mathbb{Z}$ for $n \in \mathbb{Z}$.

\textbf{Example}: 
$(\mathbb{Z}/4, +_4, 0)$ and 
$(\mathbb{Z}/2 \times \mathbb{Z}/2, +_2 \times +_2, (0, 0))$
each contain 4 elements.
However, $(\mathbb{Z}/4, +_4, 0)$ has three subgroups
and $(\mathbb{Z}/2 \times \mathbb{Z}/2, +_2 \times +_2, (0, 0))$
has five subgroups.
Thus, these two groups are not isomorphic.

\subsubsection{Isomorphism to Z and Z/n}

\textbf{Theorem}: Let $(G, \ast, e)$ be a cyclic group.
\begin{enumerate}
    \item If $G$ is infinite then $(G, \ast, e) \cong (\mathbb{Z}, +, 0)$
    \item If $G$ is finite, then  $(G, \ast, e) \cong (\mathbb{Z}/n, +_n, 0)$ for some $n$.
\end{enumerate}

\textbf{Proof}: Let $a$ be a generator of $G$.
Define $f : \mathbb{Z} \rightarrow G$ by $f(k) = a^k$.

Claim: $f$ is a homomorphism. \\
$f(k + l) = a^{k + l} = a^k \ast a^l = f(k) \ast f(l)$.

$f$ is surjective, by $G$ being cyclic.

If $\text{Ker}(f) = 0$, then done 
and $(G, \ast, e) \cong (\mathbb{Z}, +, 0)$.

If $\text{Ker}(f) \neq 0$, then 
let $n$ be the smallest positive integer such that $n \in \text{Ker}(f)$.

Claim: $G = \{e, a, a^2, a^3, \dots, a^{n-1}\}$ 
and all these are distinct. \\
For each $k$ in $\mathbb{Z}$,
$a^k = a^r$ where $r$ is the remainder in $k = nq + r$.

$a^k = a^{nq + r} = (a^n)^{q} \ast a^r = e^q \ast a^r = a^r$.

If $a^i = a^j$ for $0 \leq i, j < n$, then $i = j$.
Without loss of generality, $i \leq j$, 
and then $a^j \ast a^{-i} = a^{j-i}$, $j-i = 0$ and $j = 1$.

Thus, $(G, \ast, e) \cong (\mathbb{Z}/n, +_n, 0)$.


\subsubsection{Generated Subgroups}

Let $(G, \ast, e)$ be a group and $S \subseteq G$ be a subset.
The subgroup generated by $S$, denoted $\langle S \rangle$,
is the smallest subgroup containing $S$.

\textbf{Examples}: 
\begin{itemize}
    \item $(\mathbb{Z}, +, 0), S = \{2\}, \langle S \rangle = 2\mathbb{Z}$
    \item $(\mathbb{Z}, +, 0), S = \{2,4\}, \langle S \rangle = 2\mathbb{Z}$
    \item $(\mathbb{Z}, +, 0), S = \{2,3\}, \langle S \rangle = \mathbb{Z}$
    \item $(\mathbb{Z}, +, 0), S = \{6,9\}, \langle S \rangle = 3\mathbb{Z}$
    \item $(\mathbb{R}, +, 0), S = \mathbb{Z}, \langle S \rangle = \mathbb{Z}$
    \item $(\mathbb{R}^\times, \cdot, 1), S = \mathbb{Z} \setminus 0, \langle S \rangle = \mathbb{Q}^\times$
\end{itemize}

\textbf{Proposition}: For a group $(G, \ast, e)$ and $S \subseteq G$,
we have that
\begin{equation*}
    \begin{split}
        \langle S \rangle = \{(a_1)^{\alpha_1} \ast \dots \ast (a_n)^{\alpha_n}\}
    \end{split} \mid \begin{split}
        a_1, \dots, a_n \in S \\
\alpha_1, \dots, \alpha_n \in \{-1, 1\} \\
n \in \mathbb{N}
    \end{split}
\end{equation*}

\textbf{Proof}:
$X$ is a subgroup of $G$. Moreover, $X \subseteq S$.
It is the smallest such subgroup.

\subsubsection{Cayley Theorem}: 

For any group $(G, \ast, e)$,
there is a set $X$ and a monomorphism $f : G \rightarrow \text{Bij}(X)$.

\textbf{Proof}: Let $X = G$ and define $f : G \rightarrow \text{Bij}(X)$.
$f(a) = \lambda_a$ where $\lambda_a : G \rightarrow G$ 
given by $\lambda_a(b) = a \ast b$.

Claim 1: $\lambda_a$ is a bijection, thus $f$ is well-defined. \\
Claim 2: $f$ is a homomorphism. \\
Claim 3: $f$ is injective.

\begin{enumerate}
    \item $\lambda_a$ is a bijection because it has an inverse, $\lambda_{a^{-1}}$
    \[
        (\lambda_a \circ \lambda_b)(b) = \lambda_a(a^{-1} \ast b) = a \ast (a^{-1} \ast b)
    \]
    \item HW
    \item If $f(a) = f(a')$, we want to show that $a = a'$.
    Thus, $\lambda_a = \lambda_{a'}$, so $\lambda_a(e) = \lambda_{a'}(e)$
    which means that $a = a \ast e = a' \ast e = a'$.
\end{enumerate}

\input{Foundations/20191001.tex}
