%! TEX root = ../notes.tex

\subsubsection{Noetherian Rings}

Let $(R, +, \cdot, 0, 1)$ be a commutative ring with unity.

$R$ is \underline{Noetherian} 
if given any ascending sequence of ideals
$I_1 \subseteq I_2 \subseteq I_3 \subseteq \dots$,
there is $m \in \mathbb{N}$ such that 
for all $n \geq m$,
we have $I_m = I_n$.

\[
    I_1 \subseteq I_2 \subseteq I_3 \subseteq \dots \subseteq I_m = I_{m + 1} = I_{m + 2} = I_{m + 3}
\]

\textbf{Theorem}: Every PID is Noetherian.

\textbf{Example}: Why is $\mathbb{Z}$ Noetherian?

Let $I_1 = \mangle{60}$.\\
Let $I_2 = \mangle{30}$.\\
Let $I_3 = \mangle{15}$.\\
Let $I_3 = \mangle{5}$.\\
Let $I_3 = \mangle{1}$.

Any ideal of $\mathbb{Z}$ can be shown Noetherian 
by factoring the generator.

\textbf{Proof}:

Let $(D, +, \cdot, 0, 1)$ be a PID
and consider an ascending sequence of ideals
$I_1 \subseteq I_2 \subseteq I_3 \subseteq \dots$ in $D$.
Want to show: this sequence stabilizes.

We know that $I = \cup_{n=1}^\infty I_n$ is an ideal in $D$.
Since $D$ is a PID, 
we have that $I = \mangle{a}$
for some $a \in D$.

Thus $a \in I_m$ for some $m \in \mathbb{N}$.
Thus $\mangle{a} \subseteq I_m \subseteq I_n \subseteq I = \mangle{a}$,
so $I_m = I_{m + 1}$.

\textbf{Proposition}: Let $(D, +, \cdot, 0, 1)$ be a PID
and $a \in D$ be non-zero and non-unit.
Then $a$ has an irreducible factor.

\textbf{Proof}:

Let $a \in D$ be non-zero and non-unit.
If $a$ is irreducible, done ($a = a \cdot 1$).
Otherwise, there are $a_1,b_1 \in D$ 
such that $a = a_1 \cdot b_1$ 
and neither $a_1$ nor $b_1$ is a unit.
We obtain $\mangle{a} \subset \mangle{a_1}$.

If $a_1$ is irreducible, done ($a = a_1 \cdot b_1$).
Otherwise, there are $a_2, b_2 \in D$
such that $a_1 = a_2 \cdot b_2$
and neither $a_2$ nor $b_2$ is a unit.
We obtain $\mangle{a} \subset \mangle{a_1} \subset \mangle{a_2}$.

We continue finding $a_3, a_4, \dots$ 
with $\mangle{a} \subset \mangle{a_1} \subset \mangle{a_2} \subset \mangle{a_3} \subset \dots$.
Since $D$ is Noetherian, this must stabilize
and so $a_n$ is irreducible for some $n \in \mathbb{N}$.

\textbf{Recall}: Let $(D, +, \cdot, 0, 1)$ be an integral domain. 
An element $a \in D$ is:
\begin{align*}
    \text{prime if} && \text{irreducible if} \\
    a | b \cdot c &\Rightarrow a | b \text{ or } & a = b \cdot c &\Rightarrow a | b \text{ or }\\
    &\Rightarrow a | c && \Rightarrow a | c
\end{align*}

Prime $\Rightarrow$ irreducible, 
but irreducible $\Rightarrow$ prime in PID only.

\subsubsection{All PIDs are UFDs}

\textbf{Definition}:
A \underline{Universal Factoring Domain} (UFD)
is an integral domain \\
$(D, +, \cdot, 0, 1)$ such that
\begin{enumerate}[label=(\arabic*)]
    \item Given $a \in D$ nonunit and nonzero, we can write $a = a_1 \cdot \dots \cdot a_m$ where $a_1, \dots, a_m$ are irreducible.
    \item Given such $a$ and two factorizations into irreducibles, $a_1 \cdot \dots \cdot a_m = a = b_1 \cdot \dots \cdot b_n$,
    we have that $m = n$ and after possibly changing the order of $b_1,\dots,b_m$, we have that $(a_1,b_1),\dots,(a_m,b_m)$ are associate pairs.
\end{enumerate}

\textbf{Example}: 
In $\mathbb{Z}$:\\
$2 \cdot 3 = 6 = 3 \cdot 2$, so we need the ``possibly changing the order''.\\
$(-2) \cdot (-3) = 6 = 2 \cdot 3$, so we need the ``$(a_1,b_1),\dots,(a_m,b_m)$ are associate pairs''.

\textbf{Theorem}:
Every PID satisfies Condition (1) of the definition of a UFD.

\textbf{Proof}:

Let $(D, +, \cdot, 0, 1)$ be a PID
and $a \in D$ be a nonzero, nonunit element.
Then $a = a_1 \cdot b_1$ with $a_1$ irreducible.
If $b_1$ is irreducible, then done, since both factors are irreducible.
Otherwise, $b = a_2 \cdot b_2$ with $a_2$ irreducible.

If $a_2$ is irreducible, then done, 
since $a = a_1 \cdot a_2 \cdot b_2$,
and $a_1,a_2,b_2$ are irreducible.
Otherwise, repeat the process,
forming $\mangle{a} \subseteq \mangle{b_1} \subseteq \mangle{b_2} \subseteq \dots$.
Since $D$ is Noetherian, the sequence must stabilize,
and we know that $b_n$ is irreducible for some $n \in \mathbb{N}$.
So, $a = a_1 \cdot a_2 \cdot \dots \cdot a_n \cdot b_n$ 
with $a_1, a_2, \dots, a_n, b_n$ are irreducible.

\textbf{Lemma}: 
Let $(D, +, \cdot, 0, 1)$ be an integral domain
and $a \in D$ be a prime element.
If $a | a_i \cdot \dots \cdot a_n$,
then $a | a_i$ for some $i = 1, \dots, n$.

\textbf{Remark}: $a$ being prime has the following property:
\[
    a | a_1 \cdot a_2 \Rightarrow a | a_1 \text{ or } a | a_2
\]

\textbf{Proof}: \textit{via induction wrt $n$}

For $n = 1$, we assume $a | a_1$ 
and can reasonably say $a | a$.

Suppose the statement holds for some $n \in \mathbb{N}$
and $a | a_i \cdot a_n \cdot a_{n+1}$.
We have that $a | a_1 \cdot \dots \cdot a_n) \cdot (a_{n + 1})$
and $a$ is prime.
So, $a | a_1 \cdot \dots \cdot a_n$ or $a | a_{n + 1}$.
If $a | a_{n + 1}$, then done.
If $a | a_1 \cdot \dots \cdot a_n$, 
then by the inductive hypothesis,
we get that $a | a_i$ for some $i = 1, \dots, n$.

\textbf{Corollary}: Let $(D, +, \cdot, 0, 1)$ be a PID
and $a \in D$ be irreducible.
If $a | a_1 \cdot \dots \cdot a_n$,
then $a | a_i$ for some $1,\dots,n$.

\textbf{Proof}: An irreducible element in a PID is prime,
so the lemma applies.

\textbf{Theorem}: Every PID satisfies Condition (2) of UFD.

\textbf{Proof}: 

Let $(D, +, \cdot, 0, 1)$ be a PID
and $a \in D$ be a nonzero, nonunit element.
Suppose we have two factorizations into irreducibles:
\[
    a_1 \cdot \dots \cdot a_m = a = b_1 \cdot \dots \cdot b_n
\]
Want to show that $m = n$ 
and after possibly changing the order $b_i$'s,
$(a_1,b_1),\dots,(a_m,b_m)$ are associate pairs.

We know that $a_1 | b_1 \cdot \dots \cdot b_n$,
so by the corollary, $a_1$ must divide at least one of the $b_i$'s.

By possibly changing the order of $b_i$'s, 
we assume $a_1 | b_1$.
Thus $b_1 = a_1 \cdot c_1$ 
and since $a_1,b_1$ are irreducible,
$c_1$ must be a unit.

By cancellation: $a_2 \cdot \dots \cdot a_m = c_1 \cdot b_2 \cdot \dots \cdot b_n$
Since $a_2 | c_1 \cdot b_2 \cdot \dots \cdot b_n$,
we get that $a_2 | b_i$ for some $i$
and after possibly changing the order $a_2 | b_2$,
so $b_2 = a_2 \cdot c_2$ with unit 
which reduces the problem to 
$a_3 \cdot \dots \cdot a_m = c_1 \cdot c_2 \cdot b_3 \cdot \dots \cdot b_n$.

Continuing, we obtain:
\[
    1 = c_1 \cdot \dots \cdot c_m \cdot b_{m + 1} \cdot \dots \cdot b_{n}
\]

Since the remaining $b$ elements cannot divide $1$,
they cannot exist as irreducible elements,
so $m = n$.

Since $b_1 = a_1 \cdot c_1$ with $c_1$ being a unit,
we get $(a_1,b_1)$ as an associate pair.
Similarly, since $b_2 = a_2 \cdot c_2$ with $c_2$ being a unit,
we get $(a_2,b_2)$ as an associate pair.
Continuing, we get that $m = n$ 
and all $a_i$'s and $b_i$'s form an associate pair.

\textbf{Corollary}: Every PID is a UFD.

\textbf{Proof}: See above for conditions (1) and (2).

\textbf{Corollary}: Fundamental Theorem of Arithmetic

\textbf{Proof}: $\mathbb{Z}$ is a PID.

\textbf{Corollary}: Uniqueness of Factorization in $F[x]$

\textbf{Proof}: $F[x]$ is a PID.

\subsubsection{Euclidean Domains}

\textbf{Definition}:

A \underline{Euclidean Domain} is an integral domain $(D, +, \cdot, 0, 1)$
with a function \\ $d : D \setminus \{0\} \rightarrow \mathbb{N}$
such that for any $a,b \in D \setminus \{0\}$,
there are $q,r \in D$
such that $a = b \cdot q + r$ 
with $r = 0$ or $d(r) < d(b)$.

\textbf{Examples}:

\begin{itemize}
    \item $\mathbb{Z}$ with $d(k) = |k|$
    \item $F[x]$ with $d(f(x)) = \text{deg}(f(x))$
\end{itemize}

\textbf{Theorem}: Every ED is a PID.

\textbf{Proof}: Challenge.

Fields $\subset$ ED $\subset$ PID $\subset$ UFD $\subset$ ID $\subset$ Rings
