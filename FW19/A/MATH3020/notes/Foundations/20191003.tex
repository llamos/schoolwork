%! TEX root = ../notes.tex

\subsubsection{Left and Right Cosets}

\textbf{Definitions}:
\begin{enumerate}
    \item The \underline{left coset} of $H$ containing $a$ is $aH = \{a \ast h \mid h \in H\}$
    \item The \underline{right coset} of $H$ containing $a$ is $Ha = \{h \ast a \mid h \in H\}$
\end{enumerate}

\textbf{Examples}:
\begin{itemize}
    \item $G = \mathbb{Z}, H = 2\mathbb{Z}$:
    \begin{itemize}
        \item $a = 0 \Rightarrow aH = \{0 + 2k \mid k \in \mathbb{Z}\}$
        \item $a = 1 \Rightarrow aH = \{1 + 2k \mid k \in \mathbb{Z}\}$
    \end{itemize}
    \item $G = \mathbb{Z}, H = 3\mathbb{Z}$:
    \begin{itemize}
        \item $a = 0 \Rightarrow aH = \{0 + 3k \mid k \in \mathbb{Z}\}$
        \item $a = 1 \Rightarrow aH = \{1 + 3k \mid k \in \mathbb{Z}\}$
        \item $a = 2 \Rightarrow aH = \{2 + 3k \mid k \in \mathbb{Z}\}$
    \end{itemize}
    \item $G = \mathbb{Z}/4, H = \{0,2\}$
    \begin{itemize}
        \item $a = 0 \Rightarrow aH = \{0 +_4 h \mid h \in H\} = \{0,2\}$
        \item $a = 1 \Rightarrow aH = \{1 +_4 h \mid h \in H\} = \{1,3\}$
    \end{itemize}
\end{itemize}

\textbf{Lemma}:
For $H \leq G$ and $a \in G$,
(1) $[a]_{\sim_L} = aH$ and (2) $[a]_{\sim_R} = Ha$.

\textbf{Proof}:
\begin{enumerate}[label=(\arabic*)]
    \item Let $x \in [a]_{\sim_L}$, then $a \sim_L x$, i.e. $a^{-1} \ast x \in H$.
    Therefore, $a \ast (a ^{-1} \ast x) \in aH$
    and $x \in aH$.

    Let $x \in aH$, then $x = a \ast h$, for some $h \in H$.
    Then, $a^{-1} \ast x = h \in H$.
    Therefore, $a \sim_L x$ and $x \in [a]_{\sim_L}$

    \item Follows similarly.
\end{enumerate}

\textbf{Lemma}: Let $\Phi_a : H \mapsto aH \mid \Phi_a(b) = a \ast b$.
$\Phi_a$ is a bijection.

\textbf{Proof}:
\begin{itemize}
    \item Injectivity: If $\Phi_a(b_1) = \Phi_a(b_2)$,
    then $a \ast b_1 = a \ast b_2 \Rightarrow b_1 = b_2$
    by cancellation laws.
    \item Surjectivity: If $c \in aH$, 
    $\exists b \in H \mid c = a \ast b$.
    So $\Phi_a(b) = c$.
\end{itemize}

\textbf{Proof 2}:
Construct the inverse.
$\phi_a : aH \mapsto H \mid \phi_a(b) = a^{-1} \ast b$.
Check composition with $\Phi_a$ produces identities.

\subsubsection{Lagrange's Theorem}

\textbf{Theorem}: If $G$ is a finite group,
and $H \leq G$,
then $\#H|\#G$.

\textbf{Proof}: We know $G = \cup_{a \in G} aH$
with $\#aH = \#H$.
So, $\#G = \#\text{distinct cosets} \times \#H$.
Thus, $\#H|\#G$.

\textbf{Corollary}: Let $(G, \ast, e)$ be a group and $a \in G$.
If $G$ is finite, then $\text{ord}(a)|\#G$.

\textbf{Proof}: $\text{ord}(a) = 
\text{order of } <a> = \{e, a^1, a^2, \dots, a^{n-1}\} = n$.
Since $a$ is a subgroup, $\text{ord}(a) = \#<a>|\#G$.

\textbf{Example}: $G = \mathbb{Z}/6, \#G = 6$.
$\text{ord}(0) = 1, \text{ord}(1) = 6, \text{ord}(2) = 3, \dots$.

\textbf{Corollary}: Let $(G, \ast, e)$ be a group of prime order.
Then,
\begin{enumerate}
    \item $G$ is cyclic
    \item $H \leq G \Rightarrow H = \{e\} \text{ or } H = G$
\end{enumerate}

\textbf{Proof}:
\begin{enumerate}
    \item Let $a \in G \mid \text{ord}(a) \neq 1$
    so $\text{ord}(a) = p$ where $p = \#G$.
    \[
        <a> = \{e, a^1, a^2, \dots, a^{p-1}\}
    \]
    $<a>$ has $p$ distinct elements.
    So $G = <a>$.
    \item Suppose $H \leq G$.
    $\#H = 1$ or $\#H = p$ so $H = \{e\}$ or $H=G$.
\end{enumerate}

\subsubsection{Indices}
\textbf{Remark}: Let $(G, \ast, e)$ is a finite group.
$(G : H) = \frac{\#G}{\#H}$.

\textbf{Example}: $G = \mathbb{Z}, H = 3\mathbb{Z}$, then $(G : H) = 3$.

\textbf{Remark}: If $G$ is abelian, then $aH = Ha$.

\textbf{Example}: $G = S_3$.\\
$\text{ord}(\rho_0) = 1$, $\text{ord}(\rho_1) = 3 = \text{ord}(\rho_2)$ \\
$\text{ord}(\mu_1) = 2 = \text{ord}(\mu_2) = \text{ord}(\mu_3)$

Let $H \leq G$ be arbitrarily chosen as $\{\rho_0, \mu_1\}$. \\
$\rho_1H = \{\rho_1\rho_2, \rho_1\mu_2\} = \left\{\rho_1, \begin{pmatrix}
    1, 2, 3 \\
    2, 1, 3
\end{pmatrix}\right\} = \{\rho_1,\mu_2\}$\\
$\rho_2H = \{\rho_2,\mu_2\}$\\
$H\rho_1 = \{\rho_1, \mu_1\rho_1\} = \left\{ \rho_4, \begin{pmatrix}
    1, 2, 3 \\
    3, 2, 1
\end{pmatrix} \right\} = \{\rho_1, \mu_2\}$ \\
$H\rho_2 = \{\rho_2, \mu_3\}$

$\rho_1H \neq H\rho_1$

\subsubsection{Normal Subgroups}

\textbf{Definition}: A subgroup $H \leq G$ is normal 
if $\forall a \in G, aH = Ha$.

\textbf{Remark}: 
\begin{itemize}
    \item If $G$ is abelian, all subgroups are normal.
    \item $H = \{\rho_0, \mu_1\}$ is not normal in $S_3$.
\end{itemize}

\textbf{Example}: $G = S_3, H = \{\rho_0, \rho_1, \rho_2\}$.\\
$\mu_1H = \{\mu_1, \mu_2, \mu_3\}$\\
$H\mu_1 = \{\mu_3, \mu_2, \mu_1\}$\\
$\mu_1H = H\mu_1$

\textbf{Corollary}: If $(G : H) = 2$, then $H$ is normal.

\textbf{Lemma}: $H \leq G$ is normal 
iff $\forall a \in G, aHa^{-1} \leq H$.

\textbf{Proof $\Rightarrow$}: Suppose $H \leq G$ is normal.
Fix $a \in G, h \in H$.
Since $aH = Ha$, 
there is $h' \in H \mid a \ast h = h' \ast a$.
So $a \ast h \ast a^{-1} = h' \ast a \ast a' = h' \in H$.

\textbf{Proof $\Leftarrow$}: Suppose $\forall a \in G$,
$aHa^{-1} \leq H$.
Fix $a \in G$.
Fix $x \in aH$, then $x = a \ast h$ for some $h \in H$.
$x \ast a^{-1} = a \ast h \ast a^{-1} \in H$.
Thus, $a \ast h = (a \ast h \ast a^{-1}) \ast a \in Ha$.

Fix $x \in Ha$, 
then $x = h' \ast a$ for some $h' = a \ast h \ast a' \in H$.
$h' \ast a = (a \ast h \ast a') \ast a = a \ast h \in aH$.

\textbf{Proposition}: Let $f: (G, \ast, e) \rightarrow (G', \ast', e')$
be a group homomorphism.
Then $\text{Ker}(f) = \{a \in G \mid f(a) = e'\} \leq G$ is normal.

\textbf{Proof}: Using the previous lemma,
for $a \in G$ and $b \in \text{Ker}(f)$,
need to show that $a \ast b \ast a^{-1} \in \text{Ker}(f)$.

\begin{align*}
    f(a \ast b \ast a^{-1}) &= f(a) \ast' f(b) \ast' f(a^{-1}) \\
    &= f(a) \ast' f(b) \ast' f(a)^{-1} \\
    &= f(a) \ast' e' \ast' f(a)^{-1} \\
    &= f(a) \ast' f(a)^{-1} \\
    &= e'
\end{align*}

\textbf{Remark}: If $H \leq G$ is normal,
then $f(H) \leq G$ need not be normal.
\[
    f : (\mathbb{Z}/2, +_2, 0) \rightarrow (S_3, \cdot, \rho_0) \mid f(0) = \rho_0, f(1) = \mu_1
\]
$f$ is a homomorphism. \\
$\mathbb{Z}/2 = H \leq \mathbb{Z}/2$ is normal 
but $f(H) = \{\rho_0, \mu_1\}$ is not normal in $S_3$.

\input{Foundations/20191008.tex}
