%! TEX root = ../notes.tex

\fancyhead[R]{\date{Sept. 10 \& 12, 2019}}
\fancyhead[C]{Foundations of Mathematics}

\subsubsection{Injecivity and Surjectivity}

A function $f: A \rightarrow B$ is:
\begin{itemize}
	\item \textbf{Injective} (one-to-one): if for $a,a' \in A$,
	      if $f(a)=f(a')$ then $a = a'$.
	\item \textbf{Surjective} (onto): if for each $b \in B$,
	      there is an $a \in A$ such that $f(a) = b$.
	\item \textbf{Bijective}: if it is injective and surjective.
\end{itemize}
*\textit{Kapulkin says: Don't use the terms one-to-one and onto.}

\textbf{Remark}: $f: A \rightarrow B$ is bijective
if for any $b \in B$ there is a 
\underline{unique} $a \in A$ such that $f(a) = b$.

\textbf{Challenge}: Prove the above.

Show the following:
\begin{itemize}
	\item $f: \mathbb{Z} \rightarrow \mathbb{Z} \vert f(k)=k$ is bijective.
	\item $f: \mathbb{Z} \rightarrow \mathbb{Z} \vert f(k)=k^2$ is neither injective nor surjective.
	\item $f: \mathbb{N} \rightarrow \mathbb{N} \vert f(n)=n \text{ mod } 57$ is neither injective nor surjective.
	\item $f: \mathbb{Z} \times \mathbb{Z} \rightarrow \mathbb{Z} \vert f(k,l) = k \times l$ is surjective, but not injective.
\end{itemize}

\subsubsection{Inverse}

\textbf{Lemma}: 
$f: A \rightarrow B$ is bijective 
iff there exists a $g: B \rightarrow A$ 
such that $f \circ g = \text{id}_B$ 
and $g \circ f = \text{id}_A$.
Call $g$ the inverse of $f$.

\textbf{Proof $\Rightarrow$}: 

Suppose $f$ is bijective.
For each $b \in B$, there is a unique $a \in A$
such that $f(a) = b$.
Define $g: B \rightarrow A \vert g(b) = a$.
As such, $f(g(b)) = f(a) = b = \text{id}_B$
and $g(f(a)) = g(b) = a = \text{id}_A$.

\textbf{Proof $\Leftarrow$}:

Suppose there is $g: B \rightarrow A$ such that
$f \circ g = \text{id}_B$ and $g \circ f = \text{id}_A$.

Injectivity:
Suppose that $f(a)=f(a')$ for some $a,a' \in A$.
Then $a = g(f(a)) = (g(f(a'))) = a'$.

Surjectivity: Define $a = g(b)$. 
Then, $f(a) = f(g(b)) = b$.

\textbf{Remark}: \\
Injectivity $\Leftrightarrow$
$g: B \rightarrow A$ with $g \circ f = \text{id}_A$. \\
Surjectivity $\Leftrightarrow$
$g: B \rightarrow A$ with $f \circ g = \text{id}_B$. \\
\textbf{Exercise}: Determine which one of these four statements requires an extra assumption.

\fancyhead[R]{\date{Sept. 12, 2019}}
\fancyhead[C]{Foundations of Mathematics}

\pagebreak

\fancyhead[R]{\date{Sept. 12, 2019}}
\fancyhead[C]{Abstract Algebra}

\section{Abstract Algebra}

\subsection{Binary Operations}

\textbf{Definition}: A \underline{binary operation} on a set $S$
is a function $\ast: S \times S \rightarrow S$. \\
\textbf{Notation}: $\ast(a,b) := a \ast b$

\textbf{Examples}:
\begin{enumerate}
	\item $+: \mathbb{R} \times \mathbb{R} \rightarrow \mathbb{R} \vert +(x,y) := x + y$
	\item $\cdot: \mathbb{R} \times \mathbb{R} \rightarrow \mathbb{R} \vert \cdot(x,y) := x \cdot y$
	\item $+: \mathbb{Z} \times \mathbb{Z} \rightarrow \mathbb{Z} \vert +(k,l) := k + l$
	\item $\cdot: \mathbb{N} \times \mathbb{N} \rightarrow \mathbb{N} \vert \cdot(m,n) := m \cdot n$
	\item Let $S$ be any set.
	      Define $\text{Fun}(S) = \{f: S \rightarrow S\}$. \\
	      $\circ : \text{Fun}(S) \times \text{Fun}(S) \rightarrow \text{Fun}(S) \vert \circ(f, g) = g \circ f$ \\
	      *\textit{Kapulkin says: I recommend this as your favorite binary operation.}
	\item $M_{m \times n}(\mathbb{R}) = $ set of $m \times n$ matrices with coefficients from $\mathbb{R}$.
	      \begin{enumerate}[label=\alph*.]
	      	\item $+: M_{m \times n}(\mathbb{R}) \times M_{m \times n}(\mathbb{R}) \rightarrow M_{m \times n}(\mathbb{R})$ \\
	      	      Matrix addition
	      	\item $\cdot: M_{n \times n}(\mathbb{R}) \times M_{n \times n}(\mathbb{R}) \rightarrow M_{n \times n}(\mathbb{R})$ \\
	      	      Matrix multiplication
	      \end{enumerate}    
	\item $\mathbb{R}[x] = $ set of polynomials with coefficients from $\mathbb{R}$.
	      \begin{enumerate}[label=\alph*.]
	      	\item $+: \mathbb{R}[x] \times \mathbb{R}[x] \rightarrow \mathbb{R}[x]$
	      	\item $\cdot: \mathbb{R}[x] \times \mathbb{R}[x] \rightarrow \mathbb{R}[x]$
	      \end{enumerate}
	\item Recall $\mathbb{Z}/n=\{0,1,\dots,n-1\}$
	      \begin{enumerate}[label=\alph*.]
	      	\item $+: \mathbb{Z}/n \times \mathbb{Z}/n \rightarrow \mathbb{Z}/n \vert +(a,b) := (a + b) \text{ mod } n$
	      	\item $\cdot: \mathbb{Z}/n \times \mathbb{Z}/n \rightarrow \mathbb{Z}/n \vert \cdot(a,b) := (a \cdot b) \text{ mod } n$
	      \end{enumerate}
\end{enumerate}

\textbf{Remark}:
\begin{itemize}
    \item Nullary operation: $\rightarrow S$
    \item Unary operation: $S \rightarrow S$
    \item Binary operation: $S \times S \rightarrow S$
    \item Ternary operation: $S \times S \times S \rightarrow S$
\end{itemize}

\subsection{Magmas}
\textbf{Definition}: A \underline{magma} is a set with a binary operation. \\
\textbf{Notation}: $(S, \ast)$ is a magma

\textbf{Product}:
Suppose $(S, \ast_S)$ and $(T, \ast_T)$ are magmas.
\[
    (S \times T) \times (S \times T) \rightarrow S \times T
\]
is given by first defining 
\begin{align*}
    \ast_{S \times T}(s,t,s',t') &= \ast_S(s,s'),\ast_T(t,t') \\
    \text{or,} \qquad (s,t) \ast_{S \times T} (s',t') &= (s \ast_S s', t \ast_T t')
\end{align*}

\subsubsection{Closed Under}

\textbf{Definition}:
Let $(S, \ast)$ be a magma.
A subset $H \subseteq S$ is \underline{closed under} $\ast$
if for any $a, b \in H$, we have $a \ast b \in H$.

\textbf{Examples}: 
\begin{itemize}
    \item $(\mathbb{Z}, +)$ magma
    \begin{itemize}
        \item $\mathbb{N} \subseteq \mathbb{Z}$ is closed under $+$.
        \item $H = \{n^2 \vert n \in \mathbb{Z}\}$ is not closed under $+$. \\
        Take $1,4 \in H$, and $1 + 4 = 5 \not\in H$.
    \end{itemize}
    \item $(\mathbb{Z}, \cdot)$ magma
    \begin{itemize}
        \item $\mathbb{N} \subseteq \mathbb{Z}$ is closed under $\cdot$.
        \item $H = \{n^2 \vert n \in \mathbb{Z}\}$ is closed under $\cdot$. \\
        Take $m^2,n^2 \in H$, then $m^2 n^2 = (mn)^2 \in H$.
    \end{itemize}
\end{itemize}

\subsubsection{Associativity and Commutativity}

\textbf{Definition}: A binary operation $\ast: S \times S \rightarrow S$ is:
\begin{enumerate}
    \item Associative if for all $a,b,c \in S$, $a \ast (b \ast c) = (a \ast b) \ast c$.
    \item Commutative if for all $a,b \in S$, $a \ast b = b \ast a$.
\end{enumerate}

\textbf{Examples}:
\begin{itemize}
    \item $(\mathbb{Z}, +)$ is associative and commutative
    \item $(\mathbb{Z}, \cdot)$ is associative and commutative
    \item $(\mathbb{R}[x], \cdot)$ is associative and commutative
    \item $(M_{n \times n}(\mathbb{R}), \cdot)$ is associative, but not commutative
    \item $(\text{Fun}(S), \circ)$ is associative, but not commutative
\end{itemize}

\subsubsection{Unit}

\textbf{Definition}: Let $(S, \ast)$ be a magma.
An element $e \in S$ is a unit of $\ast$ if
for all $a \in S$, we have $a \ast e = e \ast a = a$. \\

\textbf{Examples}:
\begin{itemize}
    \item $(\mathbb{Z}, +)$ has unit $0$
    \item $(\mathbb{Z}, \cdot)$ has unit $1$
    \item $(\text{Fun}(S), \circ)$ has unit $\text{id}_S$
    \item $(M_{n \times n}(\mathbb{R}), \cdot)$ has unit $I_n$
    \item Define $2\mathbb{Z} = \{2n \vert n \in \mathbb{N}\}$
    \begin{itemize}
        \item $(2\mathbb{Z}, +)$ has unit $0$
        \item $(2\mathbb{Z}, \cdot)$ has no unit
    \end{itemize}
\end{itemize}

\textbf{Lemma}: Let $(S, \ast)$ be a magma.
If $e, e'$ are units in $(S, \ast)$,
then $e = e'$.

\textbf{Proof}: Let $e, e'$ be units of $\ast$.
\begin{align*}
    e &= e \ast e' \qquad \text{since $e'$ is a unit} \\
      &= e' \qquad \text{since $e$ is a unit}
\end{align*}

\subsubsection{Homomorphisms}

\textbf{Definition}:
A \underline{homomorphism} of magmas 
$(S, \ast_S) \rightarrow (T, \ast_T) \vert f: S \rightarrow T$ 
such that for all $a, b \in S$,
$f(a \ast_S b) = f(a) \ast_T f(b)$.

\input{Foundations/20190917.tex}
