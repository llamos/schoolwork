%! TEX root = ../notes.tex

\textbf{Theorem}: $I \subseteq R$ is maximal iff $R/I$ is a field.

\textbf{Proof $\Longrightarrow$}: 

Suppose $I \subseteq R$ is maximal.

Let $a + I \in R/I$ such that $a + I \neq I$. 
Want to show $a + I$ has a multiplicative inverse.
Equivalently: $1 + I \in \langle a + I \rangle$.

Suppose otherwise, $1 + I \notin \langle a + I \rangle$.
Note that $q^{-1}(\langle a + I \rangle) \subseteq R$ is an ideal.
Also $I = q^{-1}(\langle 0 + I \rangle) \subseteq q^{-1}(\langle a + I \rangle)$.

Moreover, $J = q^{-1}(\langle a + I \rangle) \not \subseteq I$,
since $a \notin I$ (because $a + I \neq I$),
but $a \in J$.

Since $1 + I \notin \langle a + I \rangle$, 
we get $1 \notin q^{-1}(\langle a + I \rangle) = J$.
Thus, $J$ is a proper ideal containing $I$ 
and $I$ cannot be maximal.

\textbf{Proof $\Longleftarrow$}:

Suppose $R/I$ is a field.

Consider an intermediate ideal $J$, 
assuming that $I \subset J \subset R$.

Then, $q(I) = \{I\} \subset q(J) \subset q(R) = R/I$.

Since fields can only have two ideals,
$q(J)$ cannot exist as an intermediary ideal.
So, $I$ is maximal.

\textbf{Remark}: This confirms our conjecture:
$n\mathbb{Z} \subseteq \mathbb{Z}$ is maximal iff $n$ is prime.

\pagebreak

\textbf{Corollary}: A commutative ring with unity is a field
iff it has exactly two ideals.

\textbf{Proof $\Longrightarrow$}: Shown above.

\textbf{Proof $\Longleftarrow$}:

If $(R, +, \cdot, 0)$ has exactly two ideals,
then they are $R$ and $\{0\}$.
So, $0$ is the maximal ideal.

Thus, $R/\{0\} = R$ is a field.

\subsubsection{Prime Ideals}

\textbf{Definition}:
A proper ideal $I \subseteq R$ is \underline{prime}
if given $a,b \in R$ such that $a \cdot b \in I$,
we must have either $a \in I$ or $b \in I$.

\textbf{Examples}:
\begin{itemize}
    \item $2\mathbb{Z}$ is prime since if $2 \vert a \cdot b$, $2 \vert a$ or $2 \vert b$.
    \item $2\mathbb{Z}$ is prime since if $3 \vert a \cdot b$, $3 \vert a$ or $3 \vert b$.
    \item $6\mathbb{Z}$ is not prime since $2, 3 \notin 6\mathbb{Z}$, but $2 \cdot 3 \in 6\mathbb{Z}$.
    \item $n\mathbb{Z}$ is prime iff $n$ is prime or $n = 0$.
\end{itemize}

\textbf{Remark}: If $(D, +, \cdot, 0, 1)$ is an integral domain,
then $\{0\}$ is a prime ideal.

\textbf{Theorem}: Given an ideal $I \subseteq R$, it is prime iff $R/I$ is an integral domain.

\textbf{Proof $\Longrightarrow$}:

Suppose $I \subseteq R$ is a prime ideal.

Let $a + I, b + I \in R/I$ such that $(a + I) \cdot (b + I) = I$.
Want to show that $a + I = I$ or $b + I = I$.

So, $I = (a + I) \cdot (b + I) = (a \cdot b) + I$.
If $(a \cdot b) + I = I$, then $a \cdot b \in I$.

Since $I$ is prime, either $a \in I$ or $b \in I$
and therefore $a + I = I$ or $b + I = I$.

\textbf{Proof $\Longrightarrow$}:

Suppose $R/I$ is an integral domain.

Suppose $a \cdot b \in I$. Want to show that $a \in I$ or $b \in I$.

Since $a \cdot b \in I$, we have $I = a \cdot b + I = (a + I) \cdot (b + I)$.
By $R/I$ being an integral domain, we know that either $a + I = I$ or $b + I = I$.

If $a + I = I, a \in I$.
If $b + I = I, b \in I$.
So, either $a \in I$ or $b \in I$, and $I$ is prime.

\textbf{Corollary}: Let $(R, +, \cdot, 0, 1)$ be a commutative ring with unity.
If an ideal $I \subseteq R$ is maximal, then $I$ is also prime.

\textbf{Proof}: 
If $I \subseteq R$ is maximal, then $R/I$ is a field.
So, $R/I$ is an integral domain.
Henceforth, $I$ is prime.

\textbf{Remark}: Not every prime ideal is maximal.

In $\mathbb{Z}$, maximal means $p\mathbb{Z}$ where p is prime. \\
In $\mathbb{Z}$, prime means $p\mathbb{Z}$ where p is prime or 0.

Not every non-zero prime ideal is maximal either.

Let $R = \mathbb{Z}[x]$, the polynomials with integer coefficients.

Then, $\langle x \rangle = $ ideal of polynomials without constant terms.
This is evidently an ideal.

$\mathbb{Z}[x]/\langle x \rangle \cong \mathbb{Z}$, which is an integral domain.
So, $\langle x \rangle$ is prime.
Since $\mathbb{Z}$ is not a field, $\langle x \rangle$ is not maximal.

\textbf{Remark}: 
This argument works in an arbitrary integral domain $(D, +, \cdot, 0, 1)$
that is not a field by $\langle x \rangle \subseteq D[x]$ is prime, but not maximal.

\subsubsection{Z like Single-Element Polynomials}

\textbf{Recall}:

If $R$ is a commutative ring,
then $R[x]$ is also commutative. \\
If $R$ is with unity,
then $R[x]$ is also with unity.

However, if $F$ is a field,
$F[x]$ is not a field.

\textbf{Scholium}: Let $(F, +, \cdot, 0, 1)$ be a field.
Then, $F[x]$ is very much like $\mathbb{Z}$.
Intuition: $\mathbb{Z} = F_1[x]$.

\textbf{Recall}: \underline{Division Algorithm in $\mathbb{Z}$}

Given $a,b \in \mathbb{Z}$ such that $b \neq 0$,
there are unique $q,r \in \mathbb{Z}$
such that $a = b \cdot q + r$ 
where $0 \leq r < b$.

\textbf{Challenge}: Prove it!

\underline{Division Algorithm in $F[x]$}

Given $f(x), g(x) \in F[x]$,
such that $\text{deg}(g(x)) \geq 0$,
there are unique $q(x), r(x) \in F[x]$
such that $f(x) = g(x) \cdot q(x) + r(x)$
where $\text{deg}(r(x)) < \text{deg}(g(x))$.

\textbf{Proof}:

Consider the set $S = \{f(x) - g(x) \cdot q(x) \mid q(x) \in F[x]\}$.
Let $r(x) \in S$ be an element of minimal degree.
That is, $r(x) = f(x) - g(x) \cdot q(x)$ for some $q(x) \in F[x]$.

Want to show: $\text{deg}(r(x)) < \text{deg}(g(x))$.

Let $r(x) = c_kx^k + c_{k-1}x^{k-1} + \dots + c_1x + c_0$ where $c_i \in F$. \\
Let $g(x) = b_mx^m + b_{m-1}x^{m-1} + \dots + b_1m + b_0$ where $b_i \in F$.

Assume: $c_k \cdot b_k \neq 0$.

Thus, $\text{deg}(r(x)) = k$ and $\text{deg}(g(x)) = m$.
Want to show that $k < m$.

Suppose $k \geq m$ 
and consider $\bar{r}(x) = f(x) - g(x) \cdot q(x) - c_k \cdot b_m^{-1} \cdot x^{k-m} \cdot g(x)$,
\begin{align*}
    \bar{r}(x) &= f(x) - g(x) \cdot q(x) - c_k \cdot b_m^{-1} \cdot x^{k-m} \cdot g(x) \\
    &= f(x) - g(x) \cdot (q(x) - c_k \cdot b_m^{-1} \cdot x^{k - m})
\end{align*}

Then $\text{deg}(\bar{r}(x)) < \text{deg}(r(x))$
and also $\bar{r}(x) \in S$.

This is a contradiction with minimality of $\text{deg}(r(x))$,
and proves the Division Algorithm in $F[x]$.

\textbf{Corollary}: Let $(F, +, \cdot, 0, 1)$ be a field.
A polynomial $f(x) \in F[x]$ is divisible by $(x-a)$
iff $f(a) = 0$.

\textbf{Proof $\Longrightarrow$}:

If $f(x)$ is divisible by $(x-a)$, 
then $f(x) = (x-a) \cdot q(x)$,
so $f(a) = (a - a) \cdot q(a) = 0 \cdot q(a) = 0$.

\textbf{Proof $\Longleftarrow$}:

Suppose $f(a) = 0$.
Divide $f(x)$ by $(x-a)$ with remainder:
$f(x) = (x - a) \cdot q(x) + r(x)$.
Then, $\text{deg}(r(x))$ must be 0,
so let $r(x) = b$ where $b \in F$.

Thus: $0 = f(a) = (a - a) \cdot q(a) + r(a) = 0 \cdot q(a) + b = b$.
Hence $r(x) = 0$.

\textbf{Corollary}: A polynomial of degree $n$ has, at most, $n$ roots
(values $a \in F$ such that $f(a) = 0$).

In $\mathbb{Z}$, we have the notion of a prime number.

\textbf{Definition}:
A non-constant polynomial $f(x) \in F[x]$
is \underline{irreducible} if there are no polynomials
$g(x),h(x) \in F[x]$ such that $f(x) = g(x) \cdot h(x)$ 
and $\text{deg}(g(x)),\text{deg}(h(x)) < \text{deg}(f(x))$.

\textbf{Examples}:
\begin{itemize}
    \item $f(x) = x^2 - 2$ is irreducible over $\mathbb{Q}[x]$.
    \item $f(x) = x^2 - 2$ is reducible over $\mathbb{R}[x]$: $(x - \sqrt{2}) \cdot (x + \sqrt{2})$.
    \item $f(x) = x^2 + 1$ is irreducible over $\mathbb{R}[x]$.
    \item $f(x) = x^2 + 1$ is reducible over $\mathbb{C}[x]$: $(x + i) \cdot (x - i)$.
    \item $f(x) = x^3 + x + 1$ is irreducible over $F_2[x]$.
    \begin{itemize}
        \item Since $\text{deg}(g(x))$ must be 1, $g(x) = x - 0$ or $g(x) = x - 1$.
    \end{itemize}
\end{itemize}

\input{Foundations/20191119.tex}
