%! TEX root = ../notes.tex

\fancyhead[R]{\date{Sept. 17 \& 19, 2019}}
\fancyhead[C]{Abstract Algebra}

\textbf{Examples of Groups:}
\begin{itemize}
	\item $(\mathbb{Z}, +, 0)$ is an abelian group
	\item $(\{e\}, \ast, e)$ is an abelian group
	\item $(\mathbb{R}, +, 0)$ is an abelian group
	\item $(\mathbb{R}[x], +, 0)$ is an abelian group
	\item $M_{m \times n}(\mathbb{{R}}, +, 0)$ is a abelian group
\end{itemize}

\textbf{Claim}: 
$(\mathbb{Z}/n, +_n, 0)$ is an abelian group
\begin{enumerate}
	\item $\forall a,b,c \in \mathbb{Z}/n, (a +_n b) +_n c = a +_n (b +_n c)$
	\item $\forall a \in \mathbb{Z}/n, a +_n 0 = a $
	\item $\forall a \in \mathbb{Z}/n, \exists b \in \mathbb{Z} \vert a +_n b = 0$
\end{enumerate}

\textbf{Examples of Non-groups}:
\begin{itemize}
	\item $\emptyset$ is not a group, since it does not have an $e$
	\item $(\mathbb{Z}, \cdot, 1)$ is not a group, since it is not invertible
	      \begin{itemize}
	      	\item $(\mathbb{Q}, \cdot, 1)$ is not a group, since $0$ does not have an inverse.
	      	\item $(\mathbb{Q}_{\neq 0}, \cdot, 1)$ is a group, since it now has inverses for all elements.
	      \end{itemize}
	\item $M_{n \times n}(\mathbb{{R}}, \cdot, I_n)$ is not a group, since a 0-matrix has no inverse.
	      \begin{itemize}
	      	\item $(\{A \in M_{n \times n}(\mathbb{R}) \vert A \text{ is invertible}\}, \cdot, I_n)$
	      \end{itemize}
	\item For any set $S$, $(\text{Fun}(S), \circ, \text{id}_S)$ is not a group, since all $f$ in $\text{Fun}(S)$ would need to be bijective.
\end{itemize}

\fancyhead[R]{\date{Sept. 19, 2019}}
\fancyhead[C]{Abstract Algebra}

\textbf{Claim}:
$(\text{Fun}(S), \circ, \text{id}_S)$ 
is a group iff
$S = \emptyset$ or $\vert S \vert = 1$.

\textbf{Proof $\Leftarrow$}: \\
If $S = \emptyset$, then $\text{Fun}(S) = \{\text{id}_\emptyset\}$. \\
If $S = \{s\}, then \text{Fun}(S) = \{\text{id}_S\}$.

\textbf{Proof $\Rightarrow$} by contrapositive:
If $S \neq \emptyset$ and $\vert S \vert \neq 1$,
then $(\text{Fun}(S), \circ, \text{id}_S)$ is not a group. \\
If $\vert S \vert \geq 2$, 
we define a function $f: S \rightarrow S \vert \forall a \in S, f(a)=s$.
Therefore, $f(s)=f(t) \not \Rightarrow s = t$,
and $f$ is not bijective, so does not have an inverse.


For $S$, 
define $\text{Bij}(S)=\{f: S \rightarrow S \vert f \text{ is bijective}\}$,
$(\text{Bij}(S), \circ, \text{id}_S)$ is a group.

\subsubsection{Product of Groups}

Let $(G, \ast_G, e_G), (H, \ast_H, e_H)$ be groups.

\textbf{Definition}: \\
Define the \underline{product of groups} as 
$(G \times H, \ast_{G \times H}, e_{G \times H})$ \\
where $(a, b), (a', b') \in G \times H$.
\[
	(a, b) \ast_{G \times H} (a', b') = (a \ast_G a', b \ast_H b')
\]
and $e_{G \times H} = (e_G, e_H)$.

\textbf{Claim}: This defines a group.

\textbf{Proof}:
\begin{enumerate}
	\item Given $(a,b), (a', b'), (a'', b'') \in G \times H$,
	      we have
	      \begin{align*}
	      	(a, b) \ast_{G \times H} ((a', b') \ast_{G \times H} (a'', b'')) & = (a, b) \ast_{G \times H} (a' \ast_G a'', b' \ast_H b'')          \\
	      	                                                                 & = (a \ast_G (a' \ast_G a''), b \ast_H (b' \ast_H b''))             \\
	      	                                                                 & = ((a \ast_G a') \ast_G a'', (b \ast_H b') \ast_H b'')             \\
	      	                                                                 & = (a \ast_G a', b \ast_H b') \ast_{G \times H} (a'', b'')          \\
	      	                                                                 & = ((a, b) \ast_{G \times H} (a', b') \ast_{G \times H} (a'', b'')) 
	      \end{align*}
	\item Left as exercise
	\item Left as exercise
\end{enumerate}

\subsubsection{Cancellation Laws}

Let $(G, \ast, e)$ be a group and $a, b, c \in G$.
\begin{enumerate}
	\item If $a \ast b = a \ast c$, then $b = c$
	\item If $b \ast a = c \ast a$, then $b = c$
\end{enumerate}

\textbf{Proof}:
\begin{enumerate}
	\item Suppose $a \ast b = a \ast c$ 
	      and let $\bar{a}$ be an inverse of $a$.
	      \begin{align*}
	      	b & = e \ast b                \\
	      	  & = (\bar{a} \ast a) \ast b \\
	      	  & = \bar{a} \ast (a \ast b) \\
	      	  & = \bar{a} \ast (a \ast c) \\
	      	  & = (\bar{a} \ast a) \ast c \\
	      	  & = e \ast c                \\
	      	  & = c                       
	      \end{align*}
	\item Suppose $b \ast a = c \ast a$
	      and let $\bar{a}$ be an inverse of $a$.
	      By a similar process, this is true.
\end{enumerate}

\subsubsection{Uniqueness of Propositions}

\textbf{Recall}: Units are unique. $e$ is the only unit of $G$.

\textbf{Proposition}: For any $a \in G$, 
there is a \underline{unique} $b \in G \vert a \ast b = b \ast a = e$.

\textbf{Proof}:
\begin{itemize}
    \item \textbf{Existence}: by definition of group
    \item \textbf{Uniqueness}: given $b, b'$ such that $a * b = b * a = e$ and $a * b' = b' * a = e$.
    By cancellation laws, $b = b'$.
\end{itemize}

\textbf{Notation}: Write $a^{-1}$ for the unique inverse of $a$. \\
\textbf{Corrollary}: Let $(G, \ast, e)$ be a group and $a,b \in G$.
\[
    (a \ast b)^{-1} = b^{-1} \ast a^{-1}
\]
\textbf{Proof}: We have that
\begin{align*}(b^{-1} \ast a^{-1})
    (a \ast b) \ast (b^{-1} \ast a^{-1}) &= ((a \ast b) \ast b^{-1}) \ast a^{-1} \\
    &= a \ast (b \ast b^{-1}) \ast a^{-1} \\
    &= a \ast e \ast a^{-1} \\
    &= a \ast a^{-1} \\
    &= e
\end{align*}
Also, $(a \ast b) \ast (a \ast b)^{-1} = e$.
Therefore, by the cancellation laws, 
$b^{-1} \ast a^{-1} = (a \ast b)^{-1}$.

\subsubsection{Subgroups}

\textbf{Definition}:
Let $(G, \ast, e)$ be a group.
A subset $H \subseteq G$ closed under $\ast$
is a \underline{subgroup} of $G$ 
if $H$ with $\ast$ is a group.

\textbf{Examples}:
\begin{itemize}
    \item $(\mathbb{Z}, +, 0)$ is a subgroup of $(\mathbb{R}, +, 0)$.
    \item $(\{(x, 0) \in \mathbb{R}^2 \vert x \in \mathbb{R}\}, +, (0, 0))$ is a subgroup of $(\mathbb{R}^2, +, (0, 0))$.
\end{itemize}

\textbf{Notation}: Write $H \leq G$ for $H$ being a subgroup of $G$.

\textbf{Characterization}:
A subset $H \subseteq$ of $G$ is a subgroup iff
\begin{enumerate}[label=(\arabic*)]
    \item $H$ is closed under $\ast$,
    \item $e \in H$,
    \item $\forall h \in H$, $h^{-1} \in H$
\end{enumerate}

\textbf{Proof $\Rightarrow$}: Evident

\textbf{Proof $\Leftarrow$}: Suppose $H$ satisfies (1), (2), and (3).
\begin{itemize}
    \item Associativity: $\ast$ is associative in $G$, so it must be associative in $H$.
    \item Unit: $e \in H$, so the same identity must hold.
    \item Inverses: By (3), $h, h^{-1} \in H$.
\end{itemize}

\subsubsection{Group Homomorphisms}

\textbf{Definition}:
A \underline{group homomorphism} 
from $(G, \ast_G, e_G)$ to $(H, \ast_H, e_H)$
is a function $f: G \rightarrow H$
such that $\forall a, b \in G$,
$f(a \ast_G b) = f(a) \ast_H f(b)$. \\

\textbf{Examples}:
\begin{itemize}
    \item For any $(G, \ast, e)$ 
    the $\text{id}_G: G \rightarrow G$
    is a homomorphism.
    \[
        \text{id}_G(a \ast b) = a \ast b = \text{id}_G(a) \ast \text{id}_G(b)
    \]
    \item Let $(G, \ast_G, e_G)$ and $(H, \ast_H, e_H)$ be groups.
    The following is a homomorphism:
    \[
        f: G \rightarrow H \vert f(g \in G) = e_H 
    \]
    \item $\text{det}: \text{GL}_n(\mathbb{R}) \rightarrow \mathbb{R}^\times$ is a homomorphism
    from $(\text{GL}_n(\mathbb{R}), \cdot, I_n)$ to $(\mathbb{R}^\times, \cdot, 1)$.
    \item Fix $n \in N$. $m_n: \mathbb{Z} \rightarrow \mathbb{Z} \vert m_n(k) = nk$ is a homomorphism.
    from $(\mathbb{Z}, +, 1)$ to $(\mathbb{Z}, +, 1)$.
    \item $q_n : \mathbb{Z} \rightarrow \mathbb{Z}/n \vert q_n(k) = k \text{ mod } n$ is a homomorphism
    from $(\mathbb{Z}, +, 0)$ to $(\mathbb{Z}/n, +_n, 0)$.
\end{itemize}

\textbf{Define}: A group homomorphism $f: (G, \ast_G, e_G) \rightarrow (H, \ast_H, e_H)$ is:
\begin{itemize}
    \item a \underline{monomorphism} if $f$ is injective
    \item an \underline{epimorphism} if $f$ is surjective
    \item an \underline{isomorphism} if $f$ is bijective
\end{itemize}

\textbf{Examples}:
\begin{itemize}
    \item For any $(G, \ast, e)$ 
    the $\text{id}_G: G \rightarrow G$
    is an isomorphism.
    \item $f: G \rightarrow H \vert f(k) = e_H$ is a
    \begin{itemize}
        \item epimorphism if $H = \{e_H\}$
        \item monomorphism if $G = \{e_G\}$
        \item isomorphism if $G = \{e_G\}$ and $H = \{e_H\}$
    \end{itemize}
    \item $\text{det}: \text{GL}_n(\mathbb{R}) \rightarrow \mathbb{R}^\times$ is an epimorphism.
    from $(\text{GL}_n(\mathbb{R}), \cdot, I_n)$ to $(\mathbb{R}^\times, \cdot, 1)$ varies.
    \item Fix $n \in N$. $m_n: \mathbb{Z} \rightarrow \mathbb{Z} \vert m_n(k) = nk$ is a monomorphism if $n \neq 0$ and an isomorphism if $n = 1$.
    \item $q_n : \mathbb{Z} \rightarrow \mathbb{Z}/n \vert q_n(k) = k \text{ mod } n$ is an epimorphism.
\end{itemize}

\input{Foundations/20190924.tex}
