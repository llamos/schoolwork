%! TEX root = asn1.tex

\begin{problem}
\begin{problemTitle}
	For each of the following languages, prove whether it is regular or not.
	If it is, then
	\begin{itemize}
		\renewcommand\labelitemi{-}
		\item construct a NDFSM for it
		\item convert the NDSFM into a DFSM
		\item minimize the DFSM
		\item convert one of the machines into a regular expression
	\end{itemize}
	Show your work.
	\begin{enumerate}[label=(\alph*)]
		\item $\{w^Rww^R \mid w \in \{a,b\}^*\}$
		\item $\{w \in \{0,1\}^* \mid w$ has 1010 as substring\}
		\item $\{w \in \{0,1\}^* \mid w$ does not have 1010 as substring\}
		\item $\{w \in \{a,b\}^* \mid$ every $b$ in $w$ is immediately preceeded and followed by $a$\}
		\item $\{w \in \{a,b\}^* \mid$ the third and second from the last characters are $b$'s\}
		\item $\{w \in \{a,b\}^* \mid (\#_a(w) + 2\#_b(w)) \equiv 0 $ (mod 4) \}
		\item $\{w \in \{a,b\}^* \mid \#_a(w) - 2\#_b(w) = 0\}$
		\item $\{w \in \Sigma^* \mid w$ is a C comment\}, where $\Sigma$ is the keyboard alphabet
	\end{enumerate}
\end{problemTitle}

\begin{enumerate}[label=(\alph*)]
	\item $\{w^Rww^R \mid w \in \{a,b\}^*\}$
	      	
	      \textit{Via the pumping theorem:}
	      
	      If we pick $w = ab^k$, where $k$ is our pumping constant, such that $w' \in L = w^Rww^R = b^kaab^kb^ka$,
	      and split the string into $w' = xyz$ among the constraints $|xy| \leq k, y \neq \epsilon$,
	      we should be able to construct further strings in $L$ via $w'' = xy^qz$.
	      
	      However, for $|xy|$ to be $\leq k$, it can only ever reach as far as $k$ elements,
	      meaning it only ever can be filled with $b$'s.
	      Therefore, when pumping $y$, regardless of $x$, it will only pump $b$'s.
	      When pumping infinite $b$'s, or removing $b$'s, 
	      it breaks the symmetry of the reversal in the string definition,
	      and therefore is not closed under the language.
	      
	      Thus, the language is not regular.
	      							 
	\item $\{w \in \{0,1\}^* \mid w$ has 1010 as substring\}
	      	      	      	      	
	      This string can be trivially represented as the regular expression $(0|1)^*1010(0|1)^*$.
	      	      	      	      
	      Thus, an NDFSM for the machine is:
	      	      	      	      
	      \begin{center}
	      	\begin{tikzpicture}
	      		\node[initial,state] (q1) at (0,0) {q1};
	      		\node[state] (q2) at (2,0) {q2};
	      		\node[state] (q3) at (4,0) {q3};
	      		\node[state] (q4) at (6,0) {q4};
	      		\node[state,accepting] (q5) at (8,0) {q5};
	      			      			      			      		
	      		\path[draw] (q1) edge[loop above,->] node {0,1} (q1)
	      		(q1) edge[->] node[above] {1} (q2)
	      		(q2) edge[->] node[above] {0} (q3)
	      		(q3) edge[->] node[above] {1} (q4)
	      		(q4) edge[->] node[above] {0} (q5)
	      		(q5) edge[loop above,->] node {0,1} (q5);
	      	\end{tikzpicture}
	      \end{center}
	      						  
	      \pagebreak
	      From this, we can construct a DFSM as such:
	      	      	      	      
	      \begin{center}
	      	\begin{tikzpicture}
	      		\node[initial,state] (q1) at (0,0) {1};
	      		\node[state] (q12) at (2,0) {1,2};
	      		\node[state] (q13) at (4,0) {1,3};
	      		\node[state] (q124) at (6,0) {1,2,4};
	      		\node[state,accepting] (q135) at (8,0) {1,3,5};
	      		\node[state,accepting] (q15) at (8,2) {1,5};
	      		\node[state,accepting] (q125) at (10,0) {1,2,5};
	      		\node[state,accepting] (q1245) at (8,-2) {1,2,4,5};
	      			      			      			      		
	      		\path[draw] (q1) edge[loop above,->] node {0} (q1)
	      		(q1) edge[->] node[above] {1} (q12)
	      		(q12) edge[->] node[above] {0} (q13)
	      		(q12) edge[->,loop above] node {1} (q1)
	      		(q13) edge[->,bend left] node[below] {0} (q1)
	      		(q13) edge[->] node[above] {1} (q124)
	      		(q124) edge[->] node[above] {0} (q135)
	      		(q124) edge[->, bend left] node[below] {1} (q12)
	      		(q135) edge[->] node[right] {0} (q15)
	      		(q135) edge[->] node[right] {1} (q1245)
	      		(q15) edge[->,loop left] node {0} (q15)
	      		(q15) edge[->] node[above] {1} (q125)
	      		(q125) edge[->] node[above] {0} (q135)
	      		(q125) edge[->,loop right] node {1} (q135)
	      		(q1245) edge[->,bend left] node[left] {0} (q135)
	      		(q1245) edge[->] node[above] {1} (q125);
	      	\end{tikzpicture}
	      \end{center}
	      		  
	      In all cases, once a string enters state $1,3,5$, it cannot be rejected,
	      since all further states are also accepting.
	      Thus, states $1,5$, $1,2,5$, and $1,2,4,5$ are redundant.
	      
	      However, none of the rejecting states are redundant,
	      as all outputs lead to different distances from the accepting state,
	      along the substring path.
	      
	      Therefore, a minimal DFSM is:
	      	      	      	      
	      \begin{center}
	      	\begin{tikzpicture}
	      		\node[initial,state] (q1) at (0,0) {1};
	      		\node[state] (q12) at (2,0) {1,2};
	      		\node[state] (q13) at (4,0) {1,3};
	      		\node[state] (q124) at (6,0) {1,2,4};
	      		\node[state,accepting] (q135) at (8,0) {1,3,5};
	      			      			      			      		
	      		\path[draw] (q1) edge[loop above,->] node {0} (q1)
	      		(q1) edge[->] node[above] {1} (q12)
	      		(q12) edge[->] node[above] {0} (q13)
	      		(q12) edge[->,loop above] node {1} (q1)
	      		(q13) edge[->,bend left] node[below] {0} (q1)
	      		(q13) edge[->] node[above] {1} (q124)
	      		(q124) edge[->] node[above] {0} (q135)
	      		(q124) edge[->, bend left] node[below] {1} (q12)
	      		(q135) edge[->,loop right] node {0,1} (q135);
	      	\end{tikzpicture}
	      \end{center}
	      	      	      	      
	\item $\{w \in \{0,1\}^* \mid w$ does not have 1010 as substring\}
	      	      	      	      	
	      By inverting the DFSM in (b), we receive a DFSM for this problem:
	      	      	      	      
	      \begin{center}
	      	\begin{tikzpicture}
	      		\node[initial,state,accepting] (q1) at (0,0) {1};
	      		\node[state,accepting] (q12) at (2,0) {1,2};
	      		\node[state,accepting] (q13) at (4,0) {1,3};
	      		\node[state,accepting] (q124) at (6,0) {1,2,4};
	      		\node[state] (q135) at (8,0) {1,3,5};
	      															
	      		\path[draw] (q1) edge[loop above,->] node {0} (q1)
	      		(q1) edge[->] node[above] {1} (q12)
	      		(q12) edge[->] node[above] {0} (q13)
	      		(q12) edge[->,loop above] node {1} (q1)
	      		(q13) edge[->,bend left] node[below] {0} (q1)
	      		(q13) edge[->] node[above] {1} (q124)
	      		(q124) edge[->] node[above] {0} (q135)
	      		(q124) edge[->, bend left] node[below] {1} (q12)
	      		(q135) edge[->,loop right] node {0,1} (q135);
	      	\end{tikzpicture}
	      \end{center}
	      							  
	      Similarly to $(b)$, the regular expression is $(0|1)^*(\neg(1010))(0|1)^*$.
	      
	      \pagebreak
	      	      	      	      
	\item $\{w \in \{a,b\}^* \mid$ every $b$ in $w$ is immediately preceeded and followed by $a$\}
	      	      	      	      	
	      Here is a DFSM for the language:
	      	      	      	      
	      \begin{center}
	      	\begin{tikzpicture}
	      		\node[initial,state,accepting] (q1) at (0,0) {$q_1$};
	      		\node[state,accepting] (q2) at (2,0) {$q_2$};
	      		\node[state] (q3) at (4,0) {$q_3$};
	      		\node[state] (q4) at (2,2) {$q_4$};
	      			      			      			      		
	      		\path[draw] (q1) edge[->] node[above] {$a$} (q2)
	      		(q2) edge[->,loop below] node {$a$} (q2)
	      		(q2) edge[->] node[above] {$b$} (q3)
	      		(q3) edge[->,bend left] node[below] {$a$} (q2)
	      		(q1) edge[->] node[above] {$b$} (q4)
	      		(q3) edge[->] node[above] {$b$} (q4)
	      		(q4) edge[->, loop above] node {$a,b$} (q4);
	      	\end{tikzpicture}
	      \end{center}
	      	      	      	      
	      This is also the minimal DFSM as among accepting states, $\delta(q_1,b) \neq \delta(q_2,b)$ 
	      and among rejecting states, $\delta(q_3, a) \neq \delta(q_4, a)$.
	      	      	      	      
	      This can be represented by the regular expression $(a\texttt{+}(ba\texttt{+})^*)^*$.
	      	      	      	      
	      	      	      	      	
	\item $\{w \in \{a,b\}^* \mid$ the third and second from the last characters are $b$'s\}
	      	      	      	      
	      An NDFSM for the language is:
	      	      	      	      
	      \begin{center}
	      	\begin{tikzpicture}
	      		\node[initial,state] (q1) at (0,0) {$q_1$};
	      		\node[state] (q2) at (2,0) {$q_2$};
	      		\node[state] (q3) at (4,0) {$q_3$};
	      		\node[state,accepting] (q4) at (6,0) {$q_4$};
	      			      			      			      		
	      		\path[draw] (q1) edge[->,loop above] node {$a,b$} (q1)
	      		(q1) edge[->] node[above] {$b$} (q2)
	      		(q2) edge[->] node[above] {$b$} (q3)
	      		(q3) edge[->] node[above] {$a,b$} (q4);
	      	\end{tikzpicture}
	      \end{center}
	      	      					
	      Thus, we can derive a DFSM as such:
	      	      	      	      
	      \begin{center}
	      	\begin{tikzpicture}
	      		\node[initial,state] (q1) at (0,0) {1};
	      		\node[state] (q12) at (2,0) {1,2};
	      		\node[state] (q123) at (4,0) {1,2,3};
	      		\node[state,accepting] (q124) at (4,2) {1,2,4};
	      		\node[state,accepting] (q1234) at (6,0) {1,2,3,4};
	      		\node[state,accepting] (q14) at (6,-2) {1,4};
	      			      										  
	      		\path[draw] (q1) edge[->,loop above] node {$a$} (q1)
	      		(q1) edge[->] node[above] {$b$} (q12)
	      		(q12) edge[->,bend left] node[below] {$a$} (q1)
	      		(q12) edge[->] node[above] {$b$} (q123)
	      		(q123) edge[->] node[left] {$a$} (q124)
	      		(q123) edge[->] node[above] {$b$} (q1234)
	      		(q124) edge[->, bend right] node[above] {$a$} (q1)
	      		(q124) edge[->, bend left] node[right] {$b$} (q123)
	      		(q1234) edge[->] node[right] {$a$} (q14)
	      		(q1234) edge[->,loop above] node {$b$} (q1234)
	      		(q14) edge[->,bend left] node[above] {$b$} (q12)
	      		(q14) edge[->,bend left] node[below] {$a$} (q1);
	      	\end{tikzpicture}
	      \end{center}
	      		  
	      This is a minimal DFSM, observing that among accepting states,
	      there are no permanently accepting loops, redundant states, or duplicates.
	      Among rejecting states, each is a finite, unique distance from any other accepting state.
	      
	      From the NDFSM, a regular expression of $(a|b)^*bb(a|b)$ can be constructed.
	      							  
	      \pagebreak 
	\item $\{w \in \{a,b\}^* \mid (\#_a(w) + 2\#_b(w)) \equiv 0 $ (mod 4) \}
	      	
	      For a string to be in this language, 
	      it must contain twice as many $a$'s as $b$'s, mod 4.
	      However, since there are an infinite number of ways to uniquely satisfy this
	      ($aab, aba, baa, aaaabb, aaabba, aabbaa, abbaaa, \dots$)
	      it cannot be represented as a finite set of repeating $a$'s and $b$'s,
	      in any combinative order.
	      
	      Thus, the language is not regular.
	      	      	      	      
	\item $\{w \in \{a,b\}^* \mid \#_a(w) - 2\#_b(w) = 0\}$
	      	      		
		  By the pumping theorem, with a pumping constant of $k$,
		  we can pick a string $w \in L = a^{2k}b^k$.
		  Thus, all splits $w = xyz$ have $xy$ representing a portion of $a^{2k}$, up to $a^k$.
		  However, by removing or inserting more $a$'s, it throws off the balance of $\#_a(w) - 2\#_b(w)$
		  and is no longer in $L$.

		  Thus, the language is not regular.
	      	
	\item $\{w \in \Sigma^* \mid w$ is a C comment\}, where $\Sigma$ is the keyboard alphabet
	      	      	
	      An NDFSM that checks this language is:
	      	      
	      \begin{center}
	      	\begin{tikzpicture}
	      		\node[initial,state] (q1) at (0,0) {$q_1$};
	      		\node[state] (q2) at (2,0) {$q_2$};
	      		\node[state] (q3) at (4,0) {$q_3$};
	      		\node[state] (q4) at (2,-2) {$q_4$};
	      		\node[state] (q5) at (4,-2) {$q_5$};
	      		\node[state,accepting] (q6) at (6,-1) {$q_6$};
	      			      										  
	      		\path[draw] (q1) edge[->] node[above] {/} (q2)
	      		(q2) edge[->] node[above] {/} (q3)
	      		(q3) edge[->,loop above] node {$\Sigma \setminus \{\backslash n\}$} (q3)
	      		(q3) edge[->] node[above] {$\backslash n$} (q6)
	      		(q2) edge[->] node[right] {*} (q4)
	      		(q4) edge[->,loop left] node {$\Sigma$} (q5)
	      		(q4) edge[->] node[above] {*} (q5)
	      		(q5) edge[->] node[above] {/} (q6);
	      	\end{tikzpicture}
	      \end{center}
	      	      
	      Thus, a derived DFSM is:
	      	      
	      \begin{center}
	      	\begin{tikzpicture}
	      		\node[initial,state] (q1) at (0,0) {1};
	      		\node[state] (q2) at (2,0) {2};
	      		\node[state] (q3) at (4,0) {3};
	      		\node[state] (q4) at (2,-2) {4};
	      		\node[state] (q45) at (4,-2) {4,5};
	      		\node[state,accepting] (q6) at (6,-1) {6};
	      			      										  
	      		\path[draw] (q1) edge[->] node[above] {/} (q2)
	      		(q2) edge[->] node[above] {/} (q3)
	      		(q3) edge[->,loop above] node {$\Sigma \setminus \{\backslash n\}$} (q3)
	      		(q3) edge[->] node[above] {$\backslash n$} (q6)
	      		(q2) edge[->] node[right] {*} (q4)
	      		(q4) edge[->,loop left] node {$\Sigma \setminus \{*\}$} (q45)
	      		(q4) edge[->] node[above] {*} (q45)
	      		(q45) edge[->] node[above] {/} (q6)
	      		(q45) edge[->,bend left] node[below] {$\Sigma \setminus \{/,*\}$} (q4)
	      		(q45) edge[->,loop right] node {*} (q45);
	      	\end{tikzpicture}
		  \end{center}
		  
		  Finally, a regular expression from the NDFSM is $/((/.^*\backslash n) | (.^***/))$.
	      	      
\end{enumerate}

\end{problem}
