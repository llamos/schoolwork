%! TEX root = asn3.tex

\begin{problem}
\begin{problemTitle}
	Construct a deterministric Turing Machine $M$
	that adds one to its binary input if it is even
	and subtracts one of it is odd.
	$M$ starts with the initial configuration 
	$(s, \square w)$, where $w \in \{0,1\}^*$;
	the binary input $w$ is interpreted as an integer number.
	Possible leading $0$'s have to be removed as well.
	The machine halts in the appropriate configuration
	$(h, \square(w + 1)_{(2)})$, where $w_{(2)}$ is the binary representation of $w$.
					
	Describe $M$ using the macro language.
\end{problemTitle}

This problem can be broken down into the following steps:
\begin{enumerate}[label=\arabic*.]
	\item Strip leading zeros
	\item Scroll to end of number
	      \begin{enumerate}[label=\arabic*.]
	      	\item If odd (last bit is 1), subtract one (flip last bit)
	      	\item If even (last bit is 0 or $\square$), add one (flip last bit)
	      \end{enumerate}
\end{enumerate}

Notice that regardless of parity for the input number,
the addition or subtraction will not cause carryover,
and flipping the last bit regardless of value is satisfactory,
so these steps can be combined into one ``flip last bit'' step.

\begin{enumerate}[label=\arabic*.]
	\item Strip leading zeros
	\item Scroll to end of number
	\item Flip last bit
	\item Rewind the head to the beginning of the string
\end{enumerate}

As such, the first action for M to complete 
is to strip the leading zeros as we begin reading.
We can do this via
\begin{center}
	\begin{tikzpicture}
		\node (I) at (0,0) {$>$};
		\node (L0MR) at (2,0) {$M_\square R$};
								
		\path[draw,->] (I) edge node[above] {$\square$} (L0MR)
		(L0MR) edge[loop right] node[right] {$0$} (L0MR);
	\end{tikzpicture}
\end{center}

Next, scrolling to the last character of the input is as simple as
\begin{center}
	\begin{tikzpicture}
		\node (RL) at (0,0) {$>R_\square L$};
	\end{tikzpicture}
\end{center}

The third step, flipping the bit, 
can be achieved by the following ``forking'' machine.

\begin{center}
	\begin{tikzpicture}
		\node (I) at (0,0) {$>$};
		\node (M0) at (2,0.5) {$M_0$};
		\node (M1) at (2,-0.5) {$M_1$};
				
		\path[draw,->] (I) edge node[above] {$1$} (M0)
		(I) edge node[below] {$0, \square$} (M1);
	\end{tikzpicture}
\end{center}

The final step, rewinding the head,
is also a simple one:

\begin{center}
	\begin{tikzpicture}
		\node (IL) at (0,0) {$>L_\square$};
	\end{tikzpicture}
\end{center}

Combining the four machines gives us 
the following complete Turing Machine $M$:

\begin{center}
	\begin{tikzpicture}
		\node (I) at (0,0) {$>$};
		\node (L0MR) at (2,0) {$M_\square R$};
		\node (RL) at (4,0) {$R_\square L$};
		\node (M0) at (6,0.5) {$M_0$};
		\node (M1) at (6,-0.5) {$M_1$};
		\node (FL) at (8,0) {$L_\square$};
								
		\path[draw,->] (I) edge node[above] {$\square$} (L0MR)
		(L0MR) edge[loop above] node[above] {$0$} (L0MR)
		(L0MR) edge node[above] {$\neg 0$} (RL)
		(RL) edge node[above] {$1$} (M0)
		(RL) edge node[below] {$0, \square$} (M1)
		(M0) edge (FL)
		(M1) edge (FL);
	\end{tikzpicture}
\end{center}

\end{problem}
