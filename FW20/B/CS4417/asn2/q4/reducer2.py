#!/usr/bin/env python
"""reducer2.py"""

import sys
import math
import os

num_docs = 0
with open('inputParameters') as paramFile:
    for line in paramFile:  # should only contain one line anyway
        num_docs = int(line.strip())


# input comes from STDIN (standard input)
current_word = None
current_word_docs = []
term_count = 0

for line in sys.stdin:
    word, doc, tf, n, df = line.strip().split('\t', 4)
    tf = int(tf)
    n = int(n)
    df = int(df)

    idf = math.log(float(num_docs) / float(df), 10)
    tf_idf = (float(tf) / float(n)) * idf

    print ((doc, word), (tf, idf, tf_idf))
