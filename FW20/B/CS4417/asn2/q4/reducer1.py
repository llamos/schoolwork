#!/usr/bin/env python
"""reducer1.py"""

import sys
import os

num_docs = 0
with open('inputParameters') as paramFile:
    for line in paramFile:  # should only contain one line anyway
        num_docs = int(line.strip())


# input comes from STDIN (standard input)
current_term = None
term_count = 0

for line in sys.stdin:
    word, doc, n = line.strip().split('\t', 2)
    n = int(n)

    if current_term == (word, doc, n):
        term_count += 1
    else:
        if current_term:
            print '%s\t%s\t%d\t%d' % (current_term[0], current_term[1], term_count, current_term[2])
        term_count = 1

    current_term = (word, doc, n)

if current_term is not None:
    print '%s\t%s\t%d\t%d' % (current_term[0], current_term[1], term_count, current_term[2])
