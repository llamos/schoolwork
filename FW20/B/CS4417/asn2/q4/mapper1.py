#!/usr/bin/env python
"""mapper1.py"""

import sys
import os

num_docs = 0
with open('inputParameters') as paramFile:
    for line in paramFile:  # should only contain one line anyway
        num_docs = int(line.strip())

root_dir = 'Documents'
files = [f for f in os.listdir(root_dir) if os.path.isfile(os.path.join(root_dir, f))]
if len(files) != num_docs:
    min_len = min(len(files), num_docs)
    sys.stderr.write('Warning: Mismatch in document count between inputParameters (' + str(num_docs) + ') and glob discovery (' + str(len(files)) + ').')
    sys.exit(-1)

for file in files:
    with open(os.path.join(root_dir, file)) as f:
        all_words = []
        for line in f:
            words = line.strip().split()
            for word in words:
                all_words.append(word)

        for word in all_words:
            print '%s\t%s\t%d' % (word, file, len(all_words))
