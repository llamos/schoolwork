======= Author =======
Name: Paul Norton
Student ID: 250957533
E-mail: pnorton4@uwo.ca

======= Usage =======
Place documents to be processed in ./Documents/
Also create ./inputParameters, containing the number of documents placed into ./Documents/
Then, use the following command:
	python2 mapper1.py | sort | python2 reducer1.py | python2 mapper2.py | sort | python2 reducer2.py

Output format is ((document, word), (tf, idf, tf-idf)), where:
* tf is term frequency (number of times word appears in document),
* idf is inverse document frequency (log10(number of documents / number of documents containing word))
* tf-idf is tf * idf

======= Description =======
This map reduce only uses two phases.
I found that I was able to produce all the necessary values
using only these two phases described below.

MR Phase 1:
	Map phase reads contents from ./Documents/,
	performs a similar operation to q1's mapper,
	placing each word on its own line alongside its originating doc.
	This phase also appends the total number of words per doc.
	Reduce phase inserts frequency of each word per doc to each line.
	Input: 
		Unprocessed documents.
	Output: 
		All words from all documents,
		alongside their source document,
		how many words are in that document altogether,
		and the amount of times that word appears in the document.

MR Phase 2:
    Map phase determines how many documents contain each word
    by counting the number of sequential entries with the same word,
    then writing them all with the same count value appended.
    Reduce phase computes tf-idf, and formats for target.
    Input:
        Lines of the format word    document    tf  N,
        where tf is the number of times word appears in document
        and N is the number of words in document.
    Output:
        Lines of the format ((document, word), (tf, idf, tf-idf)).