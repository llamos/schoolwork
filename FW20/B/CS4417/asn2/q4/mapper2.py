#!/usr/bin/env python
"""mapper2.py"""

import sys
import os

num_docs = 0
with open('inputParameters') as paramFile:
    for line in paramFile:  # should only contain one line anyway
        num_docs = int(line.strip())


# input comes from STDIN (standard input)
current_word = None
current_word_docs = []
term_count = 0

for line in sys.stdin:
    word, doc, tf, n = line.strip().split('\t', 3)
    tf = int(tf)
    n = int(n)

    if current_word != word:
        if current_word:
            for c_doc, c_tf, c_n in current_word_docs:
                print '%s\t%s\t%d\t%d\t%d' % (current_word, c_doc, c_tf, c_n, len(current_word_docs))
        current_word_docs = []

    current_word_docs.append((doc, tf, n))
    current_word = word

if current_word is not None:
    for c_doc, c_tf, c_n in current_word_docs:
        print '%s\t%s\t%d\t%d\t%d' % (current_word, c_doc, c_tf, c_n, len(current_word_docs))
