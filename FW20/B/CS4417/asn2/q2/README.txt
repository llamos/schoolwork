======= Author =======
Name: Paul Norton
Student ID: 250957533
E-mail: pnorton4@uwo.ca

======= Usage =======
Designed for use in Hadoop.

Manual execution:
	cat some_input.txt | python2 mapper.py | sort | python2 reducer.py

Output format is ((bigram), frequency),
to match the example set out in FAQ-March12.html.

======= Description =======
MR Phase 1:
	Map phase combines words into bigrams,
	using last_word to store previous iteration value.
	Reduce phase appends frequency and eliminates duplicates.
	Input: 
		Unprocessed documents.
	Output: 
		All adjacent word pairs in the document,
		alongside their frequency in the document.
