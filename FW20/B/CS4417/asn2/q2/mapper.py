#!/usr/bin/env python
"""mapper.py"""

import sys

# input comes from STDIN (standard input)
for line in sys.stdin:
    line = line.strip()
    words = line.split()

    last_word = None

    for word in words:
        if last_word is not None:
            print '%s\t%s' % (last_word, word)
        last_word = word
