#!/usr/bin/env python
"""reducer.py"""

from operator import itemgetter
import sys

current_word_pair = None
current_count = 0
first_word = None
second_word = None

# input comes from STDIN
for line in sys.stdin:
    # remove leading and trailing whitespace
    first_word, second_word = line.strip().split('\t', 1)

    # this IF-switch only works because Hadoop sorts map output
    # by key (here: word) before it is passed to the reducer
    if current_word_pair == (first_word, second_word):
        current_count += 1
    else:
        if current_word_pair:
            # write result to STDOUT
            print '((%s %s), %d)' % (current_word_pair[0], current_word_pair[1], current_count)
        current_count = 1
        current_word_pair = (first_word, second_word)

# do not forget to output the last word if needed!
if current_word_pair == (first_word, second_word):
    print '((%s %s), %d)' % (current_word_pair[0], current_word_pair[1], current_count)
