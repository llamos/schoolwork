#!/usr/bin/env python
"""mapper.py"""

import os

root_dir = 'inputDirectory'
files = [f for f in os.listdir(root_dir) if os.path.isfile(os.path.join(root_dir, f))]

for file in files:
    with open(os.path.join(root_dir, file)) as f:
        for line in f:
            words = line.strip().split()
            for word in words:
                print '%s\t%s' % (word, file)
