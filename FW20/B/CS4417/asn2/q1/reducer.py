#!/usr/bin/env python
"""reducer.py"""

import sys

current_term = None
current_count = 0
word = None

# input comes from STDIN
for line in sys.stdin:
    # remove leading and trailing whitespace
    word, doc = line.strip().split('\t', 1)

    if current_term == (word, doc):
        current_count += 1
    else:
        if current_term:
            # write result to STDOUT
            print '((%s,%s),%d)' % (current_term[0], current_term[1], current_count)
        current_count = 1
        current_term = (word, doc)

# do not forget to output the last word if needed!
if current_term is not None:
    print '((%s, %s), %d)' % (current_term[0], current_term[1], current_count)
