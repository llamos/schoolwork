======= Author =======
Name: Paul Norton
Student ID: 250957533
E-mail: pnorton4@uwo.ca

======= Usage =======
Place files to process in ./inputDirectory, then
	python2 mapper.py | sort | python2 reducer.py

assignment2.html specified ((term, document identifier), count) output,
while FAQ-March12.html specified (word, frequency).
I went with the assignment2.html output specification.

======= Description =======
MR Phase 1:
	Map phase combines each word with its document name.
	Reduce phase appends frequency and eliminates duplicates.
	Input: 
		Unprocessed documents.
	Output: 
		Each word in the document,
		processed on its own line,
		combined with the document name
		as well as the frequency of that document.
