#!/usr/bin/env python
"""reducer.py"""

from operator import itemgetter
import sys

# input comes from STDIN
counter = 0
for line in sys.stdin:
    bigram_count = int(line.strip())
    if bigram_count == 1:
        counter += 1

print counter
