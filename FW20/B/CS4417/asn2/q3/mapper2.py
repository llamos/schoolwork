#!/usr/bin/env python
"""mapper.py"""

import sys

# input comes from STDIN (standard input)
for line in sys.stdin:
    line = line.strip()
    words = line.split()

    bigram_count = int(words[2][0:-1])
    print bigram_count
