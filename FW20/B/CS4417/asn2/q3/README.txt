======= Author =======
Name: Paul Norton
Student ID: 250957533
E-mail: pnorton4@uwo.ca

======= Usage =======
Designed for use in Hadoop.

Manual execution:
	cat some_input.txt | python2 mapper1.py | sort | python2 reducer1.py | python2 mapper2.py | sort | python2 reducer2.py

Output format is a single integer,
as set out in FAQ-March12.html.

======= Description =======
MR Phase 1:
	Copied from q2: counts bigrams in a document.
	Input: 
		Unprocessed documents.
	Output: 
		All adjacent word pairs in the document,
		alongside their frequency in the document.

MR Phase 2:
    Map phase strips the tuple formatting off the line,
    and isolates the bigram frequency.
    Reduce phase counts the amount of 1s.
    Input:
        Lines of format ((%s %s), %d),
        identifying bigrams in (%s %s)
        and their frequency in %d.
    Output:
        A single integer counting the number of 1s in %d.