// Produce a list of users, together with the total number of times they tweeted, sorted in decreasing order.
db.tweets.aggregate([
    { $group: { _id: "$user.screen_name", count: { $sum: 1 } } },
    { $sort: { "count": -1 } }
])

// Produce a list of place names, together with the total number of tweets from that place name, sorted in decreasing order.
db.tweets.aggregate([
    { $group: { _id: "$user.location", count: { $sum: 1 } } },
    { $sort: { "count": -1 } }
])

// Produce a list of users, together with the total number of replies to that user, sorted in decreasing order.
db.tweets.aggregate([
    { $group: { _id: "$in_reply_to_screen_name", count: { $sum: 1 } } },
    { $sort: { "count": -1 } }
])

// Produce a list of users, together with the total number of hashtags used by that user, sorted in decreasing order.
db.tweets.aggregate([
    { $group: { _id: "$user.screen_name", uniqueHashtags: { $addToSet: "$entities.hashtags.text" } } },
    { $project: { "_id": 1, hashtagCount: { $size: "$uniqueHashtags" } } },
    { $sort: { "hashtagCount": -1 } }
])