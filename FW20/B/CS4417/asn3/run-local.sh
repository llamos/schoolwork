#!/bin/bash

# start mongo
docker rm -f mongo
docker run --name mongo -d mongo:4.4
docker ps

# import tweets
curl https://www.csd.uwo.ca/~dlizotte/tweets.json -o tweets.json -s
docker cp tweets.json mongo:/
docker exec -it mongo mongoimport --db tweetdb --collection tweets --file /tweets.json
docker exec -it mongo mongo
