// Produce a new collection that contains each hashtag used in the collection of tweets, along with the number of times that hashtag was used.

function myMapper() {
    this.entities.hashtags
        .map(h => h.text) // extract text from hashtag use
        .forEach(h => emit(h, 1)); // emit (hashtag, 1) for each hashtag used
}

function myReducer(k, values) {
    return { k: Array.sum(values) }; // sum all the 1's from the previous phase, per key
}

db.tweets.mapReduce(myMapper, myReducer, { query: {}, out: "mroutput" })

db.mroutput.aggregate({ $sort: { value: -1 } })