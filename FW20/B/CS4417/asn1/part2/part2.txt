=======================================================================
                            The Lucene Demo                            
=======================================================================

1. Enter query: 
   Searching for: his fiery sword
   5 total matching documents
   1. documents\RJ3.txt
   2. documents\RJ5.txt
   3. documents\RJ4.txt
   4. documents\RJ9.txt
   5. documents\RJ6.txt
   Press (q)uit or enter number to jump to a page.
   Enter query: 
   Searching for: alas o love
   5 total matching documents
   1. documents\RJ6.txt
   2. documents\RJ8.txt
   3. documents\RJ7.txt
   4. documents\RJ1.txt
   5. documents\RJ10.txt
   Press (q)uit or enter number to jump to a page.
   Enter query: 
   Searching for: and
   8 total matching documents
   1. documents\RJ3.txt
   2. documents\RJ10.txt
   3. documents\RJ5.txt
   4. documents\RJ2.txt
   5. documents\RJ4.txt
   6. documents\RJ8.txt
   7. documents\RJ7.txt
   8. documents\RJ1.txt
   Press (q)uit or enter number to jump to a page.
   Enter query: 
   Searching for: reckon
   0 total matching documents
   Enter query: 
   Searching for: q
   0 total matching documents
   Enter query: 
   Searching for: love
   4 total matching documents
   1. documents\RJ6.txt
   2. documents\RJ7.txt
   3. documents\RJ1.txt
   4. documents\RJ10.txt
   Press (q)uit or enter number to jump to a page.
   Enter query: 
   Searching for: fair
   3 total matching documents
   1. documents\RJ9.txt
   2. documents\RJ10.txt
   3. documents\RJ1.txt
   Press (q)uit or enter number to jump to a page.
   Enter query: 
   
   Process finished with exit code 0
   
2. The demo does not use stopwords. If it did, 
   common words like "his" or "and" would be 
   excluded from the search query.
   RJ4 contains only "his" from "his fiery sword",
   but is returned for that query.
   
3. The demo does not use stemming. If it did,
   "reckoning" would be included in results
   for the search query "reckon". However,
   RJ10 contains "reckoning" and is not
   returned during that search.
    
4. The default similarity metric used is HitsThresholdChecker,
   which will accumulate the number of matching words in the text,
   and rank based on that. We can observe this in the results
   with the search query "his fiery sword". Here are the results 
   along with the number of matching words.
   
   1. documents\RJ3.txt 4
   2. documents\RJ5.txt 3
   3. documents\RJ4.txt 2
   4. documents\RJ9.txt 1
   5. documents\RJ6.txt 1



=======================================================================
                           Modifying the Demo                         
=======================================================================

All changes are marked with 
// changes start - Stage
and
// changes end

1. Stopping:
   
   Enter query: 
   Searching for: fiery sword
   1 total matching documents
   1. documents\RJ3.txt
   Press (q)uit or enter number to jump to a page.
   Enter query: 
   Searching for: alas o love
   5 total matching documents
   1. documents\RJ6.txt
   2. documents\RJ8.txt
   3. documents\RJ7.txt
   4. documents\RJ1.txt
   5. documents\RJ10.txt
   Press (q)uit or enter number to jump to a page.
   Enter query: 
   Searching for: and
   8 total matching documents
   1. documents\RJ3.txt
   2. documents\RJ10.txt
   3. documents\RJ5.txt
   4. documents\RJ2.txt
   5. documents\RJ4.txt
   6. documents\RJ8.txt
   7. documents\RJ7.txt
   8. documents\RJ1.txt
   Press (q)uit or enter number to jump to a page.
   Enter query: 
   Searching for: reckon
   0 total matching documents
   Enter query: 
   Searching for: q
   0 total matching documents
   Enter query: 
   Searching for: love
   4 total matching documents
   1. documents\RJ6.txt
   2. documents\RJ7.txt
   3. documents\RJ1.txt
   4. documents\RJ10.txt
   Press (q)uit or enter number to jump to a page.
   Enter query: 
   Searching for: fair
   3 total matching documents
   1. documents\RJ9.txt
   2. documents\RJ10.txt
   3. documents\RJ1.txt
   Press (q)uit or enter number to jump to a page.
   Enter query: 
   
   Process finished with exit code 0

2. Stemming:
   
   Enter query: 
   Searching for: fieri sword
   1 total matching documents
   1. documents\RJ3.txt
   Press (q)uit or enter number to jump to a page.
   Enter query: 
   Searching for: ala o love
   5 total matching documents
   1. documents\RJ6.txt
   2. documents\RJ8.txt
   3. documents\RJ7.txt
   4. documents\RJ1.txt
   5. documents\RJ10.txt
   Press (q)uit or enter number to jump to a page.
   Enter query: 
   Searching for: and
   8 total matching documents
   1. documents\RJ3.txt
   2. documents\RJ10.txt
   3. documents\RJ5.txt
   4. documents\RJ2.txt
   5. documents\RJ4.txt
   6. documents\RJ8.txt
   7. documents\RJ7.txt
   8. documents\RJ1.txt
   Press (q)uit or enter number to jump to a page.
   Enter query: 
   Searching for: reckon
   1 total matching documents
   1. documents\RJ10.txt
   Press (q)uit or enter number to jump to a page.
   Enter query: 
   Searching for: love
   5 total matching documents
   1. documents\RJ6.txt
   2. documents\RJ7.txt
   3. documents\RJ8.txt
   4. documents\RJ1.txt
   5. documents\RJ10.txt
   Press (q)uit or enter number to jump to a page.
   Enter query: 
   Searching for: fair
   3 total matching documents
   1. documents\RJ9.txt
   2. documents\RJ10.txt
   3. documents\RJ1.txt
   Press (q)uit or enter number to jump to a page.
   Enter query: 
   
   Process finished with exit code 0

3. Similarity
   
   Enter query: 
   Searching for: fieri sword
   1 total matching documents
   1. documents\RJ3.txt
   Press (q)uit or enter number to jump to a page.
   Enter query: 
   Searching for: ala o love
   5 total matching documents
   1. documents\RJ6.txt
   2. documents\RJ8.txt
   3. documents\RJ7.txt
   4. documents\RJ1.txt
   5. documents\RJ10.txt
   Press (q)uit or enter number to jump to a page.
   Enter query: 
   Searching for: and
   8 total matching documents
   1. documents\RJ3.txt
   2. documents\RJ5.txt
   3. documents\RJ10.txt
   4. documents\RJ2.txt
   5. documents\RJ4.txt
   6. documents\RJ8.txt
   7. documents\RJ7.txt
   8. documents\RJ1.txt
   Press (q)uit or enter number to jump to a page.
   Enter query: 
   Searching for: reckon
   1 total matching documents
   1. documents\RJ10.txt
   Press (q)uit or enter number to jump to a page.
   Enter query: 
   Searching for: love
   5 total matching documents
   1. documents\RJ6.txt
   2. documents\RJ7.txt
   3. documents\RJ8.txt
   4. documents\RJ1.txt
   5. documents\RJ10.txt
   Press (q)uit or enter number to jump to a page.
   Enter query: 
   Searching for: fair
   3 total matching documents
   1. documents\RJ9.txt
   2. documents\RJ10.txt
   3. documents\RJ1.txt
   Press (q)uit or enter number to jump to a page.
   Enter query: 
   
   Process finished with exit code 0

