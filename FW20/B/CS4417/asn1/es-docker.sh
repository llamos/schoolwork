#!/bin/bash

docker run -it -v "data01:/usr/share/elasticsearch/data" -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.10.2
