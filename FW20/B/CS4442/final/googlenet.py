#%% device selection

from tqdm import tqdm
import torch
device = 'cuda' if torch.cuda.is_available() else 'cpu'

#%% download and import celeba dataset

from torchvision.datasets import CelebA
import torchvision.transforms as T

preprocess = T.Compose([
    T.Resize(256),
    T.CenterCrop(224),
    T.ToTensor(),
    T.Lambda(lambda x: x.to(device)),
])

DATASET_ROOT = 'data/'
data_train = CelebA(
    root = DATASET_ROOT,
    split = 'train',
    download = True,
    target_type='attr',
    transform=preprocess,
    target_transform=T.Lambda(lambda x: x.float().to(device)),
)

data_test = CelebA(
    root = DATASET_ROOT,
    split = 'test',
    download = False,
    target_type='attr',
    transform=preprocess,
    target_transform=T.Lambda(lambda x: x.float().to(device)),
)

#%% create data loaders

from torch.utils.data import DataLoader, Subset
BATCH_SIZE = 64
train_dl = DataLoader(
    dataset=Subset(data_train, range(0, 30_000)),
    batch_size=BATCH_SIZE,
)

test_dl = DataLoader(
    dataset=data_test,
    batch_size=BATCH_SIZE
)

#%% load net from torch hub

import os
NET_DIR = 'googlenet'
os.makedirs(NET_DIR, exist_ok=True)

from torch.hub import load
from torch import nn
VISION_REPO = 'pytorch/vision'
model = load(VISION_REPO, NET_DIR, pretrained=False)
print(model)

#%% modify net output to fit data format

classifiers = [nn.Linear(in_features=1024, out_features=40), nn.Hardsigmoid()]
model.fc = nn.Sequential(*classifiers)
model.to(device)

#%% train model

def trainModel():

    EPOCHS = 45
    MOMENTUM = 0.9
    LR_INIT = 0.001
    LR_DECAY = 0.0005

    # training init
    model.train()
    seed = torch.initial_seed()
    criterion = nn.BCELoss().to(device)
    optimizer = torch.optim.SGD(
        params=model.parameters(),
        lr=LR_INIT,
        momentum=MOMENTUM,
        weight_decay=LR_DECAY
    )

    # progress bars nest
    for epoch in tqdm(range(EPOCHS), unit='epoch'):

        # load from file if exists, skip computing if already done
        cur_epoch_file = f'{NET_DIR}/e{epoch}.pkl'
        if os.path.exists(cur_epoch_file):
            pickle_dict = torch.load(cur_epoch_file)
            seed = pickle_dict['seed']
            torch.manual_seed(seed)
            model.load_state_dict(pickle_dict['model'])
            optimizer.load_state_dict(pickle_dict['optim'])
            continue

        # perform epoch
        loss = 0.0
        for img, label in tqdm(train_dl, unit='batch', leave=False):
            optimizer.zero_grad()

            output = model(img)
            loss = criterion(output.logits, label)
            loss.backward()
            optimizer.step()

        # save state in case we're interrupted
        if epoch % 15 == 0:
            print(f'Epoch {epoch}/{EPOCHS}, loss = {loss.item():.3f}')
            state = {
                'optim': optimizer.state_dict(),
                'model': model.state_dict(),
                'seed': seed
            }
            torch.save(state, cur_epoch_file)

# load from file if already trained, otherwise train and save
MODEL_FILE = f'{NET_DIR}/celeba_attr.pkl'
if os.path.exists(MODEL_FILE):
    model.load_state_dict(torch.load(MODEL_FILE))
else:
    trainModel()
    torch.save(model.state_dict(), MODEL_FILE)

#%% test model

correct = 0
total = 0

model.eval()
with torch.no_grad():
    for img, label in tqdm(test_dl, unit='batch'):
        output = model(img)
        total += label.size(0) * 40
        correct += (torch.round(output) == label).sum().item()

print(f'Got {correct}/{total} correct')
print(f'Accuracy of {NET_DIR} on attributes is {100 * correct / total:.3f}%')
