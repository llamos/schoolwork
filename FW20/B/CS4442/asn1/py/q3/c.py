import matplotlib.pyplot as plt
from common import loadData, computeError, augmentData, chartWithRegression
from a import calcL2NormWeights
import random
from math import ceil


def crossValidate5(train_x, train_y, lamdas):
    # merge then randomize the input set
    data_merged = [(train_x[i], train_y[i]) for i in range(0, len(train_x))]
    random.shuffle(data_merged)

    # split the training data (we're lucky here since 40 / 5 = 8, a whole number, so partitioning is simple)
    training_partitions = [data_merged[i:i + 8] for i in range(0, len(data_merged), 8)]

    # errors will be a list of lists,
    # each list containing errors-per-lamda
    # and each list pertaining to one validation set
    errors = [[] for _ in training_partitions]
    for validation_ix, partitioned_validation in enumerate(training_partitions):
        # create training set from non-validation partitions
        partitioned_train = []
        for tp_i in range(0, len(training_partitions)):
            if tp_i != validation_ix:
                for item in training_partitions[tp_i]:
                    partitioned_train.append(item)

        # calculate error-per-lambda with current training-validation split
        for lamda in lamdas:
            # split back into x-y
            partitioned_train_x = [t[0] for t in partitioned_train]
            partitioned_train_y = [t[1] for t in partitioned_train]
            partitioned_validation_x = [t[0] for t in partitioned_validation]
            partitioned_validation_y = [t[1] for t in partitioned_validation]

            # calculate w with train set and error with validation set
            local_w = calcL2NormWeights(partitioned_train_x, partitioned_train_y, degree=4, lamda=lamda)
            local_err = computeError(local_w, augmentData(partitioned_validation_x, degree=4), partitioned_validation_y)
            errors[validation_ix].append(local_err)

    # now compute the average error per lambda
    lamda_errs_avg = []
    lamda_errs_all = [[] for _ in lamda_arr]
    for lamda_ix in range(0, len(lamda_arr)):  # foreach lambda
        err_acc = 0
        for err_list in errors:  # foreach validation set
            err_acc += err_list[lamda_ix]
            lamda_errs_all[lamda_ix].append(err_list[lamda_ix])

        err_acc /= len(errors)
        lamda_errs_avg.append(err_acc)

    # list of average errors and all errors
    return lamda_errs_avg, lamda_errs_all


if __name__ == '__main__':
    train_inputs = loadData('hw1xtr.dat')
    train_outputs = loadData('hw1ytr.dat')
    test_inputs = loadData('hw1xte.dat')
    test_outputs = loadData('hw1yte.dat')
    lamda_arr = [float(10 ** x) for x in range(-2, 5)]

    # call cross validation to get error per-lambda on training set
    avg_errs, all_errs = crossValidate5(train_inputs, train_outputs, lamdas=lamda_arr)

    print('Lambda Errors:')
    err_ranges = [[], []]
    for ix, lamda in enumerate(lamda_arr):
        min_err = abs(avg_errs[ix] - min(all_errs[ix]))
        max_err = abs(avg_errs[ix] - max(all_errs[ix]))
        err_ranges[0].append(min_err)
        err_ranges[1].append(max_err)
        print(f'\tl = {lamda:>5g}, err = {avg_errs[ix]:.3f} (-{min_err:.3f}+{max_err:.3f})')

    # draw cross validation errors
    fig, ax = plt.subplots(nrows=1, ncols=2)
    ax[0].errorbar(x=lamda_arr, y=avg_errs, yerr=err_ranges, color='c', alpha=0.25, capsize=5)
    ax[0].scatter(lamda_arr, avg_errs, alpha=1)
    ax[0].set_title('Cross-Validation Errors')
    ax[0].set_xscale('log')
    ax[0].set_xlabel(r'$\lambda$')
    ax[0].set_ylabel(r'Average Error')

    # draw test data with normalized regression line
    best_err = min(avg_errs)
    best_lamda = lamda_arr[avg_errs.index(best_err)]
    best_w = calcL2NormWeights(train_inputs, train_outputs, degree=4, lamda=best_lamda)
    chartWithRegression(test_inputs, test_outputs, best_w, ax[1])
    ax[1].set_title('Test Data w/ CV Line')

    plt.savefig('c.png')
    plt.show()
