import matplotlib.pyplot as plt
from common import loadData
from a import calcL2NormWeights

if __name__ == '__main__':
    train_inputs = loadData('hw1xtr.dat')
    train_outputs = loadData('hw1ytr.dat')
    test_inputs = loadData('hw1xte.dat')
    test_outputs = loadData('hw1yte.dat')

    lamda_arr = [(10 ** x) for x in range(-2, 5)]
    lamda_w = [calcL2NormWeights(train_inputs, train_outputs, degree=4, lamda=l) for l in lamda_arr]
    print(lamda_w)

    fig, ax = plt.subplots(nrows=1, ncols=1)
    colors = ['r', 'g', 'b', 'y', 'c']
    for i in range(0, 5):
        wi = list(w[i] for w in lamda_w)
        ax.scatter(lamda_arr, wi, c=colors[i], label=f'$w_{i}$', s=100, alpha=0.6)

    ax.set_title(r'$\lambda$-w Mapping')
    ax.set_xscale('log')
    ax.set_xlabel(r'$\lambda$')
    ax.set_ylabel(r'$w$')
    ax.legend(loc='lower right')

    plt.savefig('b.png')
    plt.show()
