import matplotlib.pyplot as plt
import numpy as np
from common import loadData, augmentData, computeError


# mostly the same as q2/b.py, but with lambda I
# (misspelled bc `lambda` is a python keyword)
def calcL2NormWeights(train_x, train_y, degree, lamda):
    m_train_x_aug = np.matrix(augmentData(train_x, degree))
    m_train_x_aug_t = np.transpose(m_train_x_aug)

    # numpy treats single-dim arr as row vec, so transpose to col
    m_train_y = np.transpose(np.matrix(train_y))

    # generate I from identity without bias term entry
    I = np.matrix(np.identity(degree + 1))
    I[0, 0] = 0
    lambdaI = lamda * I

    return (m_train_x_aug_t * m_train_x_aug + lambdaI).I * m_train_x_aug_t * m_train_y


if __name__ == '__main__':
    train_inputs = loadData('hw1xtr.dat')
    train_outputs = loadData('hw1ytr.dat')
    test_inputs = loadData('hw1xte.dat')
    test_outputs = loadData('hw1yte.dat')

    lamda_arr = [(10 ** x) for x in range(-2, 5)]
    lamda_w = [calcL2NormWeights(train_inputs, train_outputs, degree=4, lamda=l) for l in lamda_arr]
    lamda_err_tr = [computeError(w, augmentData(train_inputs, degree=4), train_outputs) for w in lamda_w]
    lamda_err_te = [computeError(w, augmentData(test_inputs, degree=4), test_outputs) for w in lamda_w]
    lamda_err_av = [(lamda_err_tr[i] + lamda_err_te[i]) / 2 for i in range(0, len(lamda_arr))]

    fig, ax = plt.subplots(nrows=1, ncols=1)
    ax.scatter(lamda_arr, lamda_err_tr, c='r', label='Training Error')
    ax.scatter(lamda_arr, lamda_err_te, c='b', label='Testing Error')
    ax.scatter(lamda_arr, lamda_err_av, c='g', label='Average Error')

    ax.set_title(r'$\ell_2$-norm Errors')
    ax.set_xscale('log')
    ax.set_xlabel(r'$\lambda$')
    ax.set_ylabel(r'Error')
    ax.legend(loc='upper left')

    plt.savefig('a.png')
    plt.show()
