import matplotlib.pyplot as plt
import numpy as np
from common import loadData, augmentData, chartWithRegression


def calcWeights(train_x, train_y, degree):
    m_train_x_aug = np.matrix(augmentData(train_x, degree))
    m_train_x_aug_t = np.transpose(m_train_x_aug)

    # numpy treats single-dim arr as row vec, so transpose to col
    m_train_y = np.transpose(np.matrix(train_y))
    return (m_train_x_aug_t * m_train_x_aug).I * m_train_x_aug_t * m_train_y


if __name__ == '__main__':
    train_inputs = loadData('hw1xtr.dat')
    train_outputs = loadData('hw1ytr.dat')

    fig, ax = plt.subplots(nrows=1, ncols=1)
    ax.set_title('Training Linear')

    w = calcWeights(train_inputs, train_outputs, degree=1)
    chartWithRegression(train_inputs, train_outputs, w, ax)

    plt.savefig('b.png')
    plt.show()
