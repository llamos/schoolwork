import matplotlib.pyplot as plt
from common import loadData, chartWithRegression
from b import calcWeights

if __name__ == '__main__':
    train_inputs = loadData('hw1xtr.dat')
    train_outputs = loadData('hw1ytr.dat')

    test_inputs = loadData('hw1xte.dat')
    test_outputs = loadData('hw1yte.dat')

    fig, ax = plt.subplots(nrows=1, ncols=1)
    ax.set_title('Testing Linear')

    w = calcWeights(train_inputs, train_outputs, degree=1)
    chartWithRegression(test_inputs, test_outputs, w, ax)

    plt.savefig('c.png')
    plt.show()
