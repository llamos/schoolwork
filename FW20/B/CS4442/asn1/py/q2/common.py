import numpy as np


# loads an input file to an array of floats
def loadData(file_name):
    arr = []
    with open(file_name, 'r') as f:
        for line in f:
            arr.append(float(line))
    return arr


# augments the data into (x^0, x^1, ..., x^degree)
def augmentData(arr, degree):
    return [tuple([x ** i for i in range(0, degree + 1)]) for x in arr]


# computes the error as the average of error by each point
def computeError(w, x, y):
    m = len(w)
    m_inv = 1 / m
    err = 0

    for i in range(0, m):
        # again, numpy treating single-dim as row vec, so out-of-order mult "fixes" it
        err += m_inv * ((x[i] * w - y[i]) ** 2)

    return float(err)


# charts the provided x and y scatter, with regression line w, onto ax
def chartWithRegression(x, y, w, ax):
    w_x = np.linspace(min(x), max(x), 100)
    w_y = sum((float(w[i]) * (w_x ** i)) for i in range(0, len(w)))
    err = computeError(w, augmentData(x, degree=len(w) - 1), y)

    ax.scatter(x, y)
    ax.plot(w_x, w_y, '-r', label=f'err = {err:.4f}')
    ax.legend(loc='upper left')
