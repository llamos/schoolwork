import matplotlib.pyplot as plt
from common import loadData

if __name__ == '__main__':
    train_inputs = loadData('hw1xtr.dat')
    train_outputs = loadData('hw1ytr.dat')
    test_inputs = loadData('hw1xte.dat')
    test_outputs = loadData('hw1yte.dat')

    fig, ax = plt.subplots(nrows=1, ncols=2, sharey='all', sharex='all')
    ax[0].set_title('Training Data')
    ax[0].scatter(train_inputs, train_outputs)
    ax[1].set_title('Testing Data')
    ax[1].scatter(test_inputs, test_outputs)

    fig.tight_layout()
    plt.savefig('a.png')
    plt.show()
