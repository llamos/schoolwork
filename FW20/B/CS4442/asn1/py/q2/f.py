import matplotlib.pyplot as plt
from a import loadData
from b import calcWeights, chartWithRegression

if __name__ == '__main__':
    train_inputs = loadData('hw1xtr.dat')
    train_outputs = loadData('hw1ytr.dat')

    test_inputs = loadData('hw1xte.dat')
    test_outputs = loadData('hw1yte.dat')

    fig, ax = plt.subplots(nrows=1, ncols=2)
    ax[0].set_title('Training Quartic')
    ax[1].set_title('Testing Quartic')

    w = calcWeights(train_inputs, train_outputs, degree=4)
    chartWithRegression(train_inputs, train_outputs, w, ax[0])
    chartWithRegression(test_inputs, test_outputs, w, ax[1])

    plt.savefig('f.png')
    plt.show()
