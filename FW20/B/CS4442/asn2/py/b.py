import numpy as np
from common import loadImages, saveImage

# load 100th image
images = loadImages()
i100 = images[99]

# subtract mean
m = np.mean(images, axis=0)
mean_subtracted = i100 - m

# display/save
saveImage(mean_subtracted, 'b.png')
