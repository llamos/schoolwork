from common import loadImages, saveImage
from sklearn.decomposition import PCA
import numpy as np

images = loadImages()


def reconstruct(component_count):
    pca = PCA(n_components=component_count)
    pca.fit(images)
    target_image = images[99]

    acc = np.zeros(shape=(4096, ))
    for i in range(0, component_count):
        component = pca.components_[i]
        acc += np.inner(np.outer(component, component), target_image)

    saveImage(acc, f'g{component_count}.png')
    print(f'Saved g{component_count}.png')


reconstruct(10)
reconstruct(100)
reconstruct(200)
reconstruct(399)
