from common import loadImages, saveImage
from sklearn.decomposition import PCA
import numpy as np

images = loadImages()

pca = PCA(n_components=5)
pca.fit(images)

# PCA already sorts them
saveImage(pca.components_[0], 'f1.png')
saveImage(pca.components_[1], 'f2.png')
saveImage(pca.components_[2], 'f3.png')
saveImage(pca.components_[3], 'f4.png')
saveImage(pca.components_[4], 'f5.png')
