import numpy as np
from matplotlib import pyplot as plt


def loadImages():
    images = []
    with open('faces.dat', 'r') as f:
        for line in f:
            pixels = [f for f in line.split(' ') if f != '']
            pixels = np.array(pixels, dtype=np.float64)
            images.append(pixels)

    return images


def saveImage(px_array, file_name):
    px_shaped_array = np.transpose(np.reshape(px_array, newshape=(64, 64)))
    plt.gray()
    plt.imshow(px_shaped_array)
    plt.imsave(file_name, px_shaped_array)
