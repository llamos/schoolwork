from common import loadImages
from sklearn.decomposition import PCA
import numpy as np
from matplotlib import pyplot as plt

images = loadImages()
pca = PCA()
pca.fit(images)

# get eigenvalues (PCA already sorts)
eigenvalues = pca.explained_variance_
eigenvalues[399] = 0.0  # floating point error is giving me something around 1e-26

# plot the eigenvalues (log scale for clarity)
fig, ax = plt.subplots(nrows=1, ncols=2)
ax[0].scatter(np.arange(1, 401), eigenvalues)
ax[0].set_yscale('linear')
ax[0].set_title('Linear Scale')
ax[1].scatter(np.arange(1, 401), eigenvalues)
ax[1].set_yscale('log')
ax[1].set_ylim(1e1, 1e7)
ax[1].set_title('Logarithmic Scale')

fig.tight_layout()
fig.savefig('c.png')
fig.show()
