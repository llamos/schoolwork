create table maskuse
(
    fids bigint,
    never real,
    rarely real,
    sometimes real,
    frequently real,
    always real
)