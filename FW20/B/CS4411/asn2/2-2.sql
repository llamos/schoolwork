SELECT s1.sname, s2.sname
FROM Sailors s1
    JOIN Reserves r1 ON s1.sid = r1.sid
    JOIN Sailors s2 ON s1.sid != s2.sid
    JOIN Reserves r2 ON s2.sid = r2.sid
WHERE r1.day = r2.day;