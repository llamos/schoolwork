%! TEX root = report.tex

\section{Findings}

\subsection{Performance}

Each rating will be shown ordered best-to-worst,
with Neo4j in bold for identification.
All queries can be found in the \texttt{dataset/} directory.

\subsubsection{CSV Insertion}

The first performance test was insertion
of a large dataset stored in CSV format.
For this, the file \texttt{action\_logs.csv} was imported
using each DBMS's preferred CSV loader.
\texttt{action\_logs.csv} contains 11,823,037 rows of data.
Both SQL servers used \texttt{LOAD DATA},
MongoDB used \texttt{mongoimport}
and Neo4j used \texttt{LOAD CSV} with \texttt{PERIODIC COMMIT}.

\begin{center}
    \begin{tabular}{|l|r|}
    \hline
         DBMS & Avg Insertion Time \\
         \hline
         PostgreSQL & 1 min 5.84 sec \\
         \hline
         MySQL & 4 min 53.76 sec \\
         \hline
         MongoDB & 5 min 24.73 sec \\
         \hline
         \textbf{Neo4j} & \textbf{9 min 14.58 sec} \\
         \hline
    \end{tabular} \\
    \vspace*{1mm}
    \textit{1. CSV Insertion Times}
    \end{center}

Neo4j ranks far behind its competitors in CSV bulk insertion.
When not using \texttt{PERIODIC COMMIT},
it will run out of memory and the query will fail.
This performance is unsuitable for a production environment.
It is worth noting that Neo4j provides another utility for importing CSVs,
\texttt{neo4j-admin import}.
However, this can only be used to initialize an empty database,
and cannot be used after that,
which makes it unsuitable in many use cases.

\subsubsection{Bulk Deletion}

Using the CSV data imported in the last test,
this test times how long it takes to delete all the imported data.
For SQL, this is \texttt{DELETE},
MongoDB uses \texttt{deleteMany()},
and Neo4j uses \texttt{apoc.iterate} on a \texttt{MATCH} then \texttt{DELETE},
with a batch size of 1000.

\begin{center}
    \begin{tabular}{|l|r|}
    \hline
         DBMS & Avg Deletion Time \\
         \hline
         PostgreSQL & 0.15 sec \\
         \hline
         MySQL & 0.18 sec \\
         \hline
         MongoDB & 2 min 29.72 sec \\
         \hline
         \textbf{Neo4j} & \textbf{5 min 41.41 sec} \\
         \hline
    \end{tabular} \\
    \vspace*{1mm}
    \textit{2. Bulk Deletion Times}
    \end{center}
    
From this data it is clear to see
that the SQL-based DBMS's
are simply marking the data as freed,
where the NoSQL DBMS's are processing the data individually to remove it.
This also equates to a large performance penalty,
and one that I find unreasonable,
since deleted data is no longer going to be used at all.
As such, it makes more sense to not process it whatsoever.

\subsubsection{Bulk Insertion Native Language}

For this test,
each DBMS was given a batch insert call
using the data from \texttt{action\_logs.csv}
translated into their native language.
Both SQL servers used \texttt{INSERT} with multiple \texttt{VALUES} clauses,
MongoDB used \texttt{insertMany()},
and Cypher used multiple sequential \texttt{CREATE} calls.
The dataset was truncated to the first 50,000 rows for this test.

\begin{center}
    \begin{tabular}{|l|r|}
    \hline
         DBMS & Avg Insertion Time \\
         \hline
         PostgreSQL & 0.69 sec \\
         \hline
         MySQL & 0.73 sec \\
         \hline
         MongoDB & 1.62 sec \\
         \hline
         \textbf{Neo4j} & \textbf{10 min 3 sec} \\
         \hline
    \end{tabular} \\
    \vspace*{1mm}
    \textit{3. Batch Insertion Times}
    \end{center}
    
As expected after the first test,
Neo4j is again the slowest at inserting data.
In this case, Neo4j is so slow that I had to reduce the dataset size.
MongoDB does not show such a slowdown,
despite also being a NoSQL server.
It is worth noting that using a single \texttt{CREATE} call
with multiple nodes,
although explicitly supported by the language\cite{neo4j-create-multiple},
crashed silently during the query multiple times.

\subsubsection{Relational Requests}

In this test,
each DBMS is given the task of returning the full action log set from the first test,
joined with the applicable user that made the request.
Of the over 11 million action log entries,
approximately 800 thousand of them are associated with a user.
Queries are performed without relations,
then with relations to measure the difference.
MySQL and PostgreSQL are called 
with an explicit JOIN on the unindexed key \texttt{action\_logs.requested\_user},
then called after a foreign key is created.
MongoDB does not have direct support for relations,
and attempting to use aggregation operators on this large of a dataset
took too long to execute, 
so it has been excluded from the results.
Finally, Neo4j is called with a two-fold \texttt{MATCH} first,
then a \texttt{MATCH} with an explicit relation defined between each node.

\begin{center}
    \begin{tabular}{|l|r|r|r|}
    \hline
         DBMS & No-Relation Query Time & Relation Building Time & With-Relation Query Time \\
         \hline
         PostgreSQL & 4.10 sec & 3.06 sec & 3.42 sec \\
         \hline
         MySQL & 12.55 sec & 3 min 37.00 sec & 12.25 sec \\
         \hline
         \textbf{Neo4j} & \textbf{19.13 sec} & \textbf{22.65 sec} & \textbf{16.40 sec} \\
         \hline
    \end{tabular} \\
    \vspace*{1mm}
    \textit{4. Join Query Times}
    \end{center}

Neo4j performs much better here than in previous tests.
This shows that Neo4j's focus on graph structure
must be playing a key role in query optimization.

\subsubsection{Nested Relational Requests}

In this last test,
each DBMS is given the task of returning the full action log set from the first test,
joined with the applicable user that made the request,
as well as that user's player and team, if any.
MySQL and PostgreSQL are called 
with an explicit JOIN on defined foreign keys.
MongoDB has again been excluded from the results.
Neo4j is called with a four-way \texttt{MATCH}.

\begin{center}
    \begin{tabular}{|l|r|}
    \hline
         DBMS & Query Time \\
         \hline
         MySQL & 17.14 sec \\
         \hline
         PostgreSQL & 19.56 sec \\
         \hline
         \textbf{Neo4j} & \textbf{29.28 sec} \\
         \hline
    \end{tabular} \\
    \vspace*{1mm}
    \textit{5. Nested Join Query Times}
    \end{center}

Overall, Neo4j has shown that it can be very slow during insertions,
but performs reasonably well during selections.
It parallelizes well,
but does not keep up with the big two SQL engines, expectedly.

\subsection{Scalability}

\subsubsection{Sharding}

Compared to the other DBMS's linked in this article,
Neo4j shines at setup for horizontal scaling.
While MySQL and PostgreSQL were never innately designed for such scaling,
they are both capable of doing so through extension products
(MySQL NDB Cluster and the third-party Citus for PostgreSQL).
MongoDB and Neo4j, as newer products,
both have the advantage of being created with sharding in mind,
and so both support it.

MySQL NDB Cluster \cite{mysql-ndb-cluster} is not available as part of MySQL Community Edition,
so I was not able to test this product.

Citus for Postgres \cite{citus-readme} is available in a simple docker container,
which made initial shard creation easy.
However, despite each shard sharing data and processing power,
all queries must still be routed through a single ``master'' node.
Each table must also be explicitly declared as a shared table.
As well, as a Postgres extension,
the Citus calls must be done through provided SQL methods,
which is certainly an undesired interfacing method.
\begin{lstlisting}[breaklines=true]
SELECT citus_add_node('node_ip_addr', node_port);
SELECT rebalance_table_shards();
\end{lstlisting}

To avoid a master node setup,
MongoDB employs client-side routing
through its \texttt{mongos} router system \cite{mongo-sharding-mongos}.
If desired, these \texttt{mongos} routers can also be run independently.
Documents stored within a sharded database
must include a shard id as well as an item id,
which determines which shard it is stored on.
However, queries can still be sent to multiple shards
and the results aggregated between them.
Mongo supports simultaneous sharded/unsharded collections,
and only enforces sharding on a per-collection basis.
In the case of MongoDB,
the choice of shard id is given high importance,
and must be defined manually for each collection\cite{mongo-sharding-strategy}.

Like Citus, Neo4j uses a central proxy server 
to handle connections to a sharded database\cite{neo4j-fabric-structure}.
Neo4j calls this proxy server Fabric.
Fabric is capable of sharding the data on any relation,
and recommends that database administrators
attempt to find natural breaks in their data\cite{neo4j-fabric-where-to-divide}
to determine where to divide the shards.

\subsubsection{Scaled Performance}

Unfortunately, I was unable to produce meaningful results
to test performance of sharded data,
based on my limited set of machines.

\subsection{Ease of Development}

\subsubsection{Neo4j Browser, Bloom and Desktop}
Neo4j comes bundled with an excellent data visualization tool,
the Neo4j Browser\cite{neo4j-browser}.
This browser application can both render data in the graph,
as well as provide code insights when writing queries.
Neo4j Browser is navigated through a standard web browser
and is an excellent tool for debugging queries,
or designing your graph structure.

\begin{center}
    \includegraphics[scale=0.4]{neo4j-browser.png} \\
    \textit{6. The Neo4j Browser displaying a query result.}
\end{center}

For more novice users,
the Neo4j suite includes Neo4j Bloom\cite{neo4j-bloom}.
This tool is entirely graph-based
and avoids use of Cypher.
This enables non-programmer users
to interact with, edit and browse the graph
without requiring them to learn the Cypher language.

\begin{center}
    \includegraphics[scale=0.4]{neo4j-bloom.png} \\
    \textit{7. Neo4j Bloom}
\end{center}

\pagebreak
Finally, for more involved users,
there is also the Neo4j Desktop application\cite{neo4j-desktop}.
This application includes support for 
managing multiple databases, installing extensions, and writing queries.

\begin{center}
    \includegraphics[scale=0.15]{neo4j-desktop.png} \\
    \textit{8. Neo4j Desktop}
\end{center}

Together these three applications provide a much more 
beginner-friendly introduction to Neo4j
when compared with other DBMS's.
Programmers already very familiar with an existing database platform 
may find them unnecessary,
although all are optional anyway.

\subsubsection{For Existing Database Administrators}

The largest difference between Neo4j 
and other existing DBMS platforms
is the use of its novel query language, Cypher\cite{neo4j-cypher-intro}.
Cypher is graph-based, rather than entity- or relation-based,
which is fairly unique among DBMS software.
Many operations that are straightforward in SQL are difficult in Cypher,
yet also some very complex operations are straightforward in Cypher.
The Cypher language excels at simply displaying
queries which contain lots of relationships,
compared to nested \texttt{JOIN} statements from SQL.

As seen in Performance,
Neo4j does not seem to handle batch insertions well.
However, it was great at querying data.
This sort of optimization may lend itself well
to long-running server applications,
like web servers,
which only need to poll a small portion
(like a single user's worth) of data at a time.

\subsubsection{For Developers}

The Cypher language,
while certainly distinct from other query languages,
was not overly difficult to use.
The attribute syntax can get cluttered
when inserting lots of attributes in a single query,
but the node and relationship syntax is pleasant.
Cypher ensures that even deeply nested relationships
across multiple nodes, node types, or even self-relations, are easily expressible 
thanks to its named node and relationship syntax.
The compact \texttt{(node)-[relationship]->(node)} syntax
also helps to reduce long boilerplate \texttt{JOIN} equivalent clauses in SQL.

Neo4j provides interfacing libraries for the following languages:
Java, .NET, JavaScript, Python, Go, Ruby, PHP, Erlang, Perl\cite{neo4j-drivers}.
Each driver connects to the Neo4j Database 
over the Bolt protocol.
For Java, they also provide the Neo4j-OGM library\cite{neo4j-java-ogm},
an Object Graph Mapping library
serializing Java classes directly into the graph.

Without the Neo4j-OGM library,
each of these drivers can only interface with a database instance
using the Cypher language.
Compared to the level of existing tooling for other DBMS's,
this will feel primitive.
With the Neo4j-OGM library,
this will still fall behind in features
relative to Hibernate or Spring JPA.

\subsubsection{Personal Findings}

During my time with Neo4j,
I found the technical documentation to be vast,
yet also not detailed enough.
Each command and clause of the Cypher language was documented
but there was a distinct lack of examples,
and many query structures that feel intuitive are not supported.
For example, as mentioned in the first Performance test,
simply using \texttt{LOAD CSV} on the action logs file would fail,
since the dataset was too large.
Rather than providing an error, 
or transferring data onto the destination drive into a temporary file,
the engine continued to eat up RAM on my machine 
until it slowed to a crawl.
Even at this point, the query did not error,
but seemed to wait until more memory was available,
for which none was coming.
Considering the data file is only 1 GB,
it does not seem appropriate to consume over 16 GB of RAM,
even for an in-memory insertion.
The solution for this was to include the \texttt{USING PERIODIC COMMIT} clause,
which also feels unnecessary,
if the engine was capable of using temporary files,
or streaming the data in-place.

The Neo4j Database also requires full and exclusive ownership
of all files within its directories.
This seems like a reasonable demand,
for both security and data integrity,
however it caused problems when the import folder was mounted from a shared location.
On startup, the database engine would change 
the file ownership to UID 7474, and permissions to 700,
effectively locking all other processes out of the folder.
If it was unable to claim ownership and change permissions of this folder on startup
(such as if the folder was mounted read-only),
the entire database would fail to start.

With the core database engine,
it is also impossible to delete large batches of data.
Using the expected canonical query
\texttt{MATCH (t) WHERE ... DELETE t;}
will quickly consume all available RAM on the system and fail
(yet this query \textit{does} throw an error!)
if too many nodes match the criteria.
Also problematically,
it is not possible to use \texttt{MATCH (t) WHERE ... LIMIT n DELETE t;},
since the \texttt{LIMIT} clause is not compatible with \texttt{DELETE}.
Instead, we must use the plugin extensions from APOC.
This feels decidely un-Cypher-like,
and contributes to a feeling of infancy among the software.
The APOC query looks like this:
\begin{lstlisting}[breaklines=true]
CALL apoc.periodic.iterate(
    "MATCH (a: ActionLog) RETURN a", 
    "DETACH DELETE a", 
    {batchSize: 1000}
) YIELD batches, total 
RETURN batches, total;
\end{lstlisting}

All these issues seem to stem from large datasets only,
and in my small-scale queries,
I did not run into any such problems.
However, for a piece of software labelled as a \emph{database},
I would expect that it is capable of receiving data
without choking up on 10,000 data points.
After all this time learning it,
I think Neo4j has some incredible ideas behind it,
and I enjoyed using the graph-based queries
for simplistic multi-join operations.
However, I don't feel that the project is suitable
for use in any of my upcoming programming projects.
