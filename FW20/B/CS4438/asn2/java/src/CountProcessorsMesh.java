import java.util.Vector;

/* Algorithm for counting the number of processors in a mesh network. */

public class CountProcessorsMesh extends Algorithm {

    /* Do not modify this method */
    public Object run() {
        int status = countProcessors(getID());
        return status;
    }

    public void sendForwardPacket(String rightNeighbour, String bottomNeighbour, int w, int h) throws SimulatorException {
        if (!rightNeighbour.equals("0"))
            send(makeMessage(rightNeighbour, pack(false, w, h)));
        else
            send(makeMessage(bottomNeighbour, pack(false, w, h)));
    }

    public void sendReturnPackets(String leftNeighbour, String topNeighbour, String rightNeighbour, int w, int h) throws SimulatorException {
        if (!leftNeighbour.equals("0"))
            send(makeMessage(leftNeighbour, pack(true, w, h)));
        // skip top neighbour if we have a right neighbour
        // right neighbour existence implies that its top neighbour will already speak to our top neighbour
        if (!topNeighbour.equals("0") && rightNeighbour.equals("0"))
            send(makeMessage(topNeighbour, pack(true, w, h)));
    }

    public int countProcessors(String id) {
        Vector<String> v = neighbours(); // Set of neighbours of this node.
        String topNeighbour = (String) v.elementAt(0); 
		String rightNeighbour = (String) v.elementAt(1);
		String bottomNeighbour = (String) v.elementAt(2);
		String leftNeighbour = (String) v.elementAt(3);

        // Your initialization code goes here
        boolean isStartNode = leftNeighbour.equals("0") && topNeighbour.equals("0");
        boolean isEndNode = rightNeighbour.equals("0") && bottomNeighbour.equals("0");
        boolean firstLoop = true;
        Message m = null;
        int w = 0, h = 0;
        
        try {
            while (waitForNextRound()) { // Main loop. All processors wait here for the beginning of the next round.
                // if we have no neighbours at all then we're the only processor
                if (isStartNode && isEndNode)
                    return 1;
                
                if (firstLoop && isStartNode) {
                    // top left node starts the algorithm by sending a 1-1 packet to its next neighbour
                    sendForwardPacket(rightNeighbour, bottomNeighbour, 1, 1);
                } else if (m != null) {
                    // parse incoming packet
                    String[] parts = unpack(m.data());
                    boolean isReturnPacket = Boolean.parseBoolean(parts[0]);
                    w = stringToInteger(parts[1]);
                    h = stringToInteger(parts[2]);
                    
                    // if we got the packet from our left, we increment mesh width, otherwise mesh height
                    if (!isReturnPacket)
                        if (m.source().equals(leftNeighbour)) w++; else h++;
                    
                    if (isReturnPacket || isEndNode) {
                        // send off return packets to both upper and left neighbour
                        sendReturnPackets(leftNeighbour, topNeighbour, rightNeighbour, w, h);
                        
                        // return total count after sending
                        return w * h;
                    } else {
                        // send off updated w, h to either right or bottom neighbour
                        sendForwardPacket(rightNeighbour, bottomNeighbour, w, h);
                    }
                }
                
                // mark init loop ended, receive packet for next loop
                firstLoop = false;
                m = receive();
            }
        } catch(Exception e){
            System.out.println("ERROR: " + e.toString());
        }
    
        // If we got here, something went wrong! (Exception, node failed, etc.)
        return 0;
    }

    public static void main(String[] args) {
        Simulator.main("-d -m 100 -x -r count_procs_mesh_debug.log networkMesh.txt".split(" "));
    }
}
