import java.util.Vector;

/* Algorithm for counting the number of processors in a line network. */

public class CountProcessorsLine extends Algorithm {

    /* Do not modify this method */
    public Object run() {
        int status = countProcessors(getID());
        return status;
    }
    
    public void sendForwardPacket(String rightNeighbour, int procCount) throws SimulatorException {
        send(makeMessage(rightNeighbour, pack(false, procCount)));
    }
    
    public void sendReturnPacket(String leftNeighbour, int procCount) throws SimulatorException {
        send(makeMessage(leftNeighbour, pack(true, procCount)));
    }

    public int countProcessors(String id) {
        Vector<String> v = neighbours(); // Set of neighbours of this node.
        String leftNeighbour = (String) v.elementAt(0);
        String rightNeighbour = (String) v.elementAt(1);

        // Your initialization code goes here
        boolean isLeftNode = leftNeighbour.equals("0");
        boolean isRightNode = rightNeighbour.equals("0");
        boolean firstLoop = true;

        Message m = null;

        try {
            while (waitForNextRound()) { // Main loop. All processors wait here for the beginning of the next round.

                // if we have no neighbours at all, we're the only processor
                if (isLeftNode && isRightNode)
                    return 1;

                if (firstLoop && isLeftNode) {
                    // left node starts off algorithm
                    sendForwardPacket(rightNeighbour, 1);
                } else {
                    // m will be non-null if it is our turn to act
                    if (m != null) {
                        // parse incoming packet data
                        String[] parts = unpack(m.data());
                        boolean isReturnPacket = Boolean.parseBoolean(parts[0]);
                        int processorCount = stringToInteger(parts[1]);
                        if (!isReturnPacket)
                            // increment processor count if we haven't been counted yet
                            processorCount++;
                        
                        // return packet is sent if we got a return packet, 
                        // or if we're the right-most node
                        // (which means all other nodes have already added themselves to the counter)
                        if (isRightNode || isReturnPacket) {
                            // only continue the return packet chain if we're not the last one
                            if (!isLeftNode)
                                sendReturnPacket(leftNeighbour, processorCount);
                            
                            // return count after sending the packet to the next
                            return processorCount;
                        } else {
                            // we've added ourselves to the count, so we forward on the new total to the next node
                            sendForwardPacket(rightNeighbour, processorCount);
                        }
                    }
                }

                // first loop is over, receive message for next loop
                firstLoop = false;
                m = receive();
            }
        } catch (SimulatorException e) {
            System.out.println("ERROR: " + e.toString());
        }

        // If we got here, something went wrong! (Exception, node failed, etc.)    
        return 0;
    }

    public static void main(String[] args) {
        Simulator.main("-n -m 100 -x -r count_procs_line_debug.log networkLine.txt".split(" "));
    }

}
