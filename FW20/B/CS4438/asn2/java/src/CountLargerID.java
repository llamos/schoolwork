import java.util.Vector; 

/* Algorithm for counting the number of processors with even ID in a synchronous ring network. */

public class CountLargerID extends Algorithm {

    /* Do not modify this method */
    public Object run() {
		int status = countLargerIDs(getID());
        return status;
    }
 
    public int countLargerIDs(String id) {
        Vector<String> v = neighbours(); // Set of neighbours of this node.
        String rightNeighbour = (String) v.elementAt(1); // Neighbour on the right
		String leftNeighbour = (String) v.elementAt(0);  // Neighbour on the left

        // Your initialization code goes here
        boolean firstLoop = true;
        Message m = null;
        int larger = 0;
        
        try {
            while (waitForNextRound()) { // Main loop. All processors wait here for the beginning of the next round.
                if (firstLoop) {
                    // on first loop we send our own id into the network (going right)
                    send(makeMessage(rightNeighbour, getID()));
                } else {
                    // on other loops, we receive an id from our left neighbour
                    String receivedID = m.data();
                    if (equal(receivedID, getID()))
                        // if it's our own id, we've traversed the full network and are done counting
                        return larger;
                    if (larger(receivedID, getID()))
                        // if it's not our id, but it is larger, we increment our counter
                        larger++;
                    
                    // forward on the id we just got to the next node (won't re-send our own due to early return)
                    send(makeMessage(rightNeighbour, receivedID));
                }
                
                // receive message for next loop, flag not to send our own
                m = receive();
                firstLoop = false;
            }
        } catch(SimulatorException e){
            System.out.println("ERROR: " + e.toString());
        }   
        // If we got here, something went wrong! (Exception, node failed, etc.)
		return 0;
    }

    public static void main(String[] args) {
        Simulator.main("-d -m 100 -x -r count_larger_debug.log networkRing.txt".split(" "));
    }
    
}
