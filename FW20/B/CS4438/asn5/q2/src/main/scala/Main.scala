import scala.annotation.tailrec
import scala.io.StdIn

object Main extends App {
  
  implicit class IntExt(t1: Int) {
    def \(t2: Int): Fraction = Fraction(t1, t2)
  }
  
  implicit class BigIntExt(t1: BigInt) {
    def \(t2: BigInt): Fraction = Fraction(t1, t2)
  }
  
  case class Fraction(num: BigInt, denom: BigInt) {
    def t1: Fraction = this
    
    def norm: Fraction = {
      @tailrec def gcd(a: BigInt, b: BigInt): BigInt = if (b == 0) a else gcd(b, a % b)
      val reducFactor = gcd(num, denom)
      (num / reducFactor) \ (denom / reducFactor)
    }

    def ++(t2: Fraction): Fraction = {
      val newDenom = if (t1.denom == t2.denom) t1.denom else t1.denom * t2.denom
      val t1Num = t1.num * (newDenom / t1.denom)
      val t2Num = t2.num * (newDenom / t2.denom)
      ((t1Num + t2Num) \ newDenom).norm
    }

    def **(t2: Fraction): Fraction = {
      ((t1.num * t2.num) \ (t1.denom * t2.denom)).norm
    }

    override def toString: String = s"$num/$denom"
  }
  
  val start = Seq(1 \ 6, 1 \ 6, 1 \ 6, 1 \ 6, 1 \ 6, 1 \ 6)
  
  def iter(l: Seq[Fraction]): Seq[Fraction] =
    Seq(
      (1 \ 3) ** l(1), // 1
      (1 \ 2) ** l(3), // 2
      ((1 \ 1) ** l(0)) ++ ((1 \ 3) ** l(1)), // 3
      (1 \ 2) ** l(5), // 4
      ((1 \ 3) ** l(1)) ++ ((1 \ 1) ** l(2)) ++ ((1 \ 2) ** l(3)) ++ ((1 \ 2) ** l(5)), // 5
      (1 \ 1) ** l(4), // 6
    )
    
  def printTex(l: Seq[Fraction], i: Int): Unit = {
    println(
      s"""
         |\\subsection*{Iteration $i}
         |\\begin{center}
         |    \\begin{tabular}{|c|c|c|c|c|c|c|}
         |        \\hline
         |        $$N$$ & 1 & 2 & 3 & 4 & 5 & 6 \\\\
         |        \\hline
         |        $$r(N)$$ & $$\\frac{${l(0).num}}{${l(0).denom}}$$ & $$\\frac{${l(1).num}}{${l(1).denom}}$$ & $$\\frac{${l(2).num}}{${l(2).denom}}$$ & $$\\frac{${l(3).num}}{${l(3).denom}}$$ & $$\\frac{${l(4).num}}{${l(4).denom}}$$ & $$\\frac{${l(5).num}}{${l(5).denom}}$$ \\\\
         |        \\hline
         |    \\end{tabular}
         |\\end{center}
         |""".stripMargin)
  }
    
  var last = start
  var current = iter(start)
  var c = 0
  while (last != current) {
    println(s"Iteration ${c += 1; c}")
//    printTex(current, c)
    last = current
    current = iter(current)
    
//    StdIn.readLine("Press Enter to continue...")
  }
  
  println(s"Iteration ${c += 1; c}")
  println(current)
  
}
