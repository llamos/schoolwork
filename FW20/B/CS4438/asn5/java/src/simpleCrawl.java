import java.io.*;
import java.net.*;

public class simpleCrawl {

   private static String baseURL = "https://www.csd.uwo.ca/faculty/solis/cs9668/test/";
   
   public static void main (String[] args) {

      URL theUrl, u;
      BufferedReader input;
      String s;
      String word;
      boolean inFile;
      int counter = 0;

      try {                               
         theUrl = new URL(baseURL+"test.html");  // This is the complete URL of the test page
         word = InOut.readWord();

         input = new BufferedReader(new InputStreamReader(theUrl.openStream())); // Open URL for reading
         inFile = false;

         while ((s = input.readLine()) != null) {  // Read the document specified by theUrl
            	u = extractURL(s);
            	if (u != null) ++counter;
            	if ((s.toLowerCase()).indexOf(word.toLowerCase()) != -1) {
            		inFile = true;
            	}
         }

	 if (inFile) InOut.printFileName(theUrl); // You MUST use this method to print an URL
         input.close();
         InOut.endListFiles(); // You MUST invoke this method before your program terminates
         
	 System.out.println("Number of hyperlinks found in the page: "+counter);
      } catch (MalformedURLException mue) {
         System.out.println("Malformed URL");

      } catch (IOException ioe) {
         System.out.println("IOException "+ioe.getMessage());
      } 
   } 
   
   /* If there is an URL embedded in the text passed as parameter, the URL will be extracted and
      returned; if there is no URL in the text, the value null is returned                       */
   public static URL extractURL(String text) throws MalformedURLException {
   	String textUrl;
      	int index = text.lastIndexOf("a href=");
   	if (index > -1) {
   		textUrl = baseURL+text.substring(index+8,text.length()-2);  // Form the complete URL	
   		return new URL(textUrl);
   	}
   	else return null;
   }
} 


