import java.io.*;
import java.net.*;
import java.util.*;
import java.util.stream.Collectors;

public class crawl {

    /* Do not change this base URL. All URLs for ths assignmetn are relative to this address */
    private static String baseURL = "https://www.csd.uwo.ca/faculty/solis/cs9668/test/";

    public static void main(String[] args) throws Exception {
        String queryWord = InOut.readWord();

        // list of urls to visit and urls visited
        Queue<URL> urlsToCheck = new ArrayDeque<>();
        Set<URL> urlsChecked = new HashSet<>();
        urlsToCheck.add(new URL(baseURL + "test.html"));
        
        while (!urlsToCheck.isEmpty()) {
            // pull data from next url
            URL currentURL = urlsToCheck.remove();
            List<String> lines = getLinesFromURL(currentURL);
            
            // extract all urls found in page
            List<URL> linkedPages = lines.stream()
                    .map(crawl::extractURL)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            
            // add them to the queue to check, if they haven't been visited (or already queued)
            linkedPages.stream()
                    .filter(u -> !urlsChecked.contains(u))
                    .filter(u -> !urlsToCheck.contains(u))
                    .forEach(urlsToCheck::add);
            
            // if the current page contains the search word, yield this page
            if (lines.stream().anyMatch(l -> l.contains(queryWord)))
                InOut.printFileName(currentURL);
            
            // mark the current page as completed
            urlsChecked.add(currentURL);
        }
        
        InOut.endListFiles();
    }

    public static List<String> getLinesFromURL(URL url) throws IOException {
        BufferedReader input = new BufferedReader(new InputStreamReader(url.openStream()));
        Scanner s = new Scanner(input);

        ArrayList<String> lines = new ArrayList<>();
        while (s.hasNextLine()) {
            lines.add(s.nextLine());
        }

        return lines;
    }

    /* If there is an URL embedded in the text passed as parameter, the URL will be extracted and
       returned; if there is no URL in the text, the value null is returned                       */
    public static URL extractURL(String text) {
        String textUrl;
        int index = text.lastIndexOf("a href=");
        if (index > -1) {
            textUrl = baseURL + text.substring(index + 8, text.length() - 2);   // Form the complete URL	
            try {
                return new URL(textUrl);
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }
} 


