import java.util.Scanner;
import java.net.*;

public class InOut {

	private static int counter = 0;
	public static String readWord() {
		String word = "";
		while (word == null || word.length() == 0) {
			Scanner keyboard = new Scanner(System.in);
			System.out.print(" Enter word: ");	
			word = keyboard.nextLine();
		}
		return word;
	}
	
	public static void printFileName(URL name) {
		if (counter == 0) {
			System.out.println("List of documents containing the query word");
			System.out.println("===========================================");
		}
		
		System.out.println(name.toString());
		++counter;
	}
	
	public static void endListFiles() {
		System.out.println("\nNumber of files that contain the query word: "+counter);
	}
}
