import java.util.Vector;

public class Routing extends Algorithm {

    public Object run() {
        // Our sumTree algorithm returns the total of distances from the descendants to this node 
        String result = makeRoutingTable(getID());
        return result;
    }

    public String makeRoutingTable(String id) {
        try {
            /* Your initialization code goes here */
            RoutingTable table = new RoutingTable(id);
	        Vector<String> children = getChildren();
	        Vector<String> waitingOn = new Vector<>(children);
	        String parent = getParent();
	        
            while (waitForNextRound()) {  /* synchronous loop */
                if (waitingOn.isEmpty()) {
                    if (isRoot()) {
                        // got all the routing info, we're done
                        table.printTables();
                        break;
                    }
                    
                    // send our collated routing table to our parent
                    send(makeMessage(parent, table.stringRepresentation(id, table)));
                    break;
                }
                
                Message m;
                while ((m = receive()) != null) {
                    waitingOn.remove(m.source());
                    String packedTable = m.data();

                    table.addEntry(table.getProcessor(packedTable), table.getProcessor(packedTable));
                    for (int i = 0; i < table.numEntries(packedTable); i++) {
                        table.addEntry(table.getProcessor(packedTable), table.getDestination(packedTable, i));
                    }
                }
            }
        } catch(Exception e){
            System.out.println("ERROR: " + e.toString());
        }
    
        return "";
    }

    public static void main(String[] args) {
        Simulator.main("-d -m 100 -x -r count_larger_debug.log routingNetwork1.txt".split(" "));
    }

}
