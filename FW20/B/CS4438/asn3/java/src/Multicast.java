import java.util.Vector;

public class Multicast extends Algorithm {

    public static final String HEADER_REQUEST_PACKET = "Q";
    public static final String HEADER_RESPONSE_PACKET = "S";

    public static final String ACK = "ACK";
    public static final String NAK = "NAK";

    public void sendRequestPackets(Vector<String> dests) throws SimulatorException {
        for (String dest : dests) {
            send(makeMessage(dest, HEADER_REQUEST_PACKET));
        }
    }

    public void sendResponsePacket(String dest, boolean ack) throws SimulatorException {
        send(makeMessage(dest, pack(HEADER_RESPONSE_PACKET, ack ? ACK : NAK)));
    }

    public Object run() {
        Reader input = new Reader();
        String result = multicasting(getID(), input.getk());
        return result;
    }

    /* Build a multicasting tree connecting the root processor to all processors with even ID's */
    /* ------------------------------------- */
    public String multicasting(String id, int k) {
        /* ------------------------------------- */
        try {
            Vector<String> adjacent = neighbours(); // Set of neighbors of this node
            Vector<String> waitingOn = new Vector<>(adjacent); // neighbours we haven't received packets from yet

            String parent = null;
            Vector<String> children = new Vector<>(adjacent.size());
            Vector<String> potentialParents = new Vector<>(adjacent.size());

            boolean shouldSendReqs = isRoot();
            boolean sentReqs = false;

            int myId = stringToInteger(id);
            if (isRoot())
                showMessage("k = " + k, LOCATION_BELOW);

            /* Write the initialiation code here */

            while (waitForNextRound()) {  // Synchronous loop
                showMessage("W = " + waitingOn.size(), LOCATION_ABOVE);

                // process phase
                if (waitingOn.isEmpty()) { // received packets from all
                    // for any non-root node, it must have at least one potential parent
                    // ACK that parent if we have a reason to be in the tree
                    // (self has id greater than k, or some child needs us)
                    boolean inTree = (!isRoot()) && (myId > k || !children.isEmpty());
                    if (inTree) {
                        parent = potentialParents.stream()
                                .filter(p -> Integer.parseInt(p) > k)
                                .findFirst()
                                .orElse(potentialParents.firstElement());
                        sendResponsePacket(parent, true);
                        potentialParents.remove(parent);
                    } else if (isRoot()) {
                        parent = "";
                    }

                    // NAK all others
                    for (String p : potentialParents) {
                        sendResponsePacket(p, false);
                    }
                    break;
                }

                if (shouldSendReqs) {
                    sendRequestPackets(waitingOn);
                    sentReqs = true;
                    shouldSendReqs = false;
                }

                // receive phase
                Message m;
                while ((m = receive()) != null) {
                    // mark that we've received a packet from sender
                    waitingOn.remove(m.source());
                    String[] parts = unpack(m.data());
                    switch (parts[0]) {
                        case HEADER_REQUEST_PACKET:  // request from upstream
                            if (!sentReqs) {
                                potentialParents.add(m.source());
                                shouldSendReqs = true;
                            }
                            break;
                        case HEADER_RESPONSE_PACKET:  // response from downstream
                            if (parts[1].equals(ACK)) {
                                children.add(m.source());
                            }
                            break;
                        default:
                            throw new SimulatorException("Bad packet header");
                    }
                }
            }

            printParentChildren(parent, children);
        } catch (SimulatorException e) {
            System.out.println("ERROR: " + e.getMessage());
        }
        return "";
    }

    /* Print information about parent and children of this node */
    /* ----------------------------------------------------------- */
    private void printParentChildren(String parent, Vector<String> children) {
        /* ----------------------------------------------------------- */
        String outMssg = "p: " + parent + ", c: ";
        for (int i = 0; i < children.size(); ++i)
            outMssg = outMssg + children.elementAt(i) + " ";
        showMessage(outMssg);
        printMessage(outMssg);
        try {
            Check.verify(getID(), isRoot(), parent, children);
        } catch (SimulatorException e) {
            System.out.println("Error, invalid netowrk node");
        }
    }

    public static void main(String[] args) {
        Simulator.main("-n -m 100 -x -r count_larger_debug.log multinet1.txt".split(" "));
    }
}
