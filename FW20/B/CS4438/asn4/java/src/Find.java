import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Queue;
import java.util.Vector;

public class Find extends Algorithm {
    private int m;                    // Ring of identifiers has size 2^m
    private int SizeRing;              // SizeRing = 2^m

    public Object run() {
        return find(getID());
    }

    // Each message sent by this algorithm has the form: flag, value, ID 
    // where:
    // - if flag = "GET" then the message is a request to get the document with the given key
    // - if flag = "LOOKUP" then the message is request to forward the message to the closest
    //   processor to the position of the key
    // - if flag = "FOUND" then the message contains the key and processor that stores it
    // - if flag = "NOT_FOUND" then the requested data is not in the system
    // - if flag = "END" the algorithm terminates

    /* Complete method find, which must implement the Chord search algorithm using finger tables 
       and assumming that there are two processors in the system that received the same ring identifier. */
    /* ----------------------------- */
    public Object find(String localId) {
        /* ------------------------------ */
        try {
       
             /* The following code will determine the keys to be stored in this processor, the keys that this processor
                needs to find (if any), and the addresses of the finger table                                           */
            Vector<Integer> searchKeys;        // Keys that this processor needs to find in the P2P system. Only
            // for one processor this vector will not be empty
            Vector<Integer> localKeys;        // Keys stored in this processor

            localKeys = new Vector<Integer>();
            String[] fingerTable;                  // Addresses of the fingers are stored here
            searchKeys = keysToFind();             // Read keys and fingers from configuration file
            fingerTable = getKeysAndFingers(searchKeys, localKeys, localId);  // Determine local keys, keys that need to be found, and fingers
            m = fingerTable.length - 1;
            SizeRing = exp(2, m);
            showMessage(localId + "-" + (stringToInteger(localId) % SizeRing) + ": " + localKeys, LOCATION_ABOVE);

            /* Your initialization code goes here */

            Vector<Integer> waitingOnKeys = new Vector<>(searchKeys); // will loop until all these are found
            String[] foundKeys = new String[searchKeys.size()]; // will store results here
            Queue<Message> sendQueue = new ArrayDeque<>(); // messages pending send
            
            if (searchKeys.size() > 0) {        // If this condition is true, the processor has keys that need to be found
                for (int i = 0; i < searchKeys.size(); i++) {
                    int lookupKey = searchKeys.get(i);
                    
                    if (resolveLocal(localKeys, lookupKey)) {
                        // if data is local, don't go to network
                        foundKeys[i] = lookupKey + ":" + localId;
                        waitingOnKeys.removeElement(lookupKey);
                        continue;
                    }
                    
                    FingerTarget target = findTarget(fingerTable, lookupKey);
                    if (target == null) {
                        // shouldn't happen for first node
                        throw new SimulatorException("Could not find target for identifier " + lookupKey + " in finger table");
                    } else if (target.node.equals(localId)) {
                        // if finger table says the data should have been local but it wasn't here
                        foundKeys[i] = lookupKey + ":not found";
                        waitingOnKeys.removeElement(lookupKey);
                    } else {
                        // queue packet to start lookup chain
                        sendQueue.add(makeMessage(target.node, pack(target.method, lookupKey, localId)));
                    }
                }
                
                showMessage("Waiting on: " + waitingOnKeys, LOCATION_BELOW);
            }

            boolean shouldExit = false;
            String retVal = "";
            while (waitForNextRound()) { // Synchronous loop
                if (shouldExit) {
                    // clear displays and forward end packet
                    showMessage("", LOCATION_ABOVE);
                    showMessage("", LOCATION_BELOW);
                    send(makeMessage(successor(), "END"));
                    return retVal;
                }
                
                // send all queued messages
                while (!sendQueue.isEmpty()) {
                    send(sendQueue.remove());
                }
                
                // handle incoming messages
                Message msg;
                while ((msg = receive()) != null) {
                    // check for end packet
                    if (msg.data().equals("END")) {
                        shouldExit = true;
                        break;
                    }
                    
                    // extract portions of packet
                    String[] msgParts = unpack(msg.data());
                    String command = msgParts[0];
                    int lookupKey = stringToInteger(msgParts[1]);
                    int keyIx = searchKeys.indexOf(lookupKey);
                    
                    switch (command) {
                        case "FOUND":
                            // sender has lookupKey
                            waitingOnKeys.removeElement(lookupKey);
                            showMessage("Waiting on: " + waitingOnKeys, LOCATION_BELOW);
                            foundKeys[keyIx] = lookupKey + ":" + msg.source();
                            break;
                            
                        case "NOT_FOUND":
                            // sender was responsible for lookupKey, but didn't have it
                            waitingOnKeys.removeElement(lookupKey);
                            showMessage("Waiting on: " + waitingOnKeys, LOCATION_BELOW);
                            foundKeys[keyIx] = lookupKey + ":" + "not found";
                            break;
                            
                        case "GET":
                            // must find here, no forwarding
                            String requester = msgParts[2];
                            String result = resolveLocal(localKeys, lookupKey) ? "FOUND" : "NOT_FOUND";
                            sendQueue.add(makeMessage(requester, pack(result, lookupKey)));
                            break;
                            
                        case "LOOKUP":
                            // possible proxying, but check our finger table first
                            requester = msgParts[2];
                            if (resolveLocal(localKeys, lookupKey)) {
                                // found local, break early
                                sendQueue.add(makeMessage(requester, pack("FOUND", lookupKey)));
                            } else {
                                // consult finger table
                                int minOffset = stringToInteger(msgParts[3]);
                                FingerTarget target = findTarget(fingerTable, lookupKey);
                                if (target == null || target.node.equals(localId)) {
                                    // finger table failed, or said it must be here (but wasn't found earlier)
                                    sendQueue.add(makeMessage(requester, pack("NOT_FOUND", lookupKey)));
                                } else {
                                    // forward request to next closest node in finger table
                                    sendQueue.add(makeMessage(target.node, pack(target.method, lookupKey, requester)));
                                }
                            }
                            break;
                        
                        default:
                            throw new SimulatorException("Unrecognized command " + command);
                    }
                }
                
                // check if we found all the keys and we're the processor gathering keys
                if (!searchKeys.isEmpty() && waitingOnKeys.isEmpty()) {
                    shouldExit = true;
                    
                    // create the return string
                    for (int i = 0; i < searchKeys.size(); i++) {
                        retVal += foundKeys[i];
                        if (i < searchKeys.size() - 1)
                            retVal += " ";
                    }
                }
            }


        } catch (SimulatorException e) {
            System.out.println("ERROR: " + e.toString());
        }

        /* At this point something likely went wrong. If you do not have a result you can return null */
        return null;
    }
    
    // represents a finger table lookup result
    private static class FingerTarget {
        String method;
        String node;

        public FingerTarget(String method, String node) {
            this.method = method;
            this.node = node;
        }
    }
    
    private boolean inRange(int min, int max, int lookup) {
        // special range handling that accounts for modulus
        max %= SizeRing;
        min %= SizeRing;
        lookup %= SizeRing;
        if (max < min) {
            if (lookup < max)
                lookup += SizeRing;
            max += SizeRing;
        }
        return min <= lookup && lookup < max;
    }
    
    private FingerTarget findTarget(String[] fingerTable, int target) throws SimulatorException {
        // check for must-be-at-neighbour
        int self = stringToInteger(fingerTable[fingerTable.length - 1]);
        int first = stringToInteger(fingerTable[0]);
        if (inRange(self, first, target))
            return new FingerTarget("GET", fingerTable[0]);
        
        // find next-closest via finger table
        for (int i = fingerTable.length - 1; i --> 0;) {
            String fingerAddress = fingerTable[i];
            int fingerValue = stringToInteger(fingerAddress);
            String nextFingerAddress = fingerTable[i + 1];
            int nextFingerValue = stringToInteger(nextFingerAddress);
            
            if (inRange(fingerValue, nextFingerValue, target))
                return new FingerTarget("LOOKUP", fingerAddress);
        }
        
        // couldn't find a target (bad table?)
        return null;
    }
    
    // check whether we own the key
    private boolean resolveLocal(Vector<Integer> localKeys, int key) {
        return localKeys.contains(key);
    }


    /* Determine the keys that need to be stored locally and the keys that the processor needs to find.
       Negative keys returned by the simulator's method keysToFind() are to be stored locally in this 
           processor as positive numbers.                                                                    */
    /* ---------------------------------------------------------------------------------------------------- */
    private String[] getKeysAndFingers(Vector<Integer> searchKeys, Vector<Integer> localKeys, String id) throws SimulatorException {
        /* ---------------------------------------------------------------------------------------------------- */
        Vector<Integer> fingers = new Vector<Integer>();
        String[] fingerTable;
        String local = "";
        int m;

        if (searchKeys.size() > 0) {
            for (int i = 0; i < searchKeys.size(); ) {
                if (searchKeys.elementAt(i) < 0) {    // Negative keys are the keys that must be stored locally
                    localKeys.add(-searchKeys.elementAt(i));
                    searchKeys.remove(i);
                } else if (searchKeys.elementAt(i) > 1000) {
                    fingers.add(searchKeys.elementAt(i) - 1000);
                    searchKeys.remove(i);
                } else ++i;  // Key that needs to be searched for
            }
        }

        m = fingers.size();
        // Store the finger table in an array of Strings
        fingerTable = new String[m + 1];
        for (int i = 0; i < m; ++i) fingerTable[i] = integerToString(fingers.elementAt(i));
        fingerTable[m] = id;

        for (int i = 0; i < localKeys.size(); ++i) local = local + localKeys.elementAt(i) + " ";
        showMessage(local); // Show in the simulator the keys stored in this processor
        return fingerTable;
    }

    /* Hash function to map processor ids to ring identifiers. */
    /* ------------------------------- */
    private int hp(String ID) throws SimulatorException {
        /* ------------------------------- */
        return stringToInteger(ID) % SizeRing;
    }

    /* Hash function to map keys to ring identifiers */
    /* ------------------------------- */
    private int hk(int key) {
        /* ------------------------------- */
        return key % SizeRing;
    }

    /* Compute base^exponent ("base" to the power "exponent") */
    /* --------------------------------------- */
    private int exp(int base, int exponent) {
        /* --------------------------------------- */
        int i = 0;
        int result = 1;

        while (i < exponent) {
            result = result * base;
            ++i;
        }
        return result;
    }

    public static void main(String[] args) {
        String config = args.length > 0 ? args[0] : "netPeer1";
        Simulator.main(("-d -m 100 -x -r find.log " + config + ".txt").split(" "));
    }

}
