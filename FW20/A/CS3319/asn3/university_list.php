<!DOCTYPE html>
<!-- lists universities, sometimes filtered -->

<html lang="en">
<head>
    <title>Course Equivalences - University List</title>

    <?php
    include 'html_templates/bootstrap_styles.php';
    ?>
</head>
<body>

<?php
include 'html_templates/navbar.php';
include_once 'db/php/query_all.php';
?>

<div class="container">
    <h3>Universities</h3>

    <!-- option to filter by province -->
    <form action="university_list.php" method="get" class="form-inline">
        <label for="select-prov" class="mb-2 mx-3">Filter By: </label>
        <div class="form-group mb-2 mx-3">
            <select class="form-control" name="province" id="select-prov">
                <option value="all">All</option>
                <?php
                // populate the province select from out provinces_list() util fn
                foreach (provinces_list() as $prov) {
                    if (isset($_GET['province']) && strcmp($_GET['province'], $prov) == 0) {
                        echo '<option value="' . $prov . '" selected="selected">' . $prov . '</option>';
                    } else {
                        echo '<option value="' . $prov . '">' . $prov . '</option>';
                    }
                }
                ?>
            </select>
        </div>
        <button type="submit" class="btn btn-primary mb-2">Filter</button>
    </form>

    <!-- option to filter by lack of courses -->
    <form action="university_list.php" method="get" class="form-inline">
        <label class="mb-2 mx-3">Or, view universities which have no recorded courses: </label>
        <button class="btn btn-success" name="no_equivs" value="true">Go</button>
    </form>
    <hr/>

    <!-- lists universities from db query -->
    <table class="table table-sm table-hover table-striped">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">City</th>
            <th scope="col">Province</th>
            <th scope="col">Nickname</th>
            <th scope="col">View Info</th>
        </tr>
        </thead>
        <tbody>
        <?php
        // request universities filtered by query string options
        $conn = connection_open();
        $result = do_query($conn, query_university_list($_GET['province'], $_GET['no_equivs']));

        // print each row into the db
        while ($row = mysqli_fetch_assoc($result)) {
            echo '<tr>';
            echo '<td>' . $row['uniid'] . '</td>';
            echo '<td>' . $row['uniname'] . '</td>';
            echo '<td>' . $row['city'] . '</td>';
            echo '<td>' . $row['prov'] . '</td>';
            echo '<td>' . $row['nickname'] . '</td>';
            echo '<td><form action="university_view.php" method="get"><input type="hidden" name="uniid" value="' . $row['uniid'] . '"><button type="submit" class="btn btn-primary">View</button></form></td>';
            echo '</tr>';
        }

        // close out db conn
        mysqli_free_result($result);
        connection_close($conn);
        ?>
        </tbody>
    </table>
</div>

<?php
include 'html_templates/bootstrap_scripts.php';
?>
</body>
</html>