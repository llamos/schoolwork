<!DOCTYPE html>
<!-- allows editing of a single western course -->

<html lang="en">
<head>
    <title>Course Equivalences - Edit Course</title>

    <?php
    include 'html_templates/bootstrap_styles.php';
    ?>
</head>
<body>

<?php
include 'html_templates/navbar.php';
include_once 'db/php/query_all.php';

// load the course specified by query string
$conn = connection_open();
$result = do_query($conn, query_wcourse_single($_GET['wcnum']));
$row = mysqli_fetch_assoc($result);

// store result early
$wcnum = $row['westernnum'];
$wcname = $row['westernname'];
$wcweight = $row['weight'];
$wcsuffix = $row['suffix'];

mysqli_free_result($result);
connection_close($conn);
?>

<div class="container">
    <h3>Editing Western Course <?php echo $wcnum ?></h3>
    <hr />
    <!-- form data for non-id fields -->
    <form action="wcourse_edit_submit.php" method="post" class="form">
        <input type="hidden" name="wcnum" value="<?php echo $wcnum ?>" >
        <div class="row">
            <div class="col">
                <label for="input-name">Course Name</label>
                <input id="input-name" class="form-control" name="wcname" value="<?php echo $wcname ?>" type="text" maxlength="50" >
            </div>
        </div>
        <div class="row">
            <div class="col">
                <label for="input-weight">Course Weight</label>
                <input id="input-weight" class="form-control" name="wcweight" value="<?php echo $wcweight ?>" type="number" step="0.1" min="0" max="1.0" >
            </div>
        </div>
        <div class="row">
            <div class="col">
                <label for="input-suffix">Course Suffix</label>
                <input id="input-suffix" class="form-control" name="wcsuffix" value="<?php echo $wcsuffix ?>" type="text" maxlength="3" >
            </div>
        </div>
        <button type="submit" class="btn btn-success float-right mt-2">Submit</button>
    </form>
</div>

<?php
include 'html_templates/bootstrap_scripts.php';
?>
</body>
</html>