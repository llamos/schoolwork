<!DOCTYPE html>
<!-- home page -->

<html lang="en">
<head>
    <title>Course Equivalences - Home</title>

    <?php
    include 'html_templates/bootstrap_styles.php';
    ?>
</head>
<body>

<?php
include 'html_templates/navbar.php';
?>

<div class="container">
    <h5>Welcome the Course Equivalence site.</h5>

    <p>Please use the headers above to navigate the site.</p>

    <p>Alternatively, <a href="reset_db.php">click here</a> to reset the site database to its initial state.</p>

    <div class="small text-muted">Created by Paul Norton pnorton4@uwo.ca 250957533</div>
</div>

<?php
include 'html_templates/bootstrap_scripts.php';
?>
</body>
</html>