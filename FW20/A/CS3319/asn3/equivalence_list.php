<!DOCTYPE html>
<!-- lists all (or filtered) equivalences -->

<html lang="en">
<head>
    <title>Course Equivalences - Equivalences</title>

    <?php
    include 'html_templates/bootstrap_styles.php';
    ?>
</head>
<body>

<?php
include 'html_templates/navbar.php';
include_once 'db/php/query_all.php';
?>

<div class="container">
    <h3>Course Equivalences</h3>
    <!-- options to filter by western course -->
    <form action="equivalence_list.php" method="get" class="form-inline">
        <label for="input-wcnum" class="mb-2 mx-3">Equivalences for Western Course:</label>
        <div class="input-group mb-2 mx-3">
            <div class="input-group-prepend">
                <span class="input-group-text">cs</span>
            </div>
            <input class="form-control" id="input-wcnum" name="wcnum" value="<?php echo urldecode($_GET['wcnum']) ?>"
                   type="text" pattern="\d{4}">
        </div>
        <button type="submit" class="btn btn-primary mb-2">Filter</button>
    </form>
    <!-- options to filter by date -->
    <form action="equivalence_list.php" method="get" class="form-inline mt-3">
        <label for="input-wcnum" class="mb-2 mx-3">Equivalences Before Date:</label>
        <input class="form-control mb-2" id="input-wcnum" name="date" value="<?php echo urldecode($_GET['date']) ?>"
               type="date">
        <button type="submit" class="btn btn-primary mb-2 mx-3">Filter</button>
    </form>
    <!-- options to create new equiv -->
    <span class="mx-3 mt-3">Or, add a new equivalence: </span>
    <a href="equivalence_new.php" class="btn btn-success">New Equivalence</a>
    <hr/>
    <!-- table listing equivalences from db, sometimes filtered -->
    <table class="table table-sm table-hover table-striped border">
        <thead>
        <tr>
            <th class="border text-center" scope="colgroup" colspan="3">Western</th>
            <th class="border text-center" scope="colgroup" colspan="4">Outside</th>
            <th class="border"></th>
        </tr>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col" class="border-right">Weight</th>
            <th scope="col">University</th>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col" class="border-right">Weight</th>
            <th scope="col">Eval Date</th>
        </tr>
        </thead>
        <tbody>
        <?php
        // get filtered equivalences based on query string
        $conn = connection_open();
        $result = do_query($conn, query_equivalence_list($_GET['wcnum'], $_GET['date']));

        // print each equivalence into the table
        while ($row = mysqli_fetch_assoc($result)) {
            echo '<tr>';
            echo '<td>' . $row['westernnum'] . '</td>';
            echo '<td>' . $row['westernname'] . '</td>';
            echo '<td class="border-right">' . $row['wweight'] . '</td>';
            echo '<td>' . $row['uniname'] . '</td>';
            echo '<td>' . $row['outsidenum'] . '</td>';
            echo '<td>' . $row['outsidename'] . '</td>';
            echo '<td class="border-right">' . $row['oweight'] . '</td>';
            echo '<td>' . $row['evaluateddate'] . '</td>';
            echo '</tr>';
        }

        // close out db conn
        mysqli_free_result($result);
        connection_close($conn);
        ?>
        </tbody>
    </table>
</div>

<?php
include 'html_templates/bootstrap_scripts.php';
?>
</body>
</html>