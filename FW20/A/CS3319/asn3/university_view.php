<!DOCTYPE html>
<!-- views information about a single university -->

<html lang="en">
<head>
    <title>Course Equivalences - View University</title>

    <?php
    include 'html_templates/bootstrap_styles.php';
    ?>
</head>
<body>

<?php
include 'html_templates/navbar.php';
include_once 'db/php/query_all.php';

// get the university requested
$conn = connection_open();
$result = do_query($conn, query_university_single($_GET['uniid']));
$row = mysqli_fetch_assoc($result);

// store result early
$uniid = $row['uniid'];
$uniname = $row['uniname'];
$city = $row['city'];
$prov = $row['prov'];
$nickname = $row['nickname'];

// still need connection for courses, but we're done with this result
mysqli_free_result($result);
?>

<div class="container">
    <h3>Viewing <?php echo $uniname ?></h3>
    <hr />

    <!-- basic uni info -->
    <div class="row">
        <div class="col">
            <strong>ID</strong>: <?php echo $uniid ?>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <strong>Name</strong>: <?php echo $uniname ?>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <strong>City</strong>: <?php echo $city ?>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <strong>Province</strong>: <?php echo $prov ?>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <strong>Nickname</strong>: <?php echo $nickname ?>
        </div>
    </div>

    <!-- courses offered by the uni -->
    <div class="row mt-5">
        <div class="col">
            <h3>Courses</h3>
            <table class="table table-sm table-hover table-striped">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Year</th>
                    <th scope="col">Weight</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
                <?php
                // get all the courses for this uni and print them into the table
                $result = do_query($conn, query_university_courses($uniid));
                while ($row = mysqli_fetch_assoc($result)) {
                    echo '<tr>';
                    echo '<td>' . $row['outsidenum'] . '</td>';
                    echo '<td>' . $row['outsidename'] . '</td>';
                    echo '<td>' . $row['whichyear'] . '</td>';
                    echo '<td>' . $row['weight'] . '</td>';
                    echo '</tr>';
                }
                mysqli_free_result($result);
                ?>
            </table>
        </div>
    </div>
</div>

<?php
// clean up the connection
connection_close($conn);
include 'html_templates/bootstrap_scripts.php';
?>
</body>
</html>