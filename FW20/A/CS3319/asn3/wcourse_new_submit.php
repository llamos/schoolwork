<?php
// handles form submission of wcourse_new.php

include_once 'db/php/query_all.php';

// extract relevant info from post body, prepend cs before id since we only take the numbers in the form
$wcnum_raw = $_POST['wcnum'];
$wcnum = 'cs' . $wcnum_raw;
$wcname = $_POST['wcname'];
$wcweight = $_POST['wcweight'];
$wcsuffix = $_POST['wcsuffix'];

// load the western course id from the specified id to check if it already exists
$conn = connection_open();
$result = do_query($conn, query_wcourse_single($wcnum));
$row = mysqli_fetch_assoc($result);

if ($row) {
    // fail for duplicate wcnum, redirect back to form with error message
    header('Location: wcourse_new.php?wcnum_dupe=true&wcnum=' . urlencode($wcnum_raw) . '&wcname=' . urlencode($wcname) . '&wcweight=' . urlencode($wcweight) . '&wcsuffix=' . urlencode($wcsuffix));
} else {
    // id doesn't exist, insert course and redirect to list
    do_query($conn, query_wcourse_new($wcnum, $wcname, $wcweight, $wcsuffix));
    header('Location: wcourse_list.php');
}

// close out connection regardless
connection_close($conn);

?>