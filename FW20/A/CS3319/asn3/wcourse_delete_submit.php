<?php
// handles form submission from wcourse_delete.php

include_once 'db/php/query_all.php';

// don't need to check whether course id actually exists, delete from will fail silently and ignore it
$conn = connection_open();
do_query($conn, query_wcourse_delete($_POST['wcnum']));
connection_close($conn);

// redirect back to list of all western courses
header('Location: wcourse_list.php');

?>