<!DOCTYPE html>
<!-- allows users to create a new equivalence -->

<html lang="en">
<head>
    <title>Course Equivalences - New Equivalence</title>

    <?php
    include 'html_templates/bootstrap_styles.php';
    ?>
</head>
<body>

<?php
include 'html_templates/navbar.php';
?>

<div class="container">
    <h3>Adding New Equivalence</h3>
    <hr/>
    <?php
    // error box if western course number was invalid
    if (isset($_GET['wcnum_error']) && strcmp($_GET['wcnum_error'], 'true') == 0) {
        echo '<div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Invalid Western Course ID</h4>
            <hr />
            <p>The Western course ID you entered (' . $_GET['wcnum'] . ') does not exist.</p>
        </div>';
    }

    // error box if university id and outside course number combo was invalid
    if (isset($_GET['onum_error']) && strcmp($_GET['onum_error'], 'true') == 0) {
        echo '<div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Invalid University ID</h4>
            <hr />
            <p>The university ID you entered (' . $_GET['uniid'] . ') does not offer a course with ID (' . $_GET['onum'] . ') .</p>
        </div>';
    }
    ?>

    <!-- form inputs, prefill from query string in case of errors, submit php will send back values -->
    <form action="equivalence_new_submit.php" method="post" class="form">
        <div class="row">
            <div class="col">
                <label for="input-wcnum">Western Course ID (4 digits)</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">cs</span>
                    </div>
                    <input id="input-wcnum" class="form-control" name="wcnum"
                           value="<?php echo urldecode($_GET['wcnum']) ?>" type="text" pattern="\d{4}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <label for="input-uniid">University ID</label>
                <input id="input-uniid" class="form-control" name="uniid"
                       value="<?php echo urldecode($_GET['uniid']) ?>" type="number" max="99" min="0" step="1">
            </div>
        </div>
        <div class="row">
            <div class="col">
                <label for="input-onum">Outside Course ID (3 digits)</label>
                <input id="input-onum" class="form-control" name="onum"
                       value="<?php echo urldecode($_GET['onum']) ?>" type="text" pattern="\d{3}">
            </div>
        </div>
        <button type="submit" class="btn btn-success float-right mt-2">Submit</button>
    </form>
</div>

<?php
include 'html_templates/bootstrap_scripts.php';
?>
</body>
</html>