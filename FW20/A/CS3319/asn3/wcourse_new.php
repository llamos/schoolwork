<!DOCTYPE html>
<!-- allows users to create a new western course entry -->

<html lang="en">
<head>
    <title>Course Equivalences - New Western Course</title>

    <?php
    include 'html_templates/bootstrap_styles.php';
    ?>
</head>
<body>

<?php
include 'html_templates/navbar.php';
?>

<div class="container">
    <h3>Adding New Western Course</h3>
    <hr />

    <?php
    // error box for if wcnum submitted already existed
    if (isset($_GET['wcnum_dupe']) && strcmp($_GET['wcnum_dupe'], 'true') == 0) {
        echo '<div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Duplicate Course ID</h4>
            <hr />
            <p>The course ID you entered (' . $_GET['wcnum'] . ') is already taken.</p>
        </div>';
    }
    ?>

    <!-- form fields for each field in db table, values are populated from query string in case of errors, since form submit will include those -->
    <form action="wcourse_new_submit.php" method="post" class="form">
        <div class="row">
            <div class="col">
                <label for="input-number">Course ID (4 digits)</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">cs</span>
                    </div>
                    <input id="input-number" class="form-control" name="wcnum" value="<?php echo urldecode($_GET['wcnum']) ?>" type="text" pattern="\d{4}" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <label for="input-name">Course Name</label>
                <input id="input-name" class="form-control" name="wcname" value="<?php echo urldecode($_GET['wcname']) ?>" type="text" maxlength="50" >
            </div>
        </div>
        <div class="row">
            <div class="col">
                <label for="input-weight">Course Weight</label>
                <input id="input-weight" class="form-control" name="wcweight" value="<?php echo urldecode($_GET['wcweight']) ?>" type="number" step="0.1" >
            </div>
        </div>
        <div class="row">
            <div class="col">
                <label for="input-suffix">Course Suffix</label>
                <input id="input-suffix" class="form-control" name="wcsuffix" value="<?php echo urldecode($_GET['wcsuffix']) ?>" type="text" maxlength="3" >
            </div>
        </div>
        <button type="submit" class="btn btn-success float-right mt-2">Submit</button>
    </form>
</div>

<?php
include 'html_templates/bootstrap_scripts.php';
?>
</body>
</html>