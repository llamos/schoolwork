<?php
// handles submission of wcourse_edit.php

include_once 'db/php/query_all.php';

// don't need to check if course id actually exists,
// if it doesn't then the user has messed with it anyway and it won't edit anything
$conn = connection_open();
do_query($conn, query_wcourse_edit($_POST['wcnum'], $_POST['wcname'], $_POST['wcweight'], $_POST['wcsuffix']));
connection_close($conn);

// redirect back to course list to see changes
header('Location: wcourse_list.php');

?>