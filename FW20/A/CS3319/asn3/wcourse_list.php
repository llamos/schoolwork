<!DOCTYPE html>
<!-- lists western courses, ordered by name or id -->

<html lang="en">
<head>
    <title>Course Equivalences - Western Course List</title>

    <?php
    include 'html_templates/bootstrap_styles.php';
    ?>
</head>
<body>

<?php
include 'html_templates/navbar.php';
?>

<div class="container">
    <h3>Western Courses</h3>

    <!-- options to sort results -->
    <form action="wcourse_list.php" method="get" class="form-inline">
        <label for="select-order" class="mb-2 mx-3">Sort By:  </label>
        <div class="form-group mb-2 mx-3">
            <!-- sort field -->
            <select class="form-control" name="order" id="select-order">
                <option value="number">Course Number</option>
                <option value="name">Course Name</option>
            </select>
        </div>
        <div class="form-group mb-2 mx-3">
            <!-- sort direction -->
            <select class="form-control" name="direction" id="select-asc">
                <option value="asc">Ascending</option>
                <option value="desc">Descending</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary mb-2">Sort</button>
    </form>
    <hr/>

    <!-- shows ordered course list -->
    <table class="table table-sm table-hover table-striped">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Weight</th>
            <th scope="col">Suffix</th>
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
        <?php
        // load in courses, getting order from query string
        include_once 'db/php/query_all.php';
        $conn = connection_open();
        $result = do_query($conn, query_wcourse_list($_GET['order'], $_GET['direction']));

        // print each row into the table
        while ($row = mysqli_fetch_assoc($result)) {
            echo '<tr>';
            echo '<td>' . $row['westernnum'] . '</td>';
            echo '<td>' . $row['westernname'] . '</td>';
            echo '<td>' . $row['weight'] . '</td>';
            echo '<td>' . $row['suffix'] . '</td>';
            echo '<td><form action="wcourse_edit.php" method="get"><input type="hidden" name="wcnum" value="' . $row['westernnum'] . '"><button type="submit" class="btn btn-warning">Edit</button></form></td>';
            echo '<td><form action="wcourse_delete.php" method="get"><input type="hidden" name="wcnum" value="' . $row['westernnum'] . '"><button type="submit" class="btn btn-danger">Delete</button></form></td>';
            echo '</tr>';
        }

        // close out db conn
        mysqli_free_result($result);
        connection_close($conn);
        ?>
    </table>
    <a href="wcourse_new.php" class="btn btn-success float-right">Add Course</a>
</div>

<?php
include 'html_templates/bootstrap_scripts.php';
?>
</body>
</html>