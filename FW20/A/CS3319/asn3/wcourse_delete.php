<!DOCTYPE html>
<!-- page to warn users about pending deletion action -->

<html lang="en">
<head>
    <title>Course Equivalences - Delete Course</title>

    <?php
    include 'html_templates/bootstrap_styles.php';
    ?>
</head>
<body>

<?php
include 'html_templates/navbar.php';
include_once 'db/php/query_all.php';

// load the course requested, to print relevant info
$conn = connection_open();
$result = do_query($conn, query_wcourse_single($_GET['wcnum']));
$row = mysqli_fetch_assoc($result);

// store result early
$wcnum = $row['westernnum'];
$wcname = $row['westernname'];
$wcweight = $row['weight'];
$wcsuffix = $row['suffix'];

mysqli_free_result($result);
?>

<div class="container">
    <h3>Deleting Western Course <?php echo $wcnum ?></h3>
    <hr/>
    <form action="wcourse_delete_submit.php" method="post" class="form">
        <input type="hidden" name="wcnum" value="<?php echo $wcnum ?>">
        <p>
            Are you sure want to delete Western course <?php echo $wcnum . ': ' . $wcname ?>?
            <strong>This action is irreversible!</strong>
        </p>
        This course is currently equivalent to the following courses at other universities:
        <ul>
            <?php
            // load in the outside courses that this western course is equiv to
            $result = do_query($conn, query_wcourse_equivalences($wcnum));
            $had_any_equiv = false;

            // print each into a bullet list
            while ($row = mysqli_fetch_assoc($result)) {
                echo '<li>' . $row['outsidenum'] . ' at ' . $row['uniname'] . '</li>';
                $had_any_equiv = true;
            }
            mysqli_free_result($result);

            // print None if they had no equivs
            if (!$had_any_equiv) {
                echo '<li>None</li>';
            }
            ?>
        </ul>
        <p>
            Deleting this course will also delete any equivalences to outside courses,
            but will not delete the outside courses themselves.
        </p>
        <button type="submit" class="btn btn-danger mt-2">Delete</button>
    </form>
</div>

<?php
connection_close($conn);
include 'html_templates/bootstrap_scripts.php';
?>
</body>
</html>