<?php
// handles post submission of equivalence_new.php

include_once 'db/php/query_all.php';

// get post data, fill out cs and CompSci prefixes on applicable fields
$wcnum_raw = $_POST['wcnum'];
$wcnum = 'cs' . $wcnum_raw;
$uniid = $_POST['uniid'];
$onum_raw = $_POST['onum'];
$onum = 'CompSci' . $onum_raw;

// open connection and prepare in case of form errors
$conn = connection_open();
$has_error = false;
$error_header = 'Location: equivalence_new.php?wcnum=' . $wcnum_raw . '&uniid=' . $uniid . '&onum=' . $onum_raw;

// check that specified wcourse exists
$result = do_query($conn, query_wcourse_single($wcnum));
$row = mysqli_fetch_assoc($result);
if (!$row) {
    // fail for nonexistant wcourse
    $has_error = true;
    $error_header = $error_header . '&wcnum_error=true';
}
mysqli_free_result($result);

// ensure that specified ocourse exists
$result = do_query($conn, query_ocourse_single($onum, $uniid));
$row = mysqli_fetch_assoc($result);
if (!$row) {
    // fail for nonexistant ocourse
    $has_error = true;
    $error_header = $error_header . '&onum_error=true';
}
mysqli_free_result($result);

// if we had an error, return that instead of submitting
if ($has_error) {
    header($error_header);
    return;
}

// otherwise, submit the db changes
do_query($conn, query_equivalence_new($wcnum, $uniid, $onum));

// redirect user to list of equivalences on that course so they can immediately see the result of their submission
header('Location: equivalence_list.php?wcnum=' . urlencode($wcnum_raw));
connection_close($conn);

?>