<?php

// opens and returns the db connection, failing and printing out error if needed
function connection_open(): mysqli {
    $mysql_connection = mysqli_connect('localhost', 'root', 'cs3319', 'pnorton4assign3db');
    if (mysqli_connect_errno()) {
        die("Database connection failed: " . mysqli_connect_error() . " (" . mysqli_connect_errno() . ")");
    }

    return $mysql_connection;
}

?>