<?php

// utility fn to list canadian provinces/territories sorted alphabetically
function provinces_list() {
    $prov_list = ['AB', 'BC', 'SK', 'MB', 'ON', 'QB', 'PE', 'NS', 'NL', 'NB', 'NT', 'YK', 'NV'];
    sort($prov_list);
    return $prov_list;
}

// query builder to find universities either by province or by lack of courses
function query_university_list(string $province = null, string $no_equivs = null) {
    $query = "SELECT * FROM university";
    if (strcmp($no_equivs, "true") == 0) {
        $query = $query . " WHERE (SELECT COUNT(*) FROM outsidecourse WHERE uniid=university.uniid)=0";
    } elseif (isset($province) && strcmp($province, 'all') != 0) {
        $query = $query . " WHERE prov='" . $province . "'";
    }
    $query = $query . ' ORDER BY prov';

    return $query;
}

?>