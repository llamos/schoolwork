<?php

// query builder to insert a new equivalence with the current date,
// or update existing entry to current date if it already exists
function query_equivalence_new(string $wcnum, string $uniid, string $onum) {
    return "INSERT INTO equivalentto (westernnum, outsidenum, uniid, evaluateddate) VALUE ('" . $wcnum . "', '" . $onum . "', " . $uniid . ", CURRENT_DATE())
                ON DUPLICATE KEY UPDATE evaluateddate=CURRENT_DATE()";
}

?>