<?php

// query builder to get equivalences with potential filters of western course number or before date
function query_equivalence_list(string $wcnum = null, string $date = null) {
    $query = 'SELECT e.westernnum, e.outsidenum, e.evaluateddate, o.outsidename, o.weight as oweight, u.uniname, w.weight as wweight, w.westernname
                FROM equivalentto e 
                    JOIN westerncourse w on w.westernnum = e.westernnum 
                    JOIN university u on e.uniid = u.uniid
                    JOIN outsidecourse o on o.outsidenum = e.outsidenum and o.uniid = e.uniid';
    if ($wcnum) {
        $query = $query . " WHERE e.westernnum='cs" . $wcnum . "'";
    } elseif ($date) {
        $query = $query . " WHERE e.evaluateddate < '" . $date . "'";
    }

    return $query;
}

?>