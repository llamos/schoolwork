<?php

// query builder to load all outside courses equivalent to a specified western course
function query_wcourse_equivalences(string $wcnum) {
    return "SELECT * FROM outsidecourse oc JOIN university u on oc.uniid = u.uniid WHERE (outsidenum, oc.uniid) IN (SELECT outsidenum, uniid FROM equivalentto WHERE westernnum='" . $wcnum . "')";
}

?>