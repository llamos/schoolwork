<?php

// query builder to list western courses, sorted by either name or id, ascending or descending
function query_wcourse_list(String $sort = null, String $direction = null) {

    $query = 'SELECT * FROM westerncourse';
    if ($sort) {
        if (strcmp($sort, 'number') == 0) {
            $query = $query . ' ORDER BY westernnum';
        } else if (strcmp($sort, 'name') == 0) {
            $query = $query . ' ORDER BY westernname';
        }

        if ($direction) {
            if (strcmp($direction, 'asc') == 0) {
                $query = $query . ' ASC';
            } else {
                $query = $query . ' DESC';
            }
        }
    }

    return $query;
}

?>