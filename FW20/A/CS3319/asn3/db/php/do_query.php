<?php

function do_query(mysqli $mysql_connection, string $query) {
    if (!isset($mysql_connection) || !$mysql_connection) {
        die('Attempted to perform query without connection to database');
    }

    $result = mysqli_query($mysql_connection, $query);
    if (mysqli_errno($mysql_connection)) {
        die("Got error executing query: \n\n " . $query . "\n\nError was:\n\n" . mysqli_errno($mysql_connection) . ": " . mysqli_error($mysql_connection));
    }

    return $result;
}

?>