<?php

// utility function to close the connection if it exists
// does nothing if the connection is already closed
function connection_close(mysqli $mysql_connection) {
    if ($mysql_connection) {
        mysqli_close($mysql_connection);
    }
}

?>