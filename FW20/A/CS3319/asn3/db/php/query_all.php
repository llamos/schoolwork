<?php
// utility file to import all the database functions in one import

include_once 'connection_open.php';
include_once 'connection_close.php';

include_once 'do_query.php';

include_once 'query_equivalence_list.php';
include_once 'query_equivalence_new.php';

include_once 'query_ocourse_single.php';

include_once 'query_university_courses.php';
include_once 'query_university_list.php';
include_once 'query_university_single.php';

include_once 'query_wcourse_delete.php';
include_once 'query_wcourse_edit.php';
include_once 'query_wcourse_equivalences.php';
include_once 'query_wcourse_list.php';
include_once 'query_wcourse_new.php';
include_once 'query_wcourse_single.php';

?>