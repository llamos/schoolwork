<?php
// defines the navbar at the top of the page, to be reused across the site

echo '<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-5">
<a class="navbar-brand" href="index.php">Western Courses</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="wcourse_list.php">Western Courses</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="university_list.php">Universities</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="equivalence_list.php">Equivalences</a>
      </li>
    </ul>
  </div>
</nav>';

?>