### Paul Norton
### pnorton4@uwo.ca
### 250957533
### Written Sept 20, 2020
### 
### Usage: solve(N, a)

# Extended Euclidean Algorithm
# px = previous x
# nx = next x
def solve(N, a):
    # setup, preserve N and a for output str
    pr, r = N, a
    ps, s = 1, 0
    pt, t = 0, 1

    while r != 0:
        # calculate next values
        quotient = pr // r
        nr = pr % r
        ns = ps - quotient * s
        nt = pt - quotient * t
        # cycle nexts into current for next iteration
        pr, r = r, nr
        ps, s = s, ns
        pt, t = t, nt

    if pr == 1:
        # got an inverse
        if pt < 0:
            pt = N + pt
        print('the inverse of ' + str(a) + ' mod ' + str(N) + ' is ' + str(pt))
    else:
        # no inverse
        print(str(a) + ' is not invertible mod ' + str(N))

if __name__ == "__main__":
    solve(32416187893, 17829884157)
    solve(1050809256899160222623, 32416188271) 
    solve(973051009079, 959576513618)
