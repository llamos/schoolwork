class Polynomial:

    def __init__(self, p, coefficients):
        self.p = p
        self.coefficients = [x % p for x in coefficients]

    def degree(self):
        return len(self.coefficients) - 1

    def __neg__(self):
        return Polynomial(self.p, [-x for x in self.coefficients])

    def __add__(self, other):
        max_coeff = max(len(self.coefficients), len(other.coefficients))
        result = []
        for i in range(max_coeff):
            self_coeff = self.coefficients[i] if i < len(self.coefficients) else 0
            other_coeff = other.coefficients[i] if i < len(other.coefficients) else 0
            result.append(self_coeff + other_coeff)
        return Polynomial(self.p, result)

    def __mul__(self, other):
        result_len = self.degree() + other.degree() + 1
        result = [0] * result_len
        for self_index, self_coeff in enumerate(self.coefficients):
            for other_index, other_coeff in enumerate(other.coefficients):
                destination_index = self_index + other_index
                product_coeff = self_coeff * other_coeff

                current_value = result[destination_index]
                new_value = current_value + product_coeff
                result[destination_index] = new_value

        return Polynomial(self.p, result)

    def __mod__(self, other):
        self_deg = self.degree()
        other_deg = other.degree()
        if self_deg < other_deg:
            return self
        if self_deg == other_deg and self.coefficients[self_deg] < other.coefficients[other_deg]:
            return self

        other_leading_coeff = other.coefficients[other_deg]
        remainder = self
        for power in range(self_deg, 0, -1):
            rem_coeff = remainder.coefficients[power]
            quotient_coefficient = rem_coeff // other_leading_coeff
            quotient_degree = power - other_deg
            quotient_polynom = Polynomial(self.p, [0] * quotient_degree + [quotient_coefficient])
            quotient_divisor_product = quotient_polynom * other
            remainder = remainder + (-quotient_divisor_product)
        
        return remainder

    def __str__(self):
        return 'Polynomial({})'.format(self.coefficients)

def solve(p, m, q1, q2):
    m = Polynomial(p, m)
    q1 = Polynomial(p, q1)
    q2 = Polynomial(p, q2)

    sum = (q1 + q2) % m
    product = (q1 + q2) % m
    print('sum = {}'.format(sum))
    print('product = {}'.format(product))

solve(2, [1, 1, 0, 1], [1, 1, 0], [0, 1, 1])
solve(2, [1, 1, 0, 1], [1, 0, 1], [1, 0, 1])
