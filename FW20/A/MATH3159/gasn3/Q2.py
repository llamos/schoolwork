from generate_input import generate_input

# simple gcd, don't need inverses from extended eea
def gcd(a, b):
    return a if b == 0 else gcd(b, a % b)

# reorders p and q for least-value first
def rearrange(p, q):
    return (p, q) if p < q else (p, q)

# fast powering
def fp(a, p, N):
    ret = 1
    while p:
        if p & 1: # check smallest bit of p
            ret = ret * a % N
        p >>= 1 # shift over p, so `if` checks next bit
        a = (a * a) % N
    return ret

# finds r,k such that d * e - 1 = 2^k * r and r % 2 == 1
def miller_rabin_seed(e, d):
    r = e * d - 1
    k = 0
    while True:
        if r % 2 == 1: # terminate when r odd
            return r, k
        else: # reduce r by a power of 2
            k += 1
            r //= 2

def solve(N, e, d):
    # get r and k early, since they don't change per iteration
    # e * d - 1 = 2^k * r and r % 2 == 1
    r, k = miller_rabin_seed(e, d)

    # loop at most up to sqrt(N), since that guarantees there is always a solution
    for a in range(2, int(N ** 0.5)):
        # check early in case a is a factor
        g = gcd(a, N)
        if g != 1 and g != N:
            return rearrange(g, N // g)

        # check a^r = 1 mod N
        ar = fp(a, r, N) # ar = a^r % N
        g = gcd(ar - 1, N)
        if g != 1 and g != N:
            return rearrange(g, N // g)

        # this is a^{2^0 * r}
        # do it separately to avoid squaring early
        # checks a^{2^0 * r} = -1 mod N
        g = gcd(ar + 1, N)
        if g != 1 and g != N:
            return rearrange(g, N // g)

        # for each iteration k != 0, we square 
        for ki in range(1, k):
            ar = fp(ar, 2, N) # each iter is square of previous, don't recalc entire a^{2k * r}
            g = gcd(ar + 1, N) # check a^{2^ki * r} = -1 mod N
            if g != 1 and g != N:
                return rearrange(g, N // g)


def run_solve(tuple):
    N = tuple[0]
    e = tuple[1]
    d = tuple[2]
    result = solve(N, e, d)
    print('solve(' + str(N) + ', ' + str(e) + ', ' + str(d) + ') =', result)

#for test_input in [(10, 7, 3), (33, 19, 19), (323, 173, 5)]:
#    run_solve(test_input)

# programmer student is Paul Norton 250957533
for input in generate_input('533'):
    run_solve(input)
