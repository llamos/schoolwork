# Paul Norton
# pnorton4@uwo.ca
# 250957533
# Nov 13, 2020

# fast powering, copied from group assignment 3 (I was still author)
def fp(a, p, N):
    ret = 1
    while p:
        if p & 1:  # check smallest bit of p
            ret = ret * a % N
        p >>= 1  # shift over p, so `if` checks next bit
        a = (a * a) % N
    return ret

def solve(N, d, c):
    nm = fp(c, d, N)

    # convert n(m) to binary, and pad in leading 0s that may have been trimmed
    binary_string = bin(nm)[2:]
    binary_string = binary_string.rjust(60, '0')

    # split it into r, and encoded version of m
    r = binary_string[0:30]
    encoded_m = binary_string[30:]

    # now bit by bit, rebuild m from r and encoded_m
    m = ''
    for i in range(30):
        if encoded_m[i] == '1':
            # m[i] = r[i]
            m += r[i]
        else:
            # m[i] = !r[i]
            m += '0' if r[i] == '1' else '1'

    return 'Decrypted message m = {}'.format(m)

if __name__ == '__main__':
    print('solve(1152922971338552633, 1152922969191067619, 1089811254155265256)')
    print(solve(1152922971338552633, 1152922969191067619, 1089811254155265256))

    print('solve(1152924158898146959, 1152924156750660839, 461717207618635886)')
    print(solve(1152924158898146959, 1152924156750660839, 461717207618635886))

    print('solve(1152922960601051083, 1152922958453566079, 944123041038189134)')
    print(solve(1152922960601051083, 1152922958453566079, 944123041038189134))
