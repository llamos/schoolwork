# inverse of a mod N
def eea(N, a):
    while a < 0:
        a += N

    # setup, preserve N and a for output str
    pr, r = N, a
    ps, s = 1, 0
    pt, t = 0, 1

    while r != 0:
        # calculate next values
        quotient = pr // r
        nr = pr % r
        ns = ps - quotient * s
        nt = pt - quotient * t
        # cycle nexts into current for next iteration
        pr, r = r, nr
        ps, s = s, ns
        pt, t = t, nt

    if pr == 1:
        # got an inverse
        if pt < 0:
            pt = N + pt
        return pt
    else:
        # no inverse
        return None


# fast powering, copied from assignment 4
# modified to do scalar multiplication of points instead of powering
def fm(factor, point):
    ret = ECPoint(point.asTuple()[0], -1, -1)
    a = point
    while factor:
        if factor & 1:  # check smallest bit of p
            ret = ret + a
        factor >>= 1  # shift over p, so `if` checks next bit
        a = (a + a)
    return ret


# Holds info about an elliptic curve:
# 1. the prime modulus
# 2. the A and B coefficients
class EllipticCurve:

    def __init__(self, p, a, b):
        self.p = p
        self.a = a
        self.b = b

    def getP(self):
        return self.p

    def getA(self):
        return self.a

    def getB(self):
        return self.b

    def __str__(self):
        return f"EllipticCurve(p={self.p}, A={self.a}, B={self.b})"


# Represents one point within an EllipticCurve object
# Takes the EC in question, and x and y coords
# All operations will be done in, and preserve, the EC provided
class ECPoint:

    def __init__(self, ec: EllipticCurve, x: float, y: float):
        self.ec = ec
        self.x = x
        self.y = y

    # check for infty, encoded as (-1, -1)
    def isInfinity(self):
        return self.x == -1 and self.y == -1

    # unary negation of point, returns (x, -y)
    def __neg__(self):
        return ECPoint(self.ec, self.x, self.ec.getP() - self.y)

    # equality check between two points
    def __eq__(self, other):
        if not isinstance(other, ECPoint):
            return False
        if other.ec != self.ec:
            raise ValueError("Can't compare two points from differing elliptic curves")
        return self.x == other.x and self.y == other.y

    # string representation for printing to console
    def __str__(self):
        return f"ECPoint(ec={self.ec}, x={self.x}, y={self.y})"

    # adds two points together via standard EC intersection rules
    def __add__(self, other):
        if not isinstance(other, ECPoint):
            other_type = str(type(other))
            raise TypeError(f"Can't add point to non-point {other_type}.")

        # return early for unit rules
        if self.isInfinity():
            return other
        if other.isInfinity():
            return self

        # return early for P + -P
        if self == -other:
            return ECPoint(self.ec, -1, -1)

        # gonna be using this a lot
        p = self.ec.getP()

        if self == other:
            # doubling self, take tangent to self
            if self.y == 0:
                # inf if y isn't invertible
                return ECPoint(self.ec, -1, -1)
            
            # numerator is 3x^2 + A
            slope_num = (3 * (self.x ** 2) + self.ec.getA()) % p

            # divisor is (2y)^-1
            slope_div_inv = (2 * self.y) % p
            slope_div = eea(p, slope_div_inv)

            # slope is num / div
            slope = (slope_num * slope_div) % p

            # not actually called v, but idk its real name
            v = (self.y - (slope * self.x)) % p

        else:
            # P != Q, take slope between
            # slope calculation is pretty straightforward, dy/dx
            slope_num = (other.y - self.y) % p
            slope_div_inv = (other.x - self.x) % p

            # we can assume slope_div_inv is invertible, due to self == other / self == -other checks above
            # take inverse for modular "division"
            slope_div = eea(p, slope_div_inv)

            # slope is num / div
            slope = (slope_num * slope_div) % p

            # not actually called v, but idk its real name
            v = (other.y - slope * other.x) % p

        # finally calculate result from slope and v, within same ec
        res_x = ((slope ** 2) - self.x - other.x) % p
        res_y = (-slope * res_x - v) % p

        # and return the result
        return ECPoint(self.ec, res_x, res_y)

    # for testing
    # n * P = P + P, n times
    def naive_mul(self, factor):
        result = self
        for i in range(1, factor):
            result = result + self
        return result

    # uses fm function, which is modified fast-powering, for double-and-add
    def fast_mul(self, factor):
        return fm(factor, self)

    # multiplies point by integer factor via repeated addition
    def __mul__(self, factor):
        if type(factor) != int:
            raise TypeError("Can't multiply point by non-integer.")

        # 0 * P should be infty
        if factor == 0:
            return ECPoint(self.ec, -1, -1)

        # 1 * P = P
        elif factor == 1:
            return self

        else:
            return self.fast_mul(factor)

    # allows for n * P syntax, calls __mul__
    def __rmul__(self, factor):
        return self * factor

    # for output
    def asTuple(self):
        return (self.ec, self.x, self.y)


# uses addition and multiplication defined in ECPoint class
def solve(p, A, B, Px, Py, n):
    # construct EC and ECPoint from params
    ec = EllipticCurve(p, A, B)
    P = ECPoint(ec, Px, Py)
    result = n * P

    # slice off to just (x, y)
    return result.asTuple()[1:]

if __name__ == "__main__":
    from generate_input import generate_input
    input_lists = generate_input('533')

    for input_list in input_lists:
        # convert to tuple
        input_tuple = tuple(input_list)

        # compute locally
        local_result = solve(*(input_tuple))
        print(f"solve{input_tuple}")
        print(f"\t = {local_result}")
