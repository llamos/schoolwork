def eea(N, a):
    while a < 0:
        a += N

    # setup, preserve N and a for output str
    pr, r = N, a
    ps, s = 1, 0
    pt, t = 0, 1

    while r != 0:
        # calculate next values
        quotient = pr // r
        nr = pr % r
        ns = ps - quotient * s
        nt = pt - quotient * t
        # cycle nexts into current for next iteration
        pr, r = r, nr
        ps, s = s, ns
        pt, t = t, nt

    if pr == 1:
        # got an inverse
        if pt < 0:
            pt = N + pt
        return pt
    else:
        # no inverse
        return None

def solve(p, g, A, D, Dprime, S1, S2, S1prime, S2prime):
    # p is a large prime
    # g is a primitive root of Fp*
    # A = g^a mod p
    # D and D' are documents
    # (S1, S2), (S1prime, S2prime) are signatures
    #    for D and D' respectively

    #Determine if two signatures are the same
    if (S1 != S1prime):
        print("no")
        return
    print("Finding s...", end="")
    s = eea(p - 1, S2 - S2prime)
    if s is None:
        return None
    print(f"s = {s} \n Finding r...", end="")
    r = (s*(D - Dprime)) % (p - 1)
    print(f"r = {r}, \n Finding t...", end="")
    t = eea(p, S1)
    if t is None:
        return None
    print(f"t = {t}, \n Finding a...", end="")
    a = t*(r*S2 - D) % (p - 1)
    return (r, a)

def verify(p, g, A, D, Dprime, S1, S2, S1prime, S2prime):
    print(f"Running solve({p}, {g}, {A}, {D}, {Dprime}, {S1}, {S2}, {S1prime}, {S2prime})...")
    result = solve(p, g, A, D, Dprime, S1, S2, S1prime, S2prime)
    if (result is None):
        return
    r = result[0]
    a = result[1]
    print(f"Returned a = {a}")
    got = (eea(p - 1, r)*(D - a*S1)) % (p - 1)
    if (S2 != got):
        print(f"Failed verification, expected {S2} - got {got}")

verify(31, 26, 30, 18, 11, 6, 24, 6, 25)
verify(348149, 113459, 185149, 153405, 127561, 208913, 209176, 208913, 217800)
verify(4139, 32, 1644, 3782, 2220, 3776, 1722, 2924, 3616)
