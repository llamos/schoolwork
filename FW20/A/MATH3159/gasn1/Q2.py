### Paul Norton
### pnorton4@uwo.ca
### 250957533
### Written Sept 26, 2020
### 
### Usage: solve(p, a, c, b)

# Extended Euclidean Algorithm
# copied from assignment 1
# with modification to return value instead of print
def eea(N, a):
    # setup, preserve N and a for output str
    pr, r = N, a
    ps, s = 1, 0
    pt, t = 0, 1

    while r != 0:
        # calculate next values
        quotient = pr // r
        nr = pr % r
        ns = ps - quotient * s
        nt = pt - quotient * t
        # cycle nexts into current for next iteration
        pr, r = r, nr
        ps, s = s, ns
        pt, t = t, nt

    if pr == 1:
        # got an inverse
        if pt < 0:
            pt = N + pt
        return pt
    else:
        # no inverse
        return None

# based on math proof
# since math proof is inductive
# the solution here is iterative
# going through all the c's up to c_a
# 
# each loop knows the current c
# and calculates the next c
def solve(p, a, c, b):
    # c_1
    current_c = c

    # exclude current_a = a, since a-1 calculates c_a
    for current_a in range(1, a):
        # save for reuse
        pa = p ** current_a
        # rearranged from c_a^2 - b = d_a * p^a
        d = (current_c ** 2 - b) // pa
        d_neg = -d
        # inverse of 2c using extended euclidean algorithm
        inv_2c = eea(pa, 2 * current_c)
        # rearranged from 2xc_a = -d_a mod p^a using modular inverse of 2c_a
        x = (inv_2c * d_neg) % pa
        # now we have x, can calculate c_{a+1} and loop
        current_c = current_c + (x * pa)

    pa = p ** a
    current_c = current_c % (p ** a)
    d_squared_is_equiv_b_mod_pa = ((current_c ** 2) % (p ** a)) == (b % (p ** a))
    d_is_equiv_c_mod_p = (current_c % p) == (c % p)
    print('p =', p)
    print('a =', a)
    print('c =', c)
    print('b =', b)
    print('d =', d)
    print('d^2 = b mod p^a: ', str(d_squared_is_equiv_b_mod_pa))
    print('d = c mod p: ', str(d_is_equiv_c_mod_p))
    return current_c

if __name__ == "__main__":
    input_tuples = [
        (32416187893, 12, 1115887800490119639623705338951463477886544199890562565557495059497583670837335373022565529632437822142784605828918785593753801, 1178218485814776857461789304958950810977319911451580110793422902991807572933648267927049252052757394517504465482369884421276693),
        (32416188271, 4, 765056899826468732886169656253399361025754, 468578354878427876779786442328514142594220),
        (32416188113, 4, 202358476972322107003356039554641818253517, 697560625107094749676179964184663833551264)
    ]
    # input_tuples = [
    #     (3, 2, 1, 4),
    #     (5, 3, 12, 19),
    #     (3, 4, 1, 4)
    # ]
    for tuple in input_tuples:
        print(
            'solve({0}, {1}, {2}, {3})'.format(tuple[0], tuple[1], tuple[2], tuple[3]),
            solve(tuple[0], tuple[1], tuple[2], tuple[3]),
            sep='\n\t\t=',
            end='\n\n'
        )
