# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


import random
from typing import List, Dict, Tuple, Union

import pacman
import util
from game import Agent, Directions  # noqa
from util import manhattanDistance  # noqa


class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.

      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """

    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices)  # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        "*** YOUR CODE HERE ***"
        def taxiDist(pos1, pos2):
            return abs(pos1[0] - pos2[0]) + abs(pos1[1] - pos2[1])

        def closestTarget(start, targets):
            target = min(targets, key=lambda x: taxiDist(start, x))
            dist = taxiDist(start, target)
            return target, dist

        newGhostPositions = [ghostState.getPosition() for ghostState in newGhostStates]
        moveScore = 0
        if newFood.count() < currentGameState.getFood().count():
            # eat nearby food if possible
            moveScore += 100

        if newPos in newGhostPositions:
            ghostIndex = newGhostPositions.index(newPos)
            if newScaredTimes[ghostIndex] > 2:
                # eat ghost if it's probably safe
                moveScore += 200
            elif newScaredTimes[ghostIndex] > 0:
                # avoid risky ghost eats
                moveScore += 20
            else:
                # don't die
                moveScore -= 5000

        if moveScore == 0:
            # move closer to food if there isn't any other information available
            closest, dist = closestTarget(newPos, newFood.asList())
            moveScore += 75 / dist

        return moveScore


def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.

      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    return currentGameState.getScore()


class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.

      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """

    def __init__(self, evalFn="scoreEvaluationFunction", depth="2"):
        self.index = 0  # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)


class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (question 2)
    """

    def getAction(self, gameState: pacman.GameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game
        """
        "*** YOUR CODE HERE ***"
        # multiples of agentModulo are pacman turns, otherwise ghost turns
        agentModulo = gameState.getNumAgents()
        maxDepth = self.depth * gameState.getNumAgents()
        evalFn = self.evaluationFunction

        class MinimaxNode:
            def __init__(self, depth: int, state: pacman.GameState):
                self.state = state
                self.depth = depth
                self.agentIndex = self.depth % agentModulo

            def buildChildNode(self, action: pacman.Actions):
                return MinimaxNode(self.depth + 1, self.state.generateSuccessor(self.agentIndex, action))

            def calculateValue(self) -> Tuple[Union[pacman.Actions, None], int]:
                # if max depth, state doesn't check children, evaluates to state value
                if self.depth == maxDepth:
                    return None, evalFn(self.state)

                # find children
                possibleActions: List[pacman.Actions] = self.state.getLegalActions(self.agentIndex)

                # if there are no valid moves, game is probably over, terminate recursion
                if len(possibleActions) == 0:
                    return None, evalFn(self.state)

                # evaluate value of all child nodes recursively
                actionScores = [(action, self.buildChildNode(action).calculateValue()[1]) for action in possibleActions]

                # return min or max based on agent index
                if self.agentIndex == 0:
                    # maximize for pacman turns
                    return max(actionScores, key=lambda actionTuple: actionTuple[1])
                else:
                    # minimize score for ghost turns
                    return min(actionScores, key=lambda actionTuple: actionTuple[1])

        root = MinimaxNode(0, gameState)
        return root.calculateValue()[0]


class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** YOUR CODE HERE ***"
        util.raiseNotDefined()


class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction

          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """
        "*** YOUR CODE HERE ***"
        util.raiseNotDefined()


def betterEvaluationFunction(currentGameState):
    """
      Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
      evaluation function (question 5).

      DESCRIPTION: <write something here so we know what you did>
    """
    "*** YOUR CODE HERE ***"
    util.raiseNotDefined()


# Abbreviation
better = betterEvaluationFunction
