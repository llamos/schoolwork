import sys

input_data = [
    ((1, 0, 0.5, 0), 0, 0),
    ((0, 1, 0.75, 0), 0, 0),
    ((0, 0, 1, 0), 0, 0),
    ((1, 1, 0.75, 0), 0, 0),
    ((0, 0.5, 0.25, 1), 0, 0),
    ((0, 1, 0.75, 0), 0, 0),
    ((1, 0.5, 0.25, 0), 0, 0),
    ((0, 1, 0.5, 1), 0, 0),
    ((0, 1, 0.5, 0), 0, 0),
    ((1, 0, 0.25, 1), 0, 0)
]

A = (0, 0, 0, 0)
B = (1, 1, 1, 1)

def dist(a, b):
    sum = 0
    for i in range(0, 4):
        sum += ((a[i] - b[i]) ** 2)
    return sum ** 0.5


i = 0
while True:

    i += 1
    orig_out = sys.stdout
    with open(f'./data/{i}.tex', 'w+') as f:
        sys.stdout = f

        input_data = [(x[0], dist(x[0], A), dist(x[0], B)) for x in input_data]

        print('\\textbf{Iteration ' + str(i) + '}')
        print('')

        print('\\begin{center}')
        print('\\begin{tabular}{|c|c|c|c|c|c|c|}')
        print('\\hline')
        print('Home Owner & Marital Status & Job Experience & Defaulted & $Dist_A$ & $Dist_B$ & Mean \\\\')
        print('\\hline')
        ams = []
        bms = []
        for x, da, db in input_data:
            print(f'{x[0]} & {x[1]} & {x[2]} & {x[3]} & {da:.3f} & {db:.3f} & {"A" if da < db else "B"} \\\\')
            print('\\hline')
            if da < db:
                ams.append(x)
            else:
                bms.append(x)

        print('\\end{tabular}')
        print('\\end{center}')
        print('')

        old_A = A
        old_B = B
        A = (sum([x[0] for x in ams]) / len(ams), sum([x[1] for x in ams]) / len(ams), sum([x[2] for x in ams]) / len(ams), sum([x[3] for x in ams]) / len(ams))
        B = (sum([x[0] for x in bms]) / len(bms), sum([x[1] for x in bms]) / len(bms), sum([x[2] for x in bms]) / len(bms), sum([x[3] for x in bms]) / len(bms))

        print('After recalculating the means, we have the following: \\\\')
        print(f'New A = $\\langle$ {A[0]:.3f}, {A[1]:.3f}, {A[2]:.3f}, {A[3]:.3f} $\\rangle$ \\\\')
        print(f'New B = $\\langle$ {B[0]:.3f}, {B[1]:.3f}, {B[2]:.3f}, {B[3]:.3f} $\\rangle$')
        print('')

    sys.stdout = orig_out
    print(f'dA = {dist(old_A, A)}, dB = {dist(old_B, B)}')
    if old_A == A and old_B == B:
        break

    print(f'Press Enter to continue to iteration {i + 1}....')
    input('')
