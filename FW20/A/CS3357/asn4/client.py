import socket
import os
import sys
import argparse
from typing import Tuple, Union
from urllib.parse import urlparse

# Define a constant for our buffer size

BUFFER_SIZE = 1024

# A function for creating HTTP GET messages.

def prepare_get_message(host, port, file_name):
    request = f'GET {file_name} HTTP/1.1\r\nHost: {host}:{port}\r\n\r\n'
    return request


# Read a single line (ending with \n) from a socket and return it.
# We will strip out the \r and the \n in the process.

def get_line_from_socket(sock):

    done = False
    line = ''
    while not done:
        char = sock.recv(1).decode()
        if char == '\r':
            pass
        elif char == '\n':
            done = True
        else:
            line = line + char
    return line

# Read a file from the socket and print it out.  (For errors primarily.)

def print_file_from_socket(sock, bytes_to_read):

    bytes_read = 0
    while bytes_read < bytes_to_read:
        chunk = sock.recv(BUFFER_SIZE)
        bytes_read += len(chunk)
        print(chunk.decode())

# Read a file from the socket and save it out.

def save_file_from_socket(sock, bytes_to_read, file_name):

    with open(file_name, 'wb') as file_to_write:
        bytes_read = 0
        while bytes_read < bytes_to_read:
            chunk = sock.recv(BUFFER_SIZE)
            bytes_read += len(chunk)
            file_to_write.write(chunk)


def parse_url(url: str, proxy_mode: bool = False) -> Union[Tuple[str, int, str], None]:
    parsed_url = urlparse(url)

    # validate url scheme
    if parsed_url.scheme != 'http':
        print(f'Warning:  Parsed non-HTTP scheme [{parsed_url.scheme}], but client only supports HTTP.')
        return None

    # validate path
    if proxy_mode and parsed_url.path != '':
        print(f'Warning:  Parsed invalid proxy URL which has path component [{parsed_url.path}]. Proxy URLs should not include a path.')
        return None

    host = parsed_url.hostname
    port = parsed_url.port if parsed_url.port is not None else 80  # default to port 80 if not specified
    path = parsed_url.path if parsed_url.path != '' else '/'  # default to / if not specified
    return host, port, path


def connect_and_handle(host: str, port: int, path: str, proxy_host: str, proxy_port: int):
    print(f'Connecting to server [{host}:{port}]', end='')
    if proxy_host is not None:
        print(f' via proxy [{proxy_host}:{proxy_port}]', end='')
    print('...')

    try:
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if proxy_host is not None:
            client_socket.connect((proxy_host, proxy_port))
        else:
            client_socket.connect((host, port))
    except ConnectionRefusedError:
        print('Error:  That host or port is not accepting connections.')
        sys.exit(-1)

    # The connection was successful, so we can prep and send our message.

    print('Connection to server established. Sending message...')
    message = prepare_get_message(host, port, path)
    client_socket.send(message.encode())

    # Receive the response from the server and start taking a look at it

    response_line = get_line_from_socket(client_socket)
    response_list = response_line.split(' ')
    headers_done = False

    # If an error is returned from the server, we dump everything sent and
    # exit right away.

    # If requested file begins with a / we strip it off.

    while path[0] == '/':
        path = path[1:]

    if response_list[1] == '301':
        # follow redirects on a 301, log first
        print('Info:  Server responded with redirect to ', end='')

        # parse headers to find new location
        location_url = None
        while not headers_done:
            header_line = get_line_from_socket(client_socket)
            header_list = header_line.split(' ', maxsplit=2)
            if header_line == '':
                headers_done = True
            elif header_list[0] == 'Location:':
                location_url = header_list[1]
                print(f'[{location_url}]')

        # make sure we got a location at all
        if location_url is None:
            print('[None]')
            print('Error:  Server did not provide a redirect target. Terminating.')
            sys.exit(-1)

        # parse the new target into parts
        redirect_result = parse_url(location_url)
        if redirect_result is None:
            print('Error:  Server provided redirect target, but it was not a valid URL. Terminating.')
            sys.exit(-1)
        redirect_host, redirect_port, redirect_path = redirect_result

        # recursively try again, but preserve proxy info
        connect_and_handle(redirect_host, redirect_port, redirect_path, proxy_host, proxy_port)
        return


    elif response_list[1] != '200':
        print('Error:  An error response was received from the server.  Details:\n')
        print(response_line)
        bytes_to_read = 0
        while not headers_done:
            header_line = get_line_from_socket(client_socket)
            print(header_line)
            header_list = header_line.split(' ')
            if header_line == '':
                headers_done = True
            elif header_list[0] == 'Content-Length:':
                bytes_to_read = int(header_list[1])
        print_file_from_socket(client_socket, bytes_to_read)
        sys.exit(-1)


    # If it's OK, we retrieve and write the file out.

    else:

        print('Success:  Server is sending file.  Downloading it now.')

        # Go through headers and find the size of the file, then save it.

        bytes_to_read = 0
        while not headers_done:
            header_line = get_line_from_socket(client_socket)
            header_list = header_line.split(' ')
            if header_line == '':
                headers_done = True
            elif header_list[0] == 'Content-Length:':
                bytes_to_read = int(header_list[1])
        save_file_from_socket(client_socket, bytes_to_read, path)


# Our main function.

def main():

    # Check the URL passed in and make sure it's valid.
    parser = argparse.ArgumentParser()
    parser.add_argument('-proxy')
    parser.add_argument('url')
    arg_result = parser.parse_args(sys.argv[1:])

    url_result = parse_url(arg_result.url)
    if url_result is None:
        print('Error:  Invalid URL.  Enter a URL of the form:  http://host:port/file')
        sys.exit(-1)
    host, port, file_name = url_result

    proxy_host = None
    proxy_port = None
    if arg_result.proxy is not None:
        proxy_result = parse_url(arg_result.proxy, proxy_mode=True)
        if proxy_result is None:
            print('Error:  Invalid proxy URL.  Enter a URL of the form:  http://host:port')
            sys.exit(-1)
        proxy_host, proxy_port, _ = proxy_result

    # Now we try to make a connection to the server.
    connect_and_handle(host, port, file_name, proxy_host, proxy_port)


if __name__ == '__main__':
    main()
