import socket
import os
import sys
import argparse
from time import time_ns
from typing import Tuple, Union
from urllib.parse import urlparse

# Define a constant for our buffer size

BUFFER_SIZE = 1024

# A function for creating HTTP GET messages.

def prepare_get_message(host, port, file_name):
    request = f'GET {file_name} HTTP/1.1\r\nHost: {host}:{port}\r\n\r\n'
    return request


# Read a single line (ending with \n) from a socket and return it.
# We will strip out the \r and the \n in the process.

def get_line_from_socket(sock):

    done = False
    line = ''
    while not done:
        char = sock.recv(1).decode()
        if char == '\r':
            pass
        elif char == '\n':
            done = True
        else:
            line = line + char
    return line

# Read a file from the socket and print it out.  (For errors primarily.)

def print_file_from_socket(sock, bytes_to_read):

    bytes_read = 0
    while bytes_read < bytes_to_read:
        chunk = sock.recv(BUFFER_SIZE)
        bytes_read += len(chunk)
        print(chunk.decode())

# Read a file from the socket and save it out.

def discard_from_socket(sock, bytes_to_read):

    # just read all the bytes, discard contents
    bytes_read = 0
    while bytes_read < bytes_to_read:
        chunk = sock.recv(BUFFER_SIZE)
        bytes_read += len(chunk)


def connect_and_handle(host: str, port: int, path: str):
    print(f'Connecting to upstream [{host}:{port}]...')
    try:
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.settimeout(15.0)
        client_socket.connect((host, port))
    except (ConnectionRefusedError, socket.timeout):
        print('Error:  That host or port is not accepting connections.')
        return -1

    # The connection was successful, so we can prep and send our message.

    print('Connection to server established. Sending message...')
    message = prepare_get_message(host, port, path)
    client_socket.send(message.encode())

    # Receive the response from the server and start taking a look at it

    response_line = get_line_from_socket(client_socket)
    response_list = response_line.split(' ')
    headers_done = False

    # If an error is returned from the server, we dump everything sent and
    # exit right away.
    if response_list[1] != '200':
        print('Error:  An error response was received from the upstream remote.  Details:\n')
        print(response_line)
        bytes_to_read = 0
        while not headers_done:
            header_line = get_line_from_socket(client_socket)
            print(header_line)
            header_list = header_line.split(' ')
            if header_line == '':
                headers_done = True
            elif header_list[0] == 'Content-Length:':
                bytes_to_read = int(header_list[1])
        print_file_from_socket(client_socket, bytes_to_read)
        return -1

    # If it's OK, we retrieve the contents, discarding them as we go (no need to actually store result).
    else:
        print('Success:  Server is sending file.  Downloading it now.')

        # Go through headers and find the size of the file, then save it.
        bytes_to_read = 0
        while not headers_done:
            header_line = get_line_from_socket(client_socket)
            header_list = header_line.split(' ')
            if header_line == '':
                headers_done = True
            elif header_list[0] == 'Content-Length:':
                bytes_to_read = int(header_list[1])

        time_start = time_ns()
        discard_from_socket(client_socket, bytes_to_read)
        time_end = time_ns()
        return time_end - time_start
