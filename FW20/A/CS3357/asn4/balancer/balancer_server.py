import socket
import os
from datetime import datetime
import signal
import sys
import argparse
from typing import Union, Tuple, List
from urllib.parse import urlparse
import random
from balancer_client import connect_and_handle

# Constant for our buffer size

BUFFER_SIZE = 1024


# Signal handler for graceful exiting.

def signal_handler(sig, frame):
    print('Interrupt received, shutting down ...')
    sys.exit(0)


# Create an HTTP response

def prepare_response_message(code: str, upstream: Union[Tuple[str, int], None], path: Union[str, None]) -> Tuple[str, str]:
    date = datetime.now()
    date_string = 'Date: ' + date.strftime('%a, %d %b %Y %H:%M:%S EDT')
    message = 'HTTP/1.1 '
    if code == '505':
        message = message + '505 Version Not Supported\r\n'
    elif code == '301' and (upstream is None or path is None):
        # in case we want to redirect but all remotes are unavailable
        message = message + '502 Bad Gateway\r\n'
        code = '502'
    elif code == '301':
        message = message + '301 Moved Permanently\r\n'
        message = message + f'Location: http://{upstream[0]}:{upstream[1]}{path}\r\n'
    message += date_string + '\r\n'
    return code, message


# Read a single line (ending with \n) from a socket and return it.
# We will strip out the \r and the \n in the process.

def get_line_from_socket(sock: socket) -> str:
    done = False
    line = ''
    while not done:
        char = sock.recv(1).decode()
        if char == '\r':
            pass
        elif char == '\n':
            done = True
        else:
            line = line + char
    return line


def parse_url(url: str) -> Union[Tuple[str, int], None]:
    parsed_url = urlparse(url)

    # validate url scheme
    if parsed_url.scheme != 'http':
        print(f'Warning:  Parsed non-HTTP scheme [{parsed_url.scheme}], but balancer only supports HTTP for upstreams.')
        return None

    # validate path
    if parsed_url.path != '':
        print(f'Warning:  Parsed invalid upstream URL which has path component [{parsed_url.path}]. Upstream URLs should not include a path.')
        return None

    host = parsed_url.hostname
    port = parsed_url.port if parsed_url.port is not None else 80  # default to port 80 if not specified
    return host, port


def send_response_to_client(sock: socket, code: str, upstream: Union[Tuple[str, int], None] = None, path: Union[str, None] = None):

    new_code, header = prepare_response_message(code, upstream, path)

    # Construct header and send it

    if new_code != 301:
        error_response = f'<!DOCTYPE html>\r\n<html><head><title>An Error Occurred</title></head><body>Sorry, an error occurred.<br>Error {new_code}</body></html>'.encode()
        header = header + f'Content-Type: text/html\r\nContent-Length: {len(error_response)}\r\n\r\n'
        sock.send(header.encode())
        sock.send(error_response)
    else:
        header += '\r\n'
        sock.send(header.encode())


def choose_upstream(weight_sum: int, weighted_upstreams: List[Tuple[Tuple[str, int], int]]) -> Union[Tuple[str, int], None]:
    # end early if no upstreams are available
    if len(weighted_upstreams) == 0:
        return None

    choice = random.randint(0, weight_sum)
    rolling_sum = 0
    for upstream, weight in weighted_upstreams:
        rolling_sum += weight
        if rolling_sum >= choice:
            return upstream


def calculate_weights(upstream_addrs: List[Tuple[str, int]]) -> Tuple[int, List[Tuple[Tuple[str, int], int]]]:
    # determine how long each upstream takes to respond with server_test.dat file
    upstream_durations = []
    for upstream in upstream_addrs:
        req_dur = connect_and_handle(upstream[0], upstream[1], 'server_test.dat')
        if req_dur != -1:
            print(f'Info:  Calculated send-time [{req_dur}] for upstream [{upstream[0]}:{upstream[1]}]')
            upstream_durations.append((upstream, req_dur))
        else:
            print(f'Warning:  Weight calculation for upstream [{upstream[0]}:{upstream[1]}] failed. It will be removed from the list of remotes.')
    upstream_durations.sort(key=lambda x: x[1])

    # now assign weights linearly from n down to 1,
    # where n is number of upstreams
    weight_sum = 0
    current_weight = len(upstream_durations)
    upstream_weights = []
    for ups, _ in upstream_durations:
        weight_sum += current_weight
        upstream_weights.append((ups, current_weight))
        print(f'Info:  Upstream [{ups[0]}:{ups[1]}] now has weight [{current_weight}]')
        current_weight -= 1

    # return both the total weight count for randint, and the upstreams with their weights
    return weight_sum, upstream_weights


def main():
    # Check the URL passed in and make sure it's valid.
    parser = argparse.ArgumentParser()
    parser.add_argument('-port', type=int, default=5522)
    parser.add_argument('-upstream', type=str, action='append')
    arg_result = parser.parse_args(sys.argv[1:])

    # validate cmd args
    bind_port = arg_result.port
    upstream_addrs = []
    for upstream_url in arg_result.upstream:
        parse_result = parse_url(upstream_url)
        if parse_result is None:
            print(f'Warning:  Upstream [{upstream_url}] has a malformed URL and will be dropped from the list of remotes.')
        else:
            upstream_addrs.append(parse_result)

    if len(upstream_addrs) == 0:
        print(f'Error:  No valid upstreams were given. Exiting.')
        sys.exit(-1)

    print(f'Starting load balancer with {len(upstream_addrs)} upstream servers.')

    # Register our signal handler for shutting down.

    signal.signal(signal.SIGINT, signal_handler)

    # Determine initial weights for each upstream

    print('Info:  Calculating initial upstream weights....')
    weight_sum, weighted_upstreams = calculate_weights(upstream_addrs)

    # ensure at least one upstream is active
    if len(weighted_upstreams) == 0:
        print(f'Error:  Unable to connect to any upstreams. Exiting.')
        sys.exit(-1)

    # Create the socket.  We will ask this to work on any interface and to pick
    # a free port at random.  We'll print this out for clients to use.

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(('', bind_port))
    print('Will wait for client connections at port ' + str(server_socket.getsockname()[1]))
    server_socket.listen(1)
    server_socket.settimeout(30.0)  # timeout after 30s of inactivity

    # Keep the server running forever.

    while True:
        try:
            print('Waiting for incoming client connection ...')
            conn, addr = server_socket.accept()
        except socket.timeout:
            print('Info:  No connection received after 30s, using this time to recalculate upstream weights.')
            weight_sum, weighted_upstreams = calculate_weights([ups for ups, w in weighted_upstreams])
            continue

        # no timeout, got connection
        print('Accepted connection from client address:', addr)
        print('Connection to client established, waiting to receive message...')

        # We obtain our request from the socket.  We look at the request and
        # figure out what to do based on the contents of things.

        request = get_line_from_socket(conn)
        print('Received request:  ' + request)
        request_list = request.split()

        # skip over headers
        header_line = get_line_from_socket(conn)
        while header_line != '':
            header_line = get_line_from_socket(conn)

        # If we did not get the proper HTTP version respond with a 505.

        if request_list[2] != 'HTTP/1.1':
            print('Invalid HTTP version received ... responding with error!')
            send_response_to_client(conn, '505')

        # We have the right request and version, so check if file exists.

        else:

            # retrieve path for redirect
            req_path = request_list[1]

            # pick a random upstream based on weights
            redirect_target = choose_upstream(weight_sum, weighted_upstreams)

            # redirect the client to that upstream, send_response_to_client will handle NoneType target
            print(f'Redirecting client to upstream [{redirect_target}]')
            send_response_to_client(conn, '301', redirect_target, req_path)


        # We are all done with this client, so close the connection and
        # Go back to get another one!

        conn.close()


if __name__ == '__main__':
    main()

