import socket
import os
import datetime
import signal
import sys
from cache_client import fetch

# Constant for our buffer size

BUFFER_SIZE = 1024


# Signal handler for graceful exiting.

def signal_handler(sig, frame):
    print('PROXYSERVER: Interrupt received, shutting down ...')
    sys.exit(0)


# Create an HTTP response

def prepare_response_message(value):
    date = datetime.datetime.now()
    date_string = 'Date: ' + date.strftime('%a, %d %b %Y %H:%M:%S EDT')
    message = 'HTTP/1.1 '
    if value == '200':
        message = message + value + ' OK\r\n' + date_string + '\r\n'
    elif value == '404':
        message = message + value + ' Not Found\r\n' + date_string + '\r\n'
    elif value == '501':
        message = message + value + ' Method Not Implemented\r\n' + date_string + '\r\n'
    elif value == '505':
        message = message + value + ' Version Not Supported\r\n' + date_string + '\r\n'
    return message


# Send the given response and file back to the client.

def send_response_to_client(sock, code, file_name):
    # Determine content type of file

    if ((file_name.endswith('.jpg')) or (file_name.endswith('.jpeg'))):
        type = 'image/jpeg'
    elif (file_name.endswith('.gif')):
        type = 'image/gif'
    elif (file_name.endswith('.png')):
        type = 'image/jpegpng'
    elif ((file_name.endswith('.html')) or (file_name.endswith('.htm'))):
        type = 'text/html'
    else:
        type = 'application/octet-stream'

    # Get size of file

    file_size = os.path.getsize(file_name)

    # Construct header and send it

    header = prepare_response_message(code) + 'Content-Type: ' + type + '\r\nContent-Length: ' + str(
        file_size) + '\r\n\r\n'
    sock.send(header.encode())

    # Open the file, read it, and send it

    with open(file_name, 'rb') as file_to_send:
        while True:
            chunk = file_to_send.read(BUFFER_SIZE)
            if chunk:
                sock.send(chunk)
            else:
                break


# Read a single line (ending with \n) from a socket and return it.
# We will strip out the \r and the \n in the process.

def get_line_from_socket(sock):
    done = False
    line = ''
    while (not done):
        char = sock.recv(1).decode()
        if (char == '\r'):
            pass
        elif (char == '\n'):
            done = True
        else:
            line = line + char
    return line


# Our main function.

def main():
    # Register our signal handler for shutting down.

    signal.signal(signal.SIGINT, signal_handler)

    # Create the socket.  We will ask this to work on any interface and to pick
    # a free port at random.  We'll print this out for clients to use.

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(('', 12345))
    print('PROXYSERVER: Will wait for client connections at port ' + str(server_socket.getsockname()[1]))
    server_socket.listen(1)

    # Keep the server running forever.

    while True:
        print('PROXYSERVER: Waiting for incoming client connection ...')
        conn, addr = server_socket.accept()
        print('PROXYSERVER: Accepted connection from client address:', addr)
        print('PROXYSERVER: Connection to client established, waiting to receive message...')

        # We obtain our request from the socket.  We look at the request and
        # figure out what to do based on the contents of things.

        request = get_line_from_socket(conn)
        print('PROXYSERVER: Received request:  ' + request)
        request_list = request.split()

        # Need to pull the Host header to determine the correct upstream
        header_line = get_line_from_socket(conn)
        upstream_host = None
        while header_line != '':
            header_parts = header_line.split(': ', maxsplit=2)
            if header_parts[0] == 'Host':
                upstream_host = header_parts[1]

            header_line = get_line_from_socket(conn)

        # Make sure we got a Host header
        if upstream_host is None:
            print('PROXYSERVER: Invalid request, did not specify Host header')
            send_response_to_client(conn, '401', 'cache_401.html')

        # If we did not get a GET command respond with a 501.

        if request_list[0] != 'GET':
            print('PROXYSERVER: Invalid type of request received ... responding with error!')
            send_response_to_client(conn, '501', 'cache_501.html')

        # If we did not get the proper HTTP version respond with a 505.

        elif request_list[2] != 'HTTP/1.1':
            print('PROXYSERVER: Invalid HTTP version received ... responding with error!')
            send_response_to_client(conn, '505', 'cache_505.html')

        # We have the right request and version, so check if file exists.

        else:

            # Ask client to call remote
            req_file = request_list[1]
            if not req_file.startswith('/'):
                req_file = f'/{req_file}'
            status, path = fetch(f'http://{upstream_host}{req_file}')

            if status == 200 or status == 304:
                print('PROXYSERVER: Serving file from cache....')
                send_response_to_client(conn, '200', path)

            else:
                print('PROXYSERVER: Client received error, relaying to client....')
                send_response_to_client(conn, f'{status}', 'cache_error.html')

        # We are all done with this client, so close the connection and
        # Go back to get another one!

        conn.close()


if __name__ == '__main__':
    main()

