The cache proxy is a modified version of both the provided client and server.
It calls on a modified version of the client main method to make the request
to the remote, and the client then returns the appropriate status, and the
path that the cache file was stored to (or already exists in).

Usage:
Client and server can be run as before,
    python server.py
    python client.py http://host:port/path

When using the cache proxy:
    python server.py
    python cache_proxy.py
    python client.py -proxy http://proxyhost:proxyport http://serverhost:serverport/path
