# Paul Norton
# pnorton4@uwo.ca
# 250957533
# Oct 20, 2020

import argparse
import socket
import logging
import sys
from urllib.parse import urlparse

# parse arguments
# available arguments are:
#    * -u <url>, the http url to request, must include protocol prefix
#    * -o <output_file>, the destination file to write to
#    * -bs <size>, buffer size to read from sockets
#    * --debug, enables debug logging statements
arg_parser = argparse.ArgumentParser(description='CS3357 A1 Client implementation. Default arguments will connect to server.py and request index.html.')
arg_parser.add_argument('--url', '-u', nargs='?', const=True, default='http://127.0.0.1:10101/index.html', help='URL to request HTTP content from.')
arg_parser.add_argument('--output', '-o', nargs='?', type=str, help='Output file to write content to.')
arg_parser.add_argument('--buffer-size', '-bs', nargs='?', const=True, type=int, default=4096, help='Buffer size to use in socket transfer.')
arg_parser.add_argument('--debug', action='store_true', help='Enables debugging output.')
args = arg_parser.parse_args()

# initialize logging
log_level = 'DEBUG' if args.debug else 'INFO'
logging.basicConfig(format='[%(asctime)s] %(levelname)8s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=log_level)
if args.debug:
    logging.debug('Debug logging is active.')

# parse provided args
buf_size = args.buffer_size
url = urlparse(args.url)

# validations on url arg
if url.scheme != 'http':
    logging.critical('URL scheme must be http. Other schemes are not supported.')
    sys.exit(-1)
if not url.netloc:
    logging.critical('URL host must be provided')
    sys.exit(-1)
if not url.path:
    logging.critical('URL path must be provided')
    sys.exit(-1)

# split url into necessary components
url_parts = url.netloc.split(':')
url_addr = url.netloc.split(':')[0]
url_port = int(url.netloc.split(':')[1]) if len(url_parts) > 1 else 80
url_host = url_addr + ':' + str(url_port)
url_path = url.path
logging.debug('Parsed URL is host=[%s], port=[%d], path=[%s]', url_addr, url_port, url_path)

# determine output path, whether one is provided or not
dest_file_path = ('.' + url_path) if args.output is None else args.output
logging.debug('Will write to local path [%s]', dest_file_path)

# open socket, timeout after 10s
logging.debug('Opening socket connection to host [%s:%d]', url_addr, url_port)
try:
    sck = socket.create_connection((url_addr, url_port), timeout=10)
except socket.timeout:
    logging.critical('Failed to connect to host [%s:%d]', url_addr, url_port)
    sys.exit(1)

# modified from server.py to allow collapsing the line buffer back into the byte buffer
# useful for content-length based transfer
class RequestBuffer:

    def __init__(self, buf_len=4096):
        self.buf_len = buf_len
        self.buffer = bytearray()  # byte buffer to place socket input into
        self.lines_buffer = []  # str buffer split by line, populated in __readSocket, popped from by readLine

    # extends the internal buffer with a read chunk directly from the socket
    # may hang if no more data is to be read
    def __readSocket(self):
        # read chunk of up to 4096 bytes from socket
        chunk = sck.recv(self.buf_len)
        self.buffer.extend(chunk)

        # check whether we got anything at all from the read
        if not chunk:
            return

        # immediately transfer byte buffer to string buffer, unlike server.py
        self.lines_buffer = self.buffer.splitlines()
        self.buffer.clear()

        for line in self.lines_buffer:
            logging.debug(' IN <<<<< %s', line)

    # read single line from string buffer
    def readLine(self):
        if len(self.lines_buffer) == 0:
            self.__readSocket()
        if len(self.lines_buffer) == 0:
            logging.warning('Called for read at end of buffer')
            return None

        # pop first element and return
        return self.lines_buffer.pop(0).decode()

    # used to indicate that we should stop parsing header text,
    # and collapse whatever body may have been consumed into line buffer back into the byte buffer
    def collapseBuffer(self):
        self.buffer.extend(b'\r\n'.join(self.lines_buffer))

    # read bytes_len bytes off the buffer
    # pulls from socket if buffer holds less than bytes_len
    def readBytes(self, bytes_len):
        total_read = 0
        while True:
            total_read += len(self.buffer)
            yield self.buffer[:]
            self.buffer.clear()
            if total_read < bytes_len:
                self.__readSocket()
                self.collapseBuffer()  # needed since __readSocket populates string buffer
            else:
                break


# logs content and writes to client
def writeToSocket(content):
    logging.debug('OUT >>>>> %s', content)
    sck.send(content)


# parses first line of http request
def readMetaLine(line):
    meta_split = line.split(' ')
    meta_version = meta_split[0]
    meta_code = int(meta_split[1])
    meta_status = meta_split[2]
    return meta_version, meta_code, meta_status


# write request data out
logging.info('Requesting resource [%s] from remote....', url_path)
writeToSocket(b'GET ' + bytearray(url_path, 'utf-8') + b' HTTP/1.1\r\nHost: ' + bytearray(url_host, 'utf-8') + b'\r\n\r\n')

# create buffer and get meta line
logging.debug('Reading response from socket with buffer size [%d]', buf_size)
buffer = RequestBuffer()
read_line = buffer.readLine()
if not read_line:
    logging.critical('Server did not respond with valid HTTP response')
    sys.exit(500)

# parse meta line and ensure valid
version, code, status = readMetaLine(read_line)
logging.debug('Got meta line with version=[%s], code=[%d], status=[%s]', version, code, status)
if version != 'HTTP/1.1':
    logging.critical('Server responded with unknown HTTP version [%s]', version)
    sys.exit(504)
if code == 404:
    logging.warning('Server responded with 404 Resource Not Found')
    sys.exit(404)
if code != 200:
    logging.critical('Server responded with unknown HTTP status code [%d]', code)
    sys.exit(status)

# skip forward for blank line, check for content-length while doing so
content_length = -1
read_line: str = buffer.readLine()
while read_line != '':
    # parse header content (only care about content-length header)
    header_parts = read_line.split(':', maxsplit=2)
    header_name = header_parts[0]
    header_value = header_parts[1] if len(header_parts) > 1 else ''
    logging.debug('Response header [%s]=[%s]', header_name, header_value)

    # parse content-length if present
    if header_name == 'Content-Length':
        content_length = int(header_value)
        logging.debug('Parsed Content-Length header with length=[%d]', content_length)

    read_line = buffer.readLine()

# switch to byte-based chunking if content-length is specified (pulls from byte buffer)
if content_length != -1:
    logging.debug('Writing file=[%s] with known Content-Length=[%d]', dest_file_path, content_length)
    with open(dest_file_path, 'w+b') as dest_file:
        buffer.collapseBuffer()
        for chunk in buffer.readBytes(content_length):
            dest_file.write(chunk)

# otherwise use blank-line-terminated write (pulls from string buffer)
else:
    logging.debug('No Content-Length given')
    logging.debug('Writing file=[%s] with empty-line expected at end', dest_file_path)
    with open(dest_file_path, 'w+') as dest_file:
        read_line = buffer.readLine()
        while read_line != '':
            dest_file.write(read_line)

# log what we did, close socket
logging.info('Retrieved [%s] from remote and wrote to local file [%s].', url_path, dest_file_path)
sck.close()
