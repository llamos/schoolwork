# Paul Norton
# pnorton4@uwo.ca
# 250957533
# Oct 20, 2020

import argparse
import socket
import logging
import os

# parse arguments
# available arguments are:
#    * -a <addr>, the address to bind to
#    * -p <port>, the port to bind to
#    * -bs <size>, buffer size to read from sockets
#    * -fbs <size>, buffer size to read from files
#    * --debug, enables debug logging statements
arg_parser = argparse.ArgumentParser(description='CS3357 A1 Server implementation. Default arguments will bind to 127.0.0.1:10101, default for client.py.')
arg_parser.add_argument('--address', '-a', nargs='?', const=True, default='127.0.0.1', help='Address to bind to.')
arg_parser.add_argument('--port', '-p', nargs='?', const=True, type=int, default=10101, help='Port to bind to.')
arg_parser.add_argument('--buffer-size', '-bs', nargs='?', const=True, type=int, default=4096, help='Buffer size to use in socket transfer.')
arg_parser.add_argument('--file-buffer-size', '-fbs', nargs='?', const=True, type=int, default=4096, help='Buffer size to read requested files.')
arg_parser.add_argument('--debug', action='store_true', help='Enables debugging output.')
args = arg_parser.parse_args()

# initialize logging
log_level = 'DEBUG' if args.debug else 'INFO'
logging.basicConfig(format='[%(asctime)s] %(levelname)8s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=log_level)
if args.debug:
    logging.debug('Debug logging is active.')

# parse args
buf_size = args.buffer_size
file_buf_size = args.file_buffer_size
bind_addr = (args.address, args.port)
logging.debug('args parsed to bind_addr=[%s:%d], buf_size=[%d], file_buf_size=[%d]',
              bind_addr[0], bind_addr[1], buf_size, file_buf_size)

# create server socket and bind to requested address
srv_sck = socket.create_server(bind_addr)
srv_sck.settimeout(.25)
bind_addr = srv_sck.getsockname()
logging.info('Server socket initialized and bound to [%s:%d]', bind_addr[0], bind_addr[1])


# buffer class used to read information from the socket
# has a string buffer (used for parsing) and a byte buffer (used for socket transfer)
class RequestBuffer:

    def __init__(self, buf_len=4096):
        self.buf_len = buf_len
        self.buffer = bytearray()  # byte buffer to place socket input into
        self.lines_buffer = []  # str buffer split by line, populated in __readSocket, popped from by readLine
        self.more_to_read = True  # flag whether we expect more from the client

    # extends the internal buffer with a read chunk directly from the socket
    # may hang if no more data is to be read
    def __readSocket(self, sck):
        # read chunk of up to 4096 bytes from socket
        chunk = sck.recv(self.buf_len)
        self.buffer.extend(chunk)

        # check whether we got anything at all from the read
        if not chunk:
            self.more_to_read = False
            return

        # need to keep ends to see if last line includes break,
        # if it does not, then the line was not fully read from the client,
        # and we have more to read later
        lines = self.buffer.splitlines(keepends=True)
        last_line = lines.pop()

        # if the last line doesn't end with a newline, it is incomplete,
        # don't place it into str buffer, and put it back into the byte buffer to be completed at later reads
        # if it does, we can add it back to the line buffer safely
        self.buffer.clear()
        if not last_line.endswith(b'\n'):
            self.buffer.extend(last_line)
        else:
            lines.append(last_line)
        self.lines_buffer = [x.rstrip().decode() for x in lines]  # copy over to line buffer without newlines
        for line in self.lines_buffer:
            logging.debug(' IN <<<<< %s', line)

        # check for whether this is the end of the request, denoted by a blank line
        if last_line == b'\r\n' or last_line == b'\n':
            self.more_to_read = False

    def readLine(self, sck):
        while len(self.lines_buffer) == 0 and self.more_to_read:
            self.__readSocket(sck)
        if len(self.lines_buffer) == 0 and not self.more_to_read:
            logging.warning('Called for read at end of buffer')
            return None

        # pop first element and return
        return self.lines_buffer.pop(0)


# utility that accepts a connection while also allowing ctrl+c termination via timeout
def acceptConnection():
    while True:
        try:
            # times out after .25s to allow KeyboardInterrupts to be recognized
            sck, addr = srv_sck.accept()
            return sck, addr
        except socket.timeout:
            continue


# parses first line of http request
def readMetaLine(line):
    meta_split = line.split(' ')
    meta_verb = meta_split[0]
    meta_path = meta_split[1]
    meta_version = meta_split[2]
    return meta_verb, meta_path, meta_version


# logs content and writes to client
def writeToSocket(sck, content):
    logging.debug('OUT >>>>> %s', content)
    sck.send(content)


# shorthand to write error message back with text preview
def writeErrorToSocket(sck, error_msg):
    writeToSocket(sck, b'HTTP/1.1 ' + error_msg + b'\r\n\r\n' + error_msg + b'\r\n\r\n')


# forcibly closes the socket, ignoring errors
def closeSocket(sck: socket.socket):
    sck_name = sck.getsockname()
    logging.debug('Closing socket [%s:%d]', sck_name[0], sck_name[1])
    try:
        sck.close()
    except Exception as e:
        logging.debug('Got error while closing socket [%s]', str(e))
        pass


try:
    # loop until keyboard interrupt
    while True:
        # get connection
        client_sck, client_addr = acceptConnection()
        logging.debug('Accepted connection from client at [%s:%d]', client_addr[0], client_addr[1])

        # create our recv buffer
        logging.debug('Creating request buffer with size [%d]', buf_size)
        request_buf = RequestBuffer(buf_size)

        # first line of request should be meta info
        meta_line = request_buf.readLine(client_sck)
        if not meta_line:
            logging.warning('Client sent empty request body')
            continue

        # parse that meta info and ensure we can support it
        verb, path, version = readMetaLine(meta_line)
        logging.debug('Got request from client verb=[%s] path=[%s] version=[%s]', verb, path, version)

        if version != 'HTTP/1.1':
            logging.warning('Client requested unsupported HTTP version [%s]', version)
            writeErrorToSocket(client_sck, b'505 HTTP Version Not Supported')
            closeSocket(client_sck)
            continue

        if verb.upper() != 'GET':
            logging.warning('Client requested unsupported HTTP action [%s]', verb)
            writeErrorToSocket(client_sck, b'501 Not Implemented')
            closeSocket(client_sck)
            continue

        # lock path request to only local dirs
        relative_path = '.' + path.replace('/../', '//')
        if not os.path.exists(relative_path) or os.path.isdir(relative_path):
            logging.warning('Client requested unknown path [%s]', path)
            writeErrorToSocket(client_sck, b'404 Not Found')
            closeSocket(client_sck)
            continue

        # compute total file size for content-length header
        file_size = os.stat(relative_path).st_size
        logging.debug('Serving contents of file [%s] with chunk size [%d]', relative_path, file_buf_size)
        logging.debug('\tfile_size=%d', file_size)

        # write headers
        writeToSocket(client_sck, b'HTTP/1.1 200 OK\r\n')
        writeToSocket(client_sck, b'Content-Length: ' + bytearray(str(file_size), 'utf-8') + b'\r\n')
        writeToSocket(client_sck, b'\r\n')

        # write body
        with open(relative_path, 'rb') as file:
            while True:
                chunk = file.read(file_buf_size)
                if not chunk:
                    break

                writeToSocket(client_sck, chunk)

        # since we included content-length header, we should *not* terminate with a blank line (RFC 2616 4.4.3)
        # https://tools.ietf.org/html/rfc2616#section-4.4
        logging.info('Served file [%s] to client [%s:%d]', relative_path, client_addr[0], client_addr[1])
        closeSocket(client_sck)

except KeyboardInterrupt:
    # suppresses stacktrace, since this is an expected termination signal
    logging.info('Got termination signal. Closing server socket.')

finally:
    # close socket always
    srv_sck.close()
